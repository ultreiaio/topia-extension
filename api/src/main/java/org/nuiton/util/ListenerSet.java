package org.nuiton.util;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.beans.Statement;
import java.lang.ref.Reference;
import java.util.HashSet;
import java.util.Iterator;

/**
 * <p>Cette classe permet de mettre en place facilement le support de listeners.
 * Elle ne permet d'ajouter qu'une seul fois le meme listener. Si elle est
 * la derniere à avoir une référence sur le listener, le listener est supprimé
 * de la liste des listeners.</p>
 * <p>Si on souhaite avoir une vérification sur le type de listener ajouté
 * il faut utiliser le constructeur qui prend une classe en paramètre. Dans ce
 * cas la méthode {@link #add(Object)} vérifie que l'object passé est bien
 * du type ou un enfant du type donné en paramètre du constructeur
 * <p>Il y a deux façon de prévenir les listeners d'un event soit par le
 * mécanisme inclu dans cette classe en utilisant la méthode {@link #fire} soit
 * en utilisant soit même l'Iterateur sur les listeners encore valide.</p>
 * <pre>
 * ListenerSet listeners = new ListenerSet();
 * ...
 * listeners.fire("monEvent", MonObjetEvent);
 * </pre>
 * ou bien
 * <pre>
 * ListenerSet listeners = new ListenerSet();
 * ...
 * for(Iterator i=listeners.iterator(); i.hasNext();){
 *     MonListener l = (MonListener)i.next();
 *     l.monEvent(MonObjetEvent);
 * }
 * </pre>
 * Cette deuxième façon de faire est plus sûr car elle n'utilise pas
 * l'introspection et donc une vérification est faite sur le nom de la méthode
 * à appeler à la compilation, mais elle est plus verbeuse à écrire.
 *
 * @param <L> listeners type
 * @see CategorisedListenerSet
 */
public class ListenerSet<L> implements Iterable<L> { // ListenerSet

    /**
     * Listeners reference set.
     */
    protected HashSet<Reference<L>> listeners = new HashSet<Reference<L>>();

    public int size() {
        return listeners.size();
    }

    /**
     * Ajoute un listener dans la liste des listeners.
     *
     * @param l le listener à ajouter. Si l'objet passé est null, rien n'est fait
     *          si l'objet n'est pas du type passé en argument du constructeur
     *          une IllegalArgumentException est levée.
     */
    public void add(L l) {
        if (l == null) {
            return;
        }

        TransparenteWeakReference<L> ref = new TransparenteWeakReference<L>(l);
        listeners.add(ref);
    }

    /**
     * ajoute tous les listeners d'un ListenerSet
     *
     * @param ls The feature to be added to the All attribute
     */
    public void addAll(ListenerSet<L> ls) {
        listeners.addAll(ls.listeners);
    }

    /**
     * Appel la méthode du listener en passant l'objet event en paramètre
     * Cette méthode echoue si la methode ou l'objet contenant la methode a
     * appeler n'est pas public
     *
     * @param methodName le nom de la methode a appeler
     * @param event      l'event a passer en parametre de la methode a appeler
     * @throws Exception si un des listeners leve une exception lors de l'appel
     */
    public void fire(String methodName, Object event) throws Exception {
        for (Iterator<L> i = iterator(); i.hasNext(); ) {
            L o = i.next();
            Statement stm = new Statement(o, methodName, new Object[]{event});
            stm.execute();
        }
    }

    /**
     * Appele la méthode du listener sans argument.
     * <p>
     * Cette méthode echoue si la methode ou l'objet contenant la methode a
     * appeler n'est pas public.
     *
     * @param methodName le nom de la methode a appeler
     * @throws Exception si un des listeners leve une exception lors de l'appel
     */
    public void fire(String methodName) throws Exception {
        for (Iterator<L> i = iterator(); i.hasNext(); ) {
            L o = i.next();
            Statement stm = new Statement(o, methodName, null);
            stm.execute();
        }
    }

    /**
     * Get iterator on listener list.
     *
     * @return iterator on listener list.
     */
    public Iterator<L> iterator() {
        return new ReferenceIterator<L>(listeners.iterator());
    }

    /**
     * Remove listener.
     *
     * @param l listener to remove
     */
    public void remove(L l) {
        TransparenteWeakReference<L> ref = new TransparenteWeakReference<L>(l);
        listeners.remove(ref);
    }

    @Override
    public String toString() {
        return listeners.toString();
    }

    /**
     * Iterator qui supprime les references vides lors du parcours
     */
    static class ReferenceIterator<T> implements Iterator<T> {
        /**
         * DOCUMENTME Description of the Field
         */
        protected Iterator<Reference<T>> iter = null;

        /**
         * DOCUMENTME Description of the Field
         */
        protected T nextObject = null;

        /**
         * Un iterator contenant des References
         *
         * @param iter DOCUMENTME Description of the Parameter
         */
        public ReferenceIterator(Iterator<Reference<T>> iter) {
            this.iter = iter;
            findNext();
        }

        /**
         * DOCUMENTME Method
         */
        protected void findNext() {
            while (iter.hasNext() && nextObject == null) {
                Reference<T> ref = iter.next();
                T o = ref.get();
                if (o != null) {
                    nextObject = o;
                } else {
                    iter.remove();
                }
            }
        }

        /**
         * DOCUMENTME Method
         *
         * @return DOCUMENTME Description of the Return Value
         */
        public boolean hasNext() {
            return nextObject != null;
        }

        /**
         * DOCUMENTME Method
         *
         * @return DOCUMENTME Description of the Return Value
         */
        public T next() {
            T result = nextObject;
            nextObject = null;
            findNext();
            return result;
        }

        /**
         * DOCUMENTME Method
         */
        public void remove() {
            iter.remove();
        }
    }

} // ListenerSet
