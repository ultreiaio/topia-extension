package org.nuiton.topia.service.migration;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Version;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.internal.support.H2TopiaSqlDllSupportImpl;
import org.nuiton.topia.persistence.internal.support.PGTopiaSqlDllSupportImpl;
import io.ultreia.java4all.util.sql.SqlScriptReader;
import io.ultreia.java4all.util.sql.SqlScriptWriter;
import org.nuiton.topia.persistence.support.TopiaSqlDllSupport;
import org.nuiton.topia.persistence.support.TopiaSqlDllSupportProvider;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import io.ultreia.java4all.util.sql.SqlWork;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceScriptLayout;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Default implementation of {@link MigrationVersionResourceExecutor} with extra methods used by {@link TopiaMigrationService#migrateVersion(TopiaMigrationServiceContext, Version)}.
 * <p>
 * This executor is pass to each version to migrate.
 * <p>
 * Created by tchemit on 06/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@SuppressWarnings("WeakerAccess")
public class TopiaMigrationServiceExecutor implements MigrationVersionResourceExecutor {

    private static final Logger log = LogManager.getLogger(TopiaMigrationServiceExecutor.class);

    protected final Version version;
    protected final TopiaSqlSupport sqlSupport;
    protected final TopiaSqlDllSupport sqlDllSupport;
    protected final MigrationVersionResourceScriptLayout scriptLayout;
    protected final String classifier;
    protected final Path scriptForVersion;
    protected final SqlScriptWriter writer;
    protected final String logPrefix;
    private final Map<String, String> scriptVariables;
    private final ClassLoader classLoader;

    protected TopiaMigrationServiceExecutor(MigrationVersionResource migrationVersionResource, TopiaSqlSupport sqlSupport, String classifier, Path scriptsPath, Map<String, String> scriptVariables) {
        this.version = Objects.requireNonNull(Objects.requireNonNull(migrationVersionResource).getVersion());
        this.scriptLayout = Objects.requireNonNull(migrationVersionResource.getScriptLayout());
        this.classLoader = Objects.requireNonNull(migrationVersionResource.getClassLoader());
        this.classifier = Objects.requireNonNull(classifier);
        this.sqlSupport = Objects.requireNonNull(sqlSupport);
        this.sqlDllSupport = TopiaSqlDllSupportProvider.get().get(classifier).orElseThrow(() -> new IllegalStateException("No TopiaSqlDllSupport for classifier: " + classifier));
        this.scriptVariables = scriptVariables;
        this.scriptForVersion = Objects.requireNonNull(scriptsPath).resolve(String.format("migration-version-%s.sql", version));
        this.writer = SqlScriptWriter.of(scriptForVersion);
        this.logPrefix = String.format("[ Version %s ] ", version);
        log.info(logPrefix + String.format("Will produce sql script at: %s", scriptForVersion));
    }

    protected TopiaMigrationServiceExecutor(TopiaMigrationServiceExecutor parent, Path scriptForVersion) {
        this.classLoader = Objects.requireNonNull(Objects.requireNonNull(parent).classLoader);
        this.version = Objects.requireNonNull(parent.version);
        this.scriptLayout = Objects.requireNonNull(parent.scriptLayout);
        this.classifier = Objects.requireNonNull(parent.classifier);
        this.sqlSupport = Objects.requireNonNull(parent.sqlSupport);
        this.sqlDllSupport = Objects.requireNonNull(parent.sqlDllSupport);
        this.scriptForVersion = Objects.requireNonNull(scriptForVersion);
        this.writer = SqlScriptWriter.of(scriptForVersion);
        this.scriptVariables = parent.scriptVariables;
        this.logPrefix = Objects.requireNonNull(parent.logPrefix);
        log.info(logPrefix + String.format("Will produce sql script at: %s", scriptForVersion));
    }

    public TopiaMigrationServiceExecutor toFinalize() {
        Path scriptPath = scriptForVersion.getParent().resolve(scriptForVersion.toFile().getName().replace(".sql", "-finalize.sql"));
        return new TopiaMigrationServiceExecutor(this, scriptPath);
    }

    public String getLogPrefix() {
        return logPrefix;
    }

    public Path getScriptForVersion() {
        return scriptForVersion;
    }

    @Override
    public void writeSql(String sql) {
        writer.writeSql(sql);
    }

    @Override
    public void addScript(String rank, String prefix) {
        loadScript(version, rank, prefix);
    }

    @Override
    public <O> O findSingleResult(TopiaSqlQuery<O> query) {
        return sqlSupport.findSingleResult(query);
    }

    @Override
    public <O> List<O> findMultipleResult(TopiaSqlQuery<O> query) {
        return sqlSupport.findMultipleResult(query);
    }

    @Override
    public <O> Set<O> findMultipleResultAstSet(TopiaSqlQuery<O> query) {
        return new LinkedHashSet<>(findMultipleResult(query));
    }

    @Override
    public void dropSchema(String schemaName) {
        writeSql(sqlDllSupport.dropSchema(sqlSupport, schemaName));
    }

    @Override
    public void dropTable(String schemaName, String tableName) {
        writeSql(sqlDllSupport.dropTable(sqlSupport, schemaName, tableName));
    }

    @Override
    public void doSqlWork(SqlWork sqlWork) {
        sqlSupport.doSqlWork(sqlWork);
    }

    @Override
    public Set<String> getTopiaIds(final String tableName) {
        return TopiaSqlDllSupport.selectAllAsSet(sqlSupport, String.format("SELECT topiaId FROM %s;", tableName));
    }

    @Override
    public String getUniqueConstraintName(String tableName, String columnName) {
        return sqlDllSupport.getUniqueConstraintName(sqlSupport, tableName, columnName);
    }

    @Override
    public String getFirstTableUniqueConstraintName(String tableName) {
        return sqlDllSupport.getFirstTableUniqueConstraintName(sqlSupport, tableName);
    }

    @Override
    public Set<String> getConstraintNames(String tableName) {
        return sqlDllSupport.getConstraintNames(sqlSupport, tableName);
    }

    @Override
    public Set<String> getForeignKeyConstraintNames(String tableName) {
        return sqlDllSupport.getForeignKeyConstraintNames(sqlSupport, tableName);
    }

    @Override
    public String getForeignKeyConstraintName(String schemaName, String tableName, String columnName, boolean mustExists) {
        return sqlDllSupport.getForeignKeyConstraintName(sqlSupport, schemaName, tableName, columnName, mustExists);
    }

    @Override
    public Set<String> getUniqueKeyConstraintNames(String tableName) {
        return sqlDllSupport.getUniqueKeyConstraintNames(sqlSupport, tableName);
    }

    @Override
    public void removeFK(String tableName) {
        sqlDllSupport.removeFK(sqlSupport, tableName).forEach(this::writeSql);
    }

    @Override
    public void removeFK(String schemaName, String tableName, String columnName) {
        String sql = sqlDllSupport.removeFK(sqlSupport, schemaName, tableName, columnName);
        writeSql(sql);
    }

    @Override
    public void removeFKIfExists(String schemaName, String tableName, String columnName) {
        sqlDllSupport.removeFKIfExists(sqlSupport, schemaName, tableName, columnName).ifPresent(this::writeSql);
    }

    @Override
    public void removePKIfExists(String schemaName, String tableName) {
        sqlDllSupport.removePKIfExists(sqlSupport, schemaName, tableName).ifPresent(this::writeSql);
    }

    @Override
    public void removeUK(String tableName) {
        sqlDllSupport.removeUK(sqlSupport, tableName).forEach(this::writeSql);
    }

    @Override
    public void executeForPG(Consumer<MigrationVersionResourceExecutor> consumer) {
        if (isPG()) {
            consumer.accept(this);
        }
    }

    @Override
    public void executeForH2(Consumer<MigrationVersionResourceExecutor> consumer) {
        if (isH2()) {
            consumer.accept(this);
        }
    }

    public long flush() throws IOException {
        writer.flush();
        return writer.getStatementCount();
    }

    @Override
    public void close() throws IOException {
        writer.close();
    }

    protected void loadScript(Version version, String rank, String prefix) {
        URL scriptLocation = getScriptLocation(version, rank, prefix, classifier);
        log.info(String.format("%sLoad migration script: %s", logPrefix, scriptLocation));
        try (SqlScriptReader scriptReader = SqlScriptReader.builder(scriptLocation).setVariables(scriptVariables).build()) {
            for (String statement : scriptReader) {
                writer.writeSql(statement);
            }
        } catch (IOException e) {
            throw new TopiaException("Could not load migration script: " + scriptLocation, e);
        }
    }

    protected URL getScriptLocation(Version version, String rank, String prefix, String classifier) {
        String scriptPath = scriptLayout.getScriptPath(version, rank, prefix, "common");
        URL scriptLocation = getResource(scriptPath);
        if (scriptLocation == null) {
            scriptPath = scriptLayout.getScriptPath(version, rank, prefix, classifier);
            scriptLocation = getResource(scriptPath);
        }
        Objects.requireNonNull(scriptLocation, "Can't find script " + scriptPath + ", nor his common version.");
        return scriptLocation;
    }

    protected URL getResource(String scriptPath) {
        return classLoader.getResource(scriptPath.substring(1));
    }

    protected boolean isH2() {
        return H2TopiaSqlDllSupportImpl.CLASSIFIER.equals(classifier);
    }

    protected ClassLoader getClassLoader() {
        return classLoader;
    }

    protected boolean isPG() {
        return PGTopiaSqlDllSupportImpl.CLASSIFIER.equals(classifier);
    }
}
