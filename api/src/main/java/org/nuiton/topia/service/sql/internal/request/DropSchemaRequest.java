package org.nuiton.topia.service.sql.internal.request;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.hibernate.dialect.Dialect;

/**
 * Created on 01/01/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.1
 */
public class DropSchemaRequest extends SqlSchemaRequest {


    public DropSchemaRequest(Class<? extends Dialect> dialect, boolean processSchema, boolean processTables) {
        super(dialect, processSchema, processTables);
        if (processSchema && !processTables) {
            throw new IllegalStateException("Can't drop schema and not tables");
        }
    }
}
