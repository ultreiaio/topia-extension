package org.nuiton.topia.service.sql.model;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import io.ultreia.java4all.util.json.JsonHelper;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 01/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.67
 */
public class TopiaEntitySqlSelectorAdapter implements JsonDeserializer<TopiaEntitySqlSelector>, JsonSerializer<TopiaEntitySqlSelector> {

    @Override
    public TopiaEntitySqlSelector deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        String[] code = json.getAsString().split("\\s*~\\s*");
        String fromClause = code[0];
        String joinClauses = code[1];
        String whereClauseAlias = code[2];
        String reverseSelectorStr = code.length < 4 ? null : code[3];
        boolean reverseSelector = reverseSelectorStr != null && !reverseSelectorStr.isEmpty() && Boolean.parseBoolean(reverseSelectorStr);
        return new TopiaEntitySqlSelector(fromClause,
                                          whereClauseAlias,
                                          joinClauses,
                                          reverseSelector);
    }

    @Override
    public JsonElement serialize(TopiaEntitySqlSelector src, Type typeOfSrc, JsonSerializationContext context) {
        List<Object> properties = new ArrayList<>(3);
        JsonHelper.addToProperties(properties, src.getFromClause());
        JsonHelper.addToProperties(properties, src.getJoinClauses());
        JsonHelper.addToProperties(properties, src.getWhereClauseAlias());
        String collect = JsonHelper.codeProperties(properties);
        if (src.isReverseSelector()) {
            collect += "~true";
        }
        return context.serialize(collect);
    }
}
