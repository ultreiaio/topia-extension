package org.nuiton.topia.service.sql.model;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import io.ultreia.java4all.util.json.JsonHelper;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created on 01/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.67
 */
public abstract class AbstractTopiaEntitySqlAssociationTableAdapter<T extends AbstractTopiaEntitySqlAssociationTable> extends AbstractTopiaEntitySqlTableAdapter<T> {

    public static class TopiaEntitySqlAssociationTableAdapter extends AbstractTopiaEntitySqlAssociationTableAdapter<AbstractTopiaEntitySqlAssociationTable> {
        @Override
        protected TopiaEntitySqlAssociationTable newInstance(String entityName, String schemaName, String tableName, Set<String> authorizedColumnNames, List<TopiaEntitySqlSelector> selectors, String joinColumnName) {
            return new TopiaEntitySqlAssociationTable(entityName, schemaName, tableName, authorizedColumnNames, selectors, joinColumnName);
        }
    }

    public static class TopiaEntitySqlSimpleAssociationTableAdapter extends AbstractTopiaEntitySqlAssociationTableAdapter<AbstractTopiaEntitySqlAssociationTable> {
        @Override
        protected TopiaEntitySqlSimpleAssociationTable newInstance(String entityName, String schemaName, String tableName, Set<String> authorizedColumnNames, List<TopiaEntitySqlSelector> selectors, String joinColumnName) {
            return new TopiaEntitySqlSimpleAssociationTable(entityName, schemaName, tableName, authorizedColumnNames, selectors, joinColumnName);
        }
    }

    public static class TopiaEntitySqlReverseAssociationTableAdapter extends AbstractTopiaEntitySqlAssociationTableAdapter<AbstractTopiaEntitySqlAssociationTable> {
        @Override
        protected TopiaEntitySqlReverseAssociationTable newInstance(String entityName, String schemaName, String tableName, Set<String> authorizedColumnNames, List<TopiaEntitySqlSelector> selectors, String joinColumnName) {
            return new TopiaEntitySqlReverseAssociationTable(entityName, schemaName, tableName, authorizedColumnNames, selectors, joinColumnName);
        }
    }

    public static class TopiaEntitySqlReverseCompositionTableAdapter extends AbstractTopiaEntitySqlAssociationTableAdapter<AbstractTopiaEntitySqlAssociationTable> {
        @Override
        protected TopiaEntitySqlReverseCompositionTable newInstance(String entityName, String schemaName, String tableName, Set<String> authorizedColumnNames, List<TopiaEntitySqlSelector> selectors, String joinColumnName) {
            return new TopiaEntitySqlReverseCompositionTable(entityName, schemaName, tableName, authorizedColumnNames, selectors, joinColumnName);
        }
    }

    protected abstract T newInstance(String entityName,
                                     String schemaName,
                                     String tableName,
                                     Set<String> authorizedColumnNames,
                                     List<TopiaEntitySqlSelector> selectors,
                                     String joinColumnName);

    @Override
    public T deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = json.getAsJsonObject();
        String[] code = object.getAsJsonPrimitive(GAV).getAsString().split("\\s*~\\s*");
        String entityName = code[0];
        String schemaName = code[1];
        String tableName = code[2];
        String joinColumnName = code[3];
        Set<String> authorizedColumnNames = getAuthorizedColumnNames(object);
        List<TopiaEntitySqlSelector> selectors = getSelectors(context, object);
        return newInstance(entityName,
                           schemaName,
                           tableName,
                           authorizedColumnNames,
                           selectors,
                           joinColumnName);
    }

    @Override
    public JsonObject serialize(T src, Type typeOfSrc, JsonSerializationContext context) {
        List<Object> properties = new ArrayList<>(4);
        JsonHelper.addToProperties(properties, src.getEntityName());
        JsonHelper.addToProperties(properties, src.getSchemaName());
        JsonHelper.addToProperties(properties, src.getTableName());
        JsonHelper.addToProperties(properties, src.getJoinColumnName());
        JsonObject result = new JsonObject();
        JsonHelper.addToResult(context, result, GAV, JsonHelper.codeProperties(properties));
        JsonHelper.addToResult(context, result, AUTHORIZED_COLUMN_NAMES, String.join(",", src.getAuthorizedColumnNames()));
        JsonHelper.addToResult(context, result, SELECTORS, src.getSelectors());
        return result;
    }

}
