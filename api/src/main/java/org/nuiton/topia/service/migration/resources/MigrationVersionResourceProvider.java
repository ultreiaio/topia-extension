package org.nuiton.topia.service.migration.resources;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Version;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaMigrationServiceException;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.stream.Collectors;

/**
 * Provider of {@link MigrationVersionResource}.
 * <p>
 * Created by tchemit on 05/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@SuppressWarnings("WeakerAccess")
public class MigrationVersionResourceProvider {

    private static final Logger log = LogManager.getLogger(MigrationVersionResourceProvider.class);
    private static MigrationVersionResourceProvider INSTANCE;
    /**
     * Available migration resources found in class-path indexed by their version.
     */
    protected final Map<Version, MigrationVersionResource> resources;

    public static synchronized MigrationVersionResourceProvider get() {
        return INSTANCE == null ? INSTANCE = new MigrationVersionResourceProvider() : INSTANCE;
    }

    protected MigrationVersionResourceProvider() {

        Map<Version, MigrationVersionResource> resourcesBuilder = new LinkedHashMap<>();
        for (MigrationVersionResource resource : ServiceLoader.load(MigrationVersionResource.class)) {
            resourcesBuilder.put(resource.getVersion(), resource);
        }
        resources = Collections.unmodifiableMap(resourcesBuilder);

        log.info(String.format("Found %d migration resource(s).", resources.size()));

    }

    public List<Version> getAvailableVersions() {
        return resources.keySet().stream().sorted(new Version.VersionComparator()).collect(Collectors.toList());
    }

    public Version getLastVersion() {
        List<Version> values = getAvailableVersions();
        return values.isEmpty() ? null : values.get(values.size() - 1);
    }


    public List<Version> getVersionsAfter(Version current) {
        List<Version> builder = new LinkedList<>();
        for (Version version : getAvailableVersions()) {
            if (version.after(current)) {
                builder.add(version);
            }
        }
        return Collections.unmodifiableList(builder);
    }

    public MigrationVersionResource getResource(Version version) {
        MigrationVersionResource result = resources.get(version);
        if (result == null) {
            throw new TopiaMigrationServiceException(String.format("Can't find resource for version: %s.", version));
        }
        return result;
    }
}
