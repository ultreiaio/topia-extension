package org.nuiton.topia.service.sql.model;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializer;
import io.ultreia.java4all.util.json.JsonHelper;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static io.ultreia.java4all.util.json.JsonHelper.readObjectList;

/**
 * Created on 01/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.67
 */
public abstract class AbstractTopiaEntitySqlTableAdapter<T extends AbstractTopiaEntitySqlTable> implements JsonDeserializer<T>, JsonSerializer<T> {

    public static final String GAV = "gav";
    public static final String AUTHORIZED_COLUMN_NAMES = "authorizedColumnNames";
    public static final String SELECTORS = "selectors";

    protected List<TopiaEntitySqlSelector> getSelectors(JsonDeserializationContext context, JsonObject object) {
        return readObjectList(context, SELECTORS, TopiaEntitySqlSelector.class, object);
    }

    protected Set<String> getAuthorizedColumnNames(JsonObject object) {
        String readString = JsonHelper.readString(AUTHORIZED_COLUMN_NAMES, object);
        return new LinkedHashSet<>(List.of(readString.split("\\s*,\\s*")));
    }
}
