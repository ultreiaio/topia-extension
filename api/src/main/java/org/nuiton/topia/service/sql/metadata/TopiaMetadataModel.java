package org.nuiton.topia.service.sql.metadata;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ExecutionException;
import java.util.stream.Stream;

/**
 * Méta-modèle topia simplifié qui contient des informations utile pour des algorithmes générique sur les entités.
 * <p>
 * Ce méta-modèle est juste un conteneur de méta-modèle d'entités.
 * <p>
 * Created on 03/01/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
@SuppressWarnings("NullableProblems")
public class TopiaMetadataModel implements Iterable<TopiaMetadataEntity> {

    /**
     * Metadata for all entities of the model.
     */
    protected final Map<String, TopiaMetadataEntity> entities;
    /**
     * Cache of metadata by entity classes.
     */
    protected final transient Map<Class<? extends TopiaEntity>, TopiaMetadataEntity> typeToMetaEntities = new LinkedHashMap<>();
    /**
     * To get all compositions for each entity type.
     */
    private final transient LoadingCache<String, Set<TopiaMetadataComposition>> compositions = CacheBuilder.newBuilder().build(new CacheLoader<>() {
        @Override
        public Set<TopiaMetadataComposition> load(String key) {
            TopiaMetadataEntity source = Objects.requireNonNull(getEntity(Objects.requireNonNull(key)));
            Set<TopiaMetadataComposition> builder = new LinkedHashSet<>();
            for (Map.Entry<String, String> link : source.getManyToOneAssociations().entrySet()) {
                TopiaMetadataEntity target = Objects.requireNonNull(getEntity(link.getValue()));
                builder.add(new TopiaMetadataComposition(source, link.getKey(), target));
            }
            return builder;
        }
    });
    /**
     * To get all reverse compositions for each entity type.
     */
    private final transient LoadingCache<String, Set<TopiaMetadataComposition>> reverseCompositions = CacheBuilder.newBuilder().build(new CacheLoader<>() {
        @Override
        public Set<TopiaMetadataComposition> load(String key) {
            TopiaMetadataEntity target = Objects.requireNonNull(getEntity(Objects.requireNonNull(key)));
            Set<TopiaMetadataComposition> builder = new LinkedHashSet<>();
            for (Iterator<TopiaMetadataEntity> it = streamWithoutAbstract().iterator(); it.hasNext(); ) {
                TopiaMetadataEntity source = it.next();
                for (Map.Entry<String, String> entry : source.getManyToOneAssociations().entrySet()) {
                    if (entry.getValue().equals(key)) {
                        builder.add(new TopiaMetadataComposition(source, entry.getKey(), target));
                    }
                }
            }
            return builder;
        }
    });
    /**
     * To get all reverse many to many associations for each entity type.
     */
    private final transient LoadingCache<String, Set<TopiaMetadataAssociation>> reverseManyToManyAssociations = CacheBuilder.newBuilder().build(new CacheLoader<>() {
        @Override
        public Set<TopiaMetadataAssociation> load(String key) {
            TopiaMetadataEntity target = Objects.requireNonNull(getEntity(Objects.requireNonNull(key)));
            Set<TopiaMetadataAssociation> builder = new LinkedHashSet<>();
            for (Iterator<TopiaMetadataEntity> it = streamWithoutAbstract().iterator(); it.hasNext(); ) {
                TopiaMetadataEntity source = it.next();
                for (Map.Entry<String, String> entry : source.getManyToManyAssociations().entrySet()) {
                    if (entry.getValue().equals(key)) {
                        builder.add(new TopiaMetadataAssociation(source, entry.getKey(), target));
                    }
                }
            }
            return builder;
        }
    });
    /**
     * To get all reverse one to many associations for each entity type.
     */
    private final transient LoadingCache<String, Set<TopiaMetadataAssociation>> reverseOneToManyAssociations = CacheBuilder.newBuilder().build(new CacheLoader<>() {
        @Override
        public Set<TopiaMetadataAssociation> load(String key) {
            TopiaMetadataEntity target = Objects.requireNonNull(getEntity(Objects.requireNonNull(key)));
            Set<TopiaMetadataAssociation> builder = new LinkedHashSet<>();
            for (Iterator<TopiaMetadataEntity> it = streamWithoutAbstract().iterator(); it.hasNext(); ) {
                TopiaMetadataEntity source = it.next();
                for (Map.Entry<String, String> entry : source.getOneToManyAssociations().entrySet()) {
                    if (entry.getValue().equals(key)) {
                        builder.add(new TopiaMetadataAssociation(source, entry.getKey(), target));
                    }
                }
            }
            return builder;
        }
    });
    /**
     * To get all reverse one to one associations for each entity type.
     */
    private final transient LoadingCache<String, Set<TopiaMetadataOneToOneComposition>> reverseOneToOneAssociations = CacheBuilder.newBuilder().build(new CacheLoader<>() {
        @Override
        public Set<TopiaMetadataOneToOneComposition> load(String key) {
            TopiaMetadataEntity target = Objects.requireNonNull(getEntity(Objects.requireNonNull(key)));
            Set<TopiaMetadataOneToOneComposition> builder = new LinkedHashSet<>();
            for (Iterator<TopiaMetadataEntity> it = streamWithoutAbstract().iterator(); it.hasNext(); ) {
                TopiaMetadataEntity source = it.next();
                for (Map.Entry<String, String> entry : source.getOneToOneCompositions().entrySet()) {
                    if (entry.getValue().equals(key)) {
                        builder.add(new TopiaMetadataOneToOneComposition(source, entry.getKey(), target));
                    }
                }
            }
            return builder;
        }
    });
    /**
     * To get all associations for each entity type.
     */
    private final transient LoadingCache<String, Set<TopiaMetadataAssociation>> associations = CacheBuilder.newBuilder().build(new CacheLoader<>() {
        @Override
        public Set<TopiaMetadataAssociation> load(String key) {
            TopiaMetadataEntity source = Objects.requireNonNull(getEntity(Objects.requireNonNull(key)));
            Set<TopiaMetadataAssociation> builder = new LinkedHashSet<>();
            for (Map.Entry<String, String> link : source.getManyToManyAssociations().entrySet()) {
                TopiaMetadataEntity target = Objects.requireNonNull(getEntity(link.getValue()));
                builder.add(new TopiaMetadataAssociation(source, link.getKey(), target));
            }
            return builder;
        }
    });
    /**
     * To get all associations for each entity type.
     */
    private final transient LoadingCache<String, Set<TopiaMetadataSimpleAssociation>> simpleAssociations = CacheBuilder.newBuilder().build(new CacheLoader<>() {
        @Override
        public Set<TopiaMetadataSimpleAssociation> load(String key) {
            TopiaMetadataEntity source = Objects.requireNonNull(getEntity(Objects.requireNonNull(key)));
            Set<TopiaMetadataSimpleAssociation> builder = new LinkedHashSet<>();
            for (Map.Entry<String, String> link : source.getManyAssociations().entrySet()) {
                String propertyName = link.getKey();
                String tableName = source.getBdManyAssociationTableName(propertyName);
                String joinColumnName = source.getDbColumnName(tableName);
                builder.add(new TopiaMetadataSimpleAssociation(source, tableName, propertyName, joinColumnName, link.getValue()));
            }
            return builder;
        }
    });
    /**
     * To get all reverse associations for each entity type.
     */
    private final transient LoadingCache<String, Set<TopiaMetadataReverseAssociation>> reverseAssociations = CacheBuilder.newBuilder().build(new CacheLoader<>() {
        @Override
        public Set<TopiaMetadataReverseAssociation> load(String key) {
            TopiaMetadataEntity source = Objects.requireNonNull(getEntity(Objects.requireNonNull(key)));
            Set<TopiaMetadataReverseAssociation> builder = new LinkedHashSet<>();
            for (Map.Entry<String, String> link : source.getReversedAssociations().entrySet()) {
                TopiaMetadataEntity target = Objects.requireNonNull(getEntity(link.getValue()));
                builder.add(new TopiaMetadataReverseAssociation(source, link.getKey(), target));
            }
            return builder;
        }
    });
    /**
     * To get all schemas (key is schema, value is set of entities in this schema).
     */
    private transient Multimap<String, TopiaMetadataEntity> schemas;

    public TopiaMetadataModel(Map<String, TopiaMetadataEntity> entities) {
        this.entities = Objects.requireNonNull(entities);
    }

    public TopiaMetadataModel() {
        this.entities = new LinkedHashMap<>();
    }

    public void applyInheritance() {
        Set<String> done = new TreeSet<>();
        for (TopiaMetadataEntity metadataEntity : this) {
            applyInheritance(metadataEntity, done);
        }
    }

    public Collection<TopiaMetadataEntity> getEntities() {
        return entities.values();
    }

    public Multimap<String, TopiaMetadataEntity> getSchemas() {
        if (schemas == null) {
            schemas = ArrayListMultimap.create();
            for (TopiaMetadataEntity entity : entities.values()) {
                schemas.put(entity.getDbSchemaName(), entity);
            }
        }
        return schemas;
    }

    public Set<String> getSchemaNames() {
        return getSchemas().keySet();
    }

    public TopiaMetadataEntity getEntity(String type) {
        return entities.get(type);
    }

    public Optional<TopiaMetadataEntity> getOptionalEntity(String type) {
        return Optional.ofNullable(getEntity(type));
    }

    public void accept(TopiaMetadataModelVisitor visitor) {
        visitor.visitModelStart(this);
        for (TopiaMetadataEntity entity : entities.values()) {
            entity.accept(visitor, this);
        }
        visitor.visitModelEnd(this);
    }

    public Set<TopiaMetadataComposition> getReverseCompositions(TopiaMetadataEntity type) {
        return getReverseCompositions(Objects.requireNonNull(type).getType());
    }

    public Set<TopiaMetadataComposition> getReverseCompositions(String type) {
        try {
            return reverseCompositions.get(Objects.requireNonNull(type));
        } catch (ExecutionException e) {
            throw new RuntimeException("Can't get reverse compositions for type: " + type, e);
        }
    }

    public Set<TopiaMetadataComposition> getCompositions(TopiaMetadataEntity type) {
        return getCompositions(Objects.requireNonNull(type).getType());
    }

    public Set<TopiaMetadataComposition> getCompositions(String type) {
        try {
            return compositions.get(Objects.requireNonNull(type));
        } catch (ExecutionException e) {
            throw new RuntimeException("Can't get compositions for type: " + type, e);
        }
    }

    public Set<TopiaMetadataAssociation> getReverseManyToManyAssociations(TopiaMetadataEntity type) {
        return getReverseManyToManyAssociations(Objects.requireNonNull(type).getType());
    }

    public Set<TopiaMetadataAssociation> getReverseManyToManyAssociations(String type) {
        try {
            return reverseManyToManyAssociations.get(Objects.requireNonNull(type));
        } catch (ExecutionException e) {
            throw new RuntimeException("Can't get reverse many to many associations for type: " + type, e);
        }
    }

    public Set<TopiaMetadataReverseAssociation> getReverseAssociations(TopiaMetadataEntity type) {
        return getReverseAssociations(Objects.requireNonNull(type).getType());
    }

    public Set<TopiaMetadataReverseAssociation> getReverseAssociations(String type) {
        try {
            return reverseAssociations.get(Objects.requireNonNull(type));
        } catch (ExecutionException e) {
            throw new RuntimeException("Can't get reverse associations for type: " + type, e);
        }
    }

    public Set<TopiaMetadataAssociation> getReverseOneToManyAssociations(TopiaMetadataEntity type) {
        return getReverseOneToManyAssociations(Objects.requireNonNull(type).getType());
    }

    public Set<TopiaMetadataAssociation> getReverseOneToManyAssociations(String type) {
        try {
            return reverseOneToManyAssociations.get(Objects.requireNonNull(type));
        } catch (ExecutionException e) {
            throw new RuntimeException("Can't get reverse one to many associations for type: " + type, e);
        }
    }


    public Set<TopiaMetadataOneToOneComposition> getReverseOneToOneAssociations(TopiaMetadataEntity type) {
        return getReverseOneToOneAssociations(Objects.requireNonNull(type).getType());
    }

    public Set<TopiaMetadataOneToOneComposition> getReverseOneToOneAssociations(String type) {
        try {
            return reverseOneToOneAssociations.get(Objects.requireNonNull(type));
        } catch (ExecutionException e) {
            throw new RuntimeException("Can't get reverse one to one associations for type: " + type, e);
        }
    }

    public Set<TopiaMetadataAssociation> getAssociations(TopiaMetadataEntity type) {
        return getAssociations(Objects.requireNonNull(type).getType());
    }

    public Set<TopiaMetadataSimpleAssociation> getSimpleAssociations(TopiaMetadataEntity type) {
        return getSimpleAssociations(Objects.requireNonNull(type).getType());
    }

    public Set<TopiaMetadataSimpleAssociation> getSimpleAssociations(String type) {
        try {
            return simpleAssociations.get(Objects.requireNonNull(type));
        } catch (ExecutionException e) {
            throw new RuntimeException("Can't get simple associations for type: " + type, e);
        }
    }

    public Set<TopiaMetadataAssociation> getAssociations(String type) {
        try {
            return associations.get(Objects.requireNonNull(type));
        } catch (ExecutionException e) {
            throw new RuntimeException("Can't get associations for type: " + type, e);
        }
    }

    @Override
    public Iterator<TopiaMetadataEntity> iterator() {
        return entities.values().iterator();
    }

    public Stream<TopiaMetadataEntity> stream() {
        return entities.values().stream();
    }

    public Stream<TopiaMetadataEntity> streamWithoutAbstract() {
        return stream().filter(e -> !e.isAbstract());
    }

    public Stream<TopiaMetadataEntity> streamWithEntryPoint() {
        return streamWithoutAbstract().filter(TopiaMetadataEntity::isEntryPoint);
    }

    public Stream<TopiaMetadataEntity> streamWithStandalone() {
        return streamWithoutAbstract().filter(TopiaMetadataEntity::isStandalone);
    }

    public TopiaMetadataEntity getMetadataEntity(Class<? extends TopiaEntity> entityType) {
        if (typeToMetaEntities.isEmpty()) {
            // build it
            stream().forEach(e -> typeToMetaEntities.put(e.getEntityType(), e));
        }
        return typeToMetaEntities.get(Objects.requireNonNull(entityType));
    }

    private void applyInheritance(TopiaMetadataEntity metadataEntity, Set<String> done) {
        if (!done.add(metadataEntity.getType())) {
            return;
        }
        String parentName = metadataEntity.getParent();
        boolean haveSuper = parentName != null;
        if (haveSuper) {
            Optional<TopiaMetadataEntity> optionalEntity = getOptionalEntity(parentName);
            if (optionalEntity.isPresent()) {
                TopiaMetadataEntity parentMetadataEntity = optionalEntity.get();
                applyInheritance(parentMetadataEntity, done);
                metadataEntity.putAll(parentMetadataEntity);
            }
        }
    }

}
