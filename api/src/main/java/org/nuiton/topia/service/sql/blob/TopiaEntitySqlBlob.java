package org.nuiton.topia.service.sql.blob;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;

/**
 * Created on 05/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.68
 */
public class TopiaEntitySqlBlob {

    private final String schemaName;
    private final String tableName;
    private final List<String> columnNames;

    public TopiaEntitySqlBlob(String schemaName, String tableName, List<String> columnNames) {
        this.schemaName = schemaName;
        this.tableName = tableName;
        this.columnNames = columnNames;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public List<String> getColumnNames() {
        return columnNames;
    }

    public String schemaAndTableName() {
        return getSchemaName() + "." + getTableName();
    }
}
