package org.nuiton.topia.service.sql.internal.consumer;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import io.ultreia.java4all.util.TimeLog;
import org.nuiton.topia.persistence.TopiaEntity;
import io.ultreia.java4all.util.sql.SqlScriptWriter;
import io.ultreia.java4all.util.sql.BlobsContainer;
import io.ultreia.java4all.util.sql.SqlWork;
import org.nuiton.topia.service.sql.internal.SqlRequestConsumer;
import org.nuiton.topia.service.sql.internal.SqlRequestSetConsumerContext;
import org.nuiton.topia.service.sql.plan.copy.TopiaEntitySqlCopyPlanTask;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePlanTask;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 06/04/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.74
 */
class CopyEntityWork implements SqlWork {
    private static final TimeLog TIME_LOG = new TimeLog(CopyEntityWork.class, 500L, 1000L);

    private final SqlRequestSetConsumerContext context;
    private final String sql;
    private final Map<String, BlobsContainer.Builder> blobsBuilder;
    private final Set<String> blobColumns;
    private final List<String> columnNames;
    private final String insertStatementSql;
    private final String recursiveProperty;
    private final SqlScriptWriter writer;

    public CopyEntityWork(SqlRequestSetConsumerContext context, TopiaEntitySqlCopyPlanTask task, String ids) {
        this.context = context;
        this.columnNames = task.getColumnNames();
        String selectClause = SqlRequestConsumer.getSelectClause(task.getTableName(), columnNames);
        this.sql = TopiaEntitySqlReplicatePlanTask.applyIds(task.getSelectSql(), selectClause, ids);
        this.blobsBuilder = context.initBlobsBuilder(task.useBlob(), task.getSchemaAndTableName());
        this.blobColumns = blobsBuilder.keySet();
        this.insertStatementSql = SqlRequestConsumer.newInsertStatementSql(task.getInsertSql(), columnNames, blobColumns);
        this.recursiveProperty = task.getRecursiveColumnName().orElse(null);
        this.writer = context.getWriter();
    }

    @Override
    public void execute(Connection connection) throws SQLException {
        long t0 = TimeLog.getTime();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setFetchSize(context.getReadFetchSize());
            try (ResultSet resultSet = statement.executeQuery()) {
                if (recursiveProperty == null) {
                    simpleCopy(statement, resultSet);
                } else {
                    recursiveCopy(statement, resultSet);
                }
            }
        } finally {
            TIME_LOG.log(t0, "Executed on table", sql);
        }
    }

    private void simpleCopy(PreparedStatement statement, ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            String sql = generateInsertSqlStatement(statement, resultSet);
            writer.writeSql(sql);
        }
    }

    private void recursiveCopy(PreparedStatement statement, ResultSet resultSet) throws SQLException {
        Map<String, String> requestsByTopiaId = new LinkedHashMap<>();
        Multimap<String, String> parents = ArrayListMultimap.create();
        String sentinel = "$$WithoutParent$$" + new Date().getTime();
        while (resultSet.next()) {
            String topiaId = resultSet.getString(TopiaEntity.PROPERTY_TOPIA_ID);
            String key = resultSet.getString(recursiveProperty);
            parents.put(key == null ? sentinel : key, topiaId);
            String sql = generateInsertSqlStatement(statement, resultSet);
            requestsByTopiaId.put(topiaId, sql);
        }
        List<String> orderedTopiaIds = context.generateOrder(parents, sentinel);
        if (orderedTopiaIds.size() != requestsByTopiaId.size()) {
            throw new IllegalStateException("Mismatch ids size!!!");
        }
        for (String orderedTopiaId : orderedTopiaIds) {
            writer.writeSql(requestsByTopiaId.get(orderedTopiaId));
        }
    }

    private String generateInsertSqlStatement(PreparedStatement statement, ResultSet readResultSet) throws SQLException {
        StringBuilder argumentsBuilder = new StringBuilder();
        String rowTopiaId = null;
        for (String columnName : columnNames) {
            if (blobColumns.contains(columnName)) {
                // processed later
                continue;
            }
            Object columnValue = readResultSet.getObject(columnName);
            if (columnValue == null) {
                argumentsBuilder.append(", NULL");
                continue;
            }
            if (columnValue instanceof String) {
                String stringValue = (String) columnValue;
                if (TopiaEntity.PROPERTY_TOPIA_ID.equals(columnName)) {
                    rowTopiaId = stringValue;
                }
                argumentsBuilder.append(", '").append(stringValue.replaceAll("'", "''")).append("'");
                continue;
            }
            if (columnValue instanceof Date) {
                argumentsBuilder.append(", '").append(columnValue).append("'");
                continue;
            }
            argumentsBuilder.append(", ").append(columnValue);
        }
        for (String columnName : blobColumns) {
            Object columnValue = readResultSet.getObject(columnName);
            if (blobColumns.contains(columnName)) {
                context.copyBlob(statement, rowTopiaId, columnValue, blobsBuilder.get(columnName));
            }
        }
        String arguments = argumentsBuilder.substring(2);
        return String.format(insertStatementSql, arguments);
    }
}
