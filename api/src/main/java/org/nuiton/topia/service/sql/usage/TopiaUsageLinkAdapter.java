package org.nuiton.topia.service.sql.usage;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import io.ultreia.java4all.util.json.JsonHelper;
import io.ultreia.java4all.lang.Objects2;
import org.nuiton.topia.persistence.TopiaEntity;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 04/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.68
 */
public abstract class TopiaUsageLinkAdapter<T extends TopiaUsageLink> implements JsonDeserializer<T>, JsonSerializer<T> {

    public static class TopiaUsageReverseOneToManyAssociationAdapter extends TopiaUsageLinkAdapter<TopiaUsageReverseOneToManyAssociation> {
        @Override
        protected TopiaUsageReverseOneToManyAssociation newLink(Class<? extends TopiaEntity> entityType, String propertyName) {
            return new TopiaUsageReverseOneToManyAssociation(entityType, propertyName);
        }
    }

    public static class TopiaUsageReverseOneToOneCompositionAdapter extends TopiaUsageLinkAdapter<TopiaUsageReverseOneToOneComposition> {
        @Override
        protected TopiaUsageReverseOneToOneComposition newLink(Class<? extends TopiaEntity> entityType, String propertyName) {
            return new TopiaUsageReverseOneToOneComposition(entityType, propertyName);
        }
    }

    public static class TopiaUsageReverseCompositionAdapter extends TopiaUsageLinkAdapter<TopiaUsageReverseComposition> {
        @Override
        protected TopiaUsageReverseComposition newLink(Class<? extends TopiaEntity> entityType, String propertyName) {
            return new TopiaUsageReverseComposition(entityType, propertyName);
        }
    }

    public static class TopiaUsageReverseManyToManyAssociationAdapter extends TopiaUsageLinkAdapter<TopiaUsageReverseManyToManyAssociation> {
        @Override
        protected TopiaUsageReverseManyToManyAssociation newLink(Class<? extends TopiaEntity> entityType, String propertyName) {
            return new TopiaUsageReverseManyToManyAssociation(entityType, propertyName);
        }
    }

    public static class TopiaUsageReverseAssociationAdapter extends TopiaUsageLinkAdapter<TopiaUsageReverseAssociation> {
        @Override
        protected TopiaUsageReverseAssociation newLink(Class<? extends TopiaEntity> entityType, String propertyName) {
            return new TopiaUsageReverseAssociation(entityType, propertyName);
        }
    }

    @Override
    public T deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        String[] code = json.getAsString().split("\\s*~\\s*");
        String typeName = code[0];
        String propertyName = code[1];
        Class<? extends TopiaEntity> entityType = Objects2.forName(typeName);
        return newLink(entityType, propertyName);
    }

    protected abstract T newLink(Class<? extends TopiaEntity> entityType, String propertyName);

    @Override
    public JsonElement serialize(T src, Type typeOfSrc, JsonSerializationContext context) {
        List<Object> properties = new ArrayList<>(2);
        JsonHelper.addToProperties(properties, src.getType().getName());
        JsonHelper.addToProperties(properties, src.getPropertyName());
        String collect = JsonHelper.codeProperties(properties);
        return context.serialize(collect);
    }
}
