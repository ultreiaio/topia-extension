package org.nuiton.topia.service.sql.plan.replicate;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.service.sql.plan.copy.TopiaEntitySqlCopyPlanTask;

import java.util.Objects;

/**
 * Created on 06/04/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.74
 */
public class TopiaEntitySqlReplicatePartialPlanTask {

    private final TopiaEntitySqlCopyPlanTask copyTask;
    private final TopiaEntitySqlReplicatePlanTask replicateTask;

    public TopiaEntitySqlReplicatePartialPlanTask(TopiaEntitySqlCopyPlanTask copyTask, TopiaEntitySqlReplicatePlanTask replicateTask) {
        this.copyTask = Objects.requireNonNull(copyTask);
        this.replicateTask = Objects.requireNonNull(replicateTask);
    }

    public String getSchemaAndTableName() {
        return replicateTask.getSchemaAndTableName();
    }

    public TopiaEntitySqlCopyPlanTask getCopyTask() {
        return copyTask;
    }

    public TopiaEntitySqlReplicatePlanTask getReplicateTask() {
        return replicateTask;
    }
}
