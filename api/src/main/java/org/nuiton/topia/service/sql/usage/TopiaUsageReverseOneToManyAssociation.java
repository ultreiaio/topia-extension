package org.nuiton.topia.service.sql.usage;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaQueryBuilderRunQueryStep;

public class TopiaUsageReverseOneToManyAssociation extends TopiaUsageLink {

    public TopiaUsageReverseOneToManyAssociation(Class<? extends TopiaEntity> type, String propertyName) {
        super(type, propertyName);
    }

    @Override
    protected <E extends TopiaEntity> TopiaQueryBuilderRunQueryStep<E> getDaoQuery(TopiaDao<E> dao, TopiaEntity entity) {
        return dao.forContains(getPropertyName(), entity);
    }
}

