package org.nuiton.topia.service.sql.metadata;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;
import io.ultreia.java4all.util.json.JsonHelper;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created on 06/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.69
 */
public class TopiaMetadataEntityAdapter implements JsonDeserializer<TopiaMetadataEntity>, JsonSerializer<TopiaMetadataEntity> {
    public static final String GAV = "gav";
    public static final String IS_ABSTRACT = "isAbstract";
    public static final String ENTRY_POINT = "entryPoint";
    public static final String STANDALONE = "standalone";
    public static final String ONE_TO_MANY_ASSOCIATIONS = "oneToManyAssociations";
    public static final String ONE_TO_MANY_ASSOCIATION_INVERSES = "oneToManyAssociationInverses";
    public static final String ONE_TO_ONE_COMPOSITIONS = "oneToOneCompositions";
    public static final String ONE_TO_ONE_COMPOSITION_INVERSES = "oneToOneCompositionInverses";
    public static final String REVERSED_ASSOCIATIONS = "reversedAssociations";
    public static final String MANY_TO_MANY_ASSOCIATIONS = "manyToManyAssociations";
    public static final String MANY_TO_ONE_ASSOCIATIONS = "manyToOneAssociations";
    public static final String MANY_ASSOCIATIONS = "manyAssociations";
    public static final String PROPERTIES = "properties";
    public static final String DB_COLUMNS_NAME = "dbColumnsName";
    public static final String DB_MANY_TO_MANY_ASSOCIATIONS_TABLE_NAME = "dbManyToManyAssociationsTableName";
    public static final String DB_MANY_ASSOCIATIONS_TABLE_NAME = "dbManyAssociationsTableName";
    public static final String BLOB_PROPERTIES = "blobProperties";
    public static final String EXTRA_COLUMN_NAMES = "extraColumnNames";
    public static final String SKIP_NAVIGATION = "skipNavigation";

    public static Map<String, String> readStringMapOrEmpty(JsonDeserializationContext context, String name, JsonObject object) {
        JsonElement element = object.get(name);
        return element == null ? Map.of() : context.deserialize(element, TypeToken.getParameterized(LinkedHashMap.class, String.class, String.class).getType());
    }

    @Override
    public TopiaMetadataEntity deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = json.getAsJsonObject();
        String[] code = object.getAsJsonPrimitive(GAV).getAsString().split("\\s*~\\s*");
        boolean withoutParent = code.length == 4;
        String parent = withoutParent ? null : code[4];
        String type = code[0];
        String fqn = code[1];
        String dbSchemaName = code[2];
        String dbTableName = code[3];
        boolean isAbstract = object.has(IS_ABSTRACT);
        boolean entryPoint = object.has(ENTRY_POINT);
        boolean standalone = object.has(STANDALONE);
        TopiaMetadataEntity result = new TopiaMetadataEntity(parent, type, fqn, isAbstract, entryPoint, standalone, dbSchemaName, dbTableName);

        result.setOneToManyAssociationInverses(JsonHelper.readStringSetOrEmpty(context, ONE_TO_MANY_ASSOCIATION_INVERSES, object));
        result.setOneToOneCompositionInverses(JsonHelper.readStringSetOrEmpty(context, ONE_TO_ONE_COMPOSITION_INVERSES, object));
        result.setBlobProperties(JsonHelper.readStringSetOrEmpty(context, BLOB_PROPERTIES, object));
        result.setExtraColumnNames(JsonHelper.readStringSetOrEmpty(context, EXTRA_COLUMN_NAMES, object));
        result.setSkipNavigation(JsonHelper.readStringSetOrEmpty(context, SKIP_NAVIGATION, object));

        result.setOneToManyAssociations(readStringMapOrEmpty(context, ONE_TO_MANY_ASSOCIATIONS, object));
        result.setReversedAssociations(readStringMapOrEmpty(context, REVERSED_ASSOCIATIONS, object));
        result.setManyToManyAssociations(readStringMapOrEmpty(context, MANY_TO_MANY_ASSOCIATIONS, object));
        result.setManyToOneAssociations(readStringMapOrEmpty(context, MANY_TO_ONE_ASSOCIATIONS, object));
        result.setOneToOneCompositions(readStringMapOrEmpty(context, ONE_TO_ONE_COMPOSITIONS, object));
        result.setManyAssociations(readStringMapOrEmpty(context, MANY_ASSOCIATIONS, object));
        result.setProperties(readStringMapOrEmpty(context, PROPERTIES, object));
        result.setDbColumnsName(readStringMapOrEmpty(context, DB_COLUMNS_NAME, object));
        result.setDbManyToManyAssociationsTableName(readStringMapOrEmpty(context, DB_MANY_TO_MANY_ASSOCIATIONS_TABLE_NAME, object));
        result.setDbManyAssociationsTableName(readStringMapOrEmpty(context, DB_MANY_ASSOCIATIONS_TABLE_NAME, object));

        return result;
    }

    @Override
    public JsonElement serialize(TopiaMetadataEntity src, Type typeOfSrc, JsonSerializationContext context) {
        List<Object> properties = new ArrayList<>(5);
        JsonHelper.addToProperties(properties, src.getType());
        JsonHelper.addToProperties(properties, src.getFullyQualifiedName());
        JsonHelper.addToProperties(properties, src.getDbSchemaName());
        JsonHelper.addToProperties(properties, src.getDbTableName());
        JsonHelper.addToProperties(properties, src.getParent());
        JsonObject result = new JsonObject();
        JsonHelper.addToResult(context, result, GAV, JsonHelper.codeProperties(properties));
        JsonHelper.addToResult(context, result, IS_ABSTRACT, src.isAbstract());
        JsonHelper.addToResult(context, result, ENTRY_POINT, src.isEntryPoint());
        JsonHelper.addToResult(context, result, STANDALONE, src.isStandalone());

        addToResult(context, result, ONE_TO_MANY_ASSOCIATIONS, src.getOneToManyAssociations());
        addToResult(context, result, REVERSED_ASSOCIATIONS, src.getReversedAssociations());
        addToResult(context, result, MANY_TO_MANY_ASSOCIATIONS, src.getManyToManyAssociations());
        addToResult(context, result, MANY_TO_ONE_ASSOCIATIONS, src.getManyToOneAssociations());
        addToResult(context, result, ONE_TO_ONE_COMPOSITIONS, src.getOneToOneCompositions());
        addToResult(context, result, MANY_ASSOCIATIONS, src.getManyAssociations());
        addToResult(context, result, PROPERTIES, src.getProperties());
        addToResult(context, result, DB_COLUMNS_NAME, src.getDbColumnsName());
        addToResult(context, result, DB_MANY_TO_MANY_ASSOCIATIONS_TABLE_NAME, src.getDbManyToManyAssociationsTableName());
        addToResult(context, result, DB_MANY_ASSOCIATIONS_TABLE_NAME, src.getDbManyAssociationsTableName());
        JsonHelper.addToResult(context, result, ONE_TO_MANY_ASSOCIATION_INVERSES, src.getOneToManyAssociationInverses());
        JsonHelper.addToResult(context, result, ONE_TO_ONE_COMPOSITION_INVERSES, src.getOneToOneCompositionInverses());
        JsonHelper.addToResult(context, result, BLOB_PROPERTIES, src.getBlobProperties());
        JsonHelper.addToResult(context, result, EXTRA_COLUMN_NAMES, src.getExtraColumnNames());
        JsonHelper.addToResult(context, result, SKIP_NAVIGATION, src.getSkipNavigation());
        return result;
    }

    void addToResult(JsonSerializationContext context, JsonObject result, String name, Map<?, ?> map) {
        if (map == null || map.isEmpty()) {
            return;
        }
        result.add(name, context.serialize(map));
    }
}
