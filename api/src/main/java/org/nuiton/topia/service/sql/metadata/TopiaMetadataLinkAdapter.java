package org.nuiton.topia.service.sql.metadata;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import io.ultreia.java4all.util.json.JsonHelper;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 06/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.69
 */
public class TopiaMetadataLinkAdapter implements JsonDeserializer<TopiaMetadataLink>, JsonSerializer<TopiaMetadataLink> {

    private final TopiaMetadataModel model;

    public TopiaMetadataLinkAdapter(TopiaMetadataModel model) {
        this.model = model;
    }

    @Override
    public TopiaMetadataLink deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String[] code = json.getAsString().split("\\s*~\\s*");
        String type = TopiaMetadataComposition.class.getPackageName() + "." + code[0];
        TopiaMetadataEntity owner = model.getEntity(code[1]);
        String targetPropertyName = code[2];
        TopiaMetadataEntity target = model.getEntity(code[3]);
        if (TopiaMetadataComposition.class.getName().equals(type)) {
            return new TopiaMetadataComposition(owner, targetPropertyName, target);
        }
        if (TopiaMetadataAssociation.class.getName().equals(type)) {
            return new TopiaMetadataAssociation(owner, targetPropertyName, target);
        }
        if (TopiaMetadataReverseAssociation.class.getName().equals(type)) {
            return new TopiaMetadataReverseAssociation(owner, targetPropertyName, target);
        }
        throw new JsonParseException("Can't instantiate link fo type: " + type);
    }

    @Override
    public JsonElement serialize(TopiaMetadataLink src, Type typeOfSrc, JsonSerializationContext context) {
        List<Object> properties = new ArrayList<>(4);
        JsonHelper.addToProperties(properties, src.getClass().getSimpleName());
        JsonHelper.addToProperties(properties, src.getOwner().getType());
        JsonHelper.addToProperties(properties, src.getTargetPropertyName());
        JsonHelper.addToProperties(properties, src.getTarget().getType());
        return context.serialize(JsonHelper.codeProperties(properties));
    }
}
