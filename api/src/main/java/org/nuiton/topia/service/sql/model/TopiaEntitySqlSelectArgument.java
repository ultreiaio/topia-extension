package org.nuiton.topia.service.sql.model;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.Set;

/**
 * Represents arguments to apply to a selector.
 * <p>
 * Created on 22/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.27
 */
public class TopiaEntitySqlSelectArgument {

    protected final Set<String> ids;

    public static TopiaEntitySqlSelectArgument of(String... ids) {
        return ids.length == 0 ? null : new TopiaEntitySqlSelectArgument(Set.of(ids));
    }

    public static TopiaEntitySqlSelectArgument of(Collection<String> ids) {
        return ids == null || ids.isEmpty() ? null : new TopiaEntitySqlSelectArgument(ids);
    }

    protected TopiaEntitySqlSelectArgument(Collection<String> ids) {
        this.ids = Set.copyOf(ids);
    }

    public Set<String> getIds() {
        return ids;
    }
}

