package org.nuiton.topia.service.sql.internal.request;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.hibernate.dialect.Dialect;
import org.nuiton.topia.service.sql.internal.SqlRequest;

/**
 * Created on 25/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.28
 */
public abstract class SqlSchemaRequest implements SqlRequest {

    private final Class<? extends Dialect> dialect;
    private final boolean processSchema;
    private final boolean processTables;

    protected SqlSchemaRequest(Class<? extends Dialect> dialect, boolean processSchema, boolean processTables) {
        this.dialect = dialect;
        this.processSchema = processSchema;
        this.processTables = processTables;
    }

    public Class<? extends Dialect> getDialect() {
        return dialect;
    }

    public boolean isH2() {
        return dialect.getSimpleName().equals("H2Dialect");
    }

    public boolean isProcessSchema() {
        return processSchema;
    }

    public boolean isProcessTables() {
        return processTables;
    }
}
