package org.nuiton.topia.service.sql.internal.consumer;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaException;
import io.ultreia.java4all.util.sql.SqlScriptWriter;
import org.nuiton.topia.service.sql.internal.SqlRequestConsumer;
import org.nuiton.topia.service.sql.internal.SqlRequestSetConsumerContext;
import org.nuiton.topia.service.sql.internal.request.AddVersionTableRequest;

/**
 * Created on 21/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.43
 */
public class AddVersionTableConsumer implements SqlRequestConsumer<AddVersionTableRequest> {

    private static final String CREATE_VERSION_TABLE_STATEMENT = "CREATE TABLE public.tms_version(version VARCHAR(255) NOT NULL, PRIMARY KEY (version));";
    private static final String FILL_VERSION_TABLE_STATEMENT = "INSERT INTO public.tms_version(version) VALUES('%s');";

    @Override
    public void consume(AddVersionTableRequest request, SqlRequestSetConsumerContext context) {
        try {
            SqlScriptWriter writer = context.getWriter();
            writer.writeSql(CREATE_VERSION_TABLE_STATEMENT);
            writer.writeSql(String.format(FILL_VERSION_TABLE_STATEMENT, request.getDbVersion()));
        } catch (Exception e) {
            throw new TopiaException(String.format("Could not add  schema for reason: %s", e.getMessage()), e);
        }
    }
}
