package org.nuiton.topia.service.sql.metadata;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import io.ultreia.java4all.lang.Objects2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaEntity;

import java.sql.Blob;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created on 03/01/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.1
 */
public class TopiaMetadataEntity implements Comparable<TopiaMetadataEntity> {

    private static final Logger log = LogManager.getLogger(TopiaMetadataEntity.class);

    /**
     * Optional parent type.
     */
    protected final String parent;
    /**
     * TopiaEntityEnum name.
     */
    protected final String type;
    /**
     * Fully qualified name of entity.
     */
    protected final String fullyQualifiedName;
    /**
     * Is abstract?
     */
    protected final boolean isAbstract;
    /**
     * Is entry point?
     */
    protected final boolean entryPoint;
    /**
     * Db schema name.
     */
    protected final String dbSchemaName;
    /**
     * Db table name.
     */
    protected final String dbTableName;
    /**
     * One to many associations (keys are property name, values are type).
     */
    protected final Map<String, String> oneToManyAssociations = new LinkedHashMap<>();
    /**
     * One to one compositions (keys are property name, values are type).
     */
    protected final Map<String, String> oneToOneCompositions = new LinkedHashMap<>();
    /**
     * Target types of one to many relations. (To get column, use getDbColumName from here).
     */
    protected final Set<String> oneToOneCompositionInverses = new TreeSet<>();
    /**
     * Target types of one to many relations. (To get column, use getDbColumName from here).
     */
    protected final Set<String> oneToManyAssociationInverses = new TreeSet<>();
    /**
     * Many to many associations (keys are property name, values are type).
     */
    protected final Map<String, String> reversedAssociations = new LinkedHashMap<>();
    /**
     * Many to many associations (keys are property name, values are type).
     */
    protected final Map<String, String> manyToManyAssociations = new LinkedHashMap<>();
    /**
     * Many to one associations (keys are property name, values are type).
     */
    protected final Map<String, String> manyToOneAssociations = new LinkedHashMap<>();
    /**
     * Many associations (keys are property name, values are type).
     */
    protected final Map<String, String> manyAssociations = new LinkedHashMap<>();
    /**
     * Properties (keys are property name, values are type).
     */
    protected final Map<String, String> properties = new LinkedHashMap<>();
    /**
     * Blob properties.
     */
    protected final Set<String> blobProperties = new LinkedHashSet<>();
    /**
     * Extra columns.
     */
    protected final Set<String> extraColumnNames = new LinkedHashSet<>();
    /**
     * Le nom des colunnes correspondants aux propriétés de l'entité.
     * <b>Note: </b> On ne conserve que les correspondances qui diffèrent du nom de la propriété.
     *
     * @see #getDbColumnName(String)
     */
    protected final Map<String, String> dbColumnsName = new LinkedHashMap<>();
    /**
     * Le nom des tables utilisées pour les associations nm.
     */
    protected final Map<String, String> dbManyToManyAssociationsTableName = new LinkedHashMap<>();
    /**
     * Le nom des tables utilisées pour les associations simples nm.
     */
    protected final Map<String, String> dbManyAssociationsTableName = new LinkedHashMap<>();
    private final boolean standalone;
    /**
     * Get all property we do not want to navigate on.
     */
    private final Set<String> skipNavigation = new LinkedHashSet<>();
    protected Class<? extends TopiaEntity> entityType;
    protected Set<String> allDbColumnNames;

    public static List<String> toFqn(Collection<TopiaMetadataEntity> entities) {
        return entities.stream().map(TopiaMetadataEntity::getFullyQualifiedName).collect(Collectors.toList());
    }

    public static List<TopiaMetadataEntity> sortByFqn(Collection<TopiaMetadataEntity> entities) {
        return sortByFqn(entities.stream());
    }

    public static List<TopiaMetadataEntity> sortByFqn(Stream<TopiaMetadataEntity> entities) {
        return streamSortByFqn(entities).collect(Collectors.toList());
    }

    public static Stream<TopiaMetadataEntity> streamSortByFqn(Stream<TopiaMetadataEntity> entities) {
        return entities.sorted(Comparator.comparing(TopiaMetadataEntity::getFullyQualifiedName));
    }

    public TopiaMetadataEntity(String parent, String type, String fullyQualifiedName, boolean isAbstract, boolean entryPoint, boolean standalone, String dbSchemaName, String dbTableName) {
        this.parent = parent;
        this.type = type;
        this.fullyQualifiedName = fullyQualifiedName;
        this.isAbstract = isAbstract;
        this.entryPoint = entryPoint;
        this.standalone = standalone;
        this.dbSchemaName = dbSchemaName;
        this.dbTableName = dbTableName;
    }

    public void putAll(TopiaMetadataEntity parent) {
        putAll(getOneToManyAssociations(), parent.getOneToManyAssociations());
        putAll(getOneToOneCompositions(), parent.getOneToOneCompositions());
        addAll(getOneToOneCompositionInverses(), parent.getOneToOneCompositionInverses());
        addAll(getOneToManyAssociationInverses(), parent.getOneToManyAssociationInverses());
        putAll(getReversedAssociations(), parent.getReversedAssociations());
        putAll(getManyToManyAssociations(), parent.getManyToManyAssociations());
        putAll(getManyToOneAssociations(), parent.getManyToOneAssociations());
        putAll(getManyAssociations(), parent.getManyAssociations());
        putAll(getProperties(), parent.getProperties());
        addAll(getExtraColumnNames(), parent.getExtraColumnNames());
        putAll(getDbColumnsName(), parent.getDbColumnsName());
        putAll(getDbColumnsName(), parent.getDbManyToManyAssociationsTableName());
        addAll(getSkipNavigation(), parent.getSkipNavigation());
    }

    private void addAll(Set<String> current, Set<String> parent) {
        Set<String> result = new LinkedHashSet<>(parent);
        result.addAll(current);
        current.clear();
        current.addAll(result);
    }

    private void putAll(Map<String, String> current, Map<String, String> parent) {
        Map<String, String> result = new LinkedHashMap<>(parent);
        result.putAll(current);
        current.clear();
        current.putAll(result);
    }

    public String getType() {
        return type;
    }

    public Class<? extends TopiaEntity> getEntityType() {
        return entityType == null ? entityType = Objects2.forName(fullyQualifiedName) : entityType;
    }

    public String getFullyQualifiedName() {
        return fullyQualifiedName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TopiaMetadataEntity that = (TopiaMetadataEntity) o;
        return Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("type", type)
                .add("dbName", dbSchemaName + "." + dbTableName)
                .toString();
    }

    public String getParent() {
        return parent;
    }

    public String getDbSchemaName() {
        return dbSchemaName;
    }

    public String getSchemaAndTableName() {
        return getDbSchemaName() + "." + getDbTableName();
    }

    public String getDbTableName() {
        return dbTableName;
    }

    public boolean isAbstract() {
        return isAbstract;
    }

    public boolean isEntryPoint() {
        return entryPoint;
    }

    public boolean isStandalone() {
        return standalone;
    }

    public Set<String> getAllDbColumnNames() {
        if (allDbColumnNames == null) {
            allDbColumnNames = new LinkedHashSet<>();
            allDbColumnNames.add(TopiaEntity.PROPERTY_TOPIA_ID);
            allDbColumnNames.add(TopiaEntity.PROPERTY_TOPIA_CREATE_DATE);
            allDbColumnNames.add(TopiaEntity.PROPERTY_TOPIA_VERSION);
            allDbColumnNames.addAll(extraColumnNames);
            allDbColumnNames.addAll(getProperties().keySet().stream()
                                            .map(this::getDbColumnName)
                                            .collect(Collectors.toCollection(LinkedHashSet::new)));
            allDbColumnNames.addAll(getManyToOneAssociations().keySet().stream()
                                            .map(this::getDbColumnName)
                                            .collect(Collectors.toCollection(LinkedHashSet::new)));
            allDbColumnNames.addAll(getReversedAssociations().keySet().stream()
                                            .map(this::getDbColumnName)
                                            .collect(Collectors.toCollection(LinkedHashSet::new)));
            allDbColumnNames.addAll(getOneToManyAssociationInverses().stream()
                                            .map(this::getDbColumnName)
                                            .collect(Collectors.toCollection(LinkedHashSet::new)));
            allDbColumnNames.addAll(getOneToOneCompositionInverses().stream()
                                            .map(this::getDbColumnName)
                                            .collect(Collectors.toCollection(LinkedHashSet::new)));
        }
        return allDbColumnNames;
    }

    public Map<String, String> getReversedAssociations() {
        return reversedAssociations;
    }

    public Map<String, String> getManyToManyAssociations() {
        return manyToManyAssociations;
    }

    public Map<String, String> getOneToManyAssociations() {
        return oneToManyAssociations;
    }

    public Map<String, String> getOneToOneCompositions() {
        return oneToOneCompositions;
    }

    public Set<String> getOneToOneCompositionInverses() {
        return oneToOneCompositionInverses;
    }

    public Map<String, String> getManyToOneAssociations() {
        return manyToOneAssociations;
    }

    public Map<String, String> getManyAssociations() {
        return manyAssociations;
    }

    public Set<String> getOneToManyAssociationInverses() {
        return oneToManyAssociationInverses;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public Set<String> getExtraColumnNames() {
        return extraColumnNames;
    }

    public Set<String> getSkipNavigation() {
        return skipNavigation;
    }

    public Set<String> getPrimitivePropertyNames(String primitiveType) {
        Set<String> names = new LinkedHashSet<>();
        for (Map.Entry<String, String> entry : properties.entrySet()) {
            String propertyType = entry.getValue();
            if (primitiveType.equals(propertyType)) {
                names.add(entry.getKey());
            }
        }
        return names;
    }

    public boolean withBlob() {
        return !blobProperties.isEmpty();
    }

    public Set<String> getBlobProperties() {
        return blobProperties;
    }

    public Map<String, String> getDbColumnsName() {
        return dbColumnsName;
    }

    public String getDbColumnName(String propertyName) {
        String dbColumnName = dbColumnsName.get(propertyName);
        if (dbColumnName == null) {
            dbColumnName = propertyName;
        }
        return dbColumnName;
    }

    public Optional<String> getOptionalRecursiveProperty() {
        return getManyToOneAssociations().entrySet().stream().filter(f -> f.getValue().equals(getType())).map(Map.Entry::getKey).findFirst();
    }

    public Map<String, String> getDbManyToManyAssociationsTableName() {
        return dbManyToManyAssociationsTableName;
    }

    public Map<String, String> getDbManyAssociationsTableName() {
        return dbManyAssociationsTableName;
    }

    public String getBdManyToManyAssociationTableName(String propertyName) {
        return dbManyToManyAssociationsTableName.get(propertyName);
    }

    public String getBdManyAssociationTableName(String propertyName) {
        return dbManyAssociationsTableName.get(propertyName);
    }

    public void addOneToManyAssociation(TopiaMetadataEntity associationClazz, String name, String dbColumnName) {
        log.debug(getType() + "/" + name + "(" + dbColumnName + ") →" + associationClazz.getType());
        oneToManyAssociations.put(name, associationClazz.getType());
        addDbColumnName(name, dbColumnName);
    }

    public void addExtraColumn(String extraColumn) {
        extraColumnNames.add(extraColumn);
    }

    public void addOneToManyAssociationInverse(TopiaMetadataEntity associationClazz, String name, String dbColumnName) {
        log.debug(getType() + "/" + name + "(" + dbColumnName + ") →" + associationClazz.getType());
        oneToManyAssociationInverses.add(name);
        addDbColumnName(name, dbColumnName);
    }


    public void addOneToOneAssociationInverse(TopiaMetadataEntity associationClazz, String name, String dbColumnName) {
        log.debug(getType() + "/" + name + "(" + dbColumnName + ") →" + associationClazz.getType());
        oneToOneCompositionInverses.add(name);
        addDbColumnName(name, dbColumnName);
    }

    public void addOneToOneAssociation(TopiaMetadataEntity associationClazz, String name, String dbColumnName) {
        log.debug(getType() + "/" + name + "(" + dbColumnName + ") →" + associationClazz.getType());
        oneToOneCompositions.put(name, associationClazz.getType());
//        addDbColumnName(name, dbColumnName);
    }

    public void addReversedAssociation(TopiaMetadataEntity associationClazz, String name, String dbColumnName) {
        log.debug(getType() + "/" + name + "(" + dbColumnName + ") →" + associationClazz.getType());
        reversedAssociations.put(name, associationClazz.getType());
        addDbColumnName(name, dbColumnName);
    }

    public void addManyToManyAssociation(TopiaMetadataEntity associationClazz, String name, String dbColumnName, String dbManyToManyAssociationTableName, String reverseDbColumnName) {
        log.debug(getType() + "/" + name + "(" + dbManyToManyAssociationTableName + ") →" + associationClazz.getType());
        manyToManyAssociations.put(name, associationClazz.getType());
        addDbColumnName(name, dbColumnName);
        if (reverseDbColumnName != null) {
            addDbColumnName(dbManyToManyAssociationTableName, reverseDbColumnName);
        }
        dbManyToManyAssociationsTableName.put(name, dbManyToManyAssociationTableName);
    }

    public void addManyAssociation(String name, String dbColumnName, String dbManyToManyAssociationTableName, String reverseDbColumnName, String type) {
        log.debug(getType() + "/" + name + "(" + dbManyToManyAssociationTableName + ") →" + type);
        manyAssociations.put(name, type);
        addDbColumnName(name, dbColumnName);
        if (reverseDbColumnName != null) {
            addDbColumnName(dbManyToManyAssociationTableName, reverseDbColumnName);
        }
        dbManyAssociationsTableName.put(name, dbManyToManyAssociationTableName);
    }

    public void addManyToOneAssociation(TopiaMetadataEntity associationClazz, String name, String dbColumnName) {
        log.debug(getType() + "/" + name + "(" + dbColumnName + ") →" + associationClazz.getType());
        manyToOneAssociations.put(name, associationClazz.getType());
        addDbColumnName(name, dbColumnName);
    }

    public void addProperty(String name, String type, String dbColumnName) {
        log.debug(getType() + "/" + name + "(" + dbColumnName + ") →" + type);
        properties.put(name, type);
        if (Blob.class.getName().equals(type)) {
            blobProperties.add(name);
        }
        addDbColumnName(name, dbColumnName);
    }

    public void accept(TopiaMetadataModelVisitor visitor, TopiaMetadataModel metadataModel) {
        visitor.visitEntityStart(metadataModel, this);
        for (Map.Entry<String, String> entry : reversedAssociations.entrySet()) {
            String propertyName = entry.getKey();
            String propertyType = entry.getValue();
            visitor.visitReversedAssociation(metadataModel, this, propertyName, metadataModel.getEntity(propertyType));
        }
        for (Map.Entry<String, String> entry : oneToManyAssociations.entrySet()) {
            String propertyName = entry.getKey();
            String propertyType = entry.getValue();
            visitor.visitOneToManyAssociation(metadataModel, this, propertyName, metadataModel.getEntity(propertyType));
        }
        for (Map.Entry<String, String> entry : oneToOneCompositions.entrySet()) {
            String propertyName = entry.getKey();
            String propertyType = entry.getValue();
            visitor.visitOneToOneAssociation(metadataModel, this, propertyName, metadataModel.getEntity(propertyType));
        }
        for (String propertyType : oneToManyAssociationInverses) {
            visitor.visitOneToManyAssociationInverse(metadataModel, this, propertyType, metadataModel.getEntity(propertyType));
        }
        //FIXME
//        for (String propertyType : oneToOneCompositionInverses) {
//            visitor.visitOneToManyAssociationInverse(metadataModel, this, propertyType, metadataModel.getEntity(propertyType));
//        }
        for (Map.Entry<String, String> entry : manyToManyAssociations.entrySet()) {
            String propertyName = entry.getKey();
            String propertyType = entry.getValue();
            visitor.visitManyToManyAssociation(metadataModel, this, propertyName, metadataModel.getEntity(propertyType));
        }
        for (Map.Entry<String, String> entry : manyAssociations.entrySet()) {
            String propertyName = entry.getKey();
            String propertyType = entry.getValue();
            visitor.visitManyAssociation(metadataModel, this, propertyName, propertyType);
        }
        for (Map.Entry<String, String> entry : manyToOneAssociations.entrySet()) {
            String propertyName = entry.getKey();
            String propertyType = entry.getValue();
            visitor.visitManyToOneAssociation(metadataModel, this, propertyName, metadataModel.getEntity(propertyType));
        }
        for (Map.Entry<String, String> entry : properties.entrySet()) {
            String propertyName = entry.getKey();
            String propertyType = entry.getValue();
            visitor.visitProperty(metadataModel, this, propertyName, propertyType);
        }
        visitor.visitEntityEnd(metadataModel, this);
    }

    public boolean withShell() {
        return !(reversedAssociations.isEmpty() && oneToManyAssociations.isEmpty() && manyToManyAssociations.isEmpty());
    }

    private void addDbColumnName(String name, String dbColumnName) {
        if (!name.equals(dbColumnName)) {
            dbColumnsName.put(name, dbColumnName);
        }
    }

    public boolean withEntities() {
        return withShell() || !manyToOneAssociations.isEmpty();
    }

    public void addSkipNavigation(String name) {
        skipNavigation.add(name);
    }

    public boolean isSkipNavigation(String name) {
        return skipNavigation.contains(name);
    }

    @Override
    public int compareTo(TopiaMetadataEntity o) {
        return getFullyQualifiedName().compareTo(o.getFullyQualifiedName());
    }

    public void setOneToManyAssociationInverses(Set<String> oneToManyAssociationInverses) {
        this.oneToManyAssociationInverses.clear();
        this.oneToManyAssociationInverses.addAll(oneToManyAssociationInverses);
    }

    public void setOneToOneCompositionInverses(Set<String> oneToOneCompositionInverses) {
        this.oneToOneCompositionInverses.clear();
        this.oneToOneCompositionInverses.addAll(oneToOneCompositionInverses);
    }

    public void setBlobProperties(Set<String> blobProperties) {
        this.blobProperties.clear();
        this.blobProperties.addAll(blobProperties);
    }

    public void setExtraColumnNames(Set<String> extraColumnNames) {
        this.extraColumnNames.clear();
        this.extraColumnNames.addAll(extraColumnNames);
    }

    public void setSkipNavigation(Set<String> skipNavigation) {
        this.skipNavigation.clear();
        this.skipNavigation.addAll(skipNavigation);
    }

    public void setReversedAssociations(Map<String, String> map) {
        this.reversedAssociations.clear();
        this.reversedAssociations.putAll(map);
    }

    public void setManyToManyAssociations(Map<String, String> map) {
        this.manyToManyAssociations.clear();
        this.manyToManyAssociations.putAll(map);
    }

    public void setManyToOneAssociations(Map<String, String> map) {
        this.manyToOneAssociations.clear();
        this.manyToOneAssociations.putAll(map);
    }

    public void setOneToOneCompositions(Map<String, String> map) {
        this.oneToOneCompositions.clear();
        this.oneToOneCompositions.putAll(map);
    }

    public void setManyAssociations(Map<String, String> map) {
        this.manyAssociations.clear();
        this.manyAssociations.putAll(map);
    }

    public void setProperties(Map<String, String> map) {
        this.properties.clear();
        this.properties.putAll(map);
    }

    public void setDbColumnsName(Map<String, String> map) {
        this.dbColumnsName.clear();
        this.dbColumnsName.putAll(map);
    }

    public void setDbManyToManyAssociationsTableName(Map<String, String> map) {
        this.dbManyToManyAssociationsTableName.clear();
        this.dbManyToManyAssociationsTableName.putAll(map);
    }

    public void setOneToManyAssociations(Map<String, String> map) {
        this.oneToManyAssociations.clear();
        this.oneToManyAssociations.putAll(map);
    }

    public void setDbManyAssociationsTableName(Map<String, String> map) {
        this.dbManyAssociationsTableName.clear();
        this.dbManyAssociationsTableName.putAll(map);
    }
}
