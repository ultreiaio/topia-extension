package org.nuiton.topia.service.sql;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.TimeLog;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.internal.AbstractTopiaApplicationContext;
import io.ultreia.java4all.util.sql.SqlScript;
import org.nuiton.topia.service.sql.blob.TopiaEntitySqlBlobModel;
import org.nuiton.topia.service.sql.internal.SqlRequestSet;
import org.nuiton.topia.service.sql.internal.SqlRequestSetConsumer;
import org.nuiton.topia.service.sql.metadata.TopiaMetadataModel;
import org.nuiton.topia.service.sql.metadata.TopiaMetadataModelPaths;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlModel;
import org.nuiton.topia.service.sql.plan.copy.TopiaEntitySqlCopyPlanModel;
import org.nuiton.topia.service.sql.plan.delete.TopiaEntitySqlDeletePlanModel;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePlanModel;
import org.nuiton.topia.service.sql.request.ReplicatePartialRequestCallback;
import org.nuiton.topia.service.sql.request.ReplicateRequestCallback;
import org.nuiton.topia.service.sql.usage.TopiaEntitySqlUsageModel;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Service to produce high level sql scripts from sql model.
 * <p>
 * Created on 24/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.67
 */
public class TopiaSqlServiceImpl implements TopiaSqlService {
    private static final TimeLog TIME_LOG = new TimeLog(TopiaSqlService.class, 100, 1000);
    private static final Logger log = LogManager.getLogger(TopiaSqlServiceImpl.class);
    /**
     * Configuration of service.
     */
    protected TopiaSqlServiceConfiguration configuration;

    @Override
    public TopiaEntitySqlModel getModel() {
        return configuration.getModel();
    }

    @Override
    public TopiaEntitySqlCopyPlanModel getCopyPlanModel() {
        return configuration.getCopyPlanModel();
    }

    @Override
    public TopiaEntitySqlReplicatePlanModel getReplicatePlanModel() {
        return configuration.getReplicatePlanModel();
    }

    @Override
    public TopiaEntitySqlDeletePlanModel getDeletePlanModel() {
        return configuration.getDeletePlanModel();
    }

    @Override
    public TopiaEntitySqlUsageModel getUsageModel() {
        return configuration.getUsageModel();
    }

    @Override
    public TopiaEntitySqlBlobModel getBlobModel() {
        return configuration.getBlobModel();
    }

    @Override
    public TopiaMetadataModel getMetaModel() {
        return configuration.getMetaModel();
    }

    @Override
    public TopiaMetadataModelPaths getMetaModelPaths() {
        return configuration.getMetaModelPaths();
    }

    @Override
    public Set<ReplicatePartialRequestCallback> getReplicatePartialRequestCallbacks() {
        return configuration.getReplicatePartialRequestCallbacks();
    }

    @Override
    public Set<ReplicateRequestCallback> getReplicateRequestCallbacks() {
        return configuration.getReplicateRequestCallbacks();
    }

    @Override
    public void initTopiaService(TopiaApplicationContext<?>  topiaApplicationContext, Map<String, String> serviceConfiguration) {
        this.configuration = TopiaSqlServiceConfiguration.of((AbstractTopiaApplicationContext<?>) Objects.requireNonNull(topiaApplicationContext), (TopiaEntitySqlModelResource) topiaApplicationContext, Objects.requireNonNull(serviceConfiguration));
    }

    @Override
    public final SqlScript generatePgSchema(Path targetPath, boolean addTables) throws IOException {
        if (Files.notExists(targetPath.getParent())) {
            Files.createDirectories(targetPath.getParent());
        }
        SqlRequestSet request = SqlRequestSet.builder(targetPath).forPostgresql().addCreateSchemaRequest(true, addTables).build(configuration);
        return consume(request);
    }

    @Override
    public final SqlScript generateH2Schema(Path targetPath, boolean addTables) throws IOException {
        if (Files.notExists(targetPath.getParent())) {
            Files.createDirectories(targetPath.getParent());
        }
        SqlRequestSet request = SqlRequestSet.builder(targetPath).forH2().addCreateSchemaRequest(true, addTables).build(configuration);
        return consume(request);
    }

    @Override
    public SqlScript consume(SqlRequestSet request) throws TopiaSqlServiceException {
        return SqlRequestSetConsumer.consume(configuration.getApplicationContext(), request);
    }

    @Override
    public SqlScript consume(TopiaSqlServiceRequest request) throws TopiaSqlServiceException {
        long t0 = TimeLog.getTime();
        try {
            Path targetPath = configuration.newTemporaryDirectory(request);
            log.info(String.format("Will consume request %s", request));
            log.info(String.format("Generate to %s", targetPath));
            SqlRequestSet sqlRequestSet = configuration.buildSqlRequestSet(request, targetPath);
            return consume(sqlRequestSet);
        } catch (Exception e) {
            throw new TopiaSqlServiceException(String.format("Could not produce sql script for: %s", request), e);
        } finally {
            TIME_LOG.log(t0, "consume request " + request.getClass().getName());
        }
    }

    @Override
    public TopiaSqlServiceConfiguration getConfiguration() {
        return configuration;
    }

    @Override
    public void close() {
        configuration = null;
    }
}
