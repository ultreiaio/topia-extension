package org.nuiton.topia.service.migration;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Version;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaMisconfigurationException;
import org.nuiton.topia.persistence.support.TopiaSqlDllSupport;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Map;
import java.util.ServiceLoader;

/**
 * Configuration of the {@link TopiaMigrationService} service.
 * <p>
 * Created by tchemit on 05/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class TopiaMigrationServiceConfiguration {

    private static final Logger log = LogManager.getLogger(TopiaMigrationServiceConfiguration.class);

    /**
     * User callback.
     */
    protected final TopiaMigrationServiceAskUserToMigrate callback;
    /**
     * Topia application context
     */
    protected final TopiaApplicationContext<?> applicationContext;
    /**
     * Persistence model version (the version to migrate to).
     */
    protected final Version modelVersion;
    /**
     * Migration classifier.
     */
    protected final String classifier;

    /**
     * Temporary directory used to create migration scripts.
     */
    protected final Path temporaryDirectory;

    public static TopiaMigrationServiceConfiguration of(TopiaApplicationContext<?> applicationContext, Map<String, String> serviceConfiguration) {

        String classifier = TopiaSqlDllSupport.getClassifier(applicationContext);
        if (classifier != null) {
            log.debug("Use Classifier          - " + classifier);
        }

        TopiaMigrationServiceAskUserToMigrate askUserToMigrate = null;
        Iterator<TopiaMigrationServiceAskUserToMigrate> serviceIterator = ServiceLoader.load(TopiaMigrationServiceAskUserToMigrate.class).iterator();
        if (serviceIterator.hasNext()) {

            askUserToMigrate = serviceIterator.next();
            if (serviceIterator.hasNext()) {
                throw new TopiaMisconfigurationException("More than one service registered for type: " + TopiaMigrationServiceAskUserToMigrate.class.getName(), applicationContext.getConfiguration());
            }
        }
        if (askUserToMigrate != null) {
            log.debug("Use callback            - " + askUserToMigrate);
        }
        String path = serviceConfiguration.get("java.io.tmpdir");
        if (path == null) {
            path = System.getProperty("java.io.tmpdir");
        }
        Path temporaryDirectory = Paths.get(path);
        try {
            if (Files.notExists(temporaryDirectory)) {
                Files.createDirectories(temporaryDirectory);
            }
        } catch (IOException e) {
            throw new TopiaException("Could not create temporary directory: " + temporaryDirectory, e);
        }
        return new TopiaMigrationServiceConfiguration(applicationContext, askUserToMigrate, classifier, temporaryDirectory);
    }

    protected TopiaMigrationServiceConfiguration(TopiaApplicationContext<?> applicationContext, TopiaMigrationServiceAskUserToMigrate callback, String classifier, Path temporaryDirectory) {
        this.applicationContext = applicationContext;
        this.callback = callback;
        this.modelVersion = Version.valueOf(applicationContext.getModelVersion());
        this.classifier = classifier;
        this.temporaryDirectory = temporaryDirectory;
    }

    public TopiaMigrationServiceAskUserToMigrate getCallback() {
        return callback;
    }

    public TopiaApplicationContext<?> getApplicationContext() {
        return applicationContext;
    }

    public Version getModelVersion() {
        return modelVersion;
    }

    public String getClassifier() {
        return classifier;
    }

    public Path getTemporaryDirectory() {
        return temporaryDirectory;
    }
}
