package org.nuiton.topia.service.migration.version;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Version;

import java.io.Serializable;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class TMSVersion implements Serializable {

    private static final long serialVersionUID = 1L;

    private String version;

    public TMSVersion() {
    }

    public TMSVersion(String version) {
        if (version == null || version.isEmpty()) {
            throw new IllegalArgumentException("version parameter can not be null nor empty.");
        }
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Version toVersion() {
        return version == null || version.isEmpty() ? null : Version.valueOf(version);
    }
}
