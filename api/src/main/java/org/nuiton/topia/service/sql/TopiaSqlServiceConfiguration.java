package org.nuiton.topia.service.sql;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Version;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.internal.AbstractTopiaApplicationContext;
import org.nuiton.topia.persistence.support.TopiaSqlDllSupport;
import org.nuiton.topia.service.sql.blob.TopiaEntitySqlBlobModel;
import org.nuiton.topia.service.sql.metadata.TopiaMetadataModel;
import org.nuiton.topia.service.sql.metadata.TopiaMetadataModelPaths;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlModel;
import org.nuiton.topia.service.sql.plan.copy.TopiaEntitySqlCopyPlanModel;
import org.nuiton.topia.service.sql.plan.delete.TopiaEntitySqlDeletePlanModel;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePlanModel;
import org.nuiton.topia.service.sql.request.ReplicatePartialRequestCallback;
import org.nuiton.topia.service.sql.request.ReplicateRequestCallback;
import org.nuiton.topia.service.sql.usage.TopiaEntitySqlUsageModel;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 24/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.67
 */
public class TopiaSqlServiceConfiguration implements TopiaEntitySqlModelResource {
    private static final Logger log = LogManager.getLogger(TopiaSqlServiceConfiguration.class);

    /**
     * Topia application context
     */
    protected final AbstractTopiaApplicationContext<?> applicationContext;
    /**
     * Persistence model version.
     */
    protected final Version modelVersion;
    /**
     * Service classifier.
     */
    protected final String classifier;
    /**
     * Temporary directory used to create migration scripts.
     */
    protected final Path temporaryDirectory;
    /**
     * Sql model support to get sql model.
     */
    private final TopiaEntitySqlModelResource sqlModelSupplier;

    public static TopiaSqlServiceConfiguration of(AbstractTopiaApplicationContext<?> applicationContext, TopiaEntitySqlModelResource modelSupplier, Map<String, String> serviceConfiguration) {
        String classifier = TopiaSqlDllSupport.getClassifier(applicationContext);
        if (classifier != null) {
            log.debug("Use Classifier          - " + classifier);
        }
        String path = serviceConfiguration.get("java.io.tmpdir");
        if (path == null) {
            path = System.getProperty("java.io.tmpdir");
        }
        Path temporaryDirectory = Paths.get(path);
        try {
            if (Files.notExists(temporaryDirectory)) {
                Files.createDirectories(temporaryDirectory);
            }
        } catch (IOException e) {
            throw new TopiaException("Could not create temporary directory: " + temporaryDirectory, e);
        }
        return new TopiaSqlServiceConfiguration(applicationContext, modelSupplier, classifier, temporaryDirectory);
    }

    protected TopiaSqlServiceConfiguration(AbstractTopiaApplicationContext<?> applicationContext, TopiaEntitySqlModelResource sqlModelLocation, String classifier, Path temporaryDirectory) {
        this.applicationContext = Objects.requireNonNull(applicationContext);
        this.modelVersion = Version.valueOf(applicationContext.getModelVersion());
        this.sqlModelSupplier = Objects.requireNonNull(sqlModelLocation);
        this.classifier = classifier;
        this.temporaryDirectory = Objects.requireNonNull(temporaryDirectory);
    }

    @Override
    public TopiaEntitySqlModel getModel() {
        return sqlModelSupplier.getModel();
    }

    @Override
    public TopiaEntitySqlCopyPlanModel getCopyPlanModel() {
        return sqlModelSupplier.getCopyPlanModel();
    }

    @Override
    public TopiaEntitySqlReplicatePlanModel getReplicatePlanModel() {
        return sqlModelSupplier.getReplicatePlanModel();
    }

    @Override
    public TopiaEntitySqlDeletePlanModel getDeletePlanModel() {
        return sqlModelSupplier.getDeletePlanModel();
    }

    @Override
    public TopiaEntitySqlUsageModel getUsageModel() {
        return sqlModelSupplier.getUsageModel();
    }

    @Override
    public TopiaEntitySqlBlobModel getBlobModel() {
        return sqlModelSupplier.getBlobModel();
    }

    @Override
    public TopiaMetadataModel getMetaModel() {
        return sqlModelSupplier.getMetaModel();
    }

    @Override
    public TopiaMetadataModelPaths getMetaModelPaths() {
        return sqlModelSupplier.getMetaModelPaths();
    }

    @Override
    public Set<ReplicatePartialRequestCallback> getReplicatePartialRequestCallbacks() {
        return sqlModelSupplier.getReplicatePartialRequestCallbacks();
    }

    @Override
    public Set<ReplicateRequestCallback> getReplicateRequestCallbacks() {
        return sqlModelSupplier.getReplicateRequestCallbacks();
    }

    public AbstractTopiaApplicationContext<?> getApplicationContext() {
        return applicationContext;
    }

    public Version getModelVersion() {
        return modelVersion;
    }

    public String getClassifier() {
        return classifier;
    }

    public Path getTemporaryDirectory() {
        return temporaryDirectory;
    }

    public Path newTemporaryDirectory(TopiaSqlServiceRequest request) {
        return temporaryDirectory.resolve(String.format("%s-%d.sql", request.getClass().getName(), System.nanoTime()));
    }

    public TopiaEntitySqlModelResource getSqlModelSupplier() {
        return sqlModelSupplier;
    }
}
