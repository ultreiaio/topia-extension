package org.nuiton.topia.service.sql.internal.consumer;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaIdFactoryForBulkSupport;
import io.ultreia.java4all.util.sql.SqlScriptWriter;
import org.nuiton.topia.service.sql.internal.SqlRequestSetConsumerContext;

import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created on 06/04/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.74
 */
class ReplicateEntityWorkContext {
    private final Map<String, String> replaceIds;
    private final String oldParentId;
    private final String newParentId;
    private final SqlRequestSetConsumerContext context;
    private final SqlScriptWriter writer;
    private final Set<String> shell;
    private final TopiaIdFactoryForBulkSupport idFactory;

    ReplicateEntityWorkContext(String oldParentId, String newParentId, SqlRequestSetConsumerContext context, Set<String> shell) {
        this.oldParentId = oldParentId;
        this.newParentId = newParentId;
        this.context = context;
        this.writer = context.getWriter();
        this.shell = shell;
        this.replaceIds = new TreeMap<>();
        this.idFactory = context.getSourceTopiaApplicationContext().newIdFactoryForBulk(System.nanoTime());
    }

    public Set<String> getShell() {
        return shell;
    }

    public String addIdToReplace(String id) {
        String prefix = Objects.requireNonNull(id).substring(0, id.indexOf("#"));
        String newId = idFactory.newTopiaId(prefix);
        replaceIds.put(id, newId);
        return newId;
    }

    public String getNewParentId() {
        return newParentId;
    }

    public String getOldParentId() {
        return oldParentId;
    }

    public String getIdToReplace(String id) {
        return replaceIds.get(id);
    }

    public SqlRequestSetConsumerContext context() {
        return context;
    }

    public void writeSql(String sql) {
        writer.writeSql(sql);
    }
}
