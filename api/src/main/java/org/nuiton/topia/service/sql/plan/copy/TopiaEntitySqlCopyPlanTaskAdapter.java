package org.nuiton.topia.service.sql.plan.copy;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import io.ultreia.java4all.util.json.JsonHelper;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Created on 04/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.68
 */
public class TopiaEntitySqlCopyPlanTaskAdapter implements JsonDeserializer<TopiaEntitySqlCopyPlanTask>, JsonSerializer<TopiaEntitySqlCopyPlanTask> {
    public static final String FROM = "from";
    public static final String USE_BLOB = "useBlob";
    public static final String RECURSIVE_COLUMN_NAME = "recursiveColumnName";
    public static final String COLUMN_NAMES = "columnNames";
    public static final String EXTRA = "extra-";
    public static final String GAV = "gav";

    @Override
    public TopiaEntitySqlCopyPlanTask deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = json.getAsJsonObject();
        String[] code = object.getAsJsonPrimitive(GAV).getAsString().split("\\s*\\.\\s*");
        String schemaName = code[0];
        String tableName = code[1];
        String from = JsonHelper.readString(FROM, object);
        boolean useBlob = JsonHelper.readBoolean(USE_BLOB, object);
        String recursiveColumn = JsonHelper.readStringOrNull(RECURSIVE_COLUMN_NAME, object);
        List<String> columnNames = List.of(JsonHelper.readString(COLUMN_NAMES, object).split("\\s*,\\s*"));
        Set<String> extraNames = object.keySet().stream().filter(e -> e.startsWith(EXTRA)).collect(Collectors.toSet());
        Map<String, Map<String, String>> extra = null;
        if (!extraNames.isEmpty()) {
            extra = new TreeMap<>();
            for (String extraName : extraNames) {
                String name = extraName.substring(EXTRA.length());
                JsonElement extraElement = object.get(extraName);
                Map<String, String> currentExtra = context.deserialize(extraElement, Map.class);
                extra.put(name, currentExtra);
            }
        }
        TopiaEntitySqlCopyPlanTask task = new TopiaEntitySqlCopyPlanTask(schemaName, tableName, from, columnNames, useBlob, recursiveColumn);
        if (extra != null) {
            task.setExtra(extra);
        }
        return task;
    }

    @Override
    public JsonElement serialize(TopiaEntitySqlCopyPlanTask src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject result = new JsonObject();
        JsonHelper.addToResult(context, result, GAV, src.getSchemaName() + "." + src.getTableName());
        JsonHelper.addToResult(context, result, FROM, src.getFrom());
        JsonHelper.addToResult(context, result, COLUMN_NAMES, String.join(",", src.getColumnNames()));
        JsonHelper.addToResult(context, result, USE_BLOB, src.useBlob());
        JsonHelper.addToResult(context, result, RECURSIVE_COLUMN_NAME, src.getRecursiveColumnName().orElse(null));
        if (src.getExtra() != null) {
            for (Map.Entry<String, Map<String, String>> entry : src.getExtra().entrySet()) {

                JsonHelper.addToResult(context, result, EXTRA + entry.getKey(), entry.getValue());
            }

        }
        return result;
    }
}

