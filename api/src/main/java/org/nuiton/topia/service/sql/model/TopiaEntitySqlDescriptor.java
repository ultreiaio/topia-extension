package org.nuiton.topia.service.sql.model;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Describes sql tables associated with an entity.
 * <p>
 * Created on 22/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.27
 */
public class TopiaEntitySqlDescriptor {
    /**
     * Main table of the entity.
     */
    private final TopiaEntitySqlTable table;
    /**
     * Association tables of the entity.
     */
    private final List<TopiaEntitySqlAssociationTable> associations;
    /**
     * Simple association tables of the entity.
     */
    private final List<TopiaEntitySqlSimpleAssociationTable> simpleAssociations;
    private final List<TopiaEntitySqlReverseAssociationTable> reverseAssociationTables;
    private final List<TopiaEntitySqlReverseCompositionTable> reverseCompositionTables;
    private final List<String> replicationOrder;
    /**
     * Is entity is entry point or standalone?
     */
    private final boolean noPath;

    public TopiaEntitySqlDescriptor(TopiaEntitySqlTable table,
                                    List<TopiaEntitySqlAssociationTable> associations,
                                    List<TopiaEntitySqlSimpleAssociationTable> simpleAssociations,
                                    List<TopiaEntitySqlReverseAssociationTable> reverseAssociationTables,
                                    List<TopiaEntitySqlReverseCompositionTable> reverseCompositionTables,
                                    List<String> replicationOrder,
                                    boolean noPath) {
        this.table = Objects.requireNonNull(table);
        this.associations = Objects.requireNonNull(associations);
        this.simpleAssociations = simpleAssociations;
        this.reverseAssociationTables = reverseAssociationTables;
        this.reverseCompositionTables = reverseCompositionTables;
        this.replicationOrder = replicationOrder;
        this.noPath = noPath;
    }

    public List<String> getReplicationOrder() {
        return replicationOrder;
    }

    public boolean isNoPath() {
        return noPath;
    }

    public TopiaEntitySqlTable getTable() {
        return table;
    }

    public List<TopiaEntitySqlAssociationTable> getAssociations() {
        return associations;
    }

    public List<TopiaEntitySqlSimpleAssociationTable> getSimpleAssociations() {
        return simpleAssociations;
    }

    public Optional<List<TopiaEntitySqlReverseAssociationTable>> getReverseAssociationTables() {
        return Optional.ofNullable(reverseAssociationTables);
    }

    public Optional<List<TopiaEntitySqlReverseCompositionTable>> getReverseCompositionTables() {
        return Optional.ofNullable(reverseCompositionTables);
    }

    public List<String> getSchemaAndTableNames() {
        List<String> result = new LinkedList<>();
        result.add(getTable().getSchemaAndTableName());
        getAssociations().forEach(t -> result.add(t.getSchemaAndTableName()));
        getSimpleAssociations().forEach(t -> result.add(t.getSchemaAndTableName()));
        return result;
    }
}
