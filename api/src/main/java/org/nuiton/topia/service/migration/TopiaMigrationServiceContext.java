package org.nuiton.topia.service.migration;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Version;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceProvider;
import org.nuiton.topia.service.migration.version.TMSVersion;
import org.nuiton.topia.service.migration.version.TMSVersionHibernateDao;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

/**
 * Contains all states of the migration service.
 * <p>
 * Created by tchemit on 05/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@SuppressWarnings("WeakerAccess")
public class TopiaMigrationServiceContext {

    private static final Logger log = LogManager.getLogger(TopiaMigrationServiceContext.class);

    /**
     * Service configuration.
     */
    protected final TopiaMigrationServiceConfiguration configuration;
    /**
     * Dao.
     */
    protected final TMSVersionHibernateDao dao;
    /**
     * Available migration resources found in class-path.
     */
    protected final MigrationVersionResourceProvider resources;
    /**
     * Is database is versioned?
     */
    protected final boolean dbNotVersioned;
    /**
     * Path where to store sql scripts.
     */
    protected final Path scriptPath;
    /**
     * Is TMSVersion table exists?
     */
    protected boolean versionTableExist;
    /**
     * Current database version.
     */
    protected Version dbVersion;

    public static TopiaMigrationServiceContext of(TopiaMigrationServiceConfiguration configuration) {

        TopiaApplicationContext<?> topiaApplicationContext = configuration.getApplicationContext();

        boolean versionTableExist;
        boolean dbNotVersioned = false;
        Version dbVersion;
        Version v = null;

        TMSVersionHibernateDao dao = new TMSVersionHibernateDao(topiaApplicationContext);

        try {

            versionTableExist = dao.isTableExists();
            if (versionTableExist) {

                Optional<TMSVersion> tmsVersion = dao.getVersion();

                if (tmsVersion.isPresent()) {
                    v = tmsVersion.get().toVersion();
                }

                if (v == null) {
                    log.warn(String.format("Version not found on table %s", TMSVersionHibernateDao.TABLE_NAME));
                }
            } else if (topiaApplicationContext.getConfiguration().isInitSchema()) {
                dao.createSchemaIfNotExist();
            }

        } finally {

            if (v == null) {
                // la base dans ce cas n'est pas versionee.
                // On dit que la version de la base est 0
                // et les schema de cette version 0 doivent
                // etre detenu en local
                v = Version.VZERO;
                dbNotVersioned = true;
                log.info("Database version not found, so database schema is considered as V0");
            } else {
                log.info(String.format("Detected database version: %s", v));
            }
            dbVersion = v;
        }

        return new TopiaMigrationServiceContext(configuration, versionTableExist, dbNotVersioned, dbVersion, MigrationVersionResourceProvider.get(), dao);
    }

    protected TopiaMigrationServiceContext(TopiaMigrationServiceConfiguration configuration, boolean versionTableExist, boolean dbNotVersioned, Version dbVersion, MigrationVersionResourceProvider resources, TMSVersionHibernateDao dao) {
        this.configuration = configuration;
        this.versionTableExist = versionTableExist;
        this.dbNotVersioned = dbNotVersioned;
        this.dbVersion = dbVersion;
        this.resources = resources;
        this.dao = dao;
        try {
            this.scriptPath = Files.createTempDirectory(configuration.getTemporaryDirectory(), "topia-migration-service");
        } catch (IOException e) {
            throw new IllegalStateException("Can't create scripts path", e);
        }
    }

    public Path getScriptPath() {
        return scriptPath;
    }

    public boolean isVersionTableExist() {
        return versionTableExist;
    }

    public boolean isDbNotVersioned() {
        return dbNotVersioned;
    }

    public Version getModelVersion() {
        return configuration.getModelVersion();
    }

    public Version getDbVersion() {
        return dbVersion;
    }

    public MigrationVersionResourceProvider getResources() {
        return resources;
    }

    public void createSchemaIfNotExist() {
        dao.createSchemaIfNotExist();
        versionTableExist = true;
    }

    public void saveModelVersion() {
        saveVersion(getModelVersion());
    }

    public Optional<TopiaMigrationServiceAskUserToMigrate> getAskUserToMigrate() {
        return Optional.ofNullable(configuration.getCallback());
    }

    protected void saveVersion(Version version) {
        log.info(String.format("[ Version %s ] Saving new database version.", version));
        dao.save(version.getVersion());
        dbVersion = version;
    }

    public SessionFactory newSessionFactory() {
        return dao.newSessionFactory();
    }

    public MigrationVersionResource getResource(Version version) {
        return resources.getResource(version);
    }

    public TopiaMigrationServiceExecutor newExecutor(MigrationVersionResource migrationVersionResource, TopiaSqlSupport sqlSupport) {
        return new TopiaMigrationServiceExecutor(migrationVersionResource, sqlSupport, configuration.getClassifier(), getScriptPath(), migrationVersionResource.getScriptVariables());
    }

}
