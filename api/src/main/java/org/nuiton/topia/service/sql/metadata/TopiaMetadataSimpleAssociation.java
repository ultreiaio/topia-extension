package org.nuiton.topia.service.sql.metadata;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;

/**
 * Created on 31/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.50
 */
public class TopiaMetadataSimpleAssociation {

    private final TopiaMetadataEntity owner;
    private final String tableName;
    private final String propertyName;
    private final String targetPropertyName;
    private final String type;

    public TopiaMetadataSimpleAssociation(TopiaMetadataEntity owner, String tableName, String propertyName, String targetPropertyName, String type) {
        this.owner = owner;
        this.tableName = tableName;
        this.propertyName = propertyName;
        this.targetPropertyName = targetPropertyName;
        this.type = type;
    }

    public String getTargetPropertyName() {
        return targetPropertyName;
    }

    public TopiaMetadataEntity getOwner() {
        return owner;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public String getType() {
        return type;
    }

    public String getTableName() {
        return tableName;
    }

    public String getSourceDbName() {
        return getOwner().getDbColumnName(getTableName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TopiaMetadataSimpleAssociation)) return false;
        TopiaMetadataSimpleAssociation that = (TopiaMetadataSimpleAssociation) o;
        return owner.equals(that.owner) && propertyName.equals(that.propertyName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, propertyName);
    }
}
