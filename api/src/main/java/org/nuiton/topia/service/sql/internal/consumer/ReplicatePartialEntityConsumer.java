package org.nuiton.topia.service.sql.internal.consumer;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.service.sql.internal.SqlRequestConsumer;
import org.nuiton.topia.service.sql.internal.SqlRequestSetConsumerContext;
import org.nuiton.topia.service.sql.internal.request.ReplicatePartialEntityRequest;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSelectArgument;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePartialPlan;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePartialPlanTask;
import org.nuiton.topia.service.sql.request.ReplicatePartialRequestCallback;

import java.util.Set;

/**
 * Created on 05/04/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.74
 */
public class ReplicatePartialEntityConsumer implements SqlRequestConsumer<ReplicatePartialEntityRequest> {

    @Override
    public void consume(ReplicatePartialEntityRequest request, SqlRequestSetConsumerContext context) {
        TopiaEntitySqlSelectArgument selectArgument = request.getSelectArgument();
        // Never optimize queries here, since removing WHERE clause breaks some logic for multiple entity paths
        String ids = context.ids(selectArgument, false);
        TopiaEntitySqlReplicatePartialPlan plan = request.getPlan();
        Set<String> shell = plan.getShell();
        ReplicateEntityWorkContext internalContext = new ReplicateEntityWorkContext(request.getOldParentId(), request.getNewParentId(), context, shell);
        for (TopiaEntitySqlReplicatePartialPlanTask task : plan) {
            ReplicateEntityWork readSqlWork = new ReplicateEntityWork(internalContext, task, ids);
            context.getSourcePersistenceContext().getSqlSupport().doSqlWork(readSqlWork);
        }
        for (ReplicatePartialRequestCallback callback : request.getCallbacks()) {
            callback.consume(request.getRequest(), context);
        }
    }
}

