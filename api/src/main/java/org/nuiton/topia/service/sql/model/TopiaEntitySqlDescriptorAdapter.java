package org.nuiton.topia.service.sql.model;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import io.ultreia.java4all.util.json.JsonHelper;

import java.lang.reflect.Type;
import java.util.List;

import static io.ultreia.java4all.util.json.JsonHelper.addToResult;
import static io.ultreia.java4all.util.json.JsonHelper.readBoolean;

/**
 * Created on 01/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.67
 */
public class TopiaEntitySqlDescriptorAdapter implements JsonDeserializer<TopiaEntitySqlDescriptor>, JsonSerializer<TopiaEntitySqlDescriptor> {

    public static final String TABLE = "table";
    public static final String ASSOCIATIONS = "associations";
    public static final String SIMPLE_ASSOCIATIONS = "simpleAssociations";
    public static final String REVERSE_ASSOCIATION_TABLES = "reverseAssociationTables";
    public static final String REVERSE_COMPOSITION_TABLES = "reverseCompositionTables";
    public static final String REPLICATION_ORDER = "replicationOrder";
    public static final String NO_PATH = "noPath";

    @Override
    public TopiaEntitySqlDescriptor deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = json.getAsJsonObject();
        TopiaEntitySqlTable table = JsonHelper.readObject(context, TABLE, TopiaEntitySqlTable.class, object);
        List<TopiaEntitySqlAssociationTable> associations = JsonHelper.readObjectListOrEmpty(context, ASSOCIATIONS, TopiaEntitySqlAssociationTable.class, object);
        List<TopiaEntitySqlSimpleAssociationTable> simpleAssociations = JsonHelper.readObjectListOrEmpty(context, SIMPLE_ASSOCIATIONS, TopiaEntitySqlSimpleAssociationTable.class, object);
        List<TopiaEntitySqlReverseAssociationTable> reverseAssociationTables = JsonHelper.readObjectListOrEmpty(context, REVERSE_ASSOCIATION_TABLES, TopiaEntitySqlReverseAssociationTable.class, object);
        List<TopiaEntitySqlReverseCompositionTable> reverseCompositionTables = JsonHelper.readObjectListOrEmpty(context, REVERSE_COMPOSITION_TABLES, TopiaEntitySqlReverseCompositionTable.class, object);
        List<String> replicationOrder = JsonHelper.readStringList(context, REPLICATION_ORDER, object);
        boolean noPath = readBoolean(NO_PATH, object);
        return new TopiaEntitySqlDescriptor(table,
                                            associations,
                                            simpleAssociations,
                                            reverseAssociationTables,
                                            reverseCompositionTables,
                                            replicationOrder,
                                            noPath);
    }

    @Override
    public JsonElement serialize(TopiaEntitySqlDescriptor src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject result = new JsonObject();
        addToResult(context, result, NO_PATH, src.isNoPath());
        addToResult(context, result, TABLE, src.getTable());
        addToResult(context, result, ASSOCIATIONS, src.getAssociations());
        addToResult(context, result, SIMPLE_ASSOCIATIONS, src.getSimpleAssociations());
        addToResult(context, result, REPLICATION_ORDER, src.getReplicationOrder());
        src.getReverseAssociationTables().ifPresent(l -> addToResult(context, result, REVERSE_ASSOCIATION_TABLES, l));
        src.getReverseCompositionTables().ifPresent(l -> addToResult(context, result, REVERSE_COMPOSITION_TABLES, l));
        return result;
    }

}
