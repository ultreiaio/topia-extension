package org.nuiton.topia.service.sql.model;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.service.sql.metadata.TopiaMetadataComposition;
import org.nuiton.topia.service.sql.metadata.TopiaMetadataReverseAssociation;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created on 28/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.67
 */
public class TopiaEntitySqlReverseAssociationTable extends AbstractTopiaEntitySqlAssociationTable {

    public TopiaEntitySqlReverseAssociationTable(String entityName,
                                                 String schemaName,
                                                 String tableName,
                                                 Set<String> authorizedColumnNames,
                                                 List<TopiaEntitySqlSelector> selectors,
                                                 String joinColumnName) {
        super(entityName,
              schemaName,
              tableName,
              authorizedColumnNames,
              selectors,
              joinColumnName);
    }

    public TopiaEntitySqlReverseAssociationTable(TopiaMetadataComposition link, List<TopiaEntitySqlSelector> selectors) {
        this(link.getOwner().getFullyQualifiedName(),
             link.getOwner().getDbSchemaName(),
             link.getOwner().getDbTableName(),
             new LinkedHashSet<>(List.of(TopiaEntity.PROPERTY_TOPIA_ID, link.getTargetDbName())),
                                 selectors,
                                 link.getTargetDbName());
    }

    public TopiaEntitySqlReverseAssociationTable(TopiaMetadataReverseAssociation link, List<TopiaEntitySqlSelector> selectors) {
        this(link.getOwner().getFullyQualifiedName(),
             link.getOwner().getDbSchemaName(),
             link.getOwner().getDbTableName(),
             new LinkedHashSet<>(List.of(TopiaEntity.PROPERTY_TOPIA_ID, link.getTargetDbName())),
             selectors,
             link.getTargetDbName());
    }
}
