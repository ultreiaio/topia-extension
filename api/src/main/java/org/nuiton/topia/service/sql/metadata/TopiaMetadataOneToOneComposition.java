package org.nuiton.topia.service.sql.metadata;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created by tchemit on 20/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class TopiaMetadataOneToOneComposition extends TopiaMetadataLink {

    public TopiaMetadataOneToOneComposition(TopiaMetadataEntity owner, String targetPropertyName, TopiaMetadataEntity target) {
        super(owner, targetPropertyName, target);
    }

    public String getSourceDbName() {
        return getOwner().getDbColumnName(getTableName());
    }

    @Override
    public String getTargetDbName() {
        return getTarget().getDbColumnName(getTargetPropertyName());
    }

    @Override
    public String getTableName() {
        return getOwner().getDbTableName();
    }
}
