package org.nuiton.topia.service.sql.internal.consumer;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.sql.SqlScriptWriter;
import org.nuiton.topia.service.sql.internal.SqlRequestConsumer;
import org.nuiton.topia.service.sql.internal.SqlRequestSetConsumerContext;
import org.nuiton.topia.service.sql.internal.request.DeletePartialEntityRequest;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSelectArgument;
import org.nuiton.topia.service.sql.plan.delete.TopiaEntitySqlDeletePlan;
import org.nuiton.topia.service.sql.plan.delete.TopiaEntitySqlDeletePlanTask;

import java.util.Map;
import java.util.Set;

/**
 * Created on 25/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.27
 */
public class DeletePartialEntityConsumer implements SqlRequestConsumer<DeletePartialEntityRequest> {
    public static final String DELETE_REVERSE_ASSOCIATION_STATEMENT = "UPDATE %1$s _main SET _main.%2$s = NULL WHERE _main.%2$s %3$s;";

    //fr.ird.data.ps.logbook.Route#1616767950667#0.2733836567509864 30/03/2019
    //fr.ird.data.ps.observation.Activity#1554021069610#0.2462353141793846
    //fr.ird.data.ps.logbook.Activity#1616767975989#0.4873500558653976

    @Override
    public void consume(DeletePartialEntityRequest request, SqlRequestSetConsumerContext context) {
        TopiaEntitySqlDeletePlan deletePlan = request.getDeletePlan();
        TopiaEntitySqlSelectArgument selectArgument = request.getSelectArgument();
        Set<String> shell = request.getShell();
        Map<String, Map<String, Set<String>>> detachTasksMapping = request.getDetachTasksMapping();
        // can optimize query only for entry point
        String ids = context.ids(selectArgument, shell == null);
        SqlScriptWriter writer = context.getWriter();
        for (TopiaEntitySqlDeletePlanTask task : deletePlan) {
            if (task.accept(shell)) {
                consume(context, writer, ids, task, detachTasksMapping == null ? null : detachTasksMapping.get(task.getGav()));
            }
        }
    }

    protected void consume(SqlRequestSetConsumerContext context, SqlScriptWriter writer, String ids, TopiaEntitySqlDeletePlanTask task, Map<String, Set<String>> detachTasks) {
        String deleteSql = task.getDeleteSql();
        String realDeleteSql = task.applyIds(deleteSql, ids);
        if (detachTasks != null) {
            for (Map.Entry<String, Set<String>> toDetach : detachTasks.entrySet()) {
                String table = toDetach.getKey();
                for (String column : toDetach.getValue()) {
                    String partialSql = realDeleteSql.substring(deleteSql.indexOf(" IN ("), realDeleteSql.length() - 1);
                    String detachSql = String.format(DELETE_REVERSE_ASSOCIATION_STATEMENT, table, column, partialSql);
                    writer.writeSql(detachSql);
                }
            }
        }
        writer.writeSql(realDeleteSql);
    }
}
