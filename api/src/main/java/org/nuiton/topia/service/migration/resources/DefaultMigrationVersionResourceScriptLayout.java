package org.nuiton.topia.service.migration.resources;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Version;

/**
 * Created on 01/02/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public class DefaultMigrationVersionResourceScriptLayout implements MigrationVersionResourceScriptLayout {

    @Override
    public String getScriptPath(Version version, String rank, String prefix, String classifier) {
        String migrationScript = String.format("%s-%s.sql", prefix, classifier);
        return String.format("/db/migration/%s/%s_%s", version, rank, migrationScript);
    }
}
