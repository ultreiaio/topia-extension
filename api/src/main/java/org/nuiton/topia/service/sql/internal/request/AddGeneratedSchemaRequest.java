package org.nuiton.topia.service.sql.internal.request;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Version;
import org.hibernate.dialect.Dialect;

import java.util.Objects;

/**
 * Created on 16/06/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.4
 */
public class AddGeneratedSchemaRequest extends SqlSchemaRequest {
    private final Version dbVersion;

    public AddGeneratedSchemaRequest(Class<? extends Dialect> dialect, boolean processSchema, boolean processTables, Version dbVersion) {
        super(dialect, processSchema, processTables);
        this.dbVersion = Objects.requireNonNull(dbVersion);
    }

    public Version getDbVersion() {
        return dbVersion;
    }
}
