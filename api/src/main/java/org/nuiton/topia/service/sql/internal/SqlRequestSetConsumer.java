package org.nuiton.topia.service.sql.internal;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.internal.AbstractTopiaApplicationContext;
import io.ultreia.java4all.util.sql.SqlScript;
import io.ultreia.java4all.util.sql.BlobsContainer;
import org.nuiton.topia.service.sql.internal.consumer.AddGeneratedSchemaConsumer;
import org.nuiton.topia.service.sql.internal.consumer.AddVersionTableConsumer;
import org.nuiton.topia.service.sql.internal.consumer.CopyEntityConsumer;
import org.nuiton.topia.service.sql.internal.consumer.CreateSchemaConsumer;
import org.nuiton.topia.service.sql.internal.consumer.DeleteEntityConsumer;
import org.nuiton.topia.service.sql.internal.consumer.DeletePartialEntityConsumer;
import org.nuiton.topia.service.sql.internal.consumer.DropSchemaConsumer;
import org.nuiton.topia.service.sql.internal.consumer.ReplicateEntityConsumer;
import org.nuiton.topia.service.sql.internal.consumer.ReplicatePartialEntityConsumer;
import org.nuiton.topia.service.sql.internal.request.AddGeneratedSchemaRequest;
import org.nuiton.topia.service.sql.internal.request.AddVersionTableRequest;
import org.nuiton.topia.service.sql.internal.request.CopyEntityRequest;
import org.nuiton.topia.service.sql.internal.request.CreateSchemaRequest;
import org.nuiton.topia.service.sql.internal.request.DeleteEntityRequest;
import org.nuiton.topia.service.sql.internal.request.DeletePartialEntityRequest;
import org.nuiton.topia.service.sql.internal.request.DropSchemaRequest;
import org.nuiton.topia.service.sql.internal.request.ReplicateEntityRequest;
import org.nuiton.topia.service.sql.internal.request.ReplicatePartialEntityRequest;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;

/**
 * Created on 25/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.28
 */
public class SqlRequestSetConsumer {
    /**
     * Consumers factory indexed by request type.
     */
    private final static Map<Class<? extends SqlRequest>, Supplier<? extends SqlRequestConsumer<?>>> CONSUMERS = Map.of(
            CreateSchemaRequest.class, CreateSchemaConsumer::new,
            AddGeneratedSchemaRequest.class, AddGeneratedSchemaConsumer::new,
            DropSchemaRequest.class, DropSchemaConsumer::new,
            AddVersionTableRequest.class, AddVersionTableConsumer::new,
            DeleteEntityRequest.class, DeleteEntityConsumer::new,
            DeletePartialEntityRequest.class, DeletePartialEntityConsumer::new,
            ReplicateEntityRequest.class, ReplicateEntityConsumer::new,
            ReplicatePartialEntityRequest.class, ReplicatePartialEntityConsumer::new,
            CopyEntityRequest.class, CopyEntityConsumer::new);

    public static SqlScript consume(AbstractTopiaApplicationContext<?> applicationContext, SqlRequestSet request) {
        Map<String, BlobsContainer> blobsContainersBuilder = new LinkedHashMap<>();
        try (SqlRequestSetConsumerContext context = new SqlRequestSetConsumerContext(applicationContext, request)) {
            for (SqlRequest sqlRequest : request.getRequests()) {
                getConsumer(sqlRequest).consume(sqlRequest, context);
            }
            context.flushBlobs(blobsContainersBuilder);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new TopiaException("Can't execute requests", e);
        }
        Set<BlobsContainer> blobsContainers = Set.copyOf(blobsContainersBuilder.values());
        return SqlScript.of(request.getPath()).addBlobsContainers(blobsContainers);
    }

    protected static <S extends SqlRequest> SqlRequestConsumer<S> getConsumer(S sqlRequest) {
        @SuppressWarnings("unchecked") Supplier<? extends SqlRequestConsumer<S>> supplier = (Supplier<? extends SqlRequestConsumer<S>>) CONSUMERS.get(sqlRequest.getClass());
        return Objects.requireNonNull(supplier, "Can't find consumer for sql request: " + sqlRequest).get();

    }
}
