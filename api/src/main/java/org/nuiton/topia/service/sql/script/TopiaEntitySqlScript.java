package org.nuiton.topia.service.sql.script;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created on 06/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.69
 */
public class TopiaEntitySqlScript {

    //FIXME Add also parameters descriptions (how many and their type) to produce a safe script
    private final List<String> requests;

    public TopiaEntitySqlScript(List<String> requests) {
        this.requests = Objects.requireNonNull(requests);
    }

    public List<String> generate(Function<String, String> lineTransformer) {
        Stream<String> requests = stream();
        return requests.map(lineTransformer).collect(Collectors.toList());
    }

    public Stream<String> stream() {
        return requests.stream();
    }

    public List<String> requests() {
        return requests;
    }
}
