package org.nuiton.topia.service.sql.internal.request;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.service.sql.internal.SqlRequest;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSelectArgument;
import org.nuiton.topia.service.sql.plan.delete.TopiaEntitySqlDeletePlan;

import java.util.Map;
import java.util.Set;

/**
 * Created on 25/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.27
 */
public class DeletePartialEntityRequest implements SqlRequest {

    private final TopiaEntitySqlDeletePlan deletePlan;
    private final TopiaEntitySqlSelectArgument selectArgument;
    private final Set<String> shell;
    private final Map<String, Map<String, Set<String>>> detachTasksMapping;

    public DeletePartialEntityRequest(TopiaEntitySqlDeletePlan deletePlan, TopiaEntitySqlSelectArgument selectArgument, Set<String> shell, Map<String, Map<String, Set<String>>> detachTasksMapping) {
        this.deletePlan = deletePlan;
        this.selectArgument = selectArgument;
        this.shell = shell;
        this.detachTasksMapping = detachTasksMapping;
    }

    public TopiaEntitySqlSelectArgument getSelectArgument() {
        return selectArgument;
    }

    public TopiaEntitySqlDeletePlan getDeletePlan() {
        return deletePlan;
    }

    public Set<String> getShell() {
        return shell;
    }

    public Map<String, Map<String, Set<String>>> getDetachTasksMapping() {
        return detachTasksMapping;
    }
}
