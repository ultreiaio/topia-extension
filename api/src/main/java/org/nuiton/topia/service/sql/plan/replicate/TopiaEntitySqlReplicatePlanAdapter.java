package org.nuiton.topia.service.sql.plan.replicate;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created on 06/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.69
 */
public class TopiaEntitySqlReplicatePlanAdapter implements JsonDeserializer<TopiaEntitySqlReplicatePlan>, JsonSerializer<TopiaEntitySqlReplicatePlan> {

    @Override
    public TopiaEntitySqlReplicatePlan deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        Set<TopiaEntitySqlReplicatePlanTask> tasks = context.deserialize(json, TypeToken.getParameterized(LinkedHashSet.class, TopiaEntitySqlReplicatePlanTask.class).getType());
        return new TopiaEntitySqlReplicatePlan(tasks);
    }

    @Override
    public JsonElement serialize(TopiaEntitySqlReplicatePlan src, Type typeOfSrc, JsonSerializationContext context) {
        return context.serialize(src.getTasks());
    }
}
