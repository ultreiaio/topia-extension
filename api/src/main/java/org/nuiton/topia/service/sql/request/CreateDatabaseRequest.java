package org.nuiton.topia.service.sql.request;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Version;
import org.nuiton.topia.service.sql.TopiaSqlServiceRequest;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;
import java.util.TreeMap;

/**
 * To create a new database sql script within some data, or not.
 * <p>
 * Created on 23/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.67
 */
public class CreateDatabaseRequest extends TopiaSqlServiceRequest {
    /**
     * Model version.
     */
    private final Version dbVersion;
    /**
     * Add schema.
     */
    private final boolean addSchema;
    /**
     * Add generated schema.
     */
    private final boolean addGeneratedSchema;
    /**
     * To add version table.
     */
    private final boolean addVersionTable;
    /**
     * Optional add standalone tables.
     */
    private final boolean addStandaloneTables;
    /**
     * If data to add.
     */
    private final boolean addData;
    /**
     * Ids of data to copy.
     */
    private final TreeMap<String, Set<String>> dataIds;

    public static class Builder {
        private final boolean postgres;
        private final Version dbVersion;
        private final TreeMap<String, Set<String>> dataIds;
        private boolean addSchema;
        private boolean addGeneratedSchema;
        private boolean addVersionTable;
        private boolean addStandaloneTables;
        private boolean addData;

        public Builder(boolean postgres, Version dbVersion) {
            this.postgres = postgres;
            this.dbVersion = dbVersion;
            this.dataIds = new TreeMap<>();
        }


        public Builder addSchema() {
            addSchema = true;
            return this;
        }

        public Builder addGeneratedSchema() {
            addGeneratedSchema = true;
            return this;
        }

        public Builder addVersionTable() {
            addVersionTable = true;
            return this;
        }

        public Builder addStandaloneTables() {
            addStandaloneTables = true;
            return this;
        }

        public Builder addAllData() {
            addData = true;
            return this;
        }

        public Builder dataIdsToAdd(String dataType, Set<String> dataIds) {
            addData = true;
            this.dataIds.computeIfAbsent(dataType, k -> new LinkedHashSet<>()).addAll(dataIds);
            return this;
        }

        public CreateDatabaseRequest build() {
            return new CreateDatabaseRequest(postgres,
                                             dbVersion,
                                             dataIds,
                                             addSchema,
                                             addGeneratedSchema,
                                             addVersionTable,
                                             addStandaloneTables,
                                             addData);
        }
    }

    public static Builder builder(boolean postgres, Version dbVersion) {
        return new Builder(postgres, dbVersion);
    }

    protected CreateDatabaseRequest(boolean postgres,
                                    Version dbVersion,
                                    TreeMap<String, Set<String>> dataIds,
                                     boolean addSchema,
                                    boolean addGeneratedSchema,
                                    boolean addVersionTable,
                                    boolean addStandaloneTables,
                                    boolean addData) {
        super(postgres);
        this.dbVersion = dbVersion;
        this.dataIds = dataIds;
        this.addSchema = addSchema;
        this.addGeneratedSchema = addGeneratedSchema;
        this.addVersionTable = addVersionTable;
        this.addStandaloneTables = addStandaloneTables;
        this.addData = addData;
    }

    public boolean isAddSchema() {
        return addSchema;
    }

    public boolean isAddGeneratedSchema() {
        return addGeneratedSchema;
    }

    public boolean isAddVersionTable() {
        return addVersionTable;
    }

    public boolean isAddStandaloneTables() {
        return addStandaloneTables;
    }

    public boolean isAddData() {
        return addData;
    }

    public Map<String, Set<String>> getDataIds() {
        return dataIds;
    }

    public Version getDbVersion() {
        return dbVersion;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CreateDatabaseRequest.class.getSimpleName() + "[", "]")
                .add("postgres=" + isPostgres())
                .add("dbVersion=" + getDbVersion())
                .add("addSchema=" + isAddSchema())
                .add("addGeneratedSchema=" + isAddGeneratedSchema())
                .add("addVersionTable=" + isAddVersionTable())
                .add("addStandaloneTables=" + isAddStandaloneTables())
                .add("addData=" + isAddData())
                .add("dataIds=" + getDataIds())
                .toString();
    }

}
