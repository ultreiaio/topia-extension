package org.nuiton.topia.service.sql.metadata;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * Represent a path from a {@link #start} data entity type to another one {@link #end}.
 * <p>
 * {@link #links} describe links to follow to reach end.
 * <p>
 * Created on 20/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class TopiaMetadataEntityPath implements Iterable<TopiaMetadataLink> {

    private final List<TopiaMetadataLink> links;
    private transient TopiaMetadataEntity start;
    private transient TopiaMetadataEntity end;
    private transient List<TopiaMetadataEntity> types;

    public static TopiaMetadataEntityPath of(TopiaMetadataLink link) {
        return new TopiaMetadataEntityPath(List.of(Objects.requireNonNull(link)));
    }

    public TopiaMetadataEntityPath(List<TopiaMetadataLink> links) {
        this.links = Objects.requireNonNull(links);
        if (links.isEmpty()) {
            throw new IllegalStateException("Can't get an empty path");
        }
    }

    public TopiaMetadataEntityPath resolve(TopiaMetadataLink link) {
        List<TopiaMetadataLink> result = new ArrayList<>(links);
        result.add(Objects.requireNonNull(link));
        return new TopiaMetadataEntityPath(Collections.unmodifiableList(result));
    }

    public TopiaMetadataEntityPath parent() {
        int linksSize = getLinksSize();
        if (linksSize == 1) {
            return null;
        }
        return new TopiaMetadataEntityPath(links.subList(0, linksSize - 1));
    }

    public List<TopiaMetadataEntity> getTypes() {
        if (types == null) {
            List<TopiaMetadataEntity> builder = new LinkedList<>();
            Iterator<TopiaMetadataLink> iterator = iterator();
            while (iterator.hasNext()) {
                TopiaMetadataLink link = iterator.next();
                builder.add(link.getOwner());
                if (!iterator.hasNext()) {
                    builder.add(link.getTarget());
                }
            }
            types = Collections.unmodifiableList(builder);
        }
        return types;
    }

    public TopiaMetadataEntity getStart() {
        if (start == null) {
            start = links.get(0).getOwner();
        }
        return start;
    }

    public TopiaMetadataEntity getEnd() {
        if (end == null) {
            end = links.get(links.size() - 1).getTarget();
        }
        return end;
    }

    public List<TopiaMetadataLink> getLinks() {
        return links;
    }

    public List<TopiaMetadataLink> getReverseLinks() {
        List<TopiaMetadataLink> result = new ArrayList<>(links);
        Collections.reverse(result);
        return Collections.unmodifiableList(result);
    }

    @Override
    public Iterator<TopiaMetadataLink> iterator() {
        return links.iterator();
    }

    @Override
    public String toString() {
        return String.format("TopiaMetadataEntityPath{From %s to %s through %s}", getStart().getType(), getEnd().getType(), links.stream().map(TopiaMetadataLink::toString).collect(Collectors.joining(", ")));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TopiaMetadataEntityPath)) return false;
        TopiaMetadataEntityPath that = (TopiaMetadataEntityPath) o;
        return links.equals(that.links);
    }

    @Override
    public int hashCode() {
        return Objects.hash(links);
    }

    public int getLinksSize() {
        return links.size();
    }

    public TopiaMetadataLink getLastLink() {
        return links.get(getLinksSize() - 1);
    }
}
