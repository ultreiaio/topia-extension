package org.nuiton.topia.service.sql.model;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;

/**
 * Created on 22/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.27
 */
public abstract class AbstractTopiaEntitySqlTable {
    /**
     * Fully qualified name of entity owner of this table.
     */
    private final String entityName;
    private final String schemaName;
    private final String tableName;
    private final Set<String> authorizedColumnNames;
    private final List<TopiaEntitySqlSelector> selectors;

    public AbstractTopiaEntitySqlTable(String entityName,
                                       String schemaName,
                                       String tableName,
                                       Set<String> authorizedColumnNames,
                                       List<TopiaEntitySqlSelector> selectors) {
        this.entityName = entityName;
        this.schemaName = Objects.requireNonNull(schemaName);
        this.tableName = Objects.requireNonNull(tableName);
        this.authorizedColumnNames = Objects.requireNonNull(authorizedColumnNames);
        this.selectors = selectors;
    }

    public abstract String getJoinColumnName();

    public String getEntityName() {
        return entityName;
    }

    public String getSchemaAndTableName() {
        return getSchemaName() + "." + getTableName();
    }

    public String getTableName() {
        return tableName;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public Set<String> getAuthorizedColumnNames() {
        return authorizedColumnNames;
    }

    public List<TopiaEntitySqlSelector> getSelectors() {
        return selectors;
    }

    public List<String> generate(String select, TopiaEntitySqlSelectArgument selectArgument) {
        List<String> requestBuilder = new ArrayList<>(selectors.size());
        for (TopiaEntitySqlSelector selector : selectors) {
            requestBuilder.add(selector.generate(select, selectArgument));
        }
        return requestBuilder;
    }

    public String generatePrototype(TopiaEntitySqlSelector selector, String select) {
        return selector.generatePrototype(select);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractTopiaEntitySqlTable)) return false;
        AbstractTopiaEntitySqlTable that = (AbstractTopiaEntitySqlTable) o;
        return schemaName.equals(that.schemaName) && tableName.equals(that.tableName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(schemaName, tableName);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", getClass().getSimpleName() + "[", "]")
                .add("entityName='" + entityName + "'")
                .add("db='" + getSchemaAndTableName() + "'")
                .toString();
    }
}
