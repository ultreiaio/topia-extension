package org.nuiton.topia.service.sql.metadata;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created on 13/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.27
 */
public class TopiaMetadataModelPaths {

    private static final List<TopiaMetadataEntityPath> EMPTY_LIST = List.of();
    private final TreeMap<TopiaMetadataEntity, List<TopiaMetadataEntityPath>> paths;

    public TopiaMetadataModelPaths(TreeMap<TopiaMetadataEntity, List<TopiaMetadataEntityPath>> paths) {
        this.paths = Objects.requireNonNull(paths);

    }

    public boolean containsKey(TopiaMetadataEntity entity) {
        return paths.containsKey(entity);
    }

    public List<TopiaMetadataEntityPath> getEntityPath(TopiaMetadataEntity entity) {
        return paths.getOrDefault(entity, EMPTY_LIST);
    }

    public Optional<TopiaMetadataEntityPath> getEntityPathsForEntryPoint(TopiaMetadataEntity entityType) {
        List<TopiaMetadataEntityPath> candidates = getEntityPath(entityType);
        return candidates.stream().min(Comparator.comparing(p -> p.getLinks().size(), Integer::compareTo));
    }

    public TreeMap<TopiaMetadataEntity, List<TopiaMetadataEntityPath>> asMap() {
        return paths;
    }

    public Set<TopiaMetadataEntity> keySet() {
        return paths.keySet();
    }

    public int size() {
        return paths.size();
    }
}
