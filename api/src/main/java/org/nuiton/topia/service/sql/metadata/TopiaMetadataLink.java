package org.nuiton.topia.service.sql.metadata;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;

/**
 * Represents an entity link to another entity.
 * <p>
 * Created by tchemit on 19/09/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class TopiaMetadataLink {

    private final TopiaMetadataEntity owner;
    private final String targetPropertyName;
    private final TopiaMetadataEntity target;

    TopiaMetadataLink(TopiaMetadataEntity owner, String targetPropertyName, TopiaMetadataEntity target) {
        this.owner = owner;
        this.targetPropertyName = targetPropertyName;
        this.target = target;
    }

    public abstract String getTableName();

    public abstract String getTargetDbName();

    //FIXME replace this by all required properties to remove entity from here
    public TopiaMetadataEntity getOwner() {
        return owner;
    }

    public String getTargetPropertyName() {
        return targetPropertyName;
    }

    //FIXME replace this by all required properties to remove entity from here
    public TopiaMetadataEntity getTarget() {
        return target;
    }

    @Override
    public String toString() {
        return String.format("%s{%s [%s] → %s}", getClass().getSimpleName(), getOwner().getType(), getTargetPropertyName(), getTarget().getType());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TopiaMetadataLink)) return false;
        TopiaMetadataLink that = (TopiaMetadataLink) o;
        return owner.equals(that.owner) &&
                targetPropertyName.equals(that.targetPropertyName) &&
                target.equals(that.target);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, targetPropertyName, target);
    }
}
