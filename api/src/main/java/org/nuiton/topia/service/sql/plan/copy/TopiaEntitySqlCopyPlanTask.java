package org.nuiton.topia.service.sql.plan.copy;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.service.sql.plan.SqlHelper;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created on 04/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.68
 */
public class TopiaEntitySqlCopyPlanTask {
    /**
     * Schema name.
     */
    private final String schemaName;
    /**
     * Table name.
     */
    private final String tableName;
    /**
     * From clause to compute selected sql.
     */
    private final String from;
    /**
     * Column names to copy.
     */
    private final List<String> columnNames;
    /**
     * Optional recursive column name on table.
     */
    private final String recursiveColumnName;
    /**
     * Is using blob?
     */
    private final boolean useBlob;
    /**
     * Select sql code to load a data row to copy.
     */
    private final transient String selectSql;
    /**
     * Insert sql code to load a data row to copy.
     */
    private final transient String insertSql;

    private transient Map<String, Map<String, String>> extra;

    public TopiaEntitySqlCopyPlanTask(String schemaName, String tableName, String from, List<String> columnNames, boolean useBlob, String recursiveColumnName) {
        this.schemaName = schemaName;
        this.tableName = tableName;
        this.from = from;
        this.columnNames = columnNames;
        this.useBlob = useBlob;
        this.recursiveColumnName = recursiveColumnName;
        this.selectSql = SqlHelper.newSelectStatementSql(schemaName, tableName, from);
        this.insertSql = SqlHelper.newInsertStatementSql(schemaName, tableName);
    }

    public String getSchemaName() {
        return schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public String getSchemaAndTableName() {
        return getSchemaName() + "." + getTableName();
    }

    public String getFrom() {
        return from;
    }

    public String getSelectSql() {
        return selectSql;
    }

    public String getInsertSql() {
        return insertSql;
    }

    public List<String> getColumnNames() {
        return columnNames;
    }

    public boolean useBlob() {
        return useBlob;
    }

    public Map<String, Map<String, String>> getExtra() {
        return extra;
    }

    public void setExtra(Map<String, Map<String, String>> extra) {
        this.extra = extra;
    }

    public Optional<String> getRecursiveColumnName() {
        return Optional.ofNullable(recursiveColumnName);
    }
}
