package org.nuiton.topia.service.sql.internal.request;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.service.sql.internal.SqlRequest;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSelectArgument;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePartialPlan;
import org.nuiton.topia.service.sql.request.ReplicatePartialRequest;
import org.nuiton.topia.service.sql.request.ReplicatePartialRequestCallback;

import java.util.Objects;
import java.util.Set;

/**
 * Created on 05/04/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.74
 */
public class ReplicatePartialEntityRequest implements SqlRequest {

    private final TopiaEntitySqlReplicatePartialPlan plan;
    private final TopiaEntitySqlSelectArgument selectArgument;
    private final ReplicatePartialRequest request;
    private final Set<ReplicatePartialRequestCallback> callbacks;
    private final String oldParentId;
    private final String newParentId;

    public ReplicatePartialEntityRequest(TopiaEntitySqlReplicatePartialPlan plan, ReplicatePartialRequest request, Set<ReplicatePartialRequestCallback> callbacks) {
        this.plan = Objects.requireNonNull(plan);
        this.request = Objects.requireNonNull(request);
        this.oldParentId = request.getOldId();
        this.callbacks = Objects.requireNonNull(callbacks);
        this.selectArgument = TopiaEntitySqlSelectArgument.of(oldParentId);
        this.newParentId = request.getNewId();
    }

    public ReplicatePartialRequest getRequest() {
        return request;
    }

    public Set<ReplicatePartialRequestCallback> getCallbacks() {
        return callbacks;
    }

    public TopiaEntitySqlReplicatePartialPlan getPlan() {
        return plan;
    }

    public TopiaEntitySqlSelectArgument getSelectArgument() {
        return selectArgument;
    }

    public String getOldParentId() {
        return oldParentId;
    }

    public String getNewParentId() {
        return newParentId;
    }
}

