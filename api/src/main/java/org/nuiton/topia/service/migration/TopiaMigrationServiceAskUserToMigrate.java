package org.nuiton.topia.service.migration;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Version;

import java.util.List;

/**
 * Callback to ask user to migrate.
 * <p>
 * To add one, you have just to implements it and make available via the {@link java.util.ServiceLoader} mechanism.
 * <p>
 * Created by tchemit on 05/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface TopiaMigrationServiceAskUserToMigrate {

    /**
     * Ask user to migrate database.
     *
     * @param dbVersion current schema version
     * @param versions  available versions to apply
     * @return {@code true} if user accept to migrate, {@code false} otherwise.
     */
    boolean canIMigrate(Version dbVersion, List<Version> versions);
}
