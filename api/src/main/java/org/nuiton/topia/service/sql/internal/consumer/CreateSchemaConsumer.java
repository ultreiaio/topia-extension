package org.nuiton.topia.service.sql.internal.consumer;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.nuiton.topia.persistence.TopiaException;
import io.ultreia.java4all.util.sql.SqlScriptWriter;
import org.nuiton.topia.service.sql.internal.SqlRequestConsumer;
import org.nuiton.topia.service.sql.internal.SqlRequestSetConsumerContext;
import org.nuiton.topia.service.sql.internal.request.CreateSchemaRequest;

/**
 * Created by tchemit on 13/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class CreateSchemaConsumer implements SqlRequestConsumer<CreateSchemaRequest> {

    private static final String CREATE_SCHEMA_STATEMENT = "CREATE SCHEMA %s;";

    @Override
    public void consume(CreateSchemaRequest request, SqlRequestSetConsumerContext context) {
        try {
            if (request.isProcessSchema()) {
                SqlScriptWriter writer = context.getWriter();
                for (String schemaName : context.getSourceTopiaApplicationContext().getSchemaNames()) {
                    writer.writeSql(String.format(CREATE_SCHEMA_STATEMENT, schemaName));
                }
            }
            if (request.isProcessTables()) {
                // FIXME Hibernate 6.0.0 use org.hibernate.tool.schema.Action.CREATE
                context.addSchemaExportScript(request.getDialect(), SchemaExport.Action.CREATE);
            }
        } catch (Exception e) {
            throw new TopiaException(String.format("Could not create schema for reason: %s", e.getMessage()), e);
        }
    }
}
