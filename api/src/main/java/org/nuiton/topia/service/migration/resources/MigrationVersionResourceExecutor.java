package org.nuiton.topia.service.migration.resources;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.support.TopiaSqlQuery;
import io.ultreia.java4all.util.sql.SqlWork;

import java.io.Closeable;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Context used at runtime on {@link MigrationVersionResource#generateSqlScript(MigrationVersionResourceExecutor)}.
 * <p>
 * Created by tchemit on 06/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface MigrationVersionResourceExecutor extends Closeable {

    void writeSql(String sql);

    void addScript(String rank, String prefix);

    <O> O findSingleResult(TopiaSqlQuery<O> query);

    <O> List<O> findMultipleResult(TopiaSqlQuery<O> query);

    <O> Set<O> findMultipleResultAstSet(TopiaSqlQuery<O> query);

    void doSqlWork(SqlWork sqlWork);

    Set<String> getTopiaIds(String tableName);

    String getUniqueConstraintName(String tableName, String columnName);

    String getFirstTableUniqueConstraintName(String tableName);

    Set<String> getConstraintNames(String tableName);

    Set<String> getForeignKeyConstraintNames(String tableName);

    String getForeignKeyConstraintName(String schemaName, String tableName, String columnName, boolean mustExists);

    Set<String> getUniqueKeyConstraintNames(String tableName);

    void removeFK(String tableName);

    void removeFK(String schemaName, String tableName, String columnName);

    void removeFKIfExists(String schemaName, String tableName, String columnName);

    void removePKIfExists(String schemaName, String tableName);

    void removeUK(String tableName);

    void dropSchema(String schemaName);

    void dropTable(String schemaName, String tableName);

    void executeForPG(Consumer<MigrationVersionResourceExecutor> consumer);

    void executeForH2(Consumer<MigrationVersionResourceExecutor> consumer);
}
