package org.nuiton.topia.service.migration.resources;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Version;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * This is the class to override for each migration version.
 * <p>
 * Just implements the {@link #generateSqlScript(MigrationVersionResourceExecutor)} method and make available this class
 * to {@link java.util.ServiceLoader} mechanism.
 * <p>
 * Created by tchemit on 05/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class MigrationVersionResource {

    private static boolean FOR_TCK = false;
    private static ClassLoader CLASS_LOADER;
    private final Version version;
    private final boolean requiresFinalize;
    private final MigrationVersionResourceScriptLayout scriptLayout;
    private final ClassLoader classLoader;
    private Map<String, String> scriptVariables;

    protected static class MigrateIdxSqlQuery extends TopiaSqlQuery<Map.Entry<String, String>> {

        private final String schemaName;
        private final String mainTable;
        private final String field;

        private MigrateIdxSqlQuery(String schemaName, String mainTable, String field) {
            this.schemaName = schemaName;
            this.mainTable = mainTable;
            this.field = field;
        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            return connection.prepareStatement(String.format("select %s, topiaId FROM %s.%s order by %s, topiaCreateDate", field, schemaName, mainTable, field));
        }

        @Override
        public Map.Entry<String, String> prepareResult(ResultSet resultSet) throws SQLException {
            return new AbstractMap.SimpleEntry<>(resultSet.getString(1), resultSet.getString(2));
        }
    }

    public static void setClassLoader(ClassLoader classLoader) {
        CLASS_LOADER = classLoader;
    }

    public static void setForTck(boolean forTck) {
        FOR_TCK = forTck;
    }

    protected static boolean isForTck() {
        return FOR_TCK;
    }

    public MigrationVersionResource(Version version) {
        this(version, false);
    }

    public MigrationVersionResource(Version version, boolean requiresFinalize) {
        this(version, new DefaultMigrationVersionResourceScriptLayout(), requiresFinalize);
    }

    public MigrationVersionResource(Version version, MigrationVersionResourceScriptLayout scriptLayout, boolean requiresFinalize) {
        this.version = Objects.requireNonNull(version);
        this.scriptLayout = scriptLayout;
        this.requiresFinalize = requiresFinalize;
        this.classLoader = CLASS_LOADER == null ? getClass().getClassLoader() : CLASS_LOADER;
    }

    public ClassLoader getClassLoader() {
        return classLoader;
    }

    public abstract void generateSqlScript(MigrationVersionResourceExecutor executor) throws IOException;

    public void generateFinalizeSqlScript(MigrationVersionResourceExecutor executor) throws IOException {
    }

    public Version getVersion() {
        return version;
    }

    public MigrationVersionResourceScriptLayout getScriptLayout() {
        return scriptLayout;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MigrationVersionResource that = (MigrationVersionResource) o;
        return Objects.equals(version, that.version);
    }

    public boolean requiresFinalize() {
        return requiresFinalize;
    }

    public Map<String, String> getScriptVariables() {
        return scriptVariables;
    }

    public void setScriptVariables(Map<String, String> scriptVariables) {
        this.scriptVariables = scriptVariables;
    }

    @Override
    public int hashCode() {
        return Objects.hash(version);
    }

    protected void migrateIdx(MigrationVersionResourceExecutor executor, String schemaName, String mainTable, String field) {
        List<Map.Entry<String, String>> result = executor.findMultipleResult(new MigrateIdxSqlQuery(schemaName, mainTable, field));
        String currentId = null;
        int position = 0;
        for (Map.Entry<String, String> pair : result) {
            String parentId = pair.getKey();
            if (!parentId.equals(currentId)) {
                position = 0;
                currentId = parentId;
            }
            String id = pair.getValue();
            executor.writeSql(String.format("UPDATE %s.%s SET %s_idx = %d WHERE topiaId = '%s';", schemaName, mainTable, field, position++, id));
        }
    }

}
