package org.nuiton.topia.service.sql.internal;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Version;
import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.H2Dialect;
import org.hibernate.dialect.PostgreSQL95Dialect;
import org.nuiton.topia.service.sql.TopiaEntitySqlModelResource;
import org.nuiton.topia.service.sql.blob.TopiaEntitySqlBlobModel;
import org.nuiton.topia.service.sql.internal.request.AddGeneratedSchemaRequest;
import org.nuiton.topia.service.sql.internal.request.AddVersionTableRequest;
import org.nuiton.topia.service.sql.internal.request.CopyEntityRequest;
import org.nuiton.topia.service.sql.internal.request.CreateSchemaRequest;
import org.nuiton.topia.service.sql.internal.request.DeleteEntityRequest;
import org.nuiton.topia.service.sql.internal.request.DeletePartialEntityRequest;
import org.nuiton.topia.service.sql.internal.request.DropSchemaRequest;
import org.nuiton.topia.service.sql.internal.request.ReplicateEntityRequest;
import org.nuiton.topia.service.sql.internal.request.ReplicatePartialEntityRequest;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSelectArgument;
import org.nuiton.topia.service.sql.plan.copy.TopiaEntitySqlCopyPlan;
import org.nuiton.topia.service.sql.plan.delete.TopiaEntitySqlDeletePlan;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePartialPlan;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePlan;
import org.nuiton.topia.service.sql.request.ReplicatePartialRequest;
import org.nuiton.topia.service.sql.request.ReplicatePartialRequestCallback;
import org.nuiton.topia.service.sql.request.ReplicateRequest;
import org.nuiton.topia.service.sql.request.ReplicateRequestCallback;

import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 25/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.28
 */
@SuppressWarnings({"UnusedReturnValue", "unused"})
public class SqlRequestSet {
    public static final String PROPERTY_READ_FETCH_SIZE = "readFetchSize";
    public static final String PROPERTY_GZIP = "gzip";
    public static final int DEFAULT_READ_FETCH_SIZE = 1000;
    public static final boolean DEFAULT_GZIP = false;
    /**
     * The fetch size used by read statements.
     */
    protected final int readFetchSize;
    /**
     * Should we gzip sql scripts.
     */
    protected final boolean gzip;
    private final Path path;
    private final List<SqlRequest> requests;
    private final Class<? extends Dialect> dialect;
    private final boolean needReplace;
    private final String newParentId;
    /**
     * Optional blob model.
     */
    private final TopiaEntitySqlBlobModel blobModel;

    public static class Builder {
        private final Path path;
        private final List<SqlRequest> requests;
        private int readFetchSize = DEFAULT_READ_FETCH_SIZE;
        private boolean gzip = DEFAULT_GZIP;
        private boolean needBlobModel = false;
        private boolean needReplace = false;
        private Class<? extends Dialect> dialect;
        private TopiaEntitySqlBlobModel blobModel;
        private String newParentId;

        public Builder(Path path) {
            this.path = path;
            this.requests = new LinkedList<>();
        }

        public TopiaEntitySqlBlobModel getBlobModel() {
            return blobModel;
        }

        public Builder forH2() {
            return dialect(H2Dialect.class);
        }

        public Builder forPostgresql() {
            return dialect(PostgreSQL95Dialect.class);
        }

        public Builder setReadFetchSize(int readFetchSize) {
            this.readFetchSize = readFetchSize;
            return this;
        }

        public Builder gzip() {
            this.gzip = true;
            return this;
        }

        public Builder dialect(Class<? extends Dialect> dialect) {
            this.dialect = Objects.requireNonNull(dialect);
            return this;
        }

        public Builder addCreateSchemaRequest(boolean addSchema, boolean addTables) {
            return addRequest(new CreateSchemaRequest(Objects.requireNonNull(dialect), addSchema, addTables));
        }

        public Builder addGeneratedSchemaRequest(Version dbVersion) {
            return addRequest(new AddGeneratedSchemaRequest(Objects.requireNonNull(dialect), false, true, dbVersion));
        }

        public Builder addVersionTableRequest(Version dbVersion) {
            return addRequest(new AddVersionTableRequest(Objects.requireNonNull(dialect), false, true, dbVersion));
        }

        public Builder addDropSchemaRequest(boolean dropSchema, boolean dropTables) {
            return addRequest(new DropSchemaRequest(Objects.requireNonNull(dialect), dropSchema, dropTables));
        }

        public Builder addReplicateTableRequest(TopiaEntitySqlReplicatePlan plan, ReplicateRequest request, Set<ReplicateRequestCallback> callbacks) {
            this.needBlobModel = true;
            this.needReplace = true;
            this.newParentId = request.getNewParentId();
            return addRequest(new ReplicateEntityRequest(plan, callbacks, request));
        }

        public Builder addReplicatePartialTableRequest(TopiaEntitySqlReplicatePartialPlan plan, ReplicatePartialRequest request, Set<ReplicatePartialRequestCallback> callbacks) {
            this.needBlobModel = true;
            this.needReplace = true;
            this.newParentId = request.getNewId();
            return addRequest(new ReplicatePartialEntityRequest(plan, request, callbacks));
        }

        public Builder addCopyTableRequest(TopiaEntitySqlCopyPlan plan, TopiaEntitySqlSelectArgument selectArgument) {
            this.needBlobModel = true;
            return addRequest(new CopyEntityRequest(plan, selectArgument));
        }

        public Builder addDeleteTableRequest(TopiaEntitySqlDeletePlan plan, TopiaEntitySqlSelectArgument selectArgument) {
            return addRequest(new DeleteEntityRequest(plan, selectArgument));
        }

        public Builder addDeletePartialTableRequest(TopiaEntitySqlDeletePlan plan, TopiaEntitySqlSelectArgument selectArgument, Set<String> shell, Map<String, Map<String, Set<String>>> detachTasksMapping) {
            return addRequest(new DeletePartialEntityRequest(plan, selectArgument, shell, detachTasksMapping));
        }

        protected Builder addRequest(SqlRequest request) {
            requests.add(request);
            return this;
        }

        public Builder setBlobModel(TopiaEntitySqlBlobModel blobModel) {
            this.blobModel = blobModel;
            return this;
        }

        public SqlRequestSet build(TopiaEntitySqlModelResource configuration) {
            if (dialect == null) {
                throw new IllegalStateException("No dialect set.");
            }
            return new SqlRequestSet(path,
                                     requests,
                                     readFetchSize,
                                     gzip,
                                     dialect,
                                     needReplace,
                                     newParentId,
                                     needBlobModel ? configuration.getBlobModel() : null);
        }
    }

    public static Builder builder(Path path) {
        return new Builder(path);
    }

    private SqlRequestSet(Path path,
                          List<SqlRequest> requests,
                          int readFetchSize,
                          boolean gzip,
                          Class<? extends Dialect> dialect,
                          boolean needReplace,
                          String newParentId, TopiaEntitySqlBlobModel blobModel) {
        this.path = path;
        this.requests = requests;
        this.readFetchSize = readFetchSize;
        this.gzip = gzip;
        this.dialect = dialect;
        this.needReplace = needReplace;
        this.newParentId = newParentId;
        this.blobModel = blobModel;
    }

    public String getNewParentId() {
        return newParentId;
    }

    public int getReadFetchSize() {
        return readFetchSize;
    }

    public Path getPath() {
        return path;
    }

    public boolean isGzip() {
        return gzip;
    }

    public boolean isNeedReplace() {
        return needReplace;
    }

    public Class<? extends Dialect> getDialect() {
        return dialect;
    }

    public List<SqlRequest> getRequests() {
        return requests;
    }

    public TopiaEntitySqlBlobModel getBlobModel() {
        return blobModel;
    }
}
