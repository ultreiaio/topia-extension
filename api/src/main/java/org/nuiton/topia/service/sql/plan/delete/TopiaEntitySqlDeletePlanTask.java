package org.nuiton.topia.service.sql.plan.delete;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Set;

/**
 * Created on 28/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.67
 */
public class TopiaEntitySqlDeletePlanTask {
    private final String gav;
    /**
     * The delete sql code.
     */
    private final String deleteSql;

    public TopiaEntitySqlDeletePlanTask(String gav, String deleteSql) {
        this.gav = gav;
        this.deleteSql = deleteSql;
    }

    public String getGav() {
        return gav;
    }

    public String getDeleteSql() {
        return deleteSql;
    }

    public String applyIds(String sql, String ids) {
        if (ids == null) {
            return sql.substring(0, sql.indexOf(" WHERE"));
        }
        return String.format(sql, ids);
    }

    public String getSchemaName() {
        return gav.substring(0, gav.indexOf("."));
    }

    public String getTableName() {
        return gav.substring(gav.indexOf("." + 1));
    }

    public boolean accept(Set<String> shell) {
        return shell == null || shell.contains(gav);
    }
}
