package org.nuiton.topia.service.sql.plan.replicate;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import io.ultreia.java4all.util.json.JsonHelper;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Set;

/**
 * Created on 06/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.69
 */
public class TopiaEntitySqlReplicatePlanTaskAdapter implements JsonDeserializer<TopiaEntitySqlReplicatePlanTask>, JsonSerializer<TopiaEntitySqlReplicatePlanTask> {
    public static final String FROM = "from";
    public static final String USE_BLOB = "useBlob";
    public static final String ENTRY_POINT = "entryPoint";
    public static final String RECURSIVE_COLUMN_NAME = "recursiveColumnName";
    public static final String COLUMN_NAMES = "columnNames";
    public static final String PARENT_COLUMN_NAME = "parentColumnName";
    public static final String COLUMNS_TO_REPLACE = "columnsToReplace";
    public static final String COLUMNS_TO_DETACH = "columnsToDetach";
    public static final String REQUIRED_TYPES = "requiredTypes";
    public static final String GAV = "gav";

    @Override
    public TopiaEntitySqlReplicatePlanTask deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = json.getAsJsonObject();
        String[] code = object.getAsJsonPrimitive(GAV).getAsString().split("\\s*\\.\\s*");
        String schemaName = code[0];
        String tableName = code[1];
        String from = JsonHelper.readString(FROM, object);
        boolean entryPoint = JsonHelper.readBoolean(ENTRY_POINT, object);
        boolean useBlob = JsonHelper.readBoolean(USE_BLOB, object);
        List<String> parentColumnName = List.of(JsonHelper.readString(PARENT_COLUMN_NAME, object).split("\\s*,\\s*"));
        List<String> columnNames = List.of(JsonHelper.readString(COLUMN_NAMES, object).split("\\s*,\\s*"));
        String recursiveColumn = JsonHelper.readStringOrNull(RECURSIVE_COLUMN_NAME, object);
        String columnsToReplaceStr = JsonHelper.readStringOrNull(COLUMNS_TO_REPLACE, object);
        String columnsToDetachStr = JsonHelper.readStringOrNull(COLUMNS_TO_DETACH, object);
        String requiredTypesStr = JsonHelper.readStringOrNull(REQUIRED_TYPES, object);
        Set<String> columnsToReplace = columnsToReplaceStr == null ? Set.of() : Set.of(columnsToReplaceStr.split("\\s*,\\s*"));
        Set<String> columnsToDetach = columnsToDetachStr == null ? Set.of() : Set.of(columnsToDetachStr.split("\\s*,\\s*"));
        Set<String> requiredTypes = requiredTypesStr == null ? Set.of() : Set.of(requiredTypesStr.split("\\s*,\\s*"));
        return new TopiaEntitySqlReplicatePlanTask(schemaName,
                                                   tableName,
                                                   from,
                                                   parentColumnName,
                                                   columnNames,
                                                   columnsToReplace,
                                                   columnsToDetach,
                                                   requiredTypes,
                                                   useBlob,
                                                   entryPoint,
                                                   recursiveColumn);
    }

    @Override
    public JsonElement serialize(TopiaEntitySqlReplicatePlanTask src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject result = new JsonObject();
        JsonHelper.addToResult(context, result, GAV, src.getSchemaName() + "." + src.getTableName());
        JsonHelper.addToResult(context, result, FROM, src.getFrom());
        JsonHelper.addToResult(context, result, PARENT_COLUMN_NAME, String.join(",",src.getParentColumnName()));
        JsonHelper.addToResult(context, result, COLUMN_NAMES, String.join(",", src.getColumnNames()));
        if (!src.getColumnsToReplace().isEmpty()) {
            JsonHelper.addToResult(context, result, COLUMNS_TO_REPLACE, String.join(",", src.getColumnsToReplace()));
        }
        if (!src.getColumnsToDetach().isEmpty()) {
            JsonHelper.addToResult(context, result, COLUMNS_TO_DETACH, String.join(",", src.getColumnsToDetach()));
        }
        if (!src.getRequiredTypes().isEmpty()) {
            JsonHelper.addToResult(context, result, REQUIRED_TYPES, String.join(",", src.getRequiredTypes()));
        }
        JsonHelper.addToResult(context, result, USE_BLOB, src.useBlob());
        JsonHelper.addToResult(context, result, ENTRY_POINT, src.isEntryPoint());
        JsonHelper.addToResult(context, result, RECURSIVE_COLUMN_NAME, src.getRecursiveColumnName().orElse(null));
        return result;
    }
}

