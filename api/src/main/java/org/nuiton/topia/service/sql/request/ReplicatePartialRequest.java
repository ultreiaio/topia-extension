package org.nuiton.topia.service.sql.request;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.service.sql.TopiaSqlServiceRequest;

import java.util.Set;
import java.util.StringJoiner;

/**
 * To replicate some layouts of an entry point.
 * <p>
 * Created on 05/04/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.74
 */
public class ReplicatePartialRequest extends TopiaSqlServiceRequest {
    /**
     * Type of entry point data to process.
     */
    private final String dataType;
    /**
     * Types to replicate.
     */
    private final Set<String> layoutTypes;
    /**
     * Id of source entry point data.
     */
    private final String oldId;
    /**
     * Id of target entry point data.
     */
    private final String newId;

    public ReplicatePartialRequest(boolean postgres, String dataType, Set<String> layoutTypes, String oldId, String newId) {
        super(postgres);
        this.dataType = dataType;
        this.layoutTypes = layoutTypes;
        this.oldId = oldId;
        this.newId = newId;
    }

    public String getDataType() {
        return dataType;
    }

    public Set<String> getLayoutTypes() {
        return layoutTypes;
    }

    public String getOldId() {
        return oldId;
    }

    public String getNewId() {
        return newId;
    }

    public DeletePartialRequest toDeleteRequest() {
        return new DeletePartialRequest(isPostgres(), getDataType(), getLayoutTypes(), getOldId());
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ReplicatePartialRequest.class.getSimpleName() + "[", "]")
                .add("postgres=" + isPostgres())
                .add("dataType=" + getDataType())
                .add("layoutTypes=" + getLayoutTypes())
                .add("oldId=" + getOldId())
                .add("newId=" + getNewId())
                .toString();
    }

}
