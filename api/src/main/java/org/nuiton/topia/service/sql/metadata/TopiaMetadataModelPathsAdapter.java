package org.nuiton.topia.service.sql.metadata;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;
import java.util.TreeMap;

/**
 * Created on 06/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.69
 */
public class TopiaMetadataModelPathsAdapter implements JsonDeserializer<TopiaMetadataModelPaths>, JsonSerializer<TopiaMetadataModelPaths> {

    private final TopiaMetadataModel model;

    public TopiaMetadataModelPathsAdapter(TopiaMetadataModel model) {
        this.model = model;
    }

    @Override
    public TopiaMetadataModelPaths deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        TreeMap<String, List<TopiaMetadataEntityPath>> paths = context.deserialize(json, TypeToken.getParameterized(TreeMap.class, String.class, TypeToken.getParameterized(List.class, TopiaMetadataEntityPath.class).getType()).getType());
        TreeMap<TopiaMetadataEntity, List<TopiaMetadataEntityPath>> result = new TreeMap<>();
        paths.forEach((k, v) -> result.put(model.getEntity(k), v));
        return new TopiaMetadataModelPaths(result);
    }

    @Override
    public JsonElement serialize(TopiaMetadataModelPaths src, Type typeOfSrc, JsonSerializationContext context) {
        return context.serialize(src.asMap());
    }
}
