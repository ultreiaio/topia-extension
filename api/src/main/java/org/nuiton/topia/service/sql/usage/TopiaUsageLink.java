package org.nuiton.topia.service.sql.usage;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaDaoSupplier;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaQueryBuilderRunQueryStep;

import java.util.List;
import java.util.Objects;

public abstract class TopiaUsageLink {

    private final Class<? extends TopiaEntity> type;
    private final String propertyName;

    protected TopiaUsageLink(Class<? extends TopiaEntity> type, String propertyName) {
        this.type = Objects.requireNonNull(type);
        this.propertyName = Objects.requireNonNull(propertyName);
    }

    public <E extends TopiaEntity> long count(TopiaDaoSupplier daoSupplier, TopiaEntity entity) {
        TopiaDao<E> dao = getDao(daoSupplier);
        TopiaQueryBuilderRunQueryStep<E> daoQuery = getDaoQuery(dao, entity);
        return daoQuery.count();
    }

    public <E extends TopiaEntity> List<E> find(TopiaDaoSupplier daoSupplier, TopiaEntity entity) {
        TopiaDao<E> dao = getDao(daoSupplier);
        TopiaQueryBuilderRunQueryStep<E> daoQuery = getDaoQuery(dao, entity);
        return daoQuery.findAll();
    }

    public <E extends TopiaEntity> TopiaDao<E> getDao(TopiaDaoSupplier daoSupplier) {
        @SuppressWarnings("unchecked") TopiaDao<E> dao = (TopiaDao<E>) daoSupplier.getDao(getType());
        return dao;
    }

    protected abstract <E extends TopiaEntity> TopiaQueryBuilderRunQueryStep<E> getDaoQuery(TopiaDao<E> dao, TopiaEntity entity);

    public Class<? extends TopiaEntity> getType() {
        return type;
    }

    public String getPropertyName() {
        return propertyName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TopiaUsageLink)) return false;
        TopiaUsageLink that = (TopiaUsageLink) o;
        return type.equals(that.type) && propertyName.equals(that.propertyName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, propertyName);
    }
}
