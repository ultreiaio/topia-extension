package org.nuiton.topia.service.sql.blob;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Map;
import java.util.Optional;

/**
 * Created on 05/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.68
 */
public class TopiaEntitySqlBlobModel {

    private final Map<String, TopiaEntitySqlBlob> entries;

    public TopiaEntitySqlBlobModel(Map<String, TopiaEntitySqlBlob> entries) {
        this.entries = entries;
    }

    //FIXME Use this in CopyPlan and others...
    public Optional<TopiaEntitySqlBlob> getBlobs(String type) {
        return Optional.ofNullable(entries.get(type));
    }

    public Map<String, TopiaEntitySqlBlob> getEntries() {
        return entries;
    }
}
