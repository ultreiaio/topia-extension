package org.nuiton.topia.service.sql.model;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.Set;

/**
 * Describes selectors on a same table.
 * Created on 22/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.27
 */
public class TopiaEntitySqlSelector {

    /**
     * From clause.
     */
    private final String fromClause;
    /**
     * Where clause alias.
     */
    private final String whereClauseAlias;
    /**
     * Join clauses.
     */
    private final String joinClauses;
    /**
     * Is the selector represents a reverse association ?
     */
    private final boolean reverseSelector;

    public static class Builder {
        /**
         * Join clauses.
         */
        private final StringBuilder joinClauses = new StringBuilder();
        /**
         * From clause.
         */
        private String fromClause;
        /**
         * Where clause alias.
         */
        private String whereClauseAlias;
        /**
         * Is the selector represents a reverse association ?
         */
        private boolean reverseSelector;

        public Builder setFromClause(String fromClause) {
            this.fromClause = Objects.requireNonNull(fromClause).trim();
            return this;
        }

        public Builder reverseSelector() {
            this.reverseSelector = true;
            return this;
        }

        public Builder setWhereClauseAlias(String whereClauseAlias) {
            this.whereClauseAlias = Objects.requireNonNull(whereClauseAlias).trim();
            return this;
        }

        public Builder addJoinClause(String joinClause) {
            if (joinClauses.length() > 0) {
                joinClauses.append(' ');
            }
            joinClauses.append(joinClause.trim());
            return this;
        }

        public TopiaEntitySqlSelector build() {
            return new TopiaEntitySqlSelector(Objects.requireNonNull(fromClause, "fromClause is null"), Objects.requireNonNull(whereClauseAlias, "whereClauseAlias is null"), joinClauses.toString(), reverseSelector);
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public TopiaEntitySqlSelector(String fromClause, String whereClauseAlias, String joinClauses, boolean reverseSelector) {
        this.fromClause = fromClause;
        this.whereClauseAlias = whereClauseAlias;
        this.joinClauses = joinClauses;
        this.reverseSelector = reverseSelector;
    }

    public String getFromClause() {
        return fromClause;
    }

    public String getWhereClauseAlias() {
        return whereClauseAlias;
    }

    public String getJoinClauses() {
        return joinClauses;
    }

    public boolean isReverseSelector() {
        return reverseSelector;
    }

    public String generatePrototype(String selectClause) {
        return String.format("SELECT %s FROM %s %s WHERE %s", selectClause, getFromClause(), joinClauses, getWhereClauseWithValuesPrototype());
    }

    public String generate(String selectClause, TopiaEntitySqlSelectArgument selectArgument) {
        boolean filter = selectArgument != null;
        String whereClause = filter ? " WHERE " + getWhereClauseWithValues(selectArgument.getIds()) : "";
        return String.format("SELECT %s FROM %s %s%s", selectClause, getFromClause(), joinClauses, whereClause);
    }

    public String getWhereClauseWithValues(Set<String> ids) {
        return getWhereClauseWithValues(whereClauseAlias, ids);
    }

    public String getWhereClauseWithValuesPrototype() {
        return getWhereClauseWithValuesPrototype(whereClauseAlias);
    }

    public String getWhereClauseWithValues(String whereClauseAlias, Set<String> ids) {
        StringBuilder result = new StringBuilder(whereClauseAlias);
        if (ids.size() == 1) {
            result.append(String.format(" = '%s'", ids.iterator().next()));
        } else {
            StringBuilder in = new StringBuilder();
            for (String id : ids) {
                in.append(String.format(", '%s'", id));
            }
            result.append(" IN (").append(in.substring(2)).append(")");
        }
        return result.toString();
    }

    public String getWhereClauseWithValuesPrototype(String whereClauseAlias) {
        return whereClauseAlias + " %s";
    }
}
