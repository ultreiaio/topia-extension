package org.nuiton.topia.service.sql.internal.consumer;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Version;
import org.nuiton.topia.persistence.TopiaException;
import io.ultreia.java4all.util.sql.SqlScriptReader;
import io.ultreia.java4all.util.sql.SqlScriptWriter;
import org.nuiton.topia.service.sql.internal.SqlRequestConsumer;
import org.nuiton.topia.service.sql.internal.SqlRequestSetConsumerContext;
import org.nuiton.topia.service.sql.internal.request.AddGeneratedSchemaRequest;

import java.net.URL;
import java.util.Objects;

/**
 * Created on 16/06/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.4
 */
public class AddGeneratedSchemaConsumer implements SqlRequestConsumer<AddGeneratedSchemaRequest> {

    @Override
    public void consume(AddGeneratedSchemaRequest request, SqlRequestSetConsumerContext context) {
        try {
            SqlScriptWriter writer = context.getWriter();
            Version dbVersion = request.getDbVersion();
            String majorVersion = dbVersion.getComponent(0).getValue().toString();
            String suffix = request.isH2() ? "H2" : "PG";
            String path = String.format("db/migration/v%s/%s/empty-schema-%s.sql", majorVersion, dbVersion, suffix);

            URL resource = getClass().getClassLoader().getResource(path);
            if (resource == null) {
                resource = Thread.currentThread().getContextClassLoader().getResource(path);
            }
            writer.writeScript(SqlScriptReader.of(Objects.requireNonNull(resource, String.format("Can't find resource in class-path: %s", path))));
        } catch (Exception e) {
            throw new TopiaException(String.format("Could not add  schema for reason: %s", e.getMessage()), e);
        }
    }
}

