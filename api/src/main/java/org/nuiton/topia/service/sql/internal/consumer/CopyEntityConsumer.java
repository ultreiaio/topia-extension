package org.nuiton.topia.service.sql.internal.consumer;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.service.sql.internal.SqlRequestConsumer;
import org.nuiton.topia.service.sql.internal.SqlRequestSetConsumerContext;
import org.nuiton.topia.service.sql.internal.request.CopyEntityRequest;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSelectArgument;
import org.nuiton.topia.service.sql.plan.copy.TopiaEntitySqlCopyPlan;
import org.nuiton.topia.service.sql.plan.copy.TopiaEntitySqlCopyPlanTask;

/**
 * Created on 25/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.27
 */
public class CopyEntityConsumer implements SqlRequestConsumer<CopyEntityRequest> {

    @Override
    public void consume(CopyEntityRequest request, SqlRequestSetConsumerContext context) {
        TopiaEntitySqlSelectArgument selectArgument = request.getSelectArgument();
        String ids = context.ids(selectArgument, true);
        TopiaEntitySqlCopyPlan copyPlan = request.getCopyPlan();
        for (TopiaEntitySqlCopyPlanTask task : copyPlan) {
            CopyEntityWork readSqlWork = new CopyEntityWork(context, task, ids);
            context.getSourcePersistenceContext().getSqlSupport().doSqlWork(readSqlWork);
        }
    }
}
