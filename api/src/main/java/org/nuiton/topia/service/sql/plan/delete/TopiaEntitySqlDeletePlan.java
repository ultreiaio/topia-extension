package org.nuiton.topia.service.sql.plan.delete;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collections;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 28/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.67
 */
public class TopiaEntitySqlDeletePlan implements Iterable<TopiaEntitySqlDeletePlanTask> {

    private final Set<TopiaEntitySqlDeletePlanTask> plans;

    public TopiaEntitySqlDeletePlan(Set<TopiaEntitySqlDeletePlanTask> plans) {
        this.plans = Collections.unmodifiableSet(Objects.requireNonNull(plans));
    }

    public Set<TopiaEntitySqlDeletePlanTask> getPlans() {
        return plans;
    }

    @Override
    public Iterator<TopiaEntitySqlDeletePlanTask> iterator() {
        return plans.iterator();
    }
}
