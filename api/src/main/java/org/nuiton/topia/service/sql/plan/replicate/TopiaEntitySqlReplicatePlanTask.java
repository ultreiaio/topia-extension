package org.nuiton.topia.service.sql.plan.replicate;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.service.sql.plan.SqlHelper;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 06/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.69
 */
public class TopiaEntitySqlReplicatePlanTask {
    private static final Logger log = LogManager.getLogger(TopiaEntitySqlReplicatePlanTask.class);
    /**
     * Schema name.
     */
    private final String schemaName;
    /**
     * Table name.
     */
    private final String tableName;
    /**
     * From clause to compute selected sql.
     */
    private final String from;
    /**
     * Column names to copy.
     */
    private final List<String> columnNames;
    /**
     * Parent column name.
     */
    private final List<String> parentColumnName;
    /**
     * Column names to replace (if not in shell).
     */
    private final Set<String> columnsToReplace;
    /**
     * Column names to detach (if not in shell).
     */
    private final Set<String> columnsToDetach;
    /**
     * Column names to detach (if not in shell) split.
     */
    private final transient Set<Map.Entry<String, String>> columnsToDetachSimple;
    /**
     * Is gav entry point of the plan?
     */
    private final boolean entryPoint;
    /**
     * Optional recursive column name on table.
     */
    private final String recursiveColumnName;
    /**
     * All required types to perform this task.
     */
    private final Set<String> requiredTypes;
    /**
     * Is use blob?
     */
    private final boolean useBlob;
    /**
     * Select sql code to load a data row to copy.
     */
    private final transient String selectSql;
    /**
     * Insert sql code to load a data row to copy.
     */
    private final transient String insertSql;

    public static String applyIds(String sql, String selectClause, String ids) {
        if (ids == null) {
            int endIndex = sql.indexOf(" WHERE");
            if (endIndex > -1) {
                // Need this for multiple table selector (if not reverse then will get the hole table, so duplicated data can happen)
                // See https://gitlab.com/ultreiaio/topia-extension/-/issues/122
                return String.format(sql, selectClause, "IS NOT NULL");
//                return String.format(sql.substring(0, endIndex), selectClause, "");
            }
        }
        return String.format(sql, selectClause, ids);
    }

    public TopiaEntitySqlReplicatePlanTask(String schemaName,
                                           String tableName,
                                           String from,
                                           List<String> parentColumnName,
                                           List<String> columnNames,
                                           Set<String> columnsToReplace,
                                           Set<String> columnsToDetach,
                                           Set<String> requiredTypes,
                                           boolean useBlob,
                                           boolean entryPoint,
                                           String recursiveColumnName) {
        this.schemaName = schemaName;
        this.tableName = tableName;
        this.from = from;
        this.parentColumnName = parentColumnName;
        this.columnNames = columnNames;
        this.columnsToReplace = columnsToReplace;
        this.columnsToDetach = columnsToDetach;
        this.columnsToDetachSimple = columnsToDetach.stream().map(s -> {
            int i = s.indexOf("~");
            String column = s.substring(0, i);
            String table = s.substring(i + 1);
            return Map.entry(column, table);
        }).collect(Collectors.toSet());
        this.requiredTypes = requiredTypes;
        this.useBlob = useBlob;
        this.entryPoint = entryPoint;
        this.recursiveColumnName = recursiveColumnName;
        this.selectSql = SqlHelper.newSelectStatementSql(schemaName, tableName, from);
        this.insertSql = SqlHelper.newInsertStatementSql(schemaName, tableName);
    }

    public boolean accept(Set<String> shell) {
        Set<String> requiredTypes = getRequiredTypes();
        if (requiredTypes.size() > 0) {
            // check if we can apply it
            for (String requiredType : requiredTypes) {
                if (!shell.contains(requiredType)) {
                    log.warn(String.format("Skip task: %s - required type not found: %s", getSchemaAndTableName(), requiredType));
                    return false;
                }
            }
        }
        return true;
    }

    public Set<Map.Entry<String, String>> getColumnsToDetachSimple() {
        return columnsToDetachSimple;
    }

    public Set<String> getRequiredTypes() {
        return requiredTypes;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public String getFrom() {
        return from;
    }

    public String getSelectSql() {
        return selectSql;
    }

    public String getInsertSql() {
        return insertSql;
    }

    public List<String> getParentColumnName() {
        return parentColumnName;
    }

    public List<String> getColumnNames() {
        return columnNames;
    }

    public Set<String> getColumnsToReplace() {
        return columnsToReplace;
    }

    public Set<String> getColumnsToDetach() {
        return columnsToDetach;
    }

    public boolean useBlob() {
        return useBlob;
    }

    public boolean isEntryPoint() {
        return entryPoint;
    }

    public Optional<String> getRecursiveColumnName() {
        return Optional.ofNullable(recursiveColumnName);
    }

    public String getSchemaAndTableName() {
        return getSchemaName() + "." + getTableName();
    }
}

