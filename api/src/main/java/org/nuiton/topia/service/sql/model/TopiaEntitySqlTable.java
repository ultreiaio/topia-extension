package org.nuiton.topia.service.sql.model;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.service.sql.metadata.TopiaMetadataEntity;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * Describes a slq table for a given entity.
 * <p>
 * Created on 22/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.27
 */
public class TopiaEntitySqlTable extends AbstractTopiaEntitySqlTable {

    private final Set<String> blobProperties;
    private final String recursiveProperty;

    public TopiaEntitySqlTable(String entityName,
                               String schemaName,
                               String tableName,
                               Set<String> authorizedColumnNames,
                               List<TopiaEntitySqlSelector> selectors,
                               Set<String> blobProperties,
                               String recursiveProperty) {
        super(entityName, schemaName, tableName, authorizedColumnNames, selectors);
        this.blobProperties = Objects.requireNonNull(blobProperties);
        this.recursiveProperty = recursiveProperty;
    }

    public TopiaEntitySqlTable(TopiaMetadataEntity metadataEntity, List<TopiaEntitySqlSelector> selectors) {
        this(metadataEntity.getFullyQualifiedName(),
             metadataEntity.getDbSchemaName(),
             metadataEntity.getDbTableName(),
             metadataEntity.getAllDbColumnNames(),
             selectors,
             metadataEntity.getBlobProperties(),
             metadataEntity.getOptionalRecursiveProperty().orElse(null));
    }

    public Set<String> getBlobProperties() {
        return blobProperties;
    }

    public Optional<String> getOptionalRecursiveProperty() {
        return Optional.ofNullable(recursiveProperty);
    }

    @Override
    public String getJoinColumnName() {
        return TopiaEntity.PROPERTY_TOPIA_ID;
    }

    public String selectIdsPrototype(TopiaEntitySqlSelector selector) {
        return generatePrototype(selector, String.format("%s.%s", getTableName(), getJoinColumnName()));
    }
}
