package org.nuiton.topia.service.sql.plan;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created on 09/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.69
 */
public class SqlHelper {
    public static final String SELECT_STATEMENT = "SELECT %%s FROM %1$s.%2$s %3$s;";
    public static final String INSERT_STATEMENT = "INSERT INTO %1$s.%2$s(%%s) VALUES (%3$s);";

    public static String newSelectStatementSql(String schemaName, String tableName, String from) {
        return String.format(SELECT_STATEMENT, schemaName, tableName, from);
    }

    public static String newInsertStatementSql(String schemaName, String tableName) {
        return String.format(INSERT_STATEMENT, schemaName, tableName,"%%s");
    }
}
