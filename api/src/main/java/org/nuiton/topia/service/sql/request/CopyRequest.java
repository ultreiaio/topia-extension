package org.nuiton.topia.service.sql.request;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.service.sql.TopiaSqlServiceRequest;

import java.util.Set;
import java.util.StringJoiner;

/**
 * To copy some entry point entities with possible schema create and more stuff.
 * <p>
 * Created on 23/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.67
 */
public class CopyRequest extends TopiaSqlServiceRequest {
    /**
     * Type of entry point data to copy.
     */
    private final String dataType;
    /**
     * Ids of data to copy.
     */
    private final Set<String> dataIds;

    public CopyRequest(boolean postgres, String dataType, String... dataIds) {
        super(postgres);
        this.dataType = dataType;
        this.dataIds = Set.of(dataIds);
    }

    public String getDataType() {
        return dataType;
    }

    public Set<String> getDataIds() {
        return dataIds;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CopyRequest.class.getSimpleName() + "[", "]")
                .add("postgres=" + isPostgres())
                .add("dataType=" + getDataType())
                .add("dataIds=" + getDataIds())
                .toString();
    }
}
