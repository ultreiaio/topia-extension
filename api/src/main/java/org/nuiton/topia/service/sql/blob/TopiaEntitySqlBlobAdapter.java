package org.nuiton.topia.service.sql.blob;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import io.ultreia.java4all.util.json.JsonHelper;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 05/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.68
 */
public class TopiaEntitySqlBlobAdapter implements JsonDeserializer<TopiaEntitySqlBlob>, JsonSerializer<TopiaEntitySqlBlob> {

    @Override
    public TopiaEntitySqlBlob deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        String[] code = json.getAsString().split("\\s*~\\s*");
        String schemaName = code[0];
        String tableName = code[1];
        List<String> columnNames = List.of(code[2].split("\\s*,\\s*"));
        return new TopiaEntitySqlBlob(schemaName, tableName, columnNames);
    }

    @Override
    public JsonElement serialize(TopiaEntitySqlBlob src, Type typeOfSrc, JsonSerializationContext context) {
        List<Object> properties = new ArrayList<>(3);
        JsonHelper.addToProperties(properties, src.getSchemaName());
        JsonHelper.addToProperties(properties, src.getTableName());
        JsonHelper.addToProperties(properties, String.join(",", src.getColumnNames()));
        String collect = JsonHelper.codeProperties(properties);
        return context.serialize(collect);
    }
}
