package org.nuiton.topia.service.sql.internal;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.List;

/**
 * Created on 25/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.28
 */
public interface SqlRequestConsumer<R extends SqlRequest> {

    static String getSelectClause(String tableName, List<String> columnNames) {
        StringBuilder columnNamesBuilder = new StringBuilder();
        for (String columnName : columnNames) {
            columnNamesBuilder.append(", ").append(tableName).append(".").append(columnName);
        }
        return columnNamesBuilder.substring(2);
    }

    static String newInsertStatementSql(String sql, List<String> columnNames, Collection<String> blobColumnNames) {
        StringBuilder columnNamesBuilder = new StringBuilder();
        for (String columnName : columnNames) {
            if (!blobColumnNames.contains(columnName)) {
                columnNamesBuilder.append(", ").append(columnName);
            }
        }
        return String.format(sql, columnNamesBuilder.substring(2));
    }

    void consume(R request, SqlRequestSetConsumerContext context);
}
