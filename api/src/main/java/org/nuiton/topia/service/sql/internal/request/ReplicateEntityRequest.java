package org.nuiton.topia.service.sql.internal.request;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.service.sql.internal.SqlRequest;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSelectArgument;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePlan;
import org.nuiton.topia.service.sql.request.ReplicateRequest;
import org.nuiton.topia.service.sql.request.ReplicateRequestCallback;

import java.util.Objects;
import java.util.Set;

/**
 * Created on 07/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.69
 */
public class ReplicateEntityRequest implements SqlRequest {

    private final TopiaEntitySqlReplicatePlan plan;
    private final Set<ReplicateRequestCallback> callbacks;
    private final ReplicateRequest request;
    private final TopiaEntitySqlSelectArgument selectArgument;
    private final String oldParentId;
    private final String newParentId;

    public ReplicateEntityRequest(TopiaEntitySqlReplicatePlan plan, Set<ReplicateRequestCallback> callbacks, ReplicateRequest request) {
        this.plan = Objects.requireNonNull(plan);
        this.callbacks = Objects.requireNonNull(callbacks);
        this.request = Objects.requireNonNull(request);
        this.selectArgument = TopiaEntitySqlSelectArgument.of(request.getDataIds());
        this.oldParentId = request.getOldParentId();
        this.newParentId = request.getNewParentId();
    }

    public TopiaEntitySqlReplicatePlan getPlan() {
        return plan;
    }

    public Set<ReplicateRequestCallback> getCallbacks() {
        return callbacks;
    }

    public ReplicateRequest getRequest() {
        return request;
    }

    public TopiaEntitySqlSelectArgument getSelectArgument() {
        return selectArgument;
    }

    public String getOldParentId() {
        return oldParentId;
    }

    public String getNewParentId() {
        return newParentId;
    }
}

