package org.nuiton.topia.service.sql.plan.replicate;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 06/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.69
 */
public class TopiaEntitySqlReplicatePlan implements Iterable<TopiaEntitySqlReplicatePlanTask> {

    private final Set<TopiaEntitySqlReplicatePlanTask> tasks;

    public TopiaEntitySqlReplicatePlan(Set<TopiaEntitySqlReplicatePlanTask> tasks) {
        this.tasks = Collections.unmodifiableSet(Objects.requireNonNull(tasks));
    }

    public Set<TopiaEntitySqlReplicatePlanTask> getTasks() {
        return tasks;
    }

    @Override
    public Iterator<TopiaEntitySqlReplicatePlanTask> iterator() {
        return tasks.iterator();
    }

    public Set<String> getShell() {
        return tasks.stream().map(TopiaEntitySqlReplicatePlanTask::getSchemaAndTableName).collect(Collectors.toCollection(LinkedHashSet::new));
    }
}
