package org.nuiton.topia.service.sql.internal.consumer;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import io.ultreia.java4all.util.TimeLog;
import org.nuiton.topia.persistence.TopiaEntity;
import io.ultreia.java4all.util.sql.BlobsContainer;
import io.ultreia.java4all.util.sql.SqlWork;
import org.nuiton.topia.service.sql.internal.SqlRequestConsumer;
import org.nuiton.topia.service.sql.internal.SqlRequestSetConsumerContext;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePartialPlanTask;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePlanTask;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 06/04/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.74
 */
class ReplicateEntityWork implements SqlWork {

    private static final TimeLog TIME_LOG = new TimeLog(ReplicateEntityWork.class, 500L, 1000L);
    private final ReplicateEntityWorkContext context;
    private final String sql;
    private final Map<String, BlobsContainer.Builder> blobsBuilder;
    private final Set<String> blobColumns;
    private final List<String> columnNames;
    private final String insertStatementSql;
    private final Set<String> columnsToReplace;
    private final String parentColumnName;
    private final boolean entryPoint;
    private final Set<String> columnsToDetach;
    private final String recursiveProperty;
    private final String[] newId;
    private final String newParentId;
    private final String oldParentId;

    public ReplicateEntityWork(ReplicateEntityWorkContext context, TopiaEntitySqlReplicatePlanTask task, String ids) {
        this.context = context;
        this.oldParentId = context.getOldParentId();
        this.newParentId = context.getNewParentId();
        this.columnNames = task.getColumnNames();
        String selectClause = SqlRequestConsumer.getSelectClause(task.getTableName(), columnNames);
        this.sql = TopiaEntitySqlReplicatePlanTask.applyIds(task.getSelectSql(), selectClause, ids);

        this.blobsBuilder = context().initBlobsBuilder(task.useBlob(), task.getSchemaAndTableName());
        this.blobColumns = blobsBuilder.keySet();
        this.insertStatementSql = SqlRequestConsumer.newInsertStatementSql(task.getInsertSql(), columnNames, blobColumns);
        this.columnsToReplace = task.getColumnsToReplace();
        List<String> parentColumnNames = task.getParentColumnName();
        String parentColumnName = null;
        if (parentColumnNames.size() == 1) {
            parentColumnName = parentColumnNames.get(0);
        } else {
            for (String columnName : parentColumnNames) {
                if (newParentId.toLowerCase().contains("." + columnName + "#")) {
                    parentColumnName = columnName;
                    break;
                }
            }
        }
        this.parentColumnName = parentColumnName;
        this.entryPoint = task.isEntryPoint();
        Set<String> shell = context.getShell();
        // need to detach only when not in shell
        this.columnsToDetach = new LinkedHashSet<>();
        for (Map.Entry<String, String> entry : task.getColumnsToDetachSimple()) {
            if (!shell.contains(entry.getValue())) {
                columnsToDetach.add(entry.getKey());
            }
        }
        this.recursiveProperty = task.getRecursiveColumnName().orElse(null);
        this.newId = new String[2];
    }

    public ReplicateEntityWork(ReplicateEntityWorkContext context, TopiaEntitySqlReplicatePartialPlanTask partialTask, String ids) {
        this.context = context;
        this.oldParentId = context.getOldParentId();
        this.newParentId = context.getNewParentId();
        TopiaEntitySqlReplicatePlanTask task = partialTask.getReplicateTask();
        this.columnNames = task.getColumnNames();
        String selectClause = SqlRequestConsumer.getSelectClause(task.getTableName(), columnNames);
        this.sql = TopiaEntitySqlReplicatePlanTask.applyIds(partialTask.getCopyTask().getSelectSql(), selectClause, ids);
        this.blobsBuilder = context().initBlobsBuilder(task.useBlob(), task.getSchemaAndTableName());
        this.blobColumns = blobsBuilder.keySet();
        this.insertStatementSql = SqlRequestConsumer.newInsertStatementSql(task.getInsertSql(), columnNames, blobColumns);
        this.columnsToReplace = task.getColumnsToReplace();
        List<String> parentColumnNames = task.getParentColumnName();
        String parentColumnName = null;
        if (parentColumnNames.size() == 1) {
            parentColumnName = parentColumnNames.get(0);
        } else {
            for (String columnName : parentColumnNames) {
                if (newParentId.toLowerCase().contains("." + columnName + "#")) {
                    parentColumnName = columnName;
                    break;
                }
            }
        }
        this.parentColumnName = parentColumnName;
        this.entryPoint = task.isEntryPoint();
        Set<String> shell = context.getShell();
        // need to detach only when not in shell
        this.columnsToDetach = new LinkedHashSet<>();
        for (Map.Entry<String, String> entry : task.getColumnsToDetachSimple()) {
            if (!shell.contains(entry.getValue())) {
                columnsToDetach.add(entry.getKey());
            }
        }
        this.recursiveProperty = task.getRecursiveColumnName().orElse(null);
        this.newId = new String[2];
    }

    public String addIdToReplace(String id) {
        return context.addIdToReplace(id);
    }

    public String getNewParentId() {
        return context.getNewParentId();
    }

    public String getIdToReplace(String id) {
        return context.getIdToReplace(id);
    }

    public void writeSql(String sql) {
        context.writeSql(sql);
    }

    public SqlRequestSetConsumerContext context() {
        return context.context();
    }

    @Override
    public void execute(Connection connection) throws SQLException {
        long t0 = TimeLog.getTime();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setFetchSize(context.context().getReadFetchSize());
            try (ResultSet resultSet = statement.executeQuery()) {
                if (recursiveProperty == null) {
                    simpleCopy(statement, resultSet);
                } else {
                    recursiveCopy(statement, resultSet);
                }
            }
        } finally {
            TIME_LOG.log(t0, "Executed on table", sql);
        }
    }

    private void simpleCopy(PreparedStatement statement, ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            String sql = generateInsertSqlStatement(statement, resultSet);
            context.writeSql(sql);
        }
    }

    private void recursiveCopy(PreparedStatement statement, ResultSet resultSet) throws SQLException {
        Map<String, String> requestsByTopiaId = new LinkedHashMap<>();
        Multimap<String, String> parents = ArrayListMultimap.create();
        String sentinel = "$$WithoutParent$$" + new Date().getTime();
        while (resultSet.next()) {
            String sql = generateInsertSqlStatement(statement, resultSet);
            String topiaId = newId[0];
            String key = resultSet.getString(recursiveProperty);
            parents.put(key == null ? sentinel : key, topiaId);
            requestsByTopiaId.put(topiaId, sql);
        }
        List<String> orderedTopiaIds = context().generateOrder(parents, sentinel);
        if (orderedTopiaIds.size() != requestsByTopiaId.size()) {
            throw new IllegalStateException("Mismatch ids size!!!");
        }
        for (String orderedTopiaId : orderedTopiaIds) {
            writeSql(requestsByTopiaId.get(orderedTopiaId));
        }
    }

    private String generateInsertSqlStatement(PreparedStatement statement, ResultSet resultSet) throws SQLException {
        newId[1] = newParentId;
        StringBuilder argumentsBuilder = new StringBuilder();
        String rowTopiaId = null;
        for (String columnName : columnNames) {
            if (blobColumns.contains(columnName)) {
                // processed later
                continue;
            }
            Object columnValue = resultSet.getObject(columnName);
            if (columnName.equals(parentColumnName)) {
                if (entryPoint) {
                    argumentsBuilder.append(", '").append(newParentId).append("'");
                } else {
                    String parentId = getIdToReplace((String) columnValue);
                    newId[1] = parentId;
                    argumentsBuilder.append(", '").append(parentId).append("'");
                }
                continue;
            }
            if (columnValue == null) {
                argumentsBuilder.append(", NULL");
                continue;
            }
            if (TopiaEntity.PROPERTY_TOPIA_ID.equals(columnName)) {
                String oldId = (String) columnValue;
                newId[0] = rowTopiaId = addIdToReplace(oldId);
                argumentsBuilder.append(", '").append(rowTopiaId).append("'");
                continue;
            }
            if (columnsToDetach.contains(columnName)) {
                argumentsBuilder.append(", NULL");
                continue;
            }
            if (columnsToReplace.contains(columnName)) {
                String parentId = getIdToReplace((String) columnValue);
                argumentsBuilder.append(", '").append(parentId).append("'");
                continue;
            }
            if (columnValue instanceof String) {
                String stringValue = (String) columnValue;
                if (entryPoint && stringValue.equals(oldParentId)) {
                    argumentsBuilder.append(", NULL");
//                    if (newParentId.toLowerCase().contains("." + columnName + "#")) {
//                        argumentsBuilder.append(", '").append(newParentId).append("'");
//                    } else {
//                        argumentsBuilder.append(", NULL");
//                    }
                    continue;
                }
                argumentsBuilder.append(", '").append(stringValue.replaceAll("'", "''")).append("'");
                continue;
            }
            if (columnValue instanceof Date) {
                argumentsBuilder.append(", '").append(columnValue).append("'");
                continue;
            }
            argumentsBuilder.append(", ").append(columnValue);
        }
        for (String columnName : blobColumns) {
            Object columnValue = resultSet.getObject(columnName);
            if (blobColumns.contains(columnName)) {
                context().copyBlob(statement, rowTopiaId, columnValue, blobsBuilder.get(columnName));
            }
        }
        String arguments = argumentsBuilder.substring(2);
        return String.format(insertStatementSql, arguments);
    }
}
