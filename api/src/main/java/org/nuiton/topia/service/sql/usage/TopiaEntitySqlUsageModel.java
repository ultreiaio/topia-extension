package org.nuiton.topia.service.sql.usage;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Map;
import java.util.Objects;

/**
 * Created on 04/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.68
 */
public class TopiaEntitySqlUsageModel {

    private final Map<String, TopiaUsageEntity> mapping;

    public TopiaEntitySqlUsageModel(Map<String, TopiaUsageEntity> mapping) {
        this.mapping = mapping;
    }

    public Map<String, TopiaUsageEntity> getMapping() {
        return mapping;
    }

    /**
     * Get the entity usage definition for the given [@code entity].
     *
     * @param entity entity on which we want to perform a usage search
     * @return entity usage definition, or {@code null} if not found
     */
    public TopiaUsageEntity getMapping(String entity) {
        return mapping.get(Objects.requireNonNull(entity));
    }

}
