package org.nuiton.topia.service.sql.usage;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import org.nuiton.topia.persistence.TopiaDaoSupplier;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Describes all usages of an entity type.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public class TopiaUsageEntity {

    private final List<TopiaUsageReverseComposition> reverseCompositions;
    private final List<TopiaUsageReverseManyToManyAssociation> reverseManyToManyAssociations;
    private final List<TopiaUsageReverseOneToManyAssociation> reverseOneToManyAssociations;
    private final List<TopiaUsageReverseOneToOneComposition> reverseOneToOneCompositions;
    private final List<TopiaUsageReverseAssociation> reverseAssociations;

    protected TopiaUsageEntity(List<TopiaUsageReverseComposition> reverseCompositions,
                               List<TopiaUsageReverseManyToManyAssociation> reverseManyToManyAssociations,
                               List<TopiaUsageReverseOneToManyAssociation> reverseOneToManyAssociations,
                               List<TopiaUsageReverseOneToOneComposition> reverseOneToOneCompositions,
                               List<TopiaUsageReverseAssociation> reverseAssociations) {
        this.reverseCompositions = reverseCompositions;
        this.reverseManyToManyAssociations = reverseManyToManyAssociations;
        this.reverseOneToManyAssociations = reverseOneToManyAssociations;
        this.reverseOneToOneCompositions = reverseOneToOneCompositions;
        this.reverseAssociations = reverseAssociations;
    }

    public void countReverseCompositions(TopiaDaoSupplier daoSupplier, TopiaEntity entity, Predicate<Class<? extends TopiaEntity>> entityTypeFilter, Map<Class<? extends TopiaEntity>, Long> result) {
        compositions(entityTypeFilter).ifPresent(links -> count(daoSupplier, links, entity, result));
    }

    public void countReverseAssociations(TopiaDaoSupplier daoSupplier, TopiaEntity entity, Predicate<Class<? extends TopiaEntity>> entityTypeFilter, Map<Class<? extends TopiaEntity>, Long> result) {
        associations(entityTypeFilter).ifPresent(links -> count(daoSupplier, links, entity, result));
    }

    public void countReverseManyToManyAssociations(TopiaDaoSupplier daoSupplier, TopiaEntity entity, Predicate<Class<? extends TopiaEntity>> entityTypeFilter, Map<Class<? extends TopiaEntity>, Long> result) {
        manyToManyAssociations(entityTypeFilter).ifPresent(links -> count(daoSupplier, links, entity, result));
    }

    public void countReverseOneToManyAssociations(TopiaDaoSupplier daoSupplier, TopiaEntity entity, Predicate<Class<? extends TopiaEntity>> entityTypeFilter, Map<Class<? extends TopiaEntity>, Long> result) {
        oneToManyAssociations(entityTypeFilter).ifPresent(links -> count(daoSupplier, links, entity, result));
    }

    public void countReverseOneToOneCompositions(TopiaDaoSupplier daoSupplier, TopiaEntity entity, Predicate<Class<? extends TopiaEntity>> entityTypeFilter, Map<Class<? extends TopiaEntity>, Long> result) {
        oneToOneCompositions(entityTypeFilter).ifPresent(links -> count(daoSupplier, links, entity, result));
    }

    public <E extends TopiaEntity> void findReverseComposition(TopiaDaoSupplier daoSupplier, TopiaEntity entity, Class<E> entityType, List<E> result) {
        compositions(entityType::equals).ifPresent(links -> find(daoSupplier, links, entity, result));
    }

    public <E extends TopiaEntity> void findReverseOneToManyAssociation(TopiaDaoSupplier daoSupplier, TopiaEntity entity, Class<E> entityType, List<E> result) {
        oneToManyAssociations(entityType::equals).ifPresent(links -> find(daoSupplier, links, entity, result));
    }

    public <E extends TopiaEntity> void findReverseOneToOneComposition(TopiaDaoSupplier daoSupplier, TopiaEntity entity, Class<E> entityType, List<E> result) {
        oneToOneCompositions(entityType::equals).ifPresent(links -> find(daoSupplier, links, entity, result));
    }

    public <E extends TopiaEntity> void findReverseManyToManyAssociation(TopiaDaoSupplier daoSupplier, TopiaEntity entity, Class<E> entityType, List<E> result) {
        manyToManyAssociations(entityType::equals).ifPresent(links -> find(daoSupplier, links, entity, result));
    }

    public <E extends TopiaEntity> void findReverseAssociation(TopiaDaoSupplier daoSupplier, TopiaEntity entity, Class<E> entityType, List<E> result) {
        associations(entityType::equals).ifPresent(links -> find(daoSupplier, links, entity, result));
    }

    protected void count(TopiaDaoSupplier daoSupplier, Stream<? extends TopiaUsageLink> links, TopiaEntity entity, Map<Class<? extends TopiaEntity>, Long> result) {
        links.forEach(link -> {
            long count = link.count(daoSupplier, entity);
            if (count > 0) {
                Long orDefault = result.getOrDefault(link.getType(), 0L);
                result.put(link.getType(), orDefault + count);
            }
        });
    }

    protected <E extends TopiaEntity> void find(TopiaDaoSupplier daoSupplier, Stream<? extends TopiaUsageLink> links, TopiaEntity entity, List<E> result) {
        links.forEach(link -> {
            List<E> count = link.find(daoSupplier, entity);
            result.addAll(count);
        });
    }

    protected Optional<Stream<TopiaUsageReverseComposition>> compositions(Predicate<Class<? extends TopiaEntity>> entityTypeFilter) {
        return reverseCompositions().map(p -> p.stream().filter(l -> entityTypeFilter.test(l.getType())));
    }

    protected Optional<Stream<TopiaUsageReverseAssociation>> associations(Predicate<Class<? extends TopiaEntity>> entityTypeFilter) {
        return reverseAssociations().map(p -> p.stream().filter(l -> entityTypeFilter.test(l.getType())));
    }

    protected Optional<Stream<TopiaUsageReverseManyToManyAssociation>> manyToManyAssociations(Predicate<Class<? extends TopiaEntity>> entityTypeFilter) {
        return reverseManyToManyAssociations().map(p -> p.stream().filter(l -> entityTypeFilter.test(l.getType())));
    }

    protected Optional<Stream<TopiaUsageReverseOneToManyAssociation>> oneToManyAssociations(Predicate<Class<? extends TopiaEntity>> entityTypeFilter) {
        return reverseOneToManyAssociations().map(p -> p.stream().filter(l -> entityTypeFilter.test(l.getType())));
    }

    protected Optional<Stream<TopiaUsageReverseOneToOneComposition>> oneToOneCompositions(Predicate<Class<? extends TopiaEntity>> entityTypeFilter) {
        return reverseOneToOneCompositions().map(p -> p.stream().filter(l -> entityTypeFilter.test(l.getType())));
    }

    protected Optional<List<TopiaUsageReverseComposition>> reverseCompositions() {
        return Optional.ofNullable(reverseCompositions);
    }

    protected Optional<List<TopiaUsageReverseManyToManyAssociation>> reverseManyToManyAssociations() {
        return Optional.ofNullable(reverseManyToManyAssociations);
    }

    protected Optional<List<TopiaUsageReverseOneToManyAssociation>> reverseOneToManyAssociations() {
        return Optional.ofNullable(reverseOneToManyAssociations);
    }

    protected Optional<List<TopiaUsageReverseOneToOneComposition>> reverseOneToOneCompositions() {
        return Optional.ofNullable(reverseOneToOneCompositions);
    }

    protected Optional<List<TopiaUsageReverseAssociation>> reverseAssociations() {
        return Optional.ofNullable(reverseAssociations);
    }
}
