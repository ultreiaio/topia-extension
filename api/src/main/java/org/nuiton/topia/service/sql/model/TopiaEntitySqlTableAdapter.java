package org.nuiton.topia.service.sql.model;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import io.ultreia.java4all.util.json.JsonHelper;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created on 01/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.67
 */
public class TopiaEntitySqlTableAdapter extends AbstractTopiaEntitySqlTableAdapter<TopiaEntitySqlTable> {

    public static final String BLOB_PROPERTIES = "blobProperties";
    public static final String RECURSIVE_PROPERTY = "recursiveProperty";

    @Override
    public TopiaEntitySqlTable deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = json.getAsJsonObject();
        String[] code = object.getAsJsonPrimitive(GAV).getAsString().split("\\s*~\\s*");
        String entityName = code[0];
        String schemaName = code[1];
        String tableName = code[2];
        Set<String> authorizedColumnNames = getAuthorizedColumnNames(object);
        List<TopiaEntitySqlSelector> selectors = getSelectors(context, object);
        String recursiveProperty = JsonHelper.readStringOrNull(RECURSIVE_PROPERTY, object);
        Set<String> blobProperties = JsonHelper.readStringSetOrEmpty(context, BLOB_PROPERTIES, object);
        return new TopiaEntitySqlTable(entityName,
                                       schemaName,
                                       tableName,
                                       authorizedColumnNames,
                                       selectors,
                                       blobProperties,
                                       recursiveProperty);
    }

    @Override
    public JsonObject serialize(TopiaEntitySqlTable src, Type typeOfSrc, JsonSerializationContext context) {
        List<Object> properties = new ArrayList<>(3);
        JsonHelper.addToProperties(properties, src.getEntityName());
        JsonHelper.addToProperties(properties, src.getSchemaName());
        JsonHelper.addToProperties(properties, src.getTableName());
        JsonObject result = new JsonObject();
        JsonHelper.addToResult(context, result, GAV, JsonHelper.codeProperties(properties));
        JsonHelper.addToResult(context, result, AUTHORIZED_COLUMN_NAMES, String.join(",", src.getAuthorizedColumnNames()));
        JsonHelper.addToResult(context, result, SELECTORS, src.getSelectors());
        JsonHelper.addToResult(context, result, RECURSIVE_PROPERTY, src.getOptionalRecursiveProperty().orElse(null));
        JsonHelper.addToResult(context, result, BLOB_PROPERTIES, src.getBlobProperties());
        return result;
    }

}
