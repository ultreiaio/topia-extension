package org.nuiton.topia.service.sql.blob;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.jdbc.JdbcPostgresHelper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Objects;

/**
 * Created on 05/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.68
 */
public class TopiaEntitySqlBlobIdsIterator implements Iterator<String>, Closeable {

    private final TopiaEntitySqlBlobModel model;

    private final Path cachePath;
    private final JdbcPostgresHelper jdbcHelper;
    private FileIterator iterator;

    private static class FileIterator implements Iterator<String>, Closeable {
        private final Iterator<String> iterator;
        private final BufferedReader bufferedReader;

        public FileIterator(Path cachePath, TopiaEntitySqlBlobModel model, JdbcPostgresHelper jdbcHelper) {
            if (Files.notExists(cachePath)) {
                try {
                    createCache(jdbcHelper, model, cachePath);
                } catch (IOException e) {
                    throw new IllegalStateException("can't create cache at " + cachePath, e);
                }
            }
            try {
                bufferedReader = Files.newBufferedReader(Objects.requireNonNull(cachePath));
            } catch (IOException e) {
                throw new IllegalStateException("can't create reader on " + cachePath, e);
            }
            iterator = bufferedReader.lines().iterator();
        }

        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }

        @Override
        public String next() {
            return iterator.next();
        }

        @Override
        public void close() throws IOException {
            bufferedReader.close();
        }

        private void createCache(JdbcPostgresHelper jdbcHelper, TopiaEntitySqlBlobModel model, Path cachePath) throws IOException {
            if (Files.notExists(cachePath.getParent())) {
                Files.createDirectory(cachePath.getParent());
            }
            try (BufferedWriter writer = Files.newBufferedWriter(cachePath)) {
                for (TopiaEntitySqlBlob blobModel : model.getEntries().values()) {
                    String schemaName = blobModel.getSchemaName();
                    String tableName = blobModel.getTableName();
                    for (String columnName : blobModel.getColumnNames()) {
                        fillCache(jdbcHelper, schemaName, tableName, columnName, writer);
                    }
                }
                writer.flush();
            }
        }

        private void fillCache(JdbcPostgresHelper jdbcHelper, String dbSchemaName, String dbTableName, String dbColumnName, BufferedWriter writer) {
            String sql = String.format("SELECT %1$s FROM %2$s.%3$s WHERE %1$s IS NOT NULL ORDER BY topiaId ASC", dbColumnName, dbSchemaName, dbTableName);
            jdbcHelper.consume(c -> {
                try (PreparedStatement preparedStatement = c.prepareStatement(sql)) {
                    try (ResultSet resultSet = preparedStatement.executeQuery()) {
                        while (resultSet.next()) {
                            writer.write(resultSet.getString(1));
                            writer.newLine();
                        }
                    }
                } catch (SQLException | IOException e) {
                    throw new IllegalStateException(e);
                }
            });
        }
    }

    public TopiaEntitySqlBlobIdsIterator(TopiaEntitySqlBlobModel model, Path cachePath, TopiaApplicationContext<?> applicationContext) {
        this.model = Objects.requireNonNull(model);
        this.cachePath = Objects.requireNonNull(cachePath);
        this.jdbcHelper = new JdbcPostgresHelper(Objects.requireNonNull(applicationContext).getConfiguration());
    }

    @Override
    public boolean hasNext() {
        return iterator().hasNext();
    }

    @Override
    public String next() {
        return iterator().next();
    }

    public FileIterator iterator() {
        if (iterator == null) {
            iterator = new FileIterator(cachePath, model, jdbcHelper);
        }
        return iterator;
    }

    @Override
    public void close() throws IOException {
        if (iterator != null) {
            iterator.close();
            iterator = null;
        }
    }
}
