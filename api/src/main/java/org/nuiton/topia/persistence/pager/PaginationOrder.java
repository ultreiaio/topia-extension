package org.nuiton.topia.persistence.pager;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;

/**
 * This class represents an 'order' information : order clause and asc/desc
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public class PaginationOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    protected String clause;
    protected boolean desc;

    public PaginationOrder(String clause, boolean desc) {
        this.clause = clause;
        this.desc = desc;
    }

    public String getClause() {
        return clause;
    }

    public void setClause(String clause) {
        this.clause = clause;
    }

    public boolean isDesc() {
        return desc;
    }

    public void setDesc(boolean desc) {
        this.desc = desc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PaginationOrder that = (PaginationOrder) o;

        if (desc != that.desc) {
            return false;
        }
        return clause != null ? clause.equals(that.clause) : that.clause == null;

    }

    @Override
    public int hashCode() {
        int result = clause != null ? clause.hashCode() : 0;
        result = 31 * result + (desc ? 1 : 0);
        return result;
    }

}
