package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.jdbc.JdbcConfiguration;

import java.util.Map;

/**
 * Configuration needed to instantiate a {@link org.nuiton.topia.persistence.TopiaApplicationContext}.
 * <p>
 * Please use an instance of {@link org.nuiton.topia.persistence.TopiaConfigurationBuilder} to build a new
 * TopiaConfiguration.
 *
 * @author Brendan Le Ny (Code Lutin)
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public interface TopiaConfiguration extends JdbcConfiguration {

    /**
     * You can set it to false if you don't want Topia to deal with the schema (you keep it
     * up to date with your own sql file).
     *
     * @return true by default
     * @since 3.0
     */
    boolean isInitSchema();

    /**
     * If true, ToPIA will validate schema against model upon starting. ToPIA will raise a
     * {@link SchemaValidationTopiaException} if the schema is not suitable for ToPIA to run
     * fine.
     *
     * @return FIXME
     * @since 3.0
     */
    boolean isValidateSchema();

    /**
     * Configuration directive to change topia Ids generation strategy.
     *
     * @return FIXME
     * @since 3.0
     */
    TopiaIdFactory getTopiaIdFactory();

    String getSchemaName();

    /**
     * Topia will automatically generate an Hibernate configuration for you based
     * on the different element of this TopiaConfiguration. However, you can tune Hibernate
     * by adding Hibernate configuration directive to this Map.
     * <p>
     * We highly recommend you to use constants in {@link org.nuiton.topia.persistence.HibernateAvailableSettings}
     * as keys of the map.
     * <p>
     * You <strong>MUST NOT</strong> pass {@link HibernateAvailableSettings#HBM2DDL_AUTO} since
     * ToPIA will use validate. If you want use it, have a look at {@link org.nuiton.topia.persistence.HibernateTopiaMigrationService}
     *
     * @return a map containing hibernate configuration directives that ToPIA will use when it
     * will instantiate Hibernate.
     */
    Map<String, String> getHibernateExtraConfiguration();

    Map<String, Class<? extends TopiaService>> getDeclaredServices();

    Map<String, Map<String, String>> getDeclaredServicesConfiguration();

    /**
     * If you want to favor using HikariCP as a connection pool.
     */
    boolean isUseHikariForJdbcConnectionPooling();

}
