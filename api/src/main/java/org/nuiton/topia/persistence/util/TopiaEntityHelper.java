package org.nuiton.topia.persistence.util;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Une classe avec des méthodes utiles sur les entités.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class TopiaEntityHelper {

    /**
     * Filter a list of entities, and keep only the ones from a given type.
     *
     * @param entities    the list of entities to filter
     * @param filterClass the type of entities to keep
     * @return the list of filtered entities for the given entity type in the
     * list
     * @since 2.1.4
     */
    public static List<TopiaEntity> filter(
            Collection<TopiaEntity> entities,
            Class<? extends TopiaEntity> filterClass) {
        List<TopiaEntity> result = new ArrayList<>();
        for (TopiaEntity e : entities) {
            if (filterClass.isAssignableFrom(e.getClass())) {
                result.add(e);
            }
        }
        return result;
    }

    public static Class<? extends TopiaEntity> getContractClass(
            TopiaEntityEnum[] contracts,
            Class<? extends TopiaEntity> klass) {
        if (contracts == null || contracts.length == 0) {
            // pas de contract connus...
            return null;
        }

        // on recupere l'ensemble des interfaces connues pour la klass donnee
        Set<Class<? extends TopiaEntity>> klassInterfaces = getInterfaces(
                klass,
                new HashSet<>()
        );

        for (TopiaEntityEnum contract : contracts) {
            Class<? extends TopiaEntity> contractClass = contract.getContract();
            if (klassInterfaces.contains(contractClass)) {
                return contractClass;
            }
        }
        return null;
    }

    @SuppressWarnings({"unchecked"})
    public static Set<Class<? extends TopiaEntity>> getInterfaces(
            Class<? extends TopiaEntity> klass,
            Set<Class<? extends TopiaEntity>> klassInterfaces) {
        if (klassInterfaces.contains(klass)) {
            // deja traite
            return klassInterfaces;
        }
        if (klass.isInterface()) {
            // on ajoute l'interface dans la liste
            // avec remplacement de toutes interfaces parent (promotion)
            // afin de n'avoir que les interfaces de plus plus haut niveau
            addInterface(klassInterfaces, klass);
            return klassInterfaces;
        }

        Class<?>[] interfaces = klass.getInterfaces();
        if (interfaces.length > 0) {
            // des interfaces detectees, on les ajoutes
            for (Class<?> interfac : interfaces) {
                getInterfaces(
                        (Class<? extends TopiaEntity>) interfac,
                        klassInterfaces
                );
            }
        }

        if (klass.getSuperclass() != null &&
                TopiaEntity.class.isAssignableFrom(klass.getSuperclass())) {
            getInterfaces(
                    (Class<? extends TopiaEntity>) klass.getSuperclass(),
                    klassInterfaces
            );
        }
        return klassInterfaces;
    }

    protected static void addInterface(
            Set<Class<? extends TopiaEntity>> interfaces,
            Class<? extends TopiaEntity> klass) {
        Iterator<Class<? extends TopiaEntity>> iterator = interfaces.iterator();
        while (iterator.hasNext()) {
            Class<? extends TopiaEntity> next = iterator.next();
            if (next.isAssignableFrom(klass)) {
                // cette interface herite de klass
                // donc on la supprime
                iterator.remove();
                continue;
            }
            if (klass.isAssignableFrom(next)) {
                // cette interface herite de klass
                // donc on la supprime
                return;
            }

        }
        interfaces.add(klass);
    }

}
