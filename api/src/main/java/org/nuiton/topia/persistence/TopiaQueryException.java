package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Map;

/**
 * Exception raised when an error occur in the context of a particular query. Catching this exception class allow you
 * to get the buggy request and its parameters.
 *
 * When catching such an exception you may ease development by logging all the provided information given by
 * {@link #getHql()} and {@link #getHqlParameters()} but it's considered dangerous since parameters can contain sensible
 * data (such as passwords) that should not be printed anywhere.
 *
 * @since 3.0
 */
public class TopiaQueryException extends TopiaException {

    private static final long serialVersionUID = 4374615882154083376L;

    protected String hql;

    protected Map<String, Object> hqlParameters;

    public TopiaQueryException(String hql, Map<String, Object> hqlParameters) {
        this.hql = hql;
        this.hqlParameters = hqlParameters;
    }

    public TopiaQueryException(String message, String hql, Map<String, Object> hqlParameters) {
        super(message);
        this.hql = hql;
        this.hqlParameters = hqlParameters;
    }

    public TopiaQueryException(String message, Throwable cause, String hql, Map<String, Object> hqlParameters) {
        super(message, cause);
        this.hql = hql;
        this.hqlParameters = hqlParameters;
    }

    public TopiaQueryException(Throwable cause, String hql, Map<String, Object> hqlParameters) {
        super(cause);
        this.hql = hql;
        this.hqlParameters = hqlParameters;
    }

    public String getHql() {
        return hql;
    }

    public Map<String, Object> getHqlParameters() {
        return hqlParameters;
    }
}
