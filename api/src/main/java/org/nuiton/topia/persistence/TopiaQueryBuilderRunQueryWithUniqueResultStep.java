package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Optional;

/**
 * A subset of {@link TopiaQueryBuilderRunQueryStep} for certain API methods that should not allow findAll.
 *
 * @author bleny
 * @since 3.0
 */
public interface TopiaQueryBuilderRunQueryWithUniqueResultStep<E extends TopiaEntity> {

    /**
     * @return true if the query returns at least one element
     */
    boolean exists();

    /**
     * Get the first element of a single-element result set.
     *
     * @return the first value from the set of result. Returned value can't be null
     * @throws TopiaNonUniqueResultException if the query returns more than one element.
     * @throws TopiaNoResultException        if the query does not return any result.
     */
    E findUnique() throws TopiaNoResultException, TopiaNonUniqueResultException;

    /**
     * Get the first element of a single-element result set or null if query result is empty.
     * <p>
     *
     * @return the first value from the set of result, or null of result set for given query is empty.
     * @throws TopiaNonUniqueResultException if the query returns more than one element.
     */
    E findUniqueOrNull() throws TopiaNonUniqueResultException;

    /**
     * Get the first element of a single-element result set.
     * <p>
     * If the call must return a result, prefer {@link #findUnique()}
     *
     * @return the first value from the set of result. It's an optional because the query may return no result.
     * @throws TopiaNonUniqueResultException if the query returns more than one element.
     */
    Optional<E> tryFindUnique() throws TopiaNonUniqueResultException;

}
