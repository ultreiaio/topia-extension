package org.nuiton.topia.persistence.internal;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.nuiton.topia.persistence.TopiaPersistenceContext;

import java.lang.ref.WeakReference;
import java.util.WeakHashMap;

/**
 * Class used to keep an association between an Hibernate Session and a TopiaPersistenceContext
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public class TopiaHibernateSessionRegistry {

    private static final Logger log = LogManager.getLogger(TopiaHibernateSessionRegistry.class);

    protected WeakHashMap<Session, WeakReference<TopiaPersistenceContext>> registry =
            new WeakHashMap<>();

    /**
     * Register the Session ↔ TopiaPersistenceContext couple
     *
     * @param session            the currently used Session
     * @param persistenceContext the current TopiaPersistenceContext using this Session
     */
    public void register(Session session, TopiaPersistenceContext persistenceContext) {
        if (log.isDebugEnabled()) {
            log.debug("New Session<->TopiaPersistenceContext registration");
        }
        Preconditions.checkArgument(session != null);
        Preconditions.checkArgument(persistenceContext != null);
        WeakReference<TopiaPersistenceContext> reference = new WeakReference<>(persistenceContext);
        registry.put(session, reference);
    }

    /**
     * Look for the TopiaPersistenceContext based on the given Hibernate session
     *
     * @param session the Hibernate Session to use
     * @return the TopiaPersistenceContext using this Session, or null if not found
     */
    public TopiaPersistenceContext getPersistenceContext(Session session) {
        Preconditions.checkArgument(session != null);
        WeakReference<TopiaPersistenceContext> reference = registry.get(session);
        TopiaPersistenceContext result = null;
        if (reference != null) {
            result = reference.get();
        }
        if (log.isTraceEnabled()) {
            log.trace("Get TopiaPersistenceContext from Session : " + (result == null ? "Not found" : "HIT !"));
        }
        return result;
    }

    /**
     * Removes the Session ↔ TopiaPersistenceContext association from the registry
     *
     * @param session the Hibernate Session to remove
     */
    public void unregister(Session session) {
        Preconditions.checkArgument(session != null);
        WeakReference<TopiaPersistenceContext> reference = registry.remove(session);
        if (log.isDebugEnabled()) {
            log.debug("Remove TopiaPersistenceContext from Session : " + (reference != null));
        }
    }

}
