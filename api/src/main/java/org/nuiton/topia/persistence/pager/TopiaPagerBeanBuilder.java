package org.nuiton.topia.persistence.pager;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * To build a {@link TopiaPagerBean} from scratch.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.14
 */
public class TopiaPagerBeanBuilder {

    protected final TopiaPagerBean bean;

    public TopiaPagerBeanBuilder() {
        this(new TopiaPagerBean());
    }

    public TopiaPagerBeanBuilder(TopiaPagerBean bean) {
        this.bean = bean;
    }

    public TopiaPagerBeanBuilder setFilterRuleGroup(FilterRuleGroupOperator groupOp) {
        bean.setGroupOp(groupOp);
        return this;
    }

    public TopiaPagerBeanBuilder setSortcolumn(String sortColumn) {
        bean.setSortColumn(sortColumn);
        return this;
    }

    public TopiaPagerBeanBuilder setSortAscendant(boolean sortAscendant) {
        bean.setSortAscendant(sortAscendant);
        return this;
    }

    public TopiaPagerBeanBuilder setFilterOperationGroup(String groupOp) {
        setFilterRuleGroup(FilterRuleGroupOperator.valueOf(groupOp));
        return this;
    }

    public TopiaPagerBeanBuilder addRules(Collection<Map<String, String>> rules) {
        for (Map<String, String> rule : rules) {
            addRule(rule);
        }
        return this;
    }

    public TopiaPagerBeanBuilder addRule(String op, String property, String value) {
        FilterRuleOperator operator = FilterRuleOperator.valueOf(op);
        return addRule(new FilterRule(operator, property, value));
    }

    public TopiaPagerBeanBuilder addRule(Map<String, String> ruleMap) {
        String op = ruleMap.get("op");
        String property = ruleMap.get("field");
        String value = ruleMap.get("data");
        return addRule(op, property, value);
    }

    public TopiaPagerBeanBuilder addRule(FilterRule rule) {
        List<FilterRule> rules = bean.getRules();
        if (rules == null) {
            rules = new LinkedList<>();
            bean.setRules(rules);
        }
        rules.add(rule);
        return this;
    }

    public TopiaPagerBean toBean() {
        return bean;
    }
}
