package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Exception thrown if one of the expected configuration entries is missing at runtime
 *
 * Created: 23 déc. 2005 23:04:28
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 */
public class TopiaNotFoundException extends TopiaException {

    private static final long serialVersionUID = -8206486077608923797L;

    /**
     * Default constructor.
     */
    public TopiaNotFoundException() {
    }

    /**
     * Constructor with {@code message}.
     *
     * @param message exception message
     */
    public TopiaNotFoundException(String message) {
        super(message);
    }

    /**
     * Constructor for a wrapped TopiaNotFoundException over a {@code cause} with a {@code message}.
     *
     * @param message exception message
     * @param cause   exception cause
     */
    public TopiaNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor for a wrapped TopiaNotFoundException over a {@code cause}.
     *
     * @param cause exception cause
     */
    public TopiaNotFoundException(Throwable cause) {
        super(cause);
    }
}
