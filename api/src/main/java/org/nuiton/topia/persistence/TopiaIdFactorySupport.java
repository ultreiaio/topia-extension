package org.nuiton.topia.persistence;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.lang.Objects2;
import io.ultreia.java4all.lang.Strings;

/**
 * Created on 08/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.60
 */
public class TopiaIdFactorySupport implements TopiaIdFactory {

    private static final long serialVersionUID = 1L;

    private final String packagePrefix;
    private final String idPrefix;

    public TopiaIdFactorySupport(String idPrefix, String packagePrefix) {
        this.idPrefix = idPrefix;
        this.packagePrefix = packagePrefix;
    }

    public String getPackagePrefix() {
        return packagePrefix;
    }

    public String getIdPrefix() {
        return idPrefix;
    }

    public String newTopiaId(String entityType) {
        return getIdFromClass(entityType) + getSeparator() + System.currentTimeMillis() + getSeparator() + getRandom();
    }

    public String newTopiaId(String entityType, String randomPart) {
        return getIdFromClass(entityType) + getSeparator() + System.currentTimeMillis() + getSeparator() + randomPart;
    }

    @Override
    public <E extends TopiaEntity> String newTopiaId(Class<E> entityClass, TopiaEntity topiaEntity) {
        if (!entityClass.isInterface()) {
            throw new IllegalArgumentException("Only interface is permit to create id: " + entityClass);
        }
        return getIdFromClass(entityClass.getName()) + getSeparator() + System.currentTimeMillis() + getSeparator() + getRandom();
    }

    public <E extends TopiaEntity> String newTopiaId(Class<E> entityClass, String idPrefix, String randomPart) {
        return getIdFromClass(entityClass.getName()) + getSeparator() + idPrefix + getSeparator() + randomPart;
    }

    @Override
    public <E extends TopiaEntity> String newTopiaId(Class<E> entityClass, String randomPart) {
        return getIdFromClass(entityClass.getName()) + getSeparator() + System.currentTimeMillis() + getSeparator() + randomPart;
    }

    @Override
    public <E extends TopiaEntity> Class<E> getClassName(String topiaId) {
        String className = getClassName0(topiaId);
        return Objects2.forName(className);
    }

    @Override
    public String getRandomPart(String topiaId) {
        return Strings.substringAfter(topiaId, getSeparator());
    }

    @Override
    public String getSeparator() {
        return "#";
    }

    @Override
    public boolean isTopiaId(String str) {
        if (str != null) {
            String className = getClassName0(str);
            Objects2.forName(className);
            return true;
        }
        return false;
    }

    private String getClassName0(String str) {
        return getClassFromId(Strings.substringBefore(str, getSeparator()));
    }

    private double getRandom() {
        double random = Math.random();
        while (Double.toString(random).contains("E-")) {
            random = Math.random();
        }
        return random;
    }

    private String getIdFromClass(String entityClassName) {
        return getIdPrefix() + Strings.removeStart(entityClassName, getPackagePrefix());
    }

    private String getClassFromId(String id) {
        return packagePrefix + Strings.removeStart(id, idPrefix);
    }
}

