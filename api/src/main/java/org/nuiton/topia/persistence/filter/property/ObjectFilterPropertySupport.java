package org.nuiton.topia.persistence.filter.property;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.filter.EntityFilterProperty;
import org.nuiton.topia.persistence.filter.FilterOperation;

import java.util.Set;
import java.util.function.Function;

/**
 * Created on 05/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.35
 */
public abstract class ObjectFilterPropertySupport<O> extends EntityFilterProperty<O> {

    public ObjectFilterPropertySupport(String propertyName, String classifier, Class<O> type, Function<String, O> factory, O defaultValue) {
        super(propertyName, FLAVORS_OBJECT, classifier, type, factory, defaultValue);
    }

    public ObjectFilterPropertySupport(String propertyName, Set<FilterOperation> flavors, String classifier, Class<O> type, String order, Function<String, O> factory, O defaultValue) {
        super(propertyName, flavors, classifier, type, order, factory, defaultValue);
    }

    public ObjectFilterPropertySupport(String propertyName, Set<FilterOperation> flavors, String classifier, Class<O> type, Function<String, O> factory, O defaultValue) {
        super(propertyName, flavors, classifier, type, factory, defaultValue);
    }

    public ObjectFilterPropertySupport(String propertyName, String classifier, Class<O> type, String order, Function<String, O> factory, O defaultValue) {
        super(propertyName, FLAVORS_OBJECT, classifier, type, order, factory, defaultValue);
    }

    @Override
    public boolean isPrimitive() {
        return false;
    }
}
