package org.nuiton.topia.persistence.filter;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaQueryBuilderAddCriteriaOrRunQueryStep;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Created on 29/06/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.30
 */
public class EntityFilterConsumer<E extends TopiaEntity> {

    private final List<EntityFilterProperty<?>> properties;
    private final Set<String> authorizedProperties;
    private final Set<String> authorizedOrders;
    private final List<String> naturalOrders;
    private final Map<String, EntityFilterProperty<?>> consumers;
    private final String labelColumnName;

    public static Set<String> getAuthorizedPropertyNames(List<EntityFilterProperty<?>> properties) {
        return properties.stream().map(EntityFilterProperty::getPropertyKey).sorted().collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public static Set<String> getAuthorizedOrders(List<EntityFilterProperty<?>> properties) {
        return properties.stream().filter(EntityFilterProperty::withOrder).map(EntityFilterProperty::getOrder).sorted().collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public static Map<String, EntityFilterProperty<?>> getPropertyConsumers(List<EntityFilterProperty<?>> properties) {
        Map<String, EntityFilterProperty<?>> result = new LinkedHashMap<>();
        properties.stream().sorted(Comparator.comparing(EntityFilterProperty::getPropertyKey)).forEach(property -> result.put(property.getPropertyKey(), property));
        return result;
    }

    public EntityFilterConsumer(String referentialLocale,
                                List<EntityFilterProperty<?>> properties,
                                Set<String> authorizedProperties,
                                Set<String> authorizedOrders,
                                List<String> naturalOrders,
                                Map<String, EntityFilterProperty<?>> consumers) {
        this.labelColumnName = referentialLocale;
        this.properties = Objects.requireNonNull(properties);
        this.authorizedProperties = Objects.requireNonNull(authorizedProperties);
        this.authorizedOrders = Objects.requireNonNull(authorizedOrders);
        this.naturalOrders = Objects.requireNonNull(naturalOrders);
        this.consumers = Objects.requireNonNull(consumers);
    }

    public final List<EntityFilterProperty<?>> properties() {
        return properties;
    }

    public final Set<String> authorizedProperties() {
        return authorizedProperties;
    }

    public final Set<String> authorizedOrders() {
        return authorizedOrders;
    }

    public final Map<String, EntityFilterProperty<?>> consumers() {
        return consumers;
    }

    public List<String> naturalOrders() {
        return naturalOrders;
    }

    public void checkFilter(ToolkitRequestFilter filter) {
        if (Objects.requireNonNull(filter).onProperties()) {
            Set<String> authorized = authorizedProperties();
            Set<String> unAuthorized = new LinkedHashSet<>(filter.getPropertiesKeys());
            unAuthorized.removeAll(authorized);
            if (!unAuthorized.isEmpty()) {
                throw new IllegalStateException(String.format("Unexpected filter property(ies): %s, possible values: %s", unAuthorized, authorized));
            }
            //FIXME Checker propertyValue (new flavor API on EntityFilterProperty)
        }
        if (filter.onOrders()) {
            Set<String> authorized = authorizedOrders();
            Set<String> unAuthorized = new LinkedHashSet<>(filter.getOrdersKeys());
            unAuthorized.removeAll(authorized);
            if (!unAuthorized.isEmpty()) {
                throw new IllegalStateException(String.format("Unexpected filter order(s): %s, possible values: %s", unAuthorized, authorized));
            }
        }
    }

    public void consume(TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> queryBuilder, ToolkitRequestFilter filter) {
        if (Objects.requireNonNull(filter).onId()) {
            consume0(queryBuilder, labelColumnName, EntityFilterProperty.ID, FilterOperation.EQUALS, filter.getId());
        } else if (filter.onProperties()) {
            for (Map.Entry<String, String> entry : filter.getProperties().entrySet()) {
                String propertyName = entry.getKey();
                for (String propertyValue : entry.getValue().split("\\s*~\\s*")) {
                    int index = propertyValue.indexOf("::");
                    FilterOperation flavor;
                    if (index == -1) {
                        // no flavor
                        flavor = FilterOperation.EQUALS;
                    } else {
                        // flavor detected
                        flavor = FilterOperation.fromFlavor(propertyValue.substring(0, index));
                        propertyValue = propertyValue.substring(index + 2);
                    }
                    consume0(queryBuilder, labelColumnName, propertyName, flavor, propertyValue);
                }
            }
        }
        TreeMap<String, OrderEnum> filterOrders = getFilterOrders(filter);
        if (!filterOrders.isEmpty()) {
            LinkedHashSet<String> orders = new LinkedHashSet<>();
            filterOrders.forEach((k, v) -> {
                String p;
                if (k.endsWith(EntityFilterProperty.CLASSIFIER_LABEL)) {
                    p = k.substring(0, k.length() - EntityFilterProperty.CLASSIFIER_LABEL.length()) + "." + labelColumnName;
                } else {
                    p = k;
                }
                orders.add(String.format("%s %s", EntityFilterProperty.replaceUnderscoreByDot(p), v));
            });
            queryBuilder.setOrder(orders);
        }
    }

    private TreeMap<String, OrderEnum> getFilterOrders(ToolkitRequestFilter filter) {
        TreeMap<String, OrderEnum> filterOrders;
        if (filter.onOrders()) {
            filterOrders = filter.getOrders();
        } else {
            filterOrders = new TreeMap<>();
            for (String naturalOrder : naturalOrders()) {
                filterOrders.put(naturalOrder, OrderEnum.ASC);
            }
        }
        return filterOrders;
    }

    protected void consume0(TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> queryBuilder, String labelColumnName, String propertyName, FilterOperation flavor, String propertyValue) {
        EntityFilterProperty<?> consumer = consumers.get(propertyName);
        Objects.requireNonNull(consumer).consume(queryBuilder, flavor, labelColumnName, propertyValue);
    }
}
