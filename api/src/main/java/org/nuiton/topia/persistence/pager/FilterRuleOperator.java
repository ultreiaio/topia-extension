package org.nuiton.topia.persistence.pager;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Map;

/**
 * Operator used in a rule.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.14
 */
public enum FilterRuleOperator {
    /**
     * Equals operator.
     */
    eq {
        @Override
        public String toHql(String paramName, String propertyName, Object data, Map<String, Object> filterParams) {
            String ruleFilter = propertyName + " = :" + paramName;
            filterParams.put(paramName, data);
            return ruleFilter;
        }
    },
    /**
     * Not equals operator.
     */
    ne {
        @Override
        public String toHql(String paramName, String propertyName, Object data, Map<String, Object> filterParams) {
            String ruleFilter = propertyName + " != :" + paramName;
            filterParams.put(paramName, data);
            return ruleFilter;
        }
    },
    /**
     * Contains operator.
     */
    cn {
        @Override
        public String toHql(String paramName, String propertyName, Object data, Map<String, Object> filterParams) {
            String ruleFilter = propertyName + " like :" + paramName;
            filterParams.put(paramName, "%" + data + "%");
            return ruleFilter;
        }
    },
    /**
     * Not contains operator.
     */
    nc {
        @Override
        public String toHql(String paramName, String propertyName, Object data, Map<String, Object> filterParams) {
            String ruleFilter = propertyName + " not like :" + paramName;
            filterParams.put(paramName, "%" + data + "%");
            return ruleFilter;
        }
    },
    /**
     * Begins with operator.
     */
    bw {
        @Override
        public String toHql(String paramName, String propertyName, Object data, Map<String, Object> filterParams) {
            String ruleFilter = propertyName + " like :" + paramName;
            filterParams.put(paramName, data + "%");
            return ruleFilter;
        }
    },
    /**
     * Not between with operator.
     */
    bn {
        @Override
        public String toHql(String paramName, String propertyName, Object data, Map<String, Object> filterParams) {
            String ruleFilter = propertyName + " not like :" + paramName;
            filterParams.put(paramName, data + "%");
            return ruleFilter;
        }
    },
    /**
     * Ends with operator.
     */
    ew {
        @Override
        public String toHql(String paramName, String propertyName, Object data, Map<String, Object> filterParams) {
            String ruleFilter = propertyName + " like :" + paramName;
            filterParams.put(paramName, "%" + data);
            return ruleFilter;
        }
    },
    /**
     * Not End with operator.
     */
    en {
        @Override
        public String toHql(String paramName, String propertyName, Object data, Map<String, Object> filterParams) {
            String ruleFilter = propertyName + " not like :" + paramName;
            filterParams.put(paramName, "%" + data);
            return ruleFilter;
        }
    },
    /**
     * Lesser than operator.
     */
    lt {
        @Override
        public String toHql(String paramName, String propertyName, Object data, Map<String, Object> filterParams) {
            String ruleFilter = propertyName + " < :" + paramName;
            filterParams.put(paramName, data);
            return ruleFilter;
        }
    },
    /**
     * Lesser or equals operator.
     */
    le {
        @Override
        public String toHql(String paramName, String propertyName, Object data, Map<String, Object> filterParams) {
            String ruleFilter = propertyName + " <= :" + paramName;
            filterParams.put(paramName, data);
            return ruleFilter;
        }
    },
    /**
     * Greater than operator.
     */
    gt {
        @Override
        public String toHql(String paramName, String propertyName, Object data, Map<String, Object> filterParams) {
            String ruleFilter = propertyName + " > :" + paramName;
            filterParams.put(paramName, data);
            return ruleFilter;
        }
    },
    /**
     * Greater or equals operator.
     */
    ge {
        @Override
        public String toHql(String paramName, String propertyName, Object data, Map<String, Object> filterParams) {
            String ruleFilter = propertyName + " >= :" + paramName;
            filterParams.put(paramName, data);
            return ruleFilter;
        }
    },
    /**
     * Is null operator.
     */
    nu {
        @Override
        public String toHql(String paramName, String propertyName, Object data, Map<String, Object> filterParams) {
            return propertyName + " is null";
        }
    },
    /**
     * Is not null operator.
     */
    nn {
        @Override
        public String toHql(String paramName, String propertyName, Object data, Map<String, Object> filterParams) {
            return propertyName + " is not null";
        }
    },
    /**
     * Is among operator.
     */
    in {
        @Override
        public String toHql(String paramName, String propertyName, Object data, Map<String, Object> filterParams) {
            throw new UnsupportedOperationException();
        }
    },
    /**
     * Not is among operator.
     */
    ni {
        @Override
        public String toHql(String paramName, String propertyName, Object data, Map<String, Object> filterParams) {
            throw new UnsupportedOperationException();
        }
    };

    public abstract String toHql(String paramName, String propertyName, Object data, Map<String, Object> filterParams);
}
