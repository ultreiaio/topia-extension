package org.nuiton.topia.persistence.filter;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaQueryBuilderAddCriteriaOrRunQueryStep;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

/**
 * Created on 05/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.34
 */
public abstract class EntityFilterProperty<O> {

    public static final String ID = "id";
    public static final String CLASSIFIER_ID = "_id";
    public static final String CLASSIFIER_CODE = "_code";
    public static final String CLASSIFIER_LABEL = "_label";

    public static final Set<FilterOperation> FLAVORS_EQUALS = new LinkedHashSet<>(List.of(
            FilterOperation.EQUALS,
            FilterOperation.NOT_EQUALS));
    public static final Set<FilterOperation> FLAVORS_PRIMITIVE = new LinkedHashSet<>(List.of(
            FilterOperation.EQUALS,
            FilterOperation.NOT_EQUALS,
            FilterOperation.MIN,
            FilterOperation.MAX));
    public static final Set<FilterOperation> FLAVORS_OBJECT = new LinkedHashSet<>(List.of(
            FilterOperation.EQUALS,
            FilterOperation.NOT_EQUALS,
            FilterOperation.NULL,
            FilterOperation.NOT_NULL));
    public static final Set<FilterOperation> FLAVORS_COMPARABLE = new LinkedHashSet<>(List.of(
            FilterOperation.EQUALS,
            FilterOperation.NOT_EQUALS,
            FilterOperation.NULL,
            FilterOperation.NOT_NULL,
            FilterOperation.MIN,
            FilterOperation.MAX));
    private final String propertyName;
    private final String propertyKey;
    private final Set<FilterOperation> flavors;
    private final Class<O> type;
    private final String order;
    private final Function<String, O> factory;
    private final O defaultValue;

    public static String replaceUnderscoreByDot(String key) {
        return key.replaceAll("_", ".");
    }

    public EntityFilterProperty(String propertyName, Set<FilterOperation> flavors, String classifier, Class<O> type, String order, Function<String, O> factory, O defaultValue) {
        this.propertyName = Objects.requireNonNull(propertyName);
        this.flavors = flavors;
        this.factory = factory;
        this.defaultValue = defaultValue;
        this.propertyKey = propertyName + Objects.requireNonNull(classifier);
        this.type = Objects.requireNonNull(type);
        this.order = order;
    }

    public EntityFilterProperty(String propertyName, Set<FilterOperation> flavors, String classifier, Class<O> type, Function<String, O> factory, O defaultValue) {
        this.propertyName = Objects.requireNonNull(propertyName);
        this.flavors = flavors;
        this.factory = factory;
        this.defaultValue = defaultValue;
        this.propertyKey = propertyName + Objects.requireNonNull(classifier);
        this.type = Objects.requireNonNull(type);
        this.order = propertyName;
    }

    protected final O convertPropertyValue(String propertyValue) {
        return factory.apply(propertyValue);
    }

    protected final <E extends TopiaEntity> void consume(TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> queryBuilder, FilterOperation flavor, String labelColumnName, String propertyValue) {
        O value;
        if (flavor.needValue()) {
            if (propertyValue == null || propertyValue.isBlank()) {
                value = defaultValue;
            } else {
                value = convertPropertyValue(propertyValue);
            }
        } else {
            value = null;
        }
        consumeObject(queryBuilder, flavor, labelColumnName, value);
    }

    public String getPropertyName() {
        return propertyName;
    }

    public Class<O> getType() {
        return type;
    }

    public String getPropertyKey() {
        return propertyKey;
    }

    public boolean withDefaultValue() {
        return defaultValue != null;
    }

    public abstract boolean isPrimitive();

    public Set<FilterOperation> getFlavors() {
        return flavors;
    }

    public String getPersistencePropertyKey(String labelColumnName) {
        String propertyKey = getPropertyKey();
        if (propertyKey.endsWith(CLASSIFIER_LABEL)) {
            propertyKey = getPropertyName() + "." + labelColumnName;
        }
        return replaceUnderscoreByDot(propertyKey);
    }

    public String getOrder() {
        return order;
    }

    public boolean withOrder() {
        return order != null;
    }


    protected <E extends TopiaEntity> void consumeObject(TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> queryBuilder, FilterOperation flavor, String labelColumnName, O value) {
        String persistencePropertyName = getPersistencePropertyKey(labelColumnName);
        switch (Objects.requireNonNull(flavor)) {
            case EQUALS:
                if (value == null) {
                    addNull(queryBuilder, persistencePropertyName);
                } else {
                    addEquals(queryBuilder, persistencePropertyName, value);
                }
                break;
            case NULL:
                addNull(queryBuilder, persistencePropertyName);
                break;
            case NOT_NULL:
                addNotNull(queryBuilder, persistencePropertyName);
                break;
            case NOT_EQUALS:
                addNotEquals(queryBuilder, persistencePropertyName, value);
                break;
        }
    }

    protected <E extends TopiaEntity> void addNull(TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> queryBuilder, String persistencePropertyName) {
        queryBuilder.addNull(persistencePropertyName);
    }

    protected <E extends TopiaEntity> void addNotNull(TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> queryBuilder, String persistencePropertyName) {
        queryBuilder.addNotNull(persistencePropertyName);
    }

    protected <E extends TopiaEntity> void addNotEquals(TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> queryBuilder, String persistencePropertyName, O value) {
        queryBuilder.addNotEquals(persistencePropertyName, value);
    }

    protected <E extends TopiaEntity> void addEquals(TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> queryBuilder, String persistencePropertyName, O value) {
        queryBuilder.addEquals(persistencePropertyName, value);
    }
}
