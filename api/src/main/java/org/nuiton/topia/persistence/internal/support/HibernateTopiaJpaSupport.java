package org.nuiton.topia.persistence.internal.support;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.loader.MultipleBagFetchException;
import org.hibernate.query.Query;
import org.nuiton.topia.persistence.QueryMissingOrderException;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaNonUniqueResultException;
import org.nuiton.topia.persistence.TopiaQueryException;
import org.nuiton.topia.persistence.support.TopiaHibernateSupport;
import org.nuiton.topia.persistence.support.TopiaJpaSupport;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * This class is the Hibernate implementation of TopiaJpaSupport. It realizes the bridge between the JPA specification
 * and the technical choice made for its implementation : Hibernate.
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public class HibernateTopiaJpaSupport implements TopiaJpaSupport {

    /**
     * Object to get Hibernate's Session, Configuration, ...
     */
    protected TopiaHibernateSupport hibernateSupport;

    /**
     * This flag permits to use (or not) the flush mode when doing queries.
     *
     * <p>The normal usage is to says yes (that's why the default value is {@code true}), in that case when doing
     * queries (says in method {@link #findAll(String, java.util.Map)} or
     * {@link #find(String, int, int, java.util.Map)}) it will use the flush mode
     * {@link org.hibernate.FlushMode#AUTO}).</p>
     *
     * <p>But sometimes, when doing a lot of queries (for some imports for example),
     * we do NOT want the session to be flushed each time we do a find, then you
     * can set this flag to {@code false} using the method {@link #setUseFlushMode(boolean)}</p>
     *
     * @since 2.5
     */
    protected boolean useFlushMode = true;

    public HibernateTopiaJpaSupport(TopiaHibernateSupport hibernateSupport) {
        this.hibernateSupport = hibernateSupport;
    }

    public TopiaHibernateSupport getHibernateSupport() {
        return hibernateSupport;
    }

    @Override
    public void setUseFlushMode(boolean useFlushMode) {
        this.useFlushMode = useFlushMode;
    }

    protected Query<?> prepareQuery(String jpaql, Map<String, Object> parameters) {
        checkHqlParameters(parameters);
        Query<?> query = hibernateSupport.getHibernateSession().createQuery(jpaql);
        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            String name = entry.getKey();
            Object value = entry.getValue();
            if (value.getClass().isArray()) {
                query.setParameterList(name, (Object[]) value);
            } else if (value instanceof Collection<?>) {
                query.setParameterList(name, (Collection<?>) value);
            } else {
                query.setParameter(name, value);
            }
        }
        // tchemit 2010-11-30 reproduce the same behaviour than before with the dao legacy
        if (useFlushMode) { // FIXME AThimel 06/08/14 I think this is the reason of the unexpected flush we have
            query.setFlushMode(FlushMode.AUTO);
        }
        return query;
    }

    protected void checkHqlParameters(Map<String, Object> parameters) {
        Preconditions.checkArgument(!parameters.containsKey("object"), "'object' is not a valid parameter name in HQL");
    }

    @Override
    public <T> List<T> findAll(String jpaql, Map<String, Object> parameters) {
        try {
            Query query = prepareQuery(jpaql, parameters);

            return query.list();
        } catch (MultipleBagFetchException mbfe) {
            throw new TopiaQueryException(
                    "unable to fetch multiple bags during findAll: " + mbfe.getBagRoles(),
                    mbfe,
                    jpaql,
                    parameters);
        } catch (RuntimeException he) {
            throw new TopiaQueryException(
                    "unable to findAll",
                    he,
                    jpaql,
                    parameters);
        }
    }

    /**
     * Like {@link #findAll(String, Map)} but getting a stream that may lazily fetch data.
     * <p>
     * Actual behavior rely on JPA implementation.
     * <p>
     * According to {@link Query#stream()}, caller should {@link Stream#close()} the stream.
     *
     * @since 3.4
     */
    @Override
    public <T> Stream<T> stream(String jpaql, Map<String, Object> parameters) {
        try {
            Query query = prepareQuery(jpaql, parameters);
            return query.stream();
        } catch (MultipleBagFetchException mbfe) {
            throw new TopiaQueryException(
                    "unable to fetch multiple bags during " + mbfe.getBagRoles(),
                    mbfe,
                    jpaql,
                    parameters);
        } catch (RuntimeException he) {
            throw new TopiaQueryException(
                    "unable to stream",
                    he,
                    jpaql,
                    parameters);
        }
    }

    @Override
    public <T> T findAny(String jpaql, Map<String, Object> parameters) {

        // Execute query, and ask for one result only
        List<T> results = find0(jpaql, 0, 0, parameters);

        // Return it or null
        return Iterables.getOnlyElement(results, null);
    }

    @Override
    public <T> T findUnique(String jpaql, Map<String, Object> parameters) {

        // Execute query, and ask for up to 2 results only
        List<T> results = find0(jpaql, 0, 1, parameters);

        // If there is more than 1 result, throw an exception
        if (results.size() > 1) {
            String message = String.format(
                    "Query '%s' returns more than 1 unique result", jpaql);
            throw new TopiaNonUniqueResultException(message, parameters);
        }

        // otherwise return the first one, or null
        return Iterables.getOnlyElement(results, null);
    }

    // FIXME AThimel 11/09/14 This method duplicates org.nuiton.topia.persistence.internal.AbstractTopiaDao.hqlContainsOrderBy()
    protected boolean hqlContainsOrderBy(String hql) {
        return hql.toLowerCase().contains("order by");
    }

    @Override
    public <T> List<T> find(String jpaql, int startIndex, int endIndex, Map<String, Object> parameters)
            throws QueryMissingOrderException {
        if (!hqlContainsOrderBy(jpaql)) {
            throw new QueryMissingOrderException(jpaql, parameters);
        }

        return find0(jpaql, startIndex, endIndex, parameters);
    }

    protected <T> List<T> find0(String jpaql, int startIndex, int endIndex, Map<String, Object> parameters) {
        try {
            Query query = prepareQuery(jpaql, parameters);

            // Set bounds
            query.setFirstResult(startIndex);
            if (endIndex >= 0 && endIndex != Integer.MAX_VALUE) {
                Preconditions.checkArgument(startIndex <= endIndex, "startIndex " + startIndex + " > " + "endIndex" + endIndex);
                query.setMaxResults(endIndex - startIndex + 1);
            }

            List result = query.list();
            return result;
        } catch (MultipleBagFetchException mbfe) {
            throw new TopiaQueryException(
                    "unable to fetch multiple bags " +
                            "during find page startIndex=" + startIndex + ", endIndex=" + endIndex
                            + ": " + mbfe.getBagRoles(),
                    mbfe,
                    jpaql,
                    parameters);
        } catch (RuntimeException e) {
            throw new TopiaQueryException(
                    "unable to find page startIndex=" + startIndex + ", endIndex=" + endIndex,
                    e,
                    jpaql,
                    parameters);
        }
    }

    @Override
    public int execute(String jpaql, Map<String, Object> parameters) {
        try {
            Query query = prepareQuery(jpaql, parameters);
            return query.executeUpdate();
        } catch (MultipleBagFetchException mbfe) {
            throw new TopiaQueryException(
                    "unable to fetch multiple bags during execute: " + mbfe.getBagRoles(),
                    mbfe,
                    jpaql,
                    parameters);
        } catch (RuntimeException e) {
            throw new TopiaQueryException(
                    "unable to execute query",
                    e,
                    jpaql,
                    parameters);
        }
    }

    @Override
    public void save(Object object) {
        try {
            hibernateSupport.getHibernateSession().save(object);
        } catch (HibernateException eee) {
            throw new TopiaException("Unable to 'save' instance", eee);
        }
    }

    @Override
    public void update(Object object) {
        try {
            hibernateSupport.getHibernateSession().update(object);
        } catch (HibernateException eee) {
            throw new TopiaException("Unable to 'update' instance", eee);
        }
    }

    @Override
    public void saveOrUpdate(Object object) {
        try {
            hibernateSupport.getHibernateSession().saveOrUpdate(object);
        } catch (HibernateException eee) {
            throw new TopiaException("Unable to 'saveOrUpdate' instance", eee);
        }
    }

    @Override
    public void delete(Object object) {
        try {
            hibernateSupport.getHibernateSession().delete(object);
        } catch (HibernateException eee) {
            throw new TopiaException("Unable to 'delete' instance", eee);
        }
    }

} // HibernateTopiaJpaSupport
