package org.nuiton.topia.persistence.filter.property;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaQueryBuilderAddCriteriaOrRunQueryStep;

import java.util.Date;
import java.util.function.Function;

/**
 * Created on 05/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.35
 */
public abstract class DateFilterPropertySupport<O extends Date> extends ComparableFilterPropertySupport<O> {

    public DateFilterPropertySupport(String propertyName, Class<O> type, Function<String, O> factory) {
        super(propertyName, type, factory, null);
    }

    @Override
    protected <E extends TopiaEntity> void addMin(TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> queryBuilder, String persistencePropertyName, O propertyValue) {
        queryBuilder.addGreaterOrEquals(persistencePropertyName, propertyValue);
    }

    @Override
    protected <E extends TopiaEntity> void addMax(TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> queryBuilder, String persistencePropertyName, O propertyValue) {
        queryBuilder.addLowerOrEquals(persistencePropertyName, propertyValue);
    }
}
