package org.nuiton.topia.persistence.internal;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Date;

/**
 * Base class of each entity. It contains the common attributes .
 * <p>
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 * @author Arnaud Thimel (Code Lutin)
 */
public abstract class AbstractTopiaEntity implements TopiaEntity {

    private static final long serialVersionUID = -7458577454878852241L;

    protected String topiaId;

    protected long topiaVersion;

    protected Date topiaCreateDate = new Date();

    transient protected boolean deleted;
    /**
     * Lazy helper class that manages all the property change notification machinery.
     * PropertyChangeSupport cannot be extended directly because it requires
     * a bean in the constructor, and the "this" argument is not valid until
     * after super construction. Hence, delegation instead of extension.
     *
     * @see #pcs()
     */
    private transient PropertyChangeSupport pcs;

    @Override
    public String getTopiaId() {
        return topiaId;
    }

    @Override
    public void setTopiaId(String v) {
        topiaId = v;
    }

    @Override
    public long getTopiaVersion() {
        return topiaVersion;
    }

    @Override
    public void setTopiaVersion(long v) {
        topiaVersion = v;
    }

    @Override
    public Date getTopiaCreateDate() {
        return topiaCreateDate;
    }

    @Override
    public void setTopiaCreateDate(Date topiaCreateDate) {
        this.topiaCreateDate = topiaCreateDate;
    }

    @Override
    public boolean isPersisted() {
        // Is or was the entity persisted ?
        boolean result = topiaId != null;
        // Is the entity deleted ?
        result &= !deleted;
        return result;
    }

    @Override
    public boolean isDeleted() {
        return deleted;
    }

    @Override
    public void notifyDeleted() {
        deleted = true;
    }

    /**
     * We are using the {@code topiaCreateDate} for the hashCode because it does not change through time.
     */
    @Override
    public int hashCode() {
        Date date = getTopiaCreateDate();
        //TC-20100220 : il se peut que la date de creation soit nulle
        // lorsque l'entite est utilise comme objet d'edition d'un formulaire
        // par exemple...
        return date == null ? 0 : date.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TopiaEntity)) {
            return false;
        }
        TopiaEntity other = (TopiaEntity) obj;
        if (getTopiaId() == null || other.getTopiaId() == null) {
            return false;
        }
        return getTopiaId().equals(other.getTopiaId());
    }


    //    @Override
    public final void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs().addPropertyChangeListener(listener);
    }

    //    @Override
    public final void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs().addPropertyChangeListener(propertyName, listener);
    }

    //    @Override
    public final void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs().removePropertyChangeListener(listener);
    }

    //    @Override
    public final void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs().removePropertyChangeListener(propertyName, listener);
    }

    /**
     * Report a bound property update to any registered listeners.
     * No event is fired if old and new are equal and non-null.
     * <p>
     * This is merely a convenience wrapper around the more general
     * firePropertyChange method that takes {@code
     * PropertyChangeEvent} value.
     *
     * @param propertyName The programmatic name of the property
     *                     that was changed.
     * @param oldValue     The old value of the property.
     * @param newValue     The new value of the property.
     */
    protected final void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        pcs().firePropertyChange(propertyName, oldValue, newValue);
    }

    protected final void firePropertyChange(String propertyName, Object newValue) {
        pcs().firePropertyChange(propertyName, null, newValue);
    }

    protected final void firePropertyChange(PropertyChangeEvent evt) {
        pcs().firePropertyChange(evt);
    }

    protected final PropertyChangeSupport pcs() {
        return pcs == null ? (pcs = new PropertyChangeSupport(this)) : pcs;
    }

}
