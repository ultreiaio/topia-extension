package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Map;

/**
 * Used to implement a service for Topia.
 *
 * A TopiaService is started at the very beginning of the {@link org.nuiton.topia.persistence.TopiaApplicationContext}
 * startup and provides user extra operations not available in the topia core replication, IO, etc.
 */
public interface TopiaService {

    /**
     * Initialize the service.
     *
     * @param topiaApplicationContext is the {@link org.nuiton.topia.persistence.TopiaApplicationContext}
     *                                to which the current service is attached. It's the service
     *                                responsibility to keep a reference to it.
     * @param serviceConfiguration all the configuration directives for this service.
     */
    void initTopiaService(TopiaApplicationContext<?> topiaApplicationContext, Map<String, String> serviceConfiguration);

    /**
     * Called when service is no longer necessary. This method is called when {@link TopiaApplicationContext#close}
     * is called.
     */
    void close();

}
