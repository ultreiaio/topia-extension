package org.nuiton.topia.persistence.jdbc;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Splitter;
import org.apache.commons.lang3.mutable.MutableInt;
import org.h2.util.StringUtils;
import io.ultreia.java4all.util.sql.SqlScript;
import io.ultreia.java4all.util.sql.SqlScriptReader;
import io.ultreia.java4all.util.sql.SqlScriptWriter;
import io.ultreia.java4all.util.sql.BlobsContainer;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPInputStream;

/**
 * FIXME: Merge this method with JdbcH2Helper.
 * Created by tchemit on 14/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class JdbcH2Helper2 extends JdbcH2Helper {

    public static final String CREATE_BLOB_TABLE = "CREATE TABLE IF NOT EXISTS SYSTEM_LOB_STREAM(ID INT NOT NULL, PART INT NOT NULL, CDATA VARCHAR, BDATA BINARY, PRIMARY KEY (ID, PART));\n" +
            "CREATE ALIAS IF NOT EXISTS SYSTEM_COMBINE_CLOB FOR \"org.h2.command.dml.ScriptCommand.combineClob\";\n" +
            "CREATE ALIAS IF NOT EXISTS SYSTEM_COMBINE_BLOB FOR \"org.h2.command.dml.ScriptCommand.combineBlob\";";
    public static final String INSERT_BLOB = "INSERT INTO SYSTEM_LOB_STREAM VALUES(%s, %s, NULL, '%s');";
    public static final String UPDATE_BLOB = "UPDATE %s SET %s = SYSTEM_COMBINE_BLOB(%s) WHERE topiaId = '%s';";

    public JdbcH2Helper2(JdbcConfiguration jdbcConfiguration) {
        super(jdbcConfiguration);
    }

    @Override
    public void restore(File file) {
        String options = "";
        try (InputStream in = new BufferedInputStream(new FileInputStream(file))) {
            in.mark(2);
            // read header to see if is compressed file
            int b = in.read();
            // redundant cast : int magic = ((int) in.read() << 8) | b;
            int magic = in.read() << 8 | b;
            in.reset();
            if (magic == GZIPInputStream.GZIP_MAGIC) {
                options += " COMPRESSION GZIP";
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        options += " CHARSET 'UTF8'";
        runUpdate("RUNSCRIPT FROM '" + file.getAbsolutePath() + "'" + options);
    }

    public void copy(SqlScript reader, SqlScriptWriter scriptWriter) throws IOException {
        try (SqlScriptReader scriptReader = reader.getLocation()) {
            scriptWriter.writeScript(scriptReader);
        }
        Set<BlobsContainer> blobsContainers = reader.getBlobsContainers();
        storeBlobs(blobsContainers, scriptWriter);
    }

    public void storeBlobs(Set<BlobsContainer> blobsContainers, SqlScriptWriter scriptWriter) {
        if (blobsContainers.isEmpty()) {
            return;
        }
        scriptWriter.writeSql(CREATE_BLOB_TABLE);
        final MutableInt idCount = new MutableInt();
        for (BlobsContainer blobsContainer : blobsContainers) {
            String tableName = blobsContainer.getTableName();
            String columnName = blobsContainer.getColumnName();
            for (Map.Entry<String, byte[]> entry : blobsContainer.getBlobsById().entrySet()) {
                String id = entry.getKey();
                final MutableInt subIdCount = new MutableInt();
                String content = StringUtils.convertBytesToHex(entry.getValue());
                Splitter.fixedLength(4096).split(content).forEach(
                        sqlPart -> {
                            String insertSql = String.format(INSERT_BLOB, idCount.getValue(), subIdCount.getAndIncrement(), sqlPart);
                            scriptWriter.writeSql(insertSql);
                        });
                String updateSql = String.format(UPDATE_BLOB, tableName, columnName, idCount.getAndIncrement(), id);
                scriptWriter.writeSql(updateSql);
            }
        }
    }

}
