package org.nuiton.topia.persistence;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import org.nuiton.topia.persistence.internal.AbstractTopiaPersistenceContext;

import java.util.Map;
import java.util.function.Supplier;

/**
 * Created on 19/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.61
 */
public abstract class TopiaDaoFactoryImpl implements TopiaDaoFactory {

    /**
     * To create new instance of dao.
     */
    private final Map<String, Supplier<TopiaDao<?>>> mapping;

    /**
     * Already loaded DAO cache within this persistence context
     */
    private final Map<String, TopiaDao<?>> daoCache;

    public TopiaDaoFactoryImpl(Map<String, Supplier<TopiaDao<?>>> mapping) {
        this.mapping = mapping;
        this.daoCache = Maps.newConcurrentMap();
    }

    protected abstract AbstractTopiaPersistenceContext persistenceContext();

    @Override
    public <E extends TopiaEntity> TopiaDao<E> getDao(Class<E> entityClass) {
        String entityClassName = entityClass.getName();
        @SuppressWarnings("unchecked") TopiaDao<E> dao = (TopiaDao<E>) daoCache.get(entityClassName);
        if (dao == null) {
            @SuppressWarnings({"unchecked", "rawtypes"}) Supplier<TopiaDao<E>> daoBuilder = (Supplier) mapping.get(entityClassName);
            dao = daoBuilder.get();
            dao.init(persistenceContext());
            daoCache.put(entityClassName, dao);
        }
        return dao;
    }

    @Override
    public void close() {
        daoCache.clear();
    }
}
