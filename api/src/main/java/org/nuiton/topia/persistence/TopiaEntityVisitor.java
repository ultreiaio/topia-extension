package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Visitor contract for any {@link TopiaEntity}.
 *
 * Created: 28 janv. 2009 18:10:34
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 * @author Tony Chemit - chemit@codelutin.com
 */
public interface TopiaEntityVisitor {

    /**
     * Start the visit of the given entity.
     *
     * @param entity the visited entity
     */
    void start(TopiaEntity entity);

    /**
     * Ends the visit of the given entity.
     *
     * @param entity the visited entity
     */
    void end(TopiaEntity entity);

    /**
     * Visit a none indexed property for the given entity.
     *
     * The property visited is defined by the other parameters.
     *
     * @param entity       the visited entity
     * @param propertyName the name of the visited property
     * @param type         the type of the visited property
     * @param value        the value of the visited property
     */
    void visit(TopiaEntity entity,
               String propertyName,
               Class<?> type,
               Object value);

    /**
     * Visit a collection property for the given entity.
     *
     * The property visited is defined by the other parameters.
     *
     * @param entity         the visited entity
     * @param propertyName   the name of the visited property
     * @param collectionType the type of the visited collection
     * @param type           the type of the visited property
     * @param value          the value of the visited property
     */
    void visit(TopiaEntity entity,
               String propertyName,
               Class<?> collectionType,
               Class<?> type,
               Object value);

    /**
     * Visit a indexed value from a collection property for the given entity.
     *
     * The property visited is defined by the other parameters.
     *
     * @param entity         the visited entity
     * @param propertyName   the name of the visited property
     * @param collectionType the type of the container of the visited property
     * @param type           the type of the visited property
     * @param index          the index of the visited property in his container
     * @param value          the value of the visited property
     */
    void visit(TopiaEntity entity,
               String propertyName,
               Class<?> collectionType,
               Class<?> type,
               int index,
               Object value);

    /**
     * Reset all states of the visitor.
     *
     * If you use internal states inside the visitor, this method should clean
     * all of them.
     *
     * This method should be invoked after usage of the visitor.
     */
    void clear();

}
