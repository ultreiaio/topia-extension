package org.nuiton.topia.persistence.filter;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaQueryBuilderAddCriteriaOrRunQueryStep;

import java.util.List;

/**
 * Created on 29/06/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.30
 */
public interface EntityFilterProvider<E extends TopiaEntity> {

    default List<E> filter(EntityFilterConsumer<E> filterConsumer, ToolkitRequestFilter filter, TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> queryBuilder) {
        filterConsumer.checkFilter(filter);
        queryBuilder.setCaseSensitive(filter.isCaseSensitive());
        filterConsumer.consume(queryBuilder, filter);
        return queryBuilder.findAll();
    }

}
