package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.hibernate.HibernateException;
import org.hibernate.tool.schema.internal.AbstractSchemaValidator;

import java.util.Arrays;

/**
 * This exception is throwed when the database schema is not suitable for the
 * current entities model. It means that a table or a column is missing
 *
 * @since 3.0
 */
public class SchemaValidationTopiaException extends TopiaException {

    /**
     * We can know that an {@link HibernateException} is about schema validation
     * if one of the stack trace element validate this predicate.
     */
    protected static final Predicate<StackTraceElement> IS_STACK_TRACE_ELEMENT_ABOUT_SCHEMA_VALIDATION =
            new Predicate<>() {

                @Override
                public boolean apply(StackTraceElement input) {
                    return input.getClassName().equals(AbstractSchemaValidator.class.getName())
                            && input.getMethodName().equals("doValidation");
                }
            };

    public SchemaValidationTopiaException(String message, HibernateException e) {
        super(message, e);
    }

    /**
     * If given {@link HibernateException} is about schema validation, throw a SchemaValidationTopiaException.
     *
     * @param hibernateException the incoming exception to test and wrap if necessary
     */
    public static void throwIfHibernateExceptionIsAboutSchemaValidation(HibernateException hibernateException) {
        // XXX brendan 06/05/15 dirty hack to know if e is about schema validation since Hibernate exception management sucks
        boolean stackTraceIsAboutSchemaValidation = Iterables.any(
                Arrays.asList(hibernateException.getStackTrace()),
                IS_STACK_TRACE_ELEMENT_ABOUT_SCHEMA_VALIDATION);
        if (stackTraceIsAboutSchemaValidation) {
            throw new SchemaValidationTopiaException(hibernateException.getMessage(), hibernateException);
        }
    }

}
