package org.nuiton.topia.persistence.jdbc;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.StandardSystemProperty;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Driver;

/**
 * Builder to create a {@link org.nuiton.topia.persistence.jdbc.JdbcConfiguration} instance
 */
public class JdbcConfigurationBuilder {

    private static final Logger log = LogManager.getLogger(JdbcConfigurationBuilder.class);

    protected static final String TIMESTAMP = String.valueOf(System.nanoTime());

    @SuppressWarnings("unchecked")
    public JdbcConfiguration forDatabase(String jdbcConnectionUrl, String jdbcConnectionUser, String jdbcConnectionPassword, String jdbcDriverClassName) {
        try {
            Class<?> jdbcDriverClass = Class.forName(jdbcDriverClassName);
            if (Driver.class.isAssignableFrom(jdbcDriverClass)) {
                return forDatabase(jdbcConnectionUrl, jdbcConnectionUser, jdbcConnectionPassword, (Class<? extends Driver>) jdbcDriverClass);
            } else {
                throw new IllegalArgumentException(jdbcDriverClassName + " seems not to be a JDBC driver: it does not implement " + Driver.class.getName());
            }
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException("unable to find JDBC driver class " + jdbcDriverClassName + " in classpath", e);
        }
    }

    public JdbcConfiguration forDatabase(String jdbcConnectionUrl, String jdbcConnectionUser, String jdbcConnectionPassword, Class<? extends Driver> jdbcDriverClass) {
        BeanJdbcConfiguration beanJdbcConfiguration = new BeanJdbcConfiguration();
        beanJdbcConfiguration.setJdbcConnectionUrl(jdbcConnectionUrl);
        beanJdbcConfiguration.setJdbcConnectionUser(jdbcConnectionUser);
        beanJdbcConfiguration.setJdbcConnectionPassword(jdbcConnectionPassword);
        beanJdbcConfiguration.setJdbcDriverClass(jdbcDriverClass);
        return beanJdbcConfiguration;
    }

    public JdbcConfiguration forDatabase(String jdbcConnectionUrl, String jdbcConnectionUser, String jdbcConnectionPassword) {
        String guessedJdbcDriverClassName = guessJdbcDriverClassName(jdbcConnectionUrl);
        if (guessedJdbcDriverClassName == null) {
            throw new UnsupportedOperationException("unable to guess JDBC driver class name for URL " + jdbcConnectionUrl);
        }
        return forDatabase(jdbcConnectionUrl, jdbcConnectionUser, jdbcConnectionPassword, guessedJdbcDriverClassName);
    }

    public JdbcConfiguration forPostgresqlDatabase(String jdbcConnectionUrl, String jdbcConnectionUser, String jdbcConnectionPassword) {
        return forDatabase(jdbcConnectionUrl, jdbcConnectionUser, jdbcConnectionPassword, getPostgreSqlJdbcDriverClassName());
    }

    public JdbcConfiguration forH2Database(String jdbcConnectionUrl, String jdbcConnectionUser, String jdbcConnectionPassword) {
        return forDatabase(jdbcConnectionUrl, jdbcConnectionUser, jdbcConnectionPassword, getH2JdbcDriverClassName());
    }

    public JdbcConfiguration forH2Database(String jdbcConnectionUrl) {
        return forH2Database(jdbcConnectionUrl, "sa", "");
    }

    public JdbcConfiguration forH2Database(File file) {
        String jdbcConnectionUrl = "jdbc:h2:file:" + file.getAbsolutePath();
        return forH2Database(jdbcConnectionUrl);
    }

//    public JdbcConfiguration forH2DatabaseInTempDirectory() {
//        return forH2Database(Files.createTempDirectory());
//    }

    public JdbcConfiguration forInMemoryH2Database() {
        throw new UnsupportedOperationException();
    }

    public JdbcConfiguration forTestDatabase(Class<?> testClass, String methodName) {
        String javaIoTmpDir = StandardSystemProperty.JAVA_IO_TMPDIR.value();
        Preconditions.checkState(
                StringUtils.isNotBlank(javaIoTmpDir),
                "'" + StandardSystemProperty.JAVA_IO_TMPDIR.key() + "' is not defined in environment"
        );
        File tempDirectoryFile = new File(javaIoTmpDir);
        try {
            Files.createDirectories(tempDirectoryFile.toPath());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        String path = Joiner.on(File.separator).join(TIMESTAMP, testClass.getName(), methodName, "h2");
        File file = new File(tempDirectoryFile, path);
        JdbcConfiguration jdbcConfiguration = forH2Database(file);
        if (log.isDebugEnabled()) {
            log.debug("will store H2 tests data in " + jdbcConfiguration.getJdbcConnectionUrl());
        }
        return jdbcConfiguration;
    }

    public String guessJdbcDriverClassName(String jdbcUrl) {
        String guessedJdbcDriverClassName;
        if (isDb2Url(jdbcUrl)) {
            guessedJdbcDriverClassName = getDb2JdbcDriverClassName();
        } else if (isDerbyUrl(jdbcUrl)) {
            guessedJdbcDriverClassName = getDerbyJdbcDriverClassName();
        } else if (isH2Url(jdbcUrl)) {
            guessedJdbcDriverClassName = getH2JdbcDriverClassName();
        } else if (isHsqlDbUrl(jdbcUrl)) {
            guessedJdbcDriverClassName = getHqlDbJdbcDriverClassName();
        } else if (isMysqlUrl(jdbcUrl)) {
            guessedJdbcDriverClassName = getMysqlJdbcDriverClassName();
        } else if (isMariaDbUrl(jdbcUrl)) {
            guessedJdbcDriverClassName = getMariaDbJdbcDriverClassName();
        } else if (isGoogleAppEngineUrl(jdbcUrl)) {
            guessedJdbcDriverClassName = getGooglaAppEngineJdbcDriverClassName();
        } else if (isOracleUrl(jdbcUrl)) {
            guessedJdbcDriverClassName = getOracleJdbcDriverClassName();
        } else if (isPostgreSqlUrl(jdbcUrl)) {
            guessedJdbcDriverClassName = getPostgreSqlJdbcDriverClassName();
        } else if (isJtdsUrl(jdbcUrl)) {
            guessedJdbcDriverClassName = getJdtsJdbcDriverClassName();
        } else if (isSqlServerUrl(jdbcUrl)) {
            guessedJdbcDriverClassName = getSqlServerJdbcDriverClassName();
        } else if (isSqliteUrl(jdbcUrl)) {
            guessedJdbcDriverClassName = getSqliteJdbcDriverClassName();
        } else if (isSqlDroidUrl(jdbcUrl)) {
            guessedJdbcDriverClassName = getSqlDroidJdbcDriverClassName();
        } else {
            guessedJdbcDriverClassName = null;
        }
        return guessedJdbcDriverClassName;
    }

    public String getDb2JdbcDriverClassName() {
        return "com.ibm.db2.jcc.DB2Driver";
    }

    public String getDerbyJdbcDriverClassName() {
        return "org.apache.derby.jdbc.EmbeddedDriver";
    }

    public String getH2JdbcDriverClassName() {
        return "org.h2.Driver";
    }

    public String getHqlDbJdbcDriverClassName() {
        return "org.hsqldb.jdbcDriver";
    }

    public String getMysqlJdbcDriverClassName() {
        return "com.mysql.jdbc.Driver";
    }

    public String getOracleJdbcDriverClassName() {
        return "oracle.jdbc.OracleDriver";
    }

    public String getGooglaAppEngineJdbcDriverClassName() {
        return "com.google.appengine.api.rdbms.AppEngineDriver";
    }

    public String getMariaDbJdbcDriverClassName() {
        return "org.mariadb.jdbc.Driver";
    }

    public String getPostgreSqlJdbcDriverClassName() {
        return "org.postgresql.Driver";
    }

    public String getJdtsJdbcDriverClassName() {
        return "net.sourceforge.jtds.jdbc.Driver";
    }

    public String getSqlServerJdbcDriverClassName() {
        return "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    }

    public String getSqliteJdbcDriverClassName() {
        return "org.sqlite.JDBC";
    }

    public String getSqlDroidJdbcDriverClassName() {
        return "org.sqldroid.SQLDroidDriver";
    }

    public boolean isDb2Url(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:db2:");
    }

    public boolean isSqlDroidUrl(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:sqldroid:");
    }

    public boolean isSqliteUrl(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:sqlite:");
    }

    public boolean isSqlServerUrl(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:sqlserver:");
    }

    public boolean isJtdsUrl(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:jtds:");
    }

    public boolean isPostgreSqlUrl(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:postgresql:");
    }

    public boolean isOracleUrl(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:oracle:");
    }

    public boolean isGoogleAppEngineUrl(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:google:");
    }

    public boolean isMariaDbUrl(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:mariadb:");
    }

    public boolean isMysqlUrl(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:mysql:");
    }

    public boolean isHsqlDbUrl(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:hsqldb:");
    }

    public boolean isH2Url(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:h2:");
    }

    public boolean isDerbyUrl(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:derby:");
    }
}
