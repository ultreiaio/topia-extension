/**
 * <p>This package contains most of the needed contracts when using ToPIA.</p>
 *
 * <p>To manipulate ToPIA you need to understand the role of both interfaces
 * {@link org.nuiton.topia.persistence.TopiaApplicationContext} and
 * {@link org.nuiton.topia.persistence.TopiaPersistenceContext}. The implementation of these contracts has been
 * generated in your own project under the name "XxxTopiaApplicationContext" and "XxxTopiaPersistenceContext" where
 * "Xxx" is the name of your project.</p>
 *
 * <p>Each entity managed by ToPIA implements the {@link org.nuiton.topia.persistence.TopiaEntity} contract through the
 * abstract class {@link org.nuiton.topia.persistence.internal.AbstractTopiaEntity}.</p>
 *
 * <p>Each Dao implements the {@link org.nuiton.topia.persistence.TopiaDao} contract through the abstract class
 * {@link org.nuiton.topia.persistence.internal.AbstractTopiaDao}.</p>
 *
 * <p>Each service implements the {@link org.nuiton.topia.persistence.TopiaService}</p>
 */
package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
