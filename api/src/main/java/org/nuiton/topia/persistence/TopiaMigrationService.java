package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * When {@link TopiaApplicationContext#initSchema()} is called, topia will look for
 * a topia-service that is able to migrate the database. Actually, it will look for
 * any service implementing this contract.
 *
 * @since 3.0
 */
public interface TopiaMigrationService extends TopiaService {

    /**
     * @return the schema version according to the meta-data information about
     * the actual current state of the schema in the database.
     */
    String getSchemaVersion() throws TopiaMigrationServiceException;

    /**
     * The schema has been created by ToPIA, the service should init the migration engine
     * and store the schema version somewhere.
     */
    void initOnCreateSchema() throws TopiaMigrationServiceException;

    /**
     * The schema already exists in an unknown version, service should update the schema
     * if necessary.
     */
    void runSchemaMigration() throws TopiaMigrationServiceException;

}
