package org.nuiton.topia.persistence.pager;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;

/**
 * Rule for a filtered pager.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.14
 */
public class FilterRule implements Serializable {

    private static final long serialVersionUID = 1L;

    protected final FilterRuleOperator op;

    protected final String field;

    protected final String data;

    public FilterRule(FilterRuleOperator op, String field, String data) {
        this.op = op;
        this.field = field;
        this.data = data;
    }

    public FilterRuleOperator getOp() {
        return op;
    }

    public String getField() {
        return field;
    }

    public String getData() {
        return data;
    }
}
