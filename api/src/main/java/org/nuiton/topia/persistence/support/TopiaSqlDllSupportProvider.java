package org.nuiton.topia.persistence.support;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.Optional;
import java.util.ServiceLoader;

/**
 * Created by tchemit on 05/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@SuppressWarnings("WeakerAccess")
public class TopiaSqlDllSupportProvider {

    private static final Logger log = LogManager.getLogger(TopiaSqlDllSupportProvider.class);

    private static TopiaSqlDllSupportProvider INSTANCE;
    /**
     * Available {@link TopiaSqlDllSupport} found in class-path indexed by their classifier.
     */
    protected final Map<String, TopiaSqlDllSupport> resources;

    public static synchronized TopiaSqlDllSupportProvider get() {
        return INSTANCE == null ? INSTANCE = new TopiaSqlDllSupportProvider() : INSTANCE;
    }

    protected TopiaSqlDllSupportProvider() {

        ImmutableMap.Builder<String, TopiaSqlDllSupport> resourcesBuilder = ImmutableMap.builder();
        for (TopiaSqlDllSupport resource : ServiceLoader.load(TopiaSqlDllSupport.class)) {
            resourcesBuilder.put(resource.getClassifier(), resource);
        }
        resources = resourcesBuilder.build();
        log.info(String.format("Found %d sql dll provider(s).", resources.size()));

    }

    public Optional<TopiaSqlDllSupport> get(String classifier) {
        return Optional.ofNullable(resources.get(classifier));
    }
}
