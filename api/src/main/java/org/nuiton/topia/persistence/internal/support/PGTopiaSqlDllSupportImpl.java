package org.nuiton.topia.persistence.internal.support;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlDllSupport;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * Created by tchemit on 05/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@SuppressWarnings("SqlDialectInspection")
@AutoService(TopiaSqlDllSupport.class)
public class PGTopiaSqlDllSupportImpl implements TopiaSqlDllSupport {

    public static String CLASSIFIER = "PG";

    private static final Logger log = LogManager.getLogger(PGTopiaSqlDllSupportImpl.class);

    @Override
    public String getClassifier() {
        return CLASSIFIER;
    }

    @Override
    public String getPrimaryKeyConstraintName(TopiaSqlSupport tx, String schemaName, String tableName) {
        return TopiaSqlDllSupport.selectFirst(tx, String.format("SELECT constraint_name FROM information_schema.table_constraints WHERE constraint_type = 'PRIMARY KEY' AND constraint_schema = '%s' AND table_name = '%s'", schemaName.toLowerCase(), tableName.toLowerCase()));
    }

    @Override
    public String getUniqueConstraintName(TopiaSqlSupport tx, String tableName, String columnName) {
        // recherche du nom de la constrainte
        List<String> result = new ArrayList<>();
        tx.doSqlWork(connection -> {
            // get table oid
            String oid = null;
            String sqlOid = "select oid from pg_class where relname=?;";
            {
                try (PreparedStatement ps = connection.prepareStatement(sqlOid)) {
                    ps.setString(1, tableName.toLowerCase());
                    try (ResultSet set = ps.executeQuery()) {
                        if (set.next()) {
                            oid = set.getString(1);
                            log.debug("found table oid " + tableName + ": " + oid);
                        }
                    } catch (Exception e) {
                        throw new SQLException("Could not obtain oid for table" + tableName, e);
                    }
                }
            }
            Objects.requireNonNull(oid);
            // get attribute num
            String attNumSql = "select attnum from pg_attribute where attrelid=? AND attname =?";
            String attNum = null;
            {
                try (PreparedStatement ps = connection.prepareStatement(attNumSql)) {
                    ps.setInt(1, Integer.parseInt(oid));
                    ps.setString(2, columnName.toLowerCase());
                    try (ResultSet set = ps.executeQuery()) {
                        if (set.next()) {
                            attNum = set.getString(1);
                            log.debug("found attribute " + columnName + " attNum : " + attNum);
                        }
                    } catch (Exception e) {
                        throw new SQLException("Could not obtain attNum for column" + columnName, e);
                    }
                }
            }
            String sql = "SELECT conname FROM pg_constraint where contype='u' and conrelid= ? and conkey = '{" + attNum + "}';";
            try (PreparedStatement ps = connection.prepareStatement(sql)) {
                ps.setInt(1, Integer.parseInt(oid));
                try (ResultSet set = ps.executeQuery()) {

                    if (set.next()) {
                        String constraintName = set.getString(1);
                        log.debug("found constraint of type unique for table " + tableName + " and column " + columnName + ": " + constraintName);
                        result.add(constraintName);
                    }
                } catch (Exception e) {
                    throw new SQLException("Could not obtain constraint unique for table" + tableName + " and column " + columnName, e);
                }
            }
        });
        if (result.isEmpty()) {
            throw new TopiaException("No unique constraint found for table " + tableName + " and column " + columnName);
        }
        return result.get(0);
    }

    @Override
    public String getFirstTableUniqueConstraintName(TopiaSqlSupport tx, String tableName) {
        List<String> result = TopiaSqlDllSupport.selectAll(tx, String.format("SELECT conname FROM pg_constraint where contype='u' and conrelid= (select oid from pg_class where relname='%s');", tableName.toLowerCase()));
        if (result.isEmpty()) {
            throw new TopiaException("Aucune contrainte de type unique trouvée sur la table " + tableName);
        }
        return result.get(0);
    }

    @Override
    public Set<String> getConstraintNames(TopiaSqlSupport tx, String tableName) {
        return TopiaSqlDllSupport.selectAllAsSet(tx, String.format("SELECT DISTINCT conname FROM pg_constraint WHERE ( contype='u' OR contype='f' ) AND conrelid = (SELECT oid FROM pg_class WHERE relname='%s');", tableName).toLowerCase());
    }

    @Override
    public Set<String> getForeignKeyConstraintNames(TopiaSqlSupport tx, String tableName) {
        return TopiaSqlDllSupport.selectAllAsSet(tx, String.format("SELECT DISTINCT conname FROM pg_constraint WHERE ( contype='f' ) AND conrelid = (SELECT oid FROM pg_class WHERE relname='%s');", tableName).toLowerCase());
    }

    @Override
    public String getForeignKeyConstraintName(TopiaSqlSupport tx, String schemaName, String tableName, String columnName, boolean mustExists) {
        List<String> result = new ArrayList<>();
        tx.doSqlWork(connection -> {
            // get table oid
            String oid = null;
            String sqlOid = "select oid from pg_class where relnamespace = (select oid from pg_catalog.pg_namespace where nspname=?) AND relname=?;";
            {
                try (PreparedStatement ps = connection.prepareStatement(sqlOid)) {
                    ps.setString(1, schemaName.toLowerCase());
                    ps.setString(2, tableName.toLowerCase());
                    try (ResultSet set = ps.executeQuery()) {
                        if (set.next()) {
                            oid = set.getString(1);
                            log.debug("found table oid " + tableName + ": " + oid);
                        }
                    }
                } catch (Exception e) {
                    throw new SQLException("Could not obtain oid for table" + tableName, e);
                }
            }
            Objects.requireNonNull(oid);
            // get attribute num
            String attNumSql = "select attnum from pg_attribute where attrelid=? AND attname =?";
            String attNum = null;
            {
                try (PreparedStatement ps = connection.prepareStatement(attNumSql)) {
                    ps.setInt(1, Integer.parseInt(oid));
                    ps.setString(2, columnName.toLowerCase());
                    try {
                        try (ResultSet set = ps.executeQuery()) {
                            if (set.next()) {
                                attNum = set.getString(1);
                                log.debug("found attribute " + columnName + " attNum : " + attNum);
                            }
                        }
                    } catch (Exception e) {
                        throw new SQLException("Could not obtain attNum for column" + columnName, e);
                    }
                }
            }

            String sql = "SELECT DISTINCT conname FROM pg_constraint WHERE contype='f' AND conrelid = ? AND conkey = '{" + attNum + "}';";
            try (PreparedStatement ps = connection.prepareStatement(sql)) {
                ps.setInt(1, Integer.parseInt(oid));
                try {
                    try (ResultSet set = ps.executeQuery()) {
                        while (set.next()) {
                            String constraintName = set.getString(1);
                            result.add(constraintName);
                        }
                    }
                } catch (Exception e) {
                    throw new SQLException("Could not obtain constraints for table " + tableName, e);
                }
            }
        });
        String constraintName;
        if (result.isEmpty()) {
            if (mustExists) {
                throw new IllegalStateException("Could not find constraint name for " + schemaName + "." + tableName + "." + columnName);
            }
            constraintName = null;
        } else {
            constraintName = result.get(0);
        }
        return constraintName;
    }

    @Override
    public Set<String> getUniqueKeyConstraintNames(TopiaSqlSupport tx, String tableName) {
        return TopiaSqlDllSupport.selectAllAsSet(tx, String.format("SELECT DISTINCT conname FROM pg_constraint WHERE ( contype='u' ) AND conrelid = (SELECT oid FROM pg_class WHERE relname='%s');", tableName.toLowerCase()));
    }

    @Override
    public Set<String> removeFK(TopiaSqlSupport tx, String tableName) {
        Set<String> result = new LinkedHashSet<>();
        Set<String> fkNames = getForeignKeyConstraintNames(tx, tableName);
        for (String contrainstName : fkNames) {
            result.add(String.format("ALTER TABLE %s DROP CONSTRAINT %s;", tableName, contrainstName));
        }
        return result;
    }

    @Override
    public String removeFK(TopiaSqlSupport tx, String schemaName, String tableName, String columnName) {
        String contrainstName = getForeignKeyConstraintName(tx, schemaName, tableName, columnName, true);
        return String.format("ALTER TABLE %s.%s DROP CONSTRAINT %s;", schemaName, tableName, contrainstName);
    }

    @Override
    public Optional<String> removeFKIfExists(TopiaSqlSupport tx, String schemaName, String tableName, String columnName) {
        String constraintName = getForeignKeyConstraintName(tx, schemaName, tableName, columnName, false);
        if (constraintName != null) {
            return Optional.of(String.format("ALTER TABLE %s.%s DROP CONSTRAINT %s;", schemaName, tableName, constraintName));
        }
        return Optional.empty();
    }

    @Override
    public Optional<String> removePKIfExists(TopiaSqlSupport tx, String schemaName, String tableName) {
        String constraintName = getPrimaryKeyConstraintName(tx, schemaName, tableName);
        if (constraintName != null) {
            return Optional.of(String.format("ALTER TABLE %s.%s DROP CONSTRAINT %s;", schemaName, tableName, constraintName));
        }
        return Optional.empty();
    }

    @Override
    public Set<String> removeUK(TopiaSqlSupport tx, String tableName) {
        Set<String> result = new LinkedHashSet<>();
        Set<String> uKNames = getUniqueKeyConstraintNames(tx, tableName);
        for (String constraintName : uKNames) {
            result.add(String.format("ALTER TABLE %s DROP CONSTRAINT %s;", tableName, constraintName));
        }
        return result;
    }

    @Override
    public String dropSchema(TopiaSqlSupport tx, String schemaName) {
        return String.format("DROP SCHEMA %s CASCADE;", schemaName);
    }

    @Override
    public String dropTable(TopiaSqlSupport tx, String schemaName, String tableName) {
        return String.format("DROP TABLE %s.%s CASCADE;", schemaName, tableName);
    }
}
