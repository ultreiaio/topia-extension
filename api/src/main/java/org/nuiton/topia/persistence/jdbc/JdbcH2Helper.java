package org.nuiton.topia.persistence.jdbc;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.io.IOUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.zip.GZIPInputStream;

public class JdbcH2Helper extends JdbcHelper {

    public JdbcH2Helper(JdbcConfiguration jdbcConfiguration) {
        super(jdbcConfiguration);
    }

    public boolean isTableExist(String tableName) {
        return isTableExist(null, tableName);
    }

    /**
     * <p>
     * Tests if a given table exists. WARNING : this may not work is the tableName is not exactly the same.
     * </p>
     * This method is designed for H2 only.
     *
     * @param schema    schema name filter
     * @param tableName table name filter
     * @return {@code true} if table exists.
     */
    public boolean isTableExist(String schema, String tableName) {
        Connection connection = null;
        ResultSet resultSet = null;
        try {
            connection = openConnection();
            resultSet = connection.getMetaData().getTables(null, schema, tableName, null);
            return resultSet.next();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeQuietly(resultSet);
            closeQuietly(connection);
        }
    }

    /**
     * Backup database in gzip compressed file.
     *
     * @param file     file to write backup
     * @param compress if true then use gzip to compress file
     */
    public void backup(File file, boolean compress) {
        String options = "";
        if (compress) {
            options += " COMPRESSION GZIP";
        }
        runUpdate("SCRIPT TO '" + file.getAbsolutePath() + "'" + options);
    }

    /**
     * Read database from gzip compressed file
     *
     * @param file the source file to use for restore
     * @throws FileNotFoundException if file does not exist
     */
    public void restore(File file) throws FileNotFoundException {
        String options = "";
        InputStream in = null;
        try {
            in = new BufferedInputStream(new FileInputStream(file));
            in.mark(2);
            // read header to see if is compressed file
            int b = in.read();
            // redundant cast : int magic = ((int) in.read() << 8) | b;
            int magic = in.read() << 8 | b;
            in.reset();
            if (magic == GZIPInputStream.GZIP_MAGIC) {
                options += " COMPRESSION GZIP";
            }
            in.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            IOUtils.closeQuietly(in);
        }
        runUpdate("RUNSCRIPT FROM '" + file.getAbsolutePath() + "'" + options);
    }

    public void clear(boolean dropDatabase) {
        String sql = "DROP ALL OBJECTS";
        if (dropDatabase) {
            sql += " DELETE FILES";
        }
        runUpdate(sql);
    }

}
