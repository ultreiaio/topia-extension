package org.nuiton.topia.persistence.jdbc;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaException;

import java.sql.Connection;
import java.util.function.Consumer;

/**
 * FIXME: Merge this method with jdbcHelper.
 * <p>
 * Created by tchemit on 13/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class JdbcHelper2 extends JdbcHelper {

    public JdbcHelper2(JdbcConfiguration jdbcConfiguration) {
        super(jdbcConfiguration);
    }

    public void consume(Consumer<Connection> consumer) {
        try {
            try (Connection connection = openConnection()) {
                consumer.accept(connection);
            }
        } catch (Exception e) {
            throw new TopiaException(e);
        }
    }

}
