package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.pager.PaginationParameter;
import org.nuiton.topia.persistence.pager.PaginationResult;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * This interface represents different common operations that a user may do after a query is defined (using the
 * {@link TopiaQueryBuilderAddCriteriaStep})
 * <p>
 * There are different methods according to the supposed existence or uniqueness of the result. Also some methods may be
 * used only if order is defined in query.
 * <p>
 *
 * @author bleny
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public interface TopiaQueryBuilderRunQueryStep<E extends TopiaEntity>
        extends TopiaQueryBuilderRunQueryWithUniqueResultStep<E> {

    /**
     * This method is equivalent as calling {@link Collection#size()} after doing a {@link #findAll()} but it
     * may be faster.
     *
     * @return the number of that the query would have returned, if executed
     */
    long count();

    /**
     * Get the first element of the non-empty result set.
     *
     * @return the first value from the set of result, according to given order. Returned value can't be null
     * @throws QueryMissingOrderException if you the query misses an order clause
     * @throws TopiaNoResultException     if the query does not return any result
     */
    E findFirst() throws QueryMissingOrderException, TopiaNoResultException;

    /**
     * Get the first element of the result set or null if query result is empty.
     * <p>
     * This method duplicates {@link #tryFindFirst()} but allows you to prevent using Guava in you project.
     *
     * @return the first value from the set of result, according to given order, or null of result set for given query
     * is empty
     * @throws QueryMissingOrderException if you the query misses an order clause
     */
    E findFirstOrNull() throws QueryMissingOrderException;

    /**
     * Get the first element of the result set.
     * <p>
     * If the call must return a result, prefer {@link #findFirst()}
     *
     * @return the first value from the set of result, according to given order. It's an optional because the query may
     * return no result.
     * @throws QueryMissingOrderException if you the query misses an order clause
     */
    Optional<E> tryFindFirst() throws QueryMissingOrderException;

    /**
     * Get the an element of the non-empty result set.
     * <p>
     * This method does not guarantee any order as no ordering clause is mandatory
     *
     * @return the first value from the set of result, without any order guarantee. Returned value can't be null
     * @throws TopiaNoResultException if the query does not return any result
     */
    E findAny() throws TopiaNoResultException;

    /**
     * Get the an element of the result set or null if query result is empty.
     * <p>
     * This method does not guarantee any order as no ordering clause is mandatory.
     * <p>
     * This method duplicates {@link #tryFindAny()} but allows you to prevent using Guava in you project.
     *
     * @return the first value from the set of result, without any order guarantee. Returned value can be null
     */
    E findAnyOrNull();

    /**
     * Get the an element of the result set.
     * <p>
     * This method does not guarantee any order as no ordering clause is mandatory.
     * <p>
     * If the call must return a result, prefer {@link #findAny()}
     *
     * @return the first value from the set of result, without any order guarantee. It's an optional because the query
     * may return no result.
     */
    Optional<E> tryFindAny();

    /**
     * Get all the elements of the result set.
     *
     * @return the full list of results.
     */
    List<E> findAll();

    /**
     * Like {@link #findAll()} but getting a stream that may lazily fetch data.
     * <p>
     * Actual behavior rely on implementation.
     * <p>
     * Caller should {@link Stream#close()} the stream.
     *
     * @since 3.4
     */
    Stream<E> stream();

    /**
     * Get the elements with the given bounds.
     *
     * @param startIndex inclusive index of the first element to return. This value is 0-based
     * @param endIndex   inclusive index of the last element to return. A value lower than 0 means infinite upper bound.
     * @return the list of loaded results.
     */
    List<E> find(int startIndex, int endIndex);

    /**
     * Get the elements according to the given {@link PaginationParameter}.
     * <p>
     * Use {@link #findPage(PaginationParameter)} to get a {@link PaginationResult} instead of a list.
     *
     * @param page information about the page to load.
     * @return the list of loaded results.
     * @see PaginationParameter
     */
    List<E> find(PaginationParameter page);

    /**
     * Get the elements according to the given {@link PaginationParameter}.
     * <p>
     * Use {@link #find(PaginationParameter)} to get a list instead of {@link PaginationResult}.
     * <p>
     * Note: compared to {@link #find(PaginationParameter)}, this method performs an additional statement to get the
     * total elements count.
     *
     * @param page information about the page to load.
     * @return the list of loaded results.
     * @see PaginationParameter
     * @see PaginationResult
     */
    PaginationResult<E> findPage(PaginationParameter page);

    /**
     * Get all the elements identifiers of the result set.
     * <p>
     * Note: compared to {@link #findAll()}, this method will not load entities, only its identifier.
     *
     * @return the full list of results ids.
     */
    List<String> findAllIds();

    /**
     * Get the elements identifiers with the given bounds.
     * <p>
     * Note: compared to {@link #find(int, int)}, this method will not load entities, only its identifier.
     *
     * @param startIndex inclusive index of the first element to return. This value is 0-based
     * @param endIndex   inclusive index of the last element to return. A value lower than 0 means infinite upper bound.
     * @return the list of loaded results ids.
     */
    List<String> findIds(int startIndex, int endIndex);

    /**
     * Get the elements identifiers according to the given {@link PaginationParameter}.
     * <p>
     * Use {@link #findIdsPage(PaginationParameter)} to get a {@link PaginationResult} instead of a list.
     * <p>
     * Note: compared to {@link #find(PaginationParameter)}, this method will not load entities, only its identifier.
     *
     * @param page information about the page to load.
     * @return the list of loaded results ids.
     * @see PaginationParameter
     */
    List<String> findIds(PaginationParameter page);

    /**
     * Get the elements identifiers according to the given {@link PaginationParameter}.
     * <p>
     * Use {@link #findIds(PaginationParameter)} to get a list instead of {@link PaginationResult}.
     * <p>
     * Note: compared to {@link #findIds(PaginationParameter)}, this method performs an additional statement to get the
     * total elements identifiers count.
     * <p>
     * Note: compared to {@link #findPage(PaginationParameter)}, this method will not load entities, only its identifier
     *
     * @param page information about the page to load.
     * @return the list of loaded results.
     * @see PaginationParameter
     * @see PaginationResult
     */
    PaginationResult<String> findIdsPage(PaginationParameter page);

}
