package org.nuiton.topia.persistence.internal;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.HqlAndParametersBuilder;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaQueryBuilderAddCriteriaOrRunQueryStep;
import org.nuiton.topia.persistence.TopiaQueryBuilderRunQueryStep;
import org.nuiton.topia.persistence.pager.PaginationOrder;
import org.nuiton.topia.persistence.pager.PaginationParameter;
import org.nuiton.topia.persistence.pager.PaginationResult;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created on 13/05/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class AbstractTopiaDaoQueryBuilderAddCriteriaOrRunQueryStep<E extends TopiaEntity> implements TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> {

    protected AbstractTopiaDao<E> topiaDao;

    protected HqlAndParametersBuilder<E> hqlAndParametersBuilder;

    protected AbstractTopiaDaoQueryBuilderAddCriteriaOrRunQueryStep(AbstractTopiaDao<E> topiaDao, HqlAndParametersBuilder<E> hqlAndParametersBuilder) {
        this.topiaDao = topiaDao;
        this.hqlAndParametersBuilder = hqlAndParametersBuilder;
    }

    @Override
    public AbstractTopiaDaoQueryBuilderRunQueryStep<E> setOrderByArguments(LinkedHashSet<String> orderByArguments) {
        hqlAndParametersBuilder.setOrderByArguments(orderByArguments);
        return getNextStep();
    }

    @Override
    public AbstractTopiaDaoQueryBuilderRunQueryStep<E> setOrderByArguments(String... orderByArguments) {
        hqlAndParametersBuilder.setOrderByArguments(orderByArguments);
        return getNextStep();
    }

    @Override
    public TopiaQueryBuilderRunQueryStep<E> setOrderByArguments(Collection<PaginationOrder> paginationOrders) {
        hqlAndParametersBuilder.setOrderByArguments(paginationOrders);
        return getNextStep();
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addEquals(String property, Object value) {
        hqlAndParametersBuilder.addEquals(property, value);
        return this;
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addNotEquals(String property, Object value) {
        hqlAndParametersBuilder.addNotEquals(property, value);
        return this;
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addIn(String property, Collection<?> values) {
        hqlAndParametersBuilder.addIn(property, values);
        return this;
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addNotIn(String property, Collection<?> values) {
        hqlAndParametersBuilder.addNotIn(property, values);
        return this;
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addContains(String property, Object value) {
        hqlAndParametersBuilder.addContains(property, value);
        return this;
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addNotContains(String property, Object value) {
        hqlAndParametersBuilder.addNotContains(property, value);
        return this;
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addNull(String property) {
        hqlAndParametersBuilder.addNull(property);
        return this;
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addNotNull(String property) {
        hqlAndParametersBuilder.addNotNull(property);
        return this;
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addTopiaIdEquals(String property, String topiaId) {
        hqlAndParametersBuilder.addTopiaIdEquals(property, topiaId);
        return this;
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addTopiaIdIn(String property, Collection<String> topiaIds) {
        hqlAndParametersBuilder.addTopiaIdIn(property, topiaIds);
        return this;
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addTopiaIdNotEquals(String property, String topiaId) {
        hqlAndParametersBuilder.addTopiaIdNotEquals(property, topiaId);
        return this;
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addTopiaIdNotIn(String property, Collection<String> topiaIds) {
        hqlAndParametersBuilder.addTopiaIdNotIn(property, topiaIds);
        return this;
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addFetch(String property) {
        hqlAndParametersBuilder.addFetch(property);
        return this;
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addAllFetches(String property, String... otherProperties) {
        hqlAndParametersBuilder.addAllFetches(property, otherProperties);
        return this;
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addAllFetches(Collection<String> properties) {
        hqlAndParametersBuilder.addAllFetches(properties);
        return this;
    }

    // shortcuts to next step

    @Override
    public boolean exists() {
        return getNextStep().exists();
    }

    @Override
    public E findAnyOrNull() {
        return getNextStep().findAnyOrNull();
    }

    @Override
    public E findUniqueOrNull() {
        return getNextStep().findUniqueOrNull();
    }

    @Override
    public E findAny() {
        return getNextStep().findAny();
    }

    @Override
    public E findUnique() {
        return getNextStep().findUnique();
    }

    @Override
    public E findFirst() {
        return getNextStep().findFirst();
    }

    @Override
    public E findFirstOrNull() {
        return getNextStep().findFirstOrNull();
    }

    @Override
    public Optional<E> tryFindAny() {
        return getNextStep().tryFindAny();
    }

    @Override
    public Optional<E> tryFindFirst() {
        return getNextStep().tryFindFirst();
    }

    @Override
    public Optional<E> tryFindUnique() {
        return getNextStep().tryFindUnique();
    }

    @Override
    public List<E> findAll() {
        return getNextStep().findAll();
    }

    @Override
    public Stream<E> stream() {
        return getNextStep().stream();
    }

    @Override
    public List<E> find(int startIndex, int endIndex) {
        return getNextStep().find(startIndex, endIndex);
    }

    @Override
    public List<E> find(PaginationParameter page) {
        return getNextStep().find(page);
    }

    @Override
    public PaginationResult<E> findPage(PaginationParameter page) {
        return getNextStep().findPage(page);
    }

    @Override
    public long count() {
        return getNextStep().count();
    }

    @Override
    public List<String> findIds(int startIndex, int endIndex) {
        return getNextStep().findIds(startIndex, endIndex);
    }

    @Override
    public List<String> findIds(PaginationParameter page) {
        return getNextStep().findIds(page);
    }

    @Override
    public PaginationResult<String> findIdsPage(PaginationParameter page) {
        return getNextStep().findIdsPage(page);
    }

    @Override
    public List<String> findAllIds() {
        return getNextStep().findAllIds();
    }

    @Override
    public void whereStringEquals(String propertyName, String propertyValue) {
        if (propertyValue.contains("%")) {
            addLike(propertyName, propertyValue);
        } else {
            addEquals(propertyName, propertyValue);
        }
    }

    @Override
    public void whereStringNotEquals(String propertyName, String propertyValue) {
        if (propertyValue.contains("%")) {
            addNotLike(propertyName, propertyValue);
        } else {
            String alias = hqlAndParametersBuilder.getAlias();
            String hqlParameterName = hqlAndParametersBuilder.putHqlParameterWithAvailableName(propertyName, propertyValue);
            hqlAndParametersBuilder.addWhereClause(String.format("( %1$s.%2$s is null OR %1$s.%2$s != :%3$s )", alias, propertyName, hqlParameterName));
        }
    }

    @Override
    public void addLowerThan(String property, java.util.Date date) {
        hqlAndParametersBuilder.addLowerThan(property, date);
    }

    @Override
    public void addLowerOrEquals(String property, java.util.Date date) {
        hqlAndParametersBuilder.addLowerOrEquals(property, date);
    }

    @Override
    public void addGreaterThan(String property, java.util.Date date) {
        hqlAndParametersBuilder.addGreaterThan(property, date);
    }

    @Override
    public void addGreaterOrEquals(String property, java.util.Date date) {
        hqlAndParametersBuilder.addGreaterOrEquals(property, date);
    }

    public void addLowerThan(String property, Number number) {
        hqlAndParametersBuilder.addLowerThan(property, number);
    }

    @Override
    public void addLowerOrEquals(String property, Number number) {
        hqlAndParametersBuilder.addLowerOrEquals(property, number);
    }

    @Override
    public void addGreaterThan(String property, Number number) {
        hqlAndParametersBuilder.addGreaterThan(property, number);
    }

    @Override
    public void addGreaterOrEquals(String property, Number number) {
        hqlAndParametersBuilder.addGreaterOrEquals(property, number);
    }

    @Override
    public void setOrder(LinkedHashSet<String> order) {
        setOrderByArguments(order);
    }

    @Override
    public void addLike(String property, String value) {
        hqlAndParametersBuilder.addLike(property, value);
    }

    @Override
    public void addNotLike(String property, String value) {
        hqlAndParametersBuilder.addNotLike(property, value);
    }

    public void setCaseSensitive(boolean caseSensitive) {
        hqlAndParametersBuilder.setCaseSensitive(caseSensitive);
    }

    protected AbstractTopiaDaoQueryBuilderRunQueryStep<E> getNextStep() {
        String hql = hqlAndParametersBuilder.getHql();
        Map<String, Object> hqlParameters = hqlAndParametersBuilder.getHqlParameters();
        boolean withOrderByClause = hqlAndParametersBuilder.isOrderByClausePresent();
        AbstractTopiaDaoQueryBuilderRunQueryStep<E> nextStep;
        if (hqlAndParametersBuilder.hasFetchProperties()) {
            String hqlForFetchStep1 = hqlAndParametersBuilder.getHqlForFetchStep1();
            String hqlForFetchStep2 = hqlAndParametersBuilder.getHqlForFetchStep2();
            nextStep = new AbstractTopiaDaoQueryBuilderRunQueryStep<>(topiaDao, false, withOrderByClause, hql, hqlParameters, hqlForFetchStep1, hqlForFetchStep2);
        } else {

            nextStep = new AbstractTopiaDaoQueryBuilderRunQueryStep<>(topiaDao, false, withOrderByClause, hql, hqlParameters);
        }
        return nextStep;
    }

}
