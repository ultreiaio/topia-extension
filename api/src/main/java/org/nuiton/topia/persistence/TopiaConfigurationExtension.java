package org.nuiton.topia.persistence;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.jdbc.JdbcConfiguration;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Path;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.TreeMap;

/**
 * Created on 08/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.60
 */
public class TopiaConfigurationExtension extends BeanTopiaConfiguration {

    private static final long serialVersionUID = 1L;
    private static final Map<String, String> HIBERNATE_GLOBAL_PROPERTIES;

    static {
        Properties p = new Properties();
        URL resource = TopiaConfigurationExtension.class.getClassLoader().getResource("/hibernate.properties");
        if (resource == null) {
            resource = Thread.currentThread().getContextClassLoader().getResource("hibernate.properties");
        }
        try (InputStream stream = Objects.requireNonNull(resource).openStream()) {
            try {
                p.load(stream);
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        HIBERNATE_GLOBAL_PROPERTIES = new TreeMap<>();
        for (String s : p.stringPropertyNames()) {
            HIBERNATE_GLOBAL_PROPERTIES.put(s, p.getProperty(s));
        }

    }

    protected final boolean showSql;
    private final boolean h2Configuration;
    private final Path temporaryDirectory;

    public TopiaConfigurationExtension(JdbcConfiguration jdbcConfiguration, Path temporaryDirectory, boolean h2Configuration, boolean showSql) {
        super(jdbcConfiguration);
        this.temporaryDirectory = temporaryDirectory;
        this.h2Configuration = h2Configuration;
        this.showSql = showSql;
        if (showSql) {
            hibernateExtraConfiguration.put(HibernateAvailableSettings.SHOW_SQL, Boolean.TRUE.toString());
        }
        hibernateExtraConfiguration.putAll(HIBERNATE_GLOBAL_PROPERTIES);
    }

    public boolean isH2Configuration() {
        return h2Configuration;
    }

    public boolean isShowSql() {
        return showSql;
    }

    public Path getTemporaryDirectory() {
        return temporaryDirectory;
    }
}

