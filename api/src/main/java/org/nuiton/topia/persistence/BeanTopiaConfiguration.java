package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.persistence.internal.FullyQualifiedNamePlusUuidTopiaIdFactory;
import org.nuiton.topia.persistence.jdbc.BeanJdbcConfiguration;
import org.nuiton.topia.persistence.jdbc.JdbcConfiguration;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class BeanTopiaConfiguration extends BeanJdbcConfiguration implements TopiaConfiguration {

    protected String schemaName;

    protected Map<String, String> hibernateExtraConfiguration =
            new LinkedHashMap<>();

    protected boolean initSchema = true;

    protected boolean validateSchema = true;

    protected Map<String, Class<? extends TopiaService>> declaredServices =
            new LinkedHashMap<>();

    protected Map<String, Map<String, String>> declaredServicesConfiguration =
            new LinkedHashMap<>();

    protected TopiaIdFactory topiaIdFactory = new FullyQualifiedNamePlusUuidTopiaIdFactory();

    protected boolean useHikariForJdbcConnectionPooling;

    public BeanTopiaConfiguration() {
    }

    public BeanTopiaConfiguration(JdbcConfiguration jdbcConfiguration) {
        setJdbcConnectionUrl(jdbcConfiguration.getJdbcConnectionUrl());
        setJdbcConnectionUser(jdbcConfiguration.getJdbcConnectionUser());
        setJdbcConnectionPassword(jdbcConfiguration.getJdbcConnectionPassword());
        setJdbcDriverClass(jdbcConfiguration.getJdbcDriverClass());
    }

    @Override
    public TopiaIdFactory getTopiaIdFactory() {
        return topiaIdFactory;
    }

    public void setTopiaIdFactory(TopiaIdFactory topiaIdFactory) {
        this.topiaIdFactory = topiaIdFactory;
    }

    public void setTopiaIdFactoryClassName(String topiaIdFactoryClassName) {
        try {
            Class<?> topiaIdFactoryClass = Class.forName(topiaIdFactoryClassName);
            if (TopiaIdFactory.class.isAssignableFrom(topiaIdFactoryClass)) {
                setTopiaIdFactoryClass((Class<? extends TopiaIdFactory>) topiaIdFactoryClass);
            } else {
                throw new IllegalArgumentException(topiaIdFactoryClassName + " seems not to be a TopiaIdFactory: it does not implement " + TopiaIdFactory.class.getName());
            }
        } catch (ClassNotFoundException e) {
            throw new TopiaMisconfigurationException(topiaIdFactoryClassName + " cannot be found", this);
        }
    }

    public void setTopiaIdFactoryClass(Class<? extends TopiaIdFactory> topiaIdFactoryClass) {
        try {
            setTopiaIdFactory(topiaIdFactoryClass.newInstance());
        } catch (InstantiationException | IllegalAccessException e) {
            throw new TopiaMisconfigurationException(topiaIdFactoryClass + " is not instantiable", this);
        }
    }

    @Override
    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    @Override
    public Map<String, String> getHibernateExtraConfiguration() {
        return hibernateExtraConfiguration;
    }

    public void setHibernateExtraConfiguration(Map<String, String> hibernateExtraConfiguration) {
        this.hibernateExtraConfiguration = hibernateExtraConfiguration;
    }

    @Override
    public boolean isInitSchema() {
        return initSchema;
    }

    public void setInitSchema(boolean initSchema) {
        this.initSchema = initSchema;
    }

    @Override
    public boolean isValidateSchema() {
        return validateSchema;
    }

    public void setValidateSchema(boolean validateSchema) {
        this.validateSchema = validateSchema;
    }

    @Override
    public boolean isUseHikariForJdbcConnectionPooling() {
        return useHikariForJdbcConnectionPooling;
    }

    public void setUseHikariForJdbcConnectionPooling(boolean useHikariForJdbcConnectionPooling) {
        this.useHikariForJdbcConnectionPooling = useHikariForJdbcConnectionPooling;
    }

    @Override
    public Map<String, Class<? extends TopiaService>> getDeclaredServices() {
        return declaredServices;
    }

    public void setDeclaredServices(Map<String, Class<? extends TopiaService>> declaredServices) {
        this.declaredServices = declaredServices;
    }

    @Override
    public Map<String, Map<String, String>> getDeclaredServicesConfiguration() {
        return declaredServicesConfiguration;
    }

    public void setDeclaredServicesConfiguration(Map<String, Map<String, String>> declaredServicesConfiguration) {
        this.declaredServicesConfiguration = declaredServicesConfiguration;
    }

    public void addDeclaredService(String serviceName, String serviceClassName, Map<String, String> serviceConfiguration) {
        try {
            Class<?> aClass = Class.forName(serviceClassName);
            @SuppressWarnings("unchecked") Class<? extends TopiaService> serviceClass = (Class<? extends TopiaService>) aClass;
            addDeclaredService(serviceName, serviceClass, serviceConfiguration);
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException(serviceClassName + " cannot be found in classpath");
        }
    }

    public void addDeclaredService(String serviceName, String serviceClassName) {
        addDeclaredService(serviceName, serviceClassName, Collections.<String, String>emptyMap());
    }

    public void addDeclaredService(String serviceName, Class<? extends TopiaService> serviceClassName, Map<String, String> serviceConfiguration) {
        declaredServices.put(serviceName, serviceClassName);
        declaredServicesConfiguration.put(serviceName, serviceConfiguration);
    }

    public void addDeclaredService(String serviceName, Class<? extends TopiaService> serviceClassName) {
        addDeclaredService(serviceName, serviceClassName, Collections.<String, String>emptyMap());
    }

    public String dangerousToString() {
        return new TopiaConfigurationBuilder().toMap(this).toString();
    }

    @Override
    public String toString() {
        return StringUtils.replace(
                dangerousToString(),
                getJdbcConnectionPassword(),
                "***** hidden by BeanTopiaConfiguration#toString *****");
    }
}
