package org.nuiton.topia.persistence;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.lang.Objects2;
import io.ultreia.java4all.lang.Strings;

import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * A special id facotyr where the timestamp part is fixed and the random part is performed by a sequence (by type)
 * Created on 09/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.69
 */
public class TopiaIdFactoryForBulkSupport implements TopiaIdFactory {

    private static final long serialVersionUID = 1L;
    private static final String ID_SUFFIX = "000000000000000000%d";
    private static final int SUFFIX_LENGTH = ID_SUFFIX.length() - 1;
    private final String packagePrefix;
    private final String idPrefix;
    private final String timestampPart;
    private final Map<String, AtomicLong> count;

    public TopiaIdFactoryForBulkSupport(String idPrefix, String packagePrefix, long timestamp) {
        this.idPrefix = idPrefix;
        this.packagePrefix = packagePrefix;
        this.timestampPart = "" + timestamp;
        this.count = new TreeMap<>();
    }

    public String getPackagePrefix() {
        return packagePrefix;
    }

    public String getIdPrefix() {
        return idPrefix;
    }

    public <E extends TopiaEntity> String newTopiaId(Class<E> entityType) {
        String idPrefix = getIdFromClass(entityType.getName());
        return newTopiaId(idPrefix);
    }

    public String newTopiaId(String idPrefix) {
        return idPrefix + getSeparator() + timestampPart + getSeparator() + getRandom(idPrefix);
    }

    @Override
    public <E extends TopiaEntity> String newTopiaId(Class<E> entityClass, TopiaEntity topiaEntity) {
        if (!entityClass.isInterface()) {
            throw new IllegalArgumentException("Only interface is permit to create id: " + entityClass);
        }
        return newTopiaId(entityClass);
    }

    @Override
    public <E extends TopiaEntity> String newTopiaId(Class<E> entityClass, String randomPart) {
        return getIdFromClass(entityClass.getName()) + getSeparator() + timestampPart + getSeparator() + randomPart;
    }

    @Override
    public <E extends TopiaEntity> Class<E> getClassName(String topiaId) {
        String className = getClassName0(topiaId);
        return Objects2.forName(className);
    }

    @Override
    public String getRandomPart(String topiaId) {
        return Strings.substringAfter(topiaId, getSeparator());
    }

    @Override
    public String getSeparator() {
        return "#";
    }

    @Override
    public boolean isTopiaId(String str) {
        if (str != null) {
            String className = getClassName0(str);
            Objects2.forName(className);
            return true;
        }
        return false;
    }

    private String getClassName0(String str) {
        return getClassFromId(Strings.substringBefore(str, getSeparator()));
    }

    private String getRandom(String type) {
        String suffix = String.format(ID_SUFFIX, count.computeIfAbsent(type, s -> new AtomicLong()).incrementAndGet());
        int length = suffix.length();
        if (length > SUFFIX_LENGTH) {
            suffix = suffix.substring(length - SUFFIX_LENGTH);
        }
        return suffix;
    }

    private String getIdFromClass(String entityClassName) {
        return getIdPrefix() + Strings.removeStart(entityClassName, getPackagePrefix());
    }

    private String getClassFromId(String id) {
        return packagePrefix + Strings.removeStart(id, idPrefix);
    }
}


