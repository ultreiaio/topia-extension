package org.nuiton.topia.persistence.internal;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaIdFactory;
import org.nuiton.topia.persistence.TopiaNotFoundException;

/**
 * Implements {@link TopiaIdFactory} with Topia 2.x and before ids.
 * Suitable if you already have a database and your application need
 * to generate ids like the old ones.
 *
 * @since 3.0
 */
public class LegacyTopiaIdFactory implements TopiaIdFactory {

    private static final long serialVersionUID = 1;
    private static final Logger log = LogManager.getLogger(LegacyTopiaIdFactory.class);

    @Override
    public <E extends TopiaEntity> String newTopiaId(Class<E> entityClass, TopiaEntity topiaEntity) {
        if (!entityClass.isInterface()) {
            throw new IllegalArgumentException(
                    "Only interface is permit to create id: " + entityClass);
        }
        double random = Math.random();
        while (Double.toString(random).contains("E-")) {
            random = Math.random();
        }
        return entityClass.getName() + getSeparator() + System.currentTimeMillis() + '#' + random;
    }

    @Override
    public <E extends TopiaEntity> String newTopiaId(Class<E> entityClass, String randomPart) {
        return entityClass.getName() + getSeparator() + System.currentTimeMillis() + '#' + randomPart;
    }

    @Override
    public <E extends TopiaEntity> Class<E> getClassName(String topiaId) {
        String className = StringUtils.substringBefore(topiaId, getSeparator());
        try {
            return (Class<E>) Class.forName(className);
        } catch (ClassNotFoundException eee) {
            throw new TopiaNotFoundException("Can't find class for " + topiaId,
                                             eee);
        }
    }

    @Override
    public String getSeparator() {
        return "#";
    }

    @Override
    public String getRandomPart(String topiaId) {
        return StringUtils.substringAfter(topiaId, getSeparator());
    }

    @Override
    public boolean isTopiaId(String str) {
        boolean isTopiaId = false;
        if (str != null) {
            String[] split = str.split(getSeparator());
            if (split.length >= 2) {
                String className = split[0];
                try {
                    Class.forName(className);
                    isTopiaId = true;
                    for (int index = 1; index < split.length; index++) {
                        isTopiaId &= StringUtils.isNoneBlank(split[index]);
                    }
                } catch (ClassNotFoundException eee) {
                    // nothing to do, just return false
                    if (log.isDebugEnabled()) {
                        log.debug(eee);
                    }
                }
            }
        }
        return isTopiaId;
    }
}
