package org.nuiton.topia.persistence;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import io.ultreia.java4all.util.SortedProperties;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.function.Function;

/**
 * To use tag values.
 * <p>
 * Created on 09/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.24
 */
public class TagValues {

    public static final String ATTRIBUTE = ".attribute.";
    private final String location;
    private final Function<String, String> typeCleaner;
    private Properties store;

    public static TagValues dto(String modelName, Function<String, String> typeCleaner) {
        return new TagValues(String.format("models/%s/%s.properties", modelName, "dto"), typeCleaner);
    }

    public static TagValues persistence(String modelName, Function<String, String> typeCleaner) {
        return new TagValues(String.format("models/%s/%s.properties", modelName, "persistence"), typeCleaner);
    }

    protected TagValues(String location, Function<String, String> typeCleaner) {
        this.location = location;
        this.typeCleaner = typeCleaner;
    }

    public Properties getStore() {
        if (store == null) {
            store = loadStore();
        }
        return store;
    }

    public String getClassTagValue(String tagValueName, Class<?> type) {
        String key = typeCleaner.apply(type.getName()) + ".class.tagValue." + tagValueName;
        return getStore().getProperty(key);
    }

    public Multimap<String, String> getAttributeStereotypes(String tagValueName) {
        Map<Pair<String, String>, String> attributeTagValues = getAttributeTagValues(tagValueName);
        Multimap<String, String> result = ArrayListMultimap.create();
        for (Pair<String, String> key : attributeTagValues.keySet()) {
            result.put(key.getKey(), key.getValue());
        }
        return result;
    }

    public Map<Pair<String, String>, String> getAttributeTagValues(String tagValueName) {
        String suffix = ".tagValue." + tagValueName;
        int suffixLength = suffix.length();
        Map<Pair<String, String>, String> result = new LinkedHashMap<>();
        Properties store = getStore();
        for (String p : store.stringPropertyNames()) {
            if (p.endsWith(suffix)) {
                int attributeIndex = p.indexOf(ATTRIBUTE);
                if (attributeIndex > -1) {
                    String className = p.substring(0, attributeIndex);
                    String propertyName = p.substring(attributeIndex + ATTRIBUTE.length(), p.length() - suffixLength);
                    result.put(Pair.of(className, propertyName), store.getProperty(p));
                }
            }
        }
        return result;
    }

    private Properties loadStore() {
        SortedProperties result = new SortedProperties();
        URL resource = Thread.currentThread().getContextClassLoader().getResource(location);
        try (InputStream stream = Objects.requireNonNull(resource).openStream()) {
            result.load(stream);
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Could not load tag-values for location: %s", location), e);
        }
        return result;
    }
}
