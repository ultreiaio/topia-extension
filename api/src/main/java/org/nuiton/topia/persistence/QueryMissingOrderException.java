package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.pager.PaginationParameter;

import java.util.Map;

/**
 * If you get this exception, it means that you asked ToPIA to make an operation that needs a deterministic way to sort
 * the result but you didn't defined such query.
 * <p>
 * To fix you issue, you may use another API call (maybe user findAny instead of findFirst) or change your query by
 * adding an order by clause. A quick and easy fix would be to just use "order by topiaId".
 *
 * @since 3.0
 */
public class QueryMissingOrderException extends TopiaQueryException {

    private static final long serialVersionUID = 8154110639594660107L;

    protected static final String MESSAGE = "Given query needs an ORDER BY clause since the API call you're using " +
            "needs the results sorting to be deterministic";

    protected PaginationParameter paginationParameter;

    public QueryMissingOrderException(String hql, Map<String, Object> hqlParameters) {
        super(MESSAGE, hql, hqlParameters);
    }

    public QueryMissingOrderException(String hql, Map<String, Object> hqlParameters, PaginationParameter paginationParameter) {
        super(MESSAGE, hql, hqlParameters);
        this.paginationParameter = paginationParameter;
    }

    public PaginationParameter getPaginationParameter() {
        return paginationParameter;
    }

}
