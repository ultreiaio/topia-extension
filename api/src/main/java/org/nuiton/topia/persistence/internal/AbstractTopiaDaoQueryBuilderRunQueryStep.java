package org.nuiton.topia.persistence.internal;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.topia.persistence.QueryMissingOrderException;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaNoResultException;
import org.nuiton.topia.persistence.TopiaNonUniqueResultException;
import org.nuiton.topia.persistence.TopiaQueryBuilderRunQueryStep;
import org.nuiton.topia.persistence.pager.PaginationParameter;
import org.nuiton.topia.persistence.pager.PaginationResult;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created on 13/05/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class AbstractTopiaDaoQueryBuilderRunQueryStep<E extends TopiaEntity> implements TopiaQueryBuilderRunQueryStep<E> {

    protected final String hql;

    protected final Map<String, Object> hqlParameters;

    protected final AbstractTopiaDao<E> topiaDao;

    protected final boolean fromHql;

    protected final boolean withOrderByClause;

    protected final String hqlForFetchStep1;

    protected final String hqlForFetchStep2;

    protected AbstractTopiaDaoQueryBuilderRunQueryStep(AbstractTopiaDao<E> topiaDao,
                                                       boolean fromHql,
                                                       boolean withOrderByClause,
                                                       String hql,
                                                       Map<String, Object> hqlParameters) {
        this(topiaDao, fromHql, withOrderByClause, hql, hqlParameters, null, null);
    }

    protected AbstractTopiaDaoQueryBuilderRunQueryStep(AbstractTopiaDao<E> topiaDao,
                                                       boolean fromHql,
                                                       boolean withOrderByClause,
                                                       String hql,
                                                       Map<String, Object> hqlParameters,
                                                       String hqlForFetchStep1,
                                                       String hqlForFetchStep2) {
        this.fromHql = fromHql;
        this.withOrderByClause = withOrderByClause;
        this.hql = hql;
        this.hqlParameters = hqlParameters;
        this.topiaDao = topiaDao;
        this.hqlForFetchStep1 = hqlForFetchStep1;
        this.hqlForFetchStep2 = hqlForFetchStep2;
    }

    @Override
    public boolean exists() {
        return topiaDao.exists(hql, hqlParameters);
    }

    @Override
    public long count() {
        String hqlWithSelectClause = "select count(topiaId) " + hql;
        return topiaDao.count(hqlWithSelectClause, hqlParameters);
    }

    @Override
    public E findUnique() throws TopiaNoResultException, TopiaNonUniqueResultException {
        return topiaDao.findUnique(hql, hqlParameters);
    }

    @Override
    public E findUniqueOrNull() throws TopiaNonUniqueResultException {
        return topiaDao.findUniqueOrNull(hql, hqlParameters);
    }

    @Override
    public Optional<E> tryFindUnique() throws TopiaNonUniqueResultException {
        return topiaDao.tryFindUnique(hql, hqlParameters);
    }

    @Override
    public E findFirst() throws QueryMissingOrderException, TopiaNoResultException {
        return topiaDao.findFirst(hql, hqlParameters);
    }

    @Override
    public E findFirstOrNull() throws QueryMissingOrderException {
        return topiaDao.findFirstOrNull(hql, hqlParameters);
    }

    @Override
    public Optional<E> tryFindFirst() throws QueryMissingOrderException {
        return topiaDao.tryFindFirst(hql, hqlParameters);
    }

    @Override
    public E findAny() throws TopiaNoResultException {
        return topiaDao.findAny(hql, hqlParameters);
    }

    @Override
    public E findAnyOrNull() {
        return topiaDao.findAnyOrNull(hql, hqlParameters);
    }

    @Override
    public Optional<E> tryFindAny() {
        return topiaDao.tryFindAny(hql, hqlParameters);
    }

    @Override
    public List<E> findAll() {
        return topiaDao.findAll(hql, hqlParameters);
    }

    @Override
    public Stream<E> stream() {
        return topiaDao.stream(hql, hqlParameters);
    }

    @Override
    public List<E> find(int startIndex, int endIndex) {
        List<E> result;
        if (!Strings.isNullOrEmpty(hqlForFetchStep1) && !Strings.isNullOrEmpty(hqlForFetchStep2)) {
            List<String> step1ResultTopiaIds = topiaDao.find(hqlForFetchStep1, hqlParameters, startIndex, endIndex);

            if (CollectionUtils.isEmpty(step1ResultTopiaIds)) {
                result = Lists.newArrayList();
            } else {
                Map<String, Object> step2Args = Maps.newHashMap();
                step2Args.put("topiaIdsForFetch_", step1ResultTopiaIds);
                List<E> entities = topiaDao.forHql(hqlForFetchStep2, step2Args).findAll();

                result = sortAccordingToIds(entities, step1ResultTopiaIds);
            }
        } else {
            result = topiaDao.find(hql, hqlParameters, startIndex, endIndex);
        }
        return result;
    }

    @Override
    public List<E> find(PaginationParameter page) {
        return topiaDao.find(hql, hqlParameters, page);
    }

    @Override
    public PaginationResult<E> findPage(PaginationParameter page) {
        PaginationResult<E> result;
        if (!Strings.isNullOrEmpty(hqlForFetchStep1) && !Strings.isNullOrEmpty(hqlForFetchStep2)) {
            PaginationResult<String> pageResult = topiaDao.findPage(hqlForFetchStep1, hqlParameters, page);
            List<String> step1ResultTopiaIds = pageResult.getElements();

            List<E> sortedEntities;
            if (CollectionUtils.isEmpty(step1ResultTopiaIds)) {
                sortedEntities = Lists.newArrayList();
            } else {
                Map<String, Object> step2Args = Maps.newHashMap();
                step2Args.put("topiaIdsForFetch_", step1ResultTopiaIds);
                List<E> entities = topiaDao.forHql(hqlForFetchStep2, step2Args).findAll();

                sortedEntities = sortAccordingToIds(entities, step1ResultTopiaIds);
            }

            result = PaginationResult.of(sortedEntities, pageResult.getCount(), pageResult.getCurrentPage());
        } else {
            result = topiaDao.findPage(hql, hqlParameters, page);
        }
        return result;
    }

    /**
     * This method can be used when it is not possible to sort entities in SQL/HQL. The list of entities
     * will be sorted according to the given list of topiaId.
     *
     * @param entities the list en entities (unsorted)
     * @param idsList  the list of ids (sorted)
     * @param <O>      must be a TopiaEntity
     * @return FIXME
     */
    protected <O extends TopiaEntity> List<O> sortAccordingToIds(List<O> entities, final List<String> idsList) {

        // Cannot sort on second query, will sort according to the first result list
        final Map<String, O> entitiesIndex = Maps.uniqueIndex(entities, TopiaEntity::getTopiaId);

        return idsList.stream().map(entitiesIndex::get).collect(Collectors.toList());
    }

    @Override
    public List<String> findAllIds() {
        String hqlWithSelectClause = "select topiaId " + hql;
        return topiaDao.findAll(hqlWithSelectClause, hqlParameters);
    }

    @Override
    public List<String> findIds(int startIndex, int endIndex) {
        String hqlWithSelectClause = "select topiaId " + hql;
        return topiaDao.find(hqlWithSelectClause, hqlParameters, startIndex, endIndex);
    }

    @Override
    public List<String> findIds(PaginationParameter page) {
        String hqlWithSelectClause = "select topiaId " + hql;
        return topiaDao.find(hqlWithSelectClause, hqlParameters, page);
    }

    @Override
    public PaginationResult<String> findIdsPage(PaginationParameter page) {
        List<String> elements = findIds(page);
        long count = count();
        return PaginationResult.of(elements, count, page);
    }

}
