package org.nuiton.topia.persistence.support;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.hibernate.dialect.H2Dialect;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.internal.HibernateProvider;
import org.nuiton.topia.persistence.internal.support.H2TopiaSqlDllSupportImpl;
import org.nuiton.topia.persistence.internal.support.PGTopiaSqlDllSupportImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Created by tchemit on 05/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface TopiaSqlDllSupport {

    static String getClassifier(TopiaApplicationContext<?> applicationContext) {
        String hibernateDialect = HibernateProvider.getHibernateDialect(applicationContext.getConfiguration());
        String classifier = null;
        if (H2Dialect.class.getName().equals(hibernateDialect)) {
            classifier = H2TopiaSqlDllSupportImpl.CLASSIFIER;
        } else if (hibernateDialect.contains("PostgreSQL")) {
            classifier = PGTopiaSqlDllSupportImpl.CLASSIFIER;
        }
        return classifier;
    }

    static Set<String> selectAllAsSet(TopiaSqlSupport tx, String sql) {
        return new LinkedHashSet<>(selectAll(tx, sql));
    }

    static List<String> selectAll(TopiaSqlSupport tx, String sql) {
        return tx.findMultipleResult(new TopiaSqlQuery<>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement(sql);
            }

            @Override
            public String prepareResult(ResultSet set) throws SQLException {
                return set.getString(1);
            }
        });
    }

    static String selectFirst(TopiaSqlSupport tx, String sql) {
        return tx.findSingleResult(new TopiaSqlQuery<>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement(sql);
            }

            @Override
            public String prepareResult(ResultSet set) throws SQLException {
                return set.getString(1);
            }
        });
    }

    String getClassifier();

    String getPrimaryKeyConstraintName(TopiaSqlSupport tx, String schemaName, String tableName);

    String getUniqueConstraintName(TopiaSqlSupport tx, String tableName, String columnName);

    String getFirstTableUniqueConstraintName(TopiaSqlSupport tx, String tableName);

    Set<String> getConstraintNames(TopiaSqlSupport tx, String tableName);

    Set<String> getForeignKeyConstraintNames(TopiaSqlSupport tx, String tableName);

    String getForeignKeyConstraintName(TopiaSqlSupport tx, String schemaName, String tableName, String columnName, boolean mustExists);

    Set<String> getUniqueKeyConstraintNames(TopiaSqlSupport tx, String tableName);

    Set<String> removeFK(TopiaSqlSupport tx, String tableName);

    String removeFK(TopiaSqlSupport tx, String schemaName, String tableName, String columnName);

    Optional<String> removeFKIfExists(TopiaSqlSupport tx, String schemaName, String tableName, String columnName);

    Optional<String> removePKIfExists(TopiaSqlSupport tx, String schemaName, String tableName);

    Set<String> removeUK(TopiaSqlSupport tx, String tableName);

    String dropSchema(TopiaSqlSupport tx, String schemaName);

    String dropTable(TopiaSqlSupport tx, String schemaName, String tableName);
}
