package org.nuiton.topia.persistence.filter.property;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created on 05/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.35
 */
public class BooleanFilterProperty extends ObjectFilterPropertySupport<Boolean> {

    public static BooleanFilterProperty create(String propertyName) {
        return new BooleanFilterProperty(propertyName, null);
    }

    public static BooleanFilterProperty createPrimitive(String propertyName) {
        return new BooleanFilterProperty(propertyName, false);
    }

    public BooleanFilterProperty(String propertyName, Boolean defaultValue) {
        super(propertyName, defaultValue == null ? FLAVORS_EQUALS : FLAVORS_OBJECT, "", Boolean.class, Boolean::valueOf, defaultValue);
    }

    @Override
    public boolean isPrimitive() {
        return withDefaultValue();
    }
}
