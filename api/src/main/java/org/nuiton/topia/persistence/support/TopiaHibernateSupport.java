package org.nuiton.topia.persistence.support;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;

/**
 * This API provides methods to interact with Hibernate
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public interface TopiaHibernateSupport extends QuerySupport {

    /**
     * @return Returns the Hibernate's Session.
     */
    Session getHibernateSession();

    /**
     * @return Returns the HibernateFactory.
     */
    SessionFactory getHibernateFactory();

    /**
     * @return Returns the Hibernate's Metadata.
     */
    Metadata getHibernateMetadata();

    /**
     * @return Returns the Hibernate configuration
     */
    Configuration getHibernateConfiguration();

    @Override
    @SuppressWarnings("unchecked")
    default <T> Query<T> getQuery(String queryName) {
        return getHibernateSession().getNamedQuery(queryName);
    }

    @SuppressWarnings("unchecked")
    @Override
    default <T> NativeQuery<T> getSqlQuery(String queryName) {
        return getHibernateSession().getNamedNativeQuery(queryName);
    }
}
