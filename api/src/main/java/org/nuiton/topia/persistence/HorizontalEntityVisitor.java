package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Visitor to run through entities horizontally, then delegate visiting to another visitor.
 *
 * @author Éric Chatellier - chatellier@codelutin.com
 * @author Tony Chemit - tchemit@codelutin.com
 */
public class HorizontalEntityVisitor implements TopiaEntityVisitor {

    private static final Logger log = LogManager.getLogger(HorizontalEntityVisitor.class);
    /**
     * Delegate visitor.
     */
    protected TopiaEntityVisitor delegateVisitor;

    /**
     * Cache used to remember entity during exploration.
     */
    protected List<TopiaEntity> alreadyExplored;

    /**
     * Entity to be visited later.
     */
    protected List<TopiaEntity> toVisitEntities;

    /**
     * Constructor.
     *
     * @param delegateVisitor visitor to delegate visiting
     */
    public HorizontalEntityVisitor(TopiaEntityVisitor delegateVisitor) {
        this.delegateVisitor = delegateVisitor;
        alreadyExplored = new ArrayList<>();
        toVisitEntities = new ArrayList<>();
    }

    @Override
    public void start(TopiaEntity entity) {
        delegateVisitor.start(entity);
        if (!alreadyExplored.contains(entity)) {
            alreadyExplored.add(entity);
        }
    }

    @Override
    public void visit(TopiaEntity entity, String propertyName, Class<?> type,
                      Object value) {
        // si c'est une entité
        if (value instanceof TopiaEntity) {
            TopiaEntity entityValue = (TopiaEntity) value;
            toVisitEntities.add(entityValue);
        } else {
            delegateVisitor.visit(entity, propertyName, type, value);
        }
    }

    @Override
    public void visit(TopiaEntity entity, String propertyName,
                      Class<?> collectionType, Class<?> type, Object value) {
        Collection<?> values = (Collection<?>) value;
        if (values != null && !values.isEmpty()) {
            int i = 0;
            for (Object currentValue : values) {
                visit(entity, propertyName, type, collectionType, i++,
                      currentValue);
            }
        }
    }

    @Override
    public void visit(TopiaEntity entity, String propertyName,
                      Class<?> collectionType, Class<?> type, int index,
                      Object value) {
        // si c'est une entité
        if (value instanceof TopiaEntity) {
            TopiaEntity entityValue = (TopiaEntity) value;
            toVisitEntities.add(entityValue);
        } else {
            delegateVisitor.visit(entity, propertyName, collectionType, type,
                                  index, value);
        }
    }

    @Override
    public void end(TopiaEntity entity) {
        delegateVisitor.end(entity);

        // here, must revisit all remembered entities
        List<TopiaEntity> currentEntities =
                new ArrayList<>(toVisitEntities);
        // TODO-chatellier-20091221 : verify if clearing here is enough
        toVisitEntities.clear();
        for (TopiaEntity currentEntity : currentEntities) {
            try {
                if (!alreadyExplored.contains(currentEntity)) {
                    currentEntity.accept(this);
                    log.debug("Post process " + currentEntity);
                }
            } catch (TopiaException ex) {
                if (log.isErrorEnabled()) {
                    log.error("Error on horizontal exploration", ex);
                }
            }
        }
    }

    @Override
    public void clear() {
        alreadyExplored.clear();
        toVisitEntities.clear();
        delegateVisitor.clear();
    }
} // HorizontallyEntityVisitor
