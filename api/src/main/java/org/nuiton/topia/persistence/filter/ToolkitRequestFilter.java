package org.nuiton.topia.persistence.filter;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import io.ultreia.java4all.util.json.JsonAware;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.TreeMap;

/**
 * Represents a filter on a toolkit request.
 * <p>
 * Created on 28/06/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.30
 */
public class ToolkitRequestFilter implements JsonAware {
    /**
     * To filter on id. (will exclude properties filter).
     *
     * @see #onId()
     */
    private String id;
    /**
     * To filter on properties (only used if id is null).
     *
     * @see #onProperties()
     */
    private TreeMap<String, String> properties;
    /**
     * Orders to apply (only works with properties).
     *
     * @see #onOrders()
     */
    private TreeMap<String, OrderEnum> orders;
    /**
     * Group by to apply (only works with properties).
     */
    private String groupBy;
    /**
     * Is textual search case sensitive?  (only works with properties).
     *
     * <b>Note:</b> default behaviour is <b>not case sensitive</b>.
     */
    private boolean caseSensitive;

    public static ToolkitRequestFilter onId(String id) {
        ToolkitRequestFilter filter = new ToolkitRequestFilter();
        filter.setId(Objects.requireNonNull(id));
        return filter;
    }

    public static ToolkitRequestFilter onOrders(Map<String, OrderEnum> orders) {
        return onProperties(Map.of(), orders);
    }

    public static ToolkitRequestFilter onProperties(Map<String, String> properties, Map<String, OrderEnum> orders) {
        ToolkitRequestFilter filter = new ToolkitRequestFilter();
        filter.setProperties(new TreeMap<>(Objects.requireNonNull(properties)));
        if (orders == null) {
            orders = new TreeMap<>();
        } else if (!(orders instanceof TreeMap)) {
            orders = new TreeMap<>(orders);
        }
        filter.setOrders((TreeMap<String, OrderEnum>) orders);
        return filter;
    }

    public static ToolkitRequestFilter requiresNotAll(Map<String, String> properties, Map<String, OrderEnum> orders) {
        if (properties == null) {
            properties = new TreeMap<>();
        }
        if (orders == null) {
            orders = new TreeMap<>();
        }
        ToolkitRequestFilter filter = new ToolkitRequestFilter();
        filter.setProperties(new TreeMap<>(Objects.requireNonNull(properties)));
        filter.setOrders(new TreeMap<>(Objects.requireNonNull(orders)));
        filter.requiresNotAll();
        return filter;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = Objects.requireNonNull(id);
    }

    public void setProperties(TreeMap<String, String> properties) {
        this.properties = Objects.requireNonNull(properties);
    }

    public String getGroupBy() {
        return groupBy;
    }

    public void setGroupBy(String groupBy) {
        this.groupBy = groupBy;
    }

    public TreeMap<String, String> getProperties() {
        return properties;
    }

    public Collection<String> getPropertiesKeys() {
        return properties.keySet();
    }

    public boolean isCaseSensitive() {
        return caseSensitive;
    }

    public void setCaseSensitive(boolean caseSensitive) {
        this.caseSensitive = caseSensitive;
    }

    public TreeMap<String, OrderEnum> getOrders() {
        return orders;
    }

    public Collection<String> getOrdersKeys() {
        return orders.keySet();
    }

    public void setOrders(TreeMap<String, OrderEnum> orders) {
        this.orders = Objects.requireNonNull(orders);
    }

    public boolean onAll() {
        return !onId() && !onProperties();
    }

    public boolean onId() {
        return id != null;
    }

    public boolean onProperties() {
        return id == null && properties != null && !properties.isEmpty();
    }

    public boolean onOrders() {
        return id == null && orders != null && !orders.isEmpty();
    }

    @Override
    public String toString() {
        StringJoiner joiner = new StringJoiner(", ", getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) + "[", "]");
        if (onId()) {
            joiner.add("id='" + id + "'");
        }
        if (onProperties()) {
            joiner.add("properties=" + properties);
            if (isCaseSensitive()) {
                joiner.add("caseSensitive");
            }
        }
        if (onOrders()) {
            joiner.add("orders=" + orders);
        }
        return joiner.toString();
    }


    public final void requiresNotAll() {
        if (onAll()) {
            throw new IllegalStateException("Requires at least one parameter filters.");
        }
    }
}
