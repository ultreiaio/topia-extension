package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Visitor to run through the entity graph by depth.
 *
 * @author Éric Chatellier - chatellier@codelutin.com
 * @author Tony Chemit - tchemit@codelutin.com
 */
public class DepthEntityVisitor implements TopiaEntityVisitor {

    private static final Logger log = LogManager.getLogger(DepthEntityVisitor.class);
    /**
     * Cache of already explored entities.
     */
    protected Collection<TopiaEntity> alreadyExplored;

    /**
     * The business visitor (optional)
     */
    protected TopiaEntityVisitor delegateVisitor;

    public DepthEntityVisitor() {
        this(null);
    }

    public DepthEntityVisitor(TopiaEntityVisitor delegateVisitor) {

        alreadyExplored = new ArrayList<>();

        this.delegateVisitor = delegateVisitor;
    }

    @Override
    public void start(TopiaEntity e) {
        if (delegateVisitor != null) {
            delegateVisitor.start(e);
        }
        if (!alreadyExplored.contains(e)) {
            alreadyExplored.add(e);
        }
    }

    @Override
    public void visit(TopiaEntity e, String propertyName, Class<?> type,
                      Object value) {
        // si c'est une entité
        if (value instanceof TopiaEntity) {
            TopiaEntity entity = (TopiaEntity) value;
            try {
                if (!alreadyExplored.contains(entity)) {
                    entity.accept(this);
                }
            } catch (TopiaException e1) {
                if (log.isErrorEnabled()) {
                    log.error("Error on depth exploration", e1);
                }
            }
        } else {
            if (delegateVisitor != null) {
                delegateVisitor.visit(e, propertyName, type, value);
            }
        }
    }

    @Override
    public void visit(TopiaEntity e, String propertyName,
                      Class<?> collectionType, Class<?> type, Object value) {

        Collection<?> cValue = (Collection<?>) value;
        if (cValue != null && !cValue.isEmpty()) {
            int i = 0;
            for (Object currentValue : cValue) {
                visit(e, propertyName, type, collectionType, i++, currentValue);
            }
        }
    }

    @Override
    public void visit(TopiaEntity e, String propertyName,
                      Class<?> collectionType, Class<?> type, int index,
                      Object value) {
        // si c'est une entité
        if (value instanceof TopiaEntity) {
            TopiaEntity entity = (TopiaEntity) value;
            try {
                if (!alreadyExplored.contains(entity)) {
                    entity.accept(this);
                }
            } catch (TopiaException e1) {
                if (log.isErrorEnabled()) {
                    log.error("Error on depth exploration", e1);
                }
            }
        } else {
            if (delegateVisitor != null) {
                delegateVisitor.visit(e, propertyName, collectionType, type,
                                      index, value);
            }
        }
    }

    @Override
    public void end(TopiaEntity e) {
        if (delegateVisitor != null) {
            delegateVisitor.end(e);
        }
    }

    @Override
    public void clear() {
        alreadyExplored.clear();
        if (delegateVisitor != null) {
            delegateVisitor.clear();
        }
    }
}
