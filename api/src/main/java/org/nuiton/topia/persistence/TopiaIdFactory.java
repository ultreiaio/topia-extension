package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;

/**
 * This contract represents a topiaId generation strategy.
 *
 * It can be used in both ways :
 * <ul>
 * <li>generate a new topiaId from a given class;</li>
 * <li>retrieves a class or random part from a given topiaId.</li>
 * </ul>
 *
 * You can change the implementation used by ToPIA by changing configuration.
 *
 * {@link java.io.Serializable} must be implemented because
 * {@link org.nuiton.topia.persistence.TopiaConfiguration} may hold some references.
 *
 * @author Brendan Le Ny - bleny@codelutin.com
 * @author Tony Chemit - tchemit@codelutin.com
 * @see org.nuiton.topia.persistence.TopiaConfiguration#getTopiaIdFactory()
 * @since 3.0
 */
public interface TopiaIdFactory extends Serializable {

    /**
     * Generates a new topiaId for the given entity type and the given entity.
     *
     * @param entityClass type of entity (must be a not null interface)
     * @param topiaEntity the entity on which we want to generate the id (must be not null)
     * @param <E>         type of entity
     * @return the new topiaId for the given entity
     */
    <E extends TopiaEntity> String newTopiaId(Class<E> entityClass, TopiaEntity topiaEntity);

    /**
     * Builds a new topiaId for the given entity type and the given random part.
     *
     * @param entityClass type of entity (must be a not null interface)
     * @param randomPart  the random part of the topiaId
     * @param <E>         type of entity
     * @return the new topiaId
     */
    <E extends TopiaEntity> String newTopiaId(Class<E> entityClass, String randomPart);


    /**
     * @param topiaId the topiaId to inspect
     * @param <E>     type of the entity
     * @return the FQN part of the topiaId
     */
    <E extends TopiaEntity> Class<E> getClassName(String topiaId);

    /**
     * @param topiaId the topiaId to inspect
     * @return the random part of the topiaId
     */
    String getRandomPart(String topiaId);

    /**
     * @return the separator between the FQN and the random part of any topiaId.
     */
    String getSeparator();

    /**
     * @param str FIXME
     * @return true if given argument is a well formatted topiaId
     */
    boolean isTopiaId(String str);

}
