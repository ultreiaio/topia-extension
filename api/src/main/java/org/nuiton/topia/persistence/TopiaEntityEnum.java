package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;

/**
 * This contract represents a generated entity. It provides some information about the classes for any entity managed
 * by the dao helper. More precisely :
 * <ul>
 * <li>- contract class of the entity (this must be an interface class)</li>
 * <li>- the implementation FQN class of an entity (at generation time, we might not have the implementation class)</li>
 * <li>- the implementation class (will be looked up at runtime, in that way we make possible to used a different
 * implementation at runtime.</li>
 * <li>- a method to accept any TopiaEntity class for this entity description</li>
 * </ul>
 *
 * For each entity of the model, a realisation of this contract is generated in an enum named XxxEntityEnum (where
 * Xxx is the name of your project).
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2.0
 */
public interface TopiaEntityEnum extends Serializable {

    /**
     * This is a convenient method, as entity enum offers this method from {@link Enum#name()}.
     *
     * @return the name of the underlying entity type.
     * @since 2.6.12
     */
    String name();

    /**
     * @return the name of database schema (null if none was filled).
     * @since 2.9.2
     */
    String dbSchemaName();

    /**
     * @return the name of database table
     * @since 2.9.2
     */
    String dbTableName();

    /**
     * @return the contract class of the entity
     */
    Class<? extends TopiaEntity> getContract();

    /**
     * Note : this is a lazy accessor.
     *
     * @return the implementation class of the entity
     */
    Class<? extends TopiaEntity> getImplementation();

    /**
     * @return the fully qualifed name of the implementation class of the entity
     */
    String getImplementationFQN();

    /**
     * @return the array of property names involved in the natural key of the entity.
     */
    String[] getNaturalIds();

    /**
     * @return the array of property names which are marked as not-null.
     * @since 2.6.9
     */
    String[] getNotNulls();

    /**
     * @return {@code true} if entity use natural ids, {@code false} otherwise.
     * @since 2.6.9
     */
    boolean isUseNaturalIds();

    /**
     * @return {@code true} if entity use some not-null properties, {@code false} otherwise.
     * @since 2.6.9
     */
    boolean isUseNotNulls();

    /**
     * Change the implementation class of the entity.
     *
     * Note : this method should reset all states of the object (implementation class, operators,...).
     *
     * @param implementationFQN the new fully qualified name of the new implementation class of the entity.
     */
    void setImplementationFQN(String implementationFQN);

    /**
     * Test if a given type of entity is matching the contract of this entity.
     *
     * Note : make sure to accept type only on the given contract class of this entity, can not accept an ancestor type,
     * since there is a specific contract for this.
     *
     * Example : A → B
     * <pre>
     * XxxEntityEnum.A.accept(Class&lt;A&gt;) → true
     * XxxEntityEnum.A.accept(Class&lt;B&gt;) → false
     * XxxEntityEnum.B.accept(Class&lt;B&gt;) → true
     * XxxEntityEnum.B.accept(Class&lt;A&gt;) → false
     * </pre>
     *
     * @param klass the type of an entity to test.
     * @return {@code true} if given type is dealed directly by this entity, {@code false} otherwise.
     */
    boolean accept(Class<? extends TopiaEntity> klass);

}
