package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Contract that provides any Dao instance from a given entity class.
 *
 * @author bleny
 * @since 3.0
 */
public interface TopiaDaoSupplier {

    /**
     * Get Dao for specified class. If the specialized Dao exists then it is returned otherwise a TopiaException will
     * be thrown.
     *
     * @param <E>         type of entity
     * @param entityClass type of entity
     * @return the expected dao
     */
    <E extends TopiaEntity> TopiaDao<E> getDao(Class<E> entityClass);

    /**
     * Get Dao for specified class. If the specialized Dao exists then it is returned otherwise a TopiaException will be
     * thrown.
     *
     * @param <E>         type of entity
     * @param <D>         type of dao
     * @param entityClass type of entity
     * @param daoClass    the concrete dao class to use
     * @return the expected dao
     */
    <E extends TopiaEntity, D extends TopiaDao<E>> D getDao(Class<E> entityClass, Class<D> daoClass);

}
