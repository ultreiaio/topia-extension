package org.nuiton.topia.persistence.internal.support;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlDllSupport;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Created by tchemit on 05/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@SuppressWarnings("SqlDialectInspection")
@AutoService(TopiaSqlDllSupport.class)
public class H2TopiaSqlDllSupportImpl implements TopiaSqlDllSupport {

    public static String CLASSIFIER = "H2";

    @Override
    public String getClassifier() {
        return CLASSIFIER;
    }

    @Override
    public String getPrimaryKeyConstraintName(TopiaSqlSupport tx, String schemaName, String tableName) {
        return TopiaSqlDllSupport.selectFirst(tx, String.format("SELECT constraint_name FROM INFORMATION_SCHEMA.Constraints WHERE table_schema='%S' AND table_name='%S' AND constraint_type='PRIMARY KEY';", schemaName, tableName));
    }

    @Override
    public String getUniqueConstraintName(TopiaSqlSupport tx, String tableName, String columnName) {
        List<String> result = TopiaSqlDllSupport.selectAll(tx, String.format("SELECT CONSTRAINT_NAME FROM INFORMATION_SCHEMA.Constraints WHERE CONSTRAINT_TYPE='UNIQUE' AND TABLE_NAME='%S' AND Column_list ='%S';", tableName, columnName));
        if (result.isEmpty()) {
            throw new TopiaException(String.format("No unique constraint found for table %s and column %s", tableName, columnName));
        }
        return result.get(0);
    }

    @Override
    public String getFirstTableUniqueConstraintName(TopiaSqlSupport tx, String tableName) {
        List<String> result = TopiaSqlDllSupport.selectAll(tx, String.format("select CONSTRAINT_NAME from INFORMATION_SCHEMA.Constraints where CONSTRAINT_TYPE='UNIQUE' AND TABLE_NAME='%S';", tableName));
        if (result.isEmpty()) {
            throw new TopiaException("Aucune contrainte de type unique trouvée sur la table " + tableName);
        }
        return result.get(0);
    }

    @Override
    public Set<String> getConstraintNames(TopiaSqlSupport tx, String tableName) {
        return TopiaSqlDllSupport.selectAllAsSet(tx, String.format("SELECT DISTINCT constraint_name FROM INFORMATION_SCHEMA.Constraints WHERE (CONSTRAINT_TYPE='UNIQUE' OR CONSTRAINT_TYPE='REFERENTIAL') AND TABLE_NAME='%S';", tableName));
    }

    @Override
    public Set<String> getForeignKeyConstraintNames(TopiaSqlSupport tx, String tableName) {
        return TopiaSqlDllSupport.selectAllAsSet(tx, String.format("SELECT DISTINCT constraint_name FROM INFORMATION_SCHEMA.Constraints WHERE CONSTRAINT_TYPE='REFERENTIAL' AND TABLE_NAME='%S';", tableName));
    }

    @Override
    public String getForeignKeyConstraintName(TopiaSqlSupport tx, String schemaName, String tableName, String columnName, boolean mustExists) {

        List<String> result = TopiaSqlDllSupport.selectAll(tx, String.format("SELECT DISTINCT constraint_name FROM INFORMATION_SCHEMA.Constraints " +
                                                                                     "WHERE CONSTRAINT_TYPE='REFERENTIAL' " +
                                                                                     "AND CONSTRAINT_SCHEMA='%S' " +
                                                                                     "AND TABLE_NAME='%S' " +
                                                                                     "AND COLUMN_LIST='%S';", schemaName, tableName, columnName));
        String constraintName;
        if (result.isEmpty()) {
            if (mustExists) {
                throw new IllegalStateException("Could not find constraint name for " + schemaName + "." + tableName + "." + columnName);
            }
            constraintName = null;
        } else {
            constraintName = result.get(0);
        }
        return constraintName;
    }

    @Override
    public Set<String> getUniqueKeyConstraintNames(TopiaSqlSupport tx, String tableName) {
        return TopiaSqlDllSupport.selectAllAsSet(tx, String.format("SELECT DISTINCT constraint_name FROM INFORMATION_SCHEMA.Constraints WHERE CONSTRAINT_TYPE='UNIQUE' AND TABLE_NAME='%S';", tableName));
    }

    @Override
    public Set<String> removeFK(TopiaSqlSupport tx, String tableName) {
        Set<String> result = new LinkedHashSet<>();
        Set<String> fkNames = getForeignKeyConstraintNames(tx, tableName);
        for (String constraintName : fkNames) {
            result.add(String.format("ALTER TABLE %s DROP CONSTRAINT %s;", tableName, constraintName));
        }
        return result;
    }

    @Override
    public String removeFK(TopiaSqlSupport tx, String schemaName, String tableName, String columnName) {
        String contrainstName = getForeignKeyConstraintName(tx, schemaName, tableName, columnName, true);
        return String.format("ALTER TABLE %s.%s DROP CONSTRAINT %s;", schemaName, tableName, contrainstName);
    }

    @Override
    public Optional<String> removeFKIfExists(TopiaSqlSupport tx, String schemaName, String tableName, String columnName) {
        String constraintName = getForeignKeyConstraintName(tx, schemaName, tableName, columnName, false);
        if (constraintName != null) {
            return Optional.of(String.format("ALTER TABLE %s.%s DROP CONSTRAINT %s;", schemaName, tableName, constraintName));
        }
        return Optional.empty();
    }

    @Override
    public Optional<String> removePKIfExists(TopiaSqlSupport tx, String schemaName, String tableName) {
        String constraintName = getPrimaryKeyConstraintName(tx, schemaName, tableName);
        if (constraintName != null) {
            return Optional.of(String.format("ALTER TABLE %s.%s DROP CONSTRAINT %s;", schemaName, tableName, constraintName));
        }
        return Optional.empty();
    }

    @Override
    public Set<String> removeUK(TopiaSqlSupport tx, String tableName) {
        Set<String> result = new LinkedHashSet<>();
        Set<String> uKNames = getUniqueKeyConstraintNames(tx, tableName);
        for (String contrainstName : uKNames) {
            result.add(String.format("ALTER TABLE %s DROP CONSTRAINT %s;", tableName, contrainstName));
        }
        return result;
    }

    @Override
    public String dropSchema(TopiaSqlSupport tx, String schemaName) {
        return String.format("DROP SCHEMA %s;", schemaName);
    }

    @Override
    public String dropTable(TopiaSqlSupport tx, String schemaName, String tableName) {
        return String.format("DROP TABLE %s.%s;", schemaName, tableName);
    }
}
