package org.nuiton.topia.persistence.pager;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Represents the result of a pagination request. It contains the result elements together with the
 * {@link PaginationParameter} used to compute it. The class also contains methods to
 * navigate through the other pages.
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public class PaginationResult<O> implements Serializable {

    private static final long serialVersionUID = 1L;

    protected List<O> elements;
    protected long count;
    protected PaginationParameter currentPage;

    protected PaginationResult(List<O> elements, long count, PaginationParameter currentPage) {
        this.elements = elements;
        this.count = count;
        this.currentPage = currentPage;
    }

    /**
     * Creates an instance using the already computed list of {code}elements{/code} and {code}count{/count}, together
     * with the {code}currentPage{/code} {@link PaginationParameter} used to build it.
     *
     * @param elements    the list of elements
     * @param count       the total number of elements (through all pages)
     * @param currentPage the PaginationParameter used to build this paged result
     * @param <T>         any object type
     * @return the built instance of PaginationResult
     */
    public static <T> PaginationResult<T> of(List<T> elements, long count, PaginationParameter currentPage) {
        return new PaginationResult<>(elements, count, currentPage);
    }

    /**
     * Creates an instance using the full list of elements ({code}fullList{/code}) and the {code}requestedPage{/code}
     * {@link PaginationParameter}. The built instance of PaginationResult will contain a sub list of the given
     * {code}fullList{/code} parameter.
     *
     * @param fullList      the full list of elements
     * @param requestedPage the PaginationParameter to use to build this paged result
     * @param <T>           any object type
     * @return the built instance of PaginationResult.
     */
    public static <T> PaginationResult<T> fromFullList(List<T> fullList, PaginationParameter requestedPage) {

        List<T> subList;

        int startIndex = requestedPage.getStartIndex();

        if (requestedPage.isAll()) {
            // Full list requested, use the full list
            subList = fullList;
        } else if (startIndex >= fullList.size()) {
            // If requested page is out of range, return an empty list
            subList = new LinkedList<>();
        } else {
            int toIndex = Math.min(requestedPage.getEndIndex() + 1, fullList.size());
            subList = fullList.subList(startIndex, toIndex);
        }

        return PaginationResult.of(subList, fullList.size(), requestedPage);
    }

    public List<O> getElements() {
        return elements;
    }

    public long getCount() {
        return count;
    }

    public PaginationParameter getCurrentPage() {
        return currentPage;
    }

    public PaginationParameter getNextPage() {
        int nextPageNumber = currentPage.getPageNumber() + 1;
        int pageSize = currentPage.getPageSize();
        List<PaginationOrder> orderClauses = currentPage.getOrderClauses();
        return PaginationParameter.
                builder(nextPageNumber, pageSize).
                addOrderClauses(orderClauses).
                build();
    }

    public PaginationParameter getPreviousPage() {
        // XXX AThimel 21/05/14 Maybe, do not fail, just return the first page ?
        Preconditions.checkState(hasPreviousPage(), "You cannot get a previous page to the first one");
        int previousPageNumber = currentPage.getPageNumber() - 1;
        int pageSize = currentPage.getPageSize();
        List<PaginationOrder> orderClauses = currentPage.getOrderClauses();
        return PaginationParameter.
                builder(previousPageNumber, pageSize).
                addOrderClauses(orderClauses).
                build();
    }

    public PaginationParameter getFirstPage() {
        int firstPageNumber = 0;
        int pageSize = currentPage.getPageSize();
        List<PaginationOrder> orderClauses = currentPage.getOrderClauses();
        return PaginationParameter.
                builder(firstPageNumber, pageSize).
                addOrderClauses(orderClauses).
                build();
    }

    public PaginationParameter getLastPage() {
        // AThimel 28/05/14 Math.max(0, ...) to make sure last page is working even if there is no result
        int lastPageNumber = Math.max(0, getPageCount() - 1);
        int pageSize = currentPage.getPageSize();
        List<PaginationOrder> orderClauses = currentPage.getOrderClauses();
        return PaginationParameter.
                builder(lastPageNumber, pageSize).
                addOrderClauses(orderClauses).
                build();
    }

    public int getPageCount() {
        int pageCount = 1;
        int pageSize = currentPage.getPageSize();
        if (pageSize >= 1) {
            double countDouble = Long.valueOf(count).doubleValue();
            double pageSizeDouble = Integer.valueOf(pageSize).doubleValue();
            double pageNumberDouble = Math.ceil(countDouble / pageSizeDouble);
            pageCount = Double.valueOf(pageNumberDouble).intValue();
        }
        return pageCount;
    }

    public boolean hasNextPage() {
        int lastPageNumber = getPageCount() - 1;
        return currentPage.getPageNumber() < lastPageNumber;
    }

    public boolean hasPreviousPage() {
        return currentPage.getPageNumber() > 0;
    }

    /**
     * Creates an instance of PaginationResult transforming the current one using the given function
     */
    public <T> PaginationResult<T> transform(Function<? super O, ? extends T> function) {
        List<T> transformedElements = getElements().stream()
                .map(function)
                .collect(Collectors.toList());
        return PaginationResult.of(transformedElements, getCount(), getCurrentPage());
    }

}
