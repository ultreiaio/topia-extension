package org.nuiton.topia.persistence.filter;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Represents operation on a {@link EntityFilterProperty}.
 * <p>
 * Created on 30/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.64
 */
public enum FilterOperation {

    EQUALS("equals", true),
    NULL("null", false),
    NOT_NULL("not_null", false),
    NOT_EQUALS("not_equals", true),
    MIN("min", true),
    MAX("max", true);

    private final String flavor;
    private final boolean needValue;

    public static FilterOperation fromFlavor(String flavor) {
        for (FilterOperation value : values()) {
            if (value.flavor().equalsIgnoreCase(flavor)) {
                return value;
            }
        }
        throw new IllegalStateException(String.format("Can't find operation from flavor: %s", flavor));
    }

    FilterOperation(String flavor, boolean needValue) {
        this.flavor = flavor;
        this.needValue = needValue;
    }

    public String flavor() {
        return flavor;
    }

    public boolean needValue() {
        return needValue;
    }
}
