package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.pager.PaginationOrder;

import java.util.Collection;
import java.util.LinkedHashSet;

/**
 * Represents a step when building a query to add a constraint.
 * <p>
 * The builder implements the fluent interface DP, so you can add multiple constraints by chaining calls.
 *
 * @author bleny
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public interface TopiaQueryBuilderAddCriteriaStep<E extends TopiaEntity> {

    /**
     * @param property the name of a field of the queried entity, must be a one-to-one or a many-to-one property.
     * @param value    the value the field of the entity must be equals to
     * @return the current or next step for a fluent interface usage
     */
    TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addEquals(String property, Object value);

    /**
     * @param property the name of a field of the queried entity, must be a one-to-one or a many-to-one property.
     * @param value    the value the field of the entity must not be equals to
     * @return the current or next step for a fluent interface usage
     */
    TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addNotEquals(String property, Object value);

    /**
     * @param property the name of a field of the queried entity, must be a one-to-one or a many-to-one property
     * @param values   a collection of values the field of the entity must be equals to
     * @return the current or next step for a fluent interface usage
     */
    TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addIn(String property, Collection<?> values);

    /**
     * @param property the name of a field of the queried entity, must be a one-to-one or a many-to-one property
     * @param values   the value the field of the entity must not be equals to
     * @return the current or next step for a fluent interface usage
     */
    TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addNotIn(String property, Collection<?> values);

    /**
     * @param property the name of a field of the queried entity, must be a one-to-many or a many-to-many property
     * @param value    the property of the entity must be a collection that contains value
     * @return the current or next step for a fluent interface usage
     */
    TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addContains(String property, Object value);

    /**
     * @param property the name of a field of the queried entity, must be a one-to-many or a many-to-many property
     * @param value    the property of the entity must be a collection that doesn't contain value
     * @return the current or next step for a fluent interface usage
     */
    TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addNotContains(String property, Object value);

    /**
     * @param property the name of a field of the queried entity, must be a one-to-one or a many-to-one property
     * @return the current or next step for a fluent interface usage
     */
    TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addNull(String property);

    /**
     * @param property the name of a field of the queried entity, must be a one-to-one or a many-to-one property
     * @return the current or next step for a fluent interface usage
     */
    TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addNotNull(String property);

    /**
     * This method has the same behavior as {@link #addEquals(String, Object)} but you don't need to have the entity but
     * only the topiaId.
     * <p>
     * This method is useful when you want to do a {@link #addEquals(String, Object)} but you don't
     * have the entity you want to give as an argument, you only have the topiaId.
     *
     * <pre>
     *     // given that we want to find an entity that has a boat property valued to a
     *     // boat which topiaId is boatId, we could write:
     *
     *     addEquals("boat.topiaId", boatId); // boatId is a topiaId
     *
     *     // but instead, you can write
     *
     *     addTopiaIdEquals("boat", boatId); // boat is a topia entity
     * </pre>
     *
     * @param property the name of a field of the queried entity, must be a one-to-one or a many-to-one property
     * @param topiaId  the value the topiaId of the entity must be equals to
     * @return the current or next step for a fluent interface usage
     */
    TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addTopiaIdEquals(String property, String topiaId);

    /**
     * This method has the same behavior as {@link #addNotEquals(String, Object)} but you don't need to have the entity
     * but only the topiaId.
     *
     * @param property the name of a field of the queried entity, must be a one-to-one or a many-to-one property
     * @param topiaId  the value the topiaId of the entity must not be equals to
     * @return the current or next step for a fluent interface usage
     */
    TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addTopiaIdNotEquals(String property, String topiaId);

    /**
     * This method has the same behavior as {@link #addIn(String, Collection)} but you don't need to have the entity but
     * only the topiaId.
     *
     * @param property the name of a field of the queried entity, must be a one-to-one or a many-to-one property
     * @param topiaIds a collection of values the topiaId of the entity must be equals to
     * @return the current or next step for a fluent interface usage
     */
    TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addTopiaIdIn(String property, Collection<String> topiaIds);

    /**
     * This method has the same behavior as {@link #addNotIn(String, Collection)} but you don't need to have the entity
     * but only the topiaId.
     *
     * @param property the name of a field of the queried entity, must be a one-to-one or a many-to-one property
     * @param topiaIds a collection of values the topiaId of the entity must not be equals to
     * @return the current or next step for a fluent interface usage
     */
    TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addTopiaIdNotIn(String property, Collection<String> topiaIds);

    /**
     * @param property a property to load (fetch) with the entity(ies) (in a single query)
     * @return the current or next step for a fluent interface usage
     */
    TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addFetch(String property);

    /**
     * @param property        a property to load (fetch) with the entity(ies) (in a single query)
     * @param otherProperties an optional array of additional properties to load (fetch) with the entity(ies)
     * @return the current or next step for a fluent interface usage
     */
    TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addAllFetches(String property, String... otherProperties);

    /**
     * @param properties a collection of properties to load (fetch) with the entity(ies) (in a single query)
     * @return the current or next step for a fluent interface usage
     */
    TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addAllFetches(Collection<String> properties);

    /**
     * @param orderByArguments list of arguments that will be added as order by
     * @return the next step as this method must be used at the end
     */
    TopiaQueryBuilderRunQueryStep<E> setOrderByArguments(LinkedHashSet<String> orderByArguments);

    /**
     * @param orderByArguments list of arguments that will be added as order by
     * @return the next step as this method must be used at the end
     */
    TopiaQueryBuilderRunQueryStep<E> setOrderByArguments(String... orderByArguments);

    /**
     * @param paginationOrders list of {@link PaginationOrder} that will be added as order by
     * @return the next step as this method must be used at the end
     * @see PaginationOrder
     */
    TopiaQueryBuilderRunQueryStep<E> setOrderByArguments(Collection<PaginationOrder> paginationOrders);

    void whereStringEquals(String propertyName, String propertyValue);

    void whereStringNotEquals(String propertyName, String propertyValue);

    void addLowerThan(String property, java.util.Date date);

    void addLowerOrEquals(String property, java.util.Date date);

    void addGreaterThan(String property, java.util.Date date);

    void addGreaterOrEquals(String property, java.util.Date date);

    void addLowerThan(String property, Number number);

    void addLowerOrEquals(String property, Number number);

    void addGreaterThan(String property, Number number);

    void addGreaterOrEquals(String property, Number number);

    void setOrder(LinkedHashSet<String> order);

    void setCaseSensitive(boolean caseSensitive);

    void addLike(String property, String value);

    void addNotLike(String property, String value);


}
