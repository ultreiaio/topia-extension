package org.nuiton.topia.persistence.filter.property;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaQueryBuilderAddCriteriaOrRunQueryStep;
import org.nuiton.topia.persistence.filter.EntityFilterProperty;

import java.util.function.Function;

/**
 * Created on 05/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.35
 */
public class StringFilterProperty extends ObjectFilterPropertySupport<String> {
    public static StringFilterProperty create(String propertyName) {
        return create(propertyName, "");
    }

    public static StringFilterProperty createCode(String propertyName) {
        return create(propertyName, EntityFilterProperty.CLASSIFIER_CODE);
    }

    public static StringFilterProperty createLabel(String propertyName) {
        return create(propertyName, EntityFilterProperty.CLASSIFIER_LABEL);
    }

    public static StringFilterProperty create(String propertyName, String classifier) {
        return new StringFilterProperty(propertyName, classifier);
    }

    public StringFilterProperty(String propertyName, String classifier) {
        super(propertyName, FLAVORS_OBJECT, classifier, String.class, propertyName + classifier, Function.identity(), "");
    }

    @Override
    protected <E extends TopiaEntity> void addNotEquals(TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> queryBuilder, String propertyKey, String value) {
        queryBuilder.whereStringNotEquals(propertyKey, value);
    }

    @Override
    protected <E extends TopiaEntity> void addEquals(TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> queryBuilder, String propertyKey, String value) {
        queryBuilder.whereStringEquals(propertyKey, value);
    }
}
