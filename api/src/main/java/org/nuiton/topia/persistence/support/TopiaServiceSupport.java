package org.nuiton.topia.persistence.support;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaService;

import java.util.Map;

/**
 * This API provides methods to manipulate services
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public interface TopiaServiceSupport {

    /**
     * @return The list of registered services. The map key is the service name, and the value is the service instance.
     */
    Map<String, TopiaService> getServices();

    /**
     * @param interfaceService FIXME
     * @param <T>              type of service
     * @return Same as {@link #getServices()} but returns only services that implements
     * given contract (or is instance of given class).
     */
    <T extends TopiaService> Map<String, T> getServices(Class<T> interfaceService);

}
