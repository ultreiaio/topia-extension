package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Exception thrown when something went wrong during event firing
 *
 * Created: 5 janv. 2006 00:47:51
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 */
public class TopiaVetoException extends TopiaException {

    private static final long serialVersionUID = 6809613247516488399L;

    /**
     * Default constructor.
     */
    public TopiaVetoException() {
    }

    /**
     * Constructor with {@code message}.
     *
     * @param message exception message
     */
    public TopiaVetoException(String message) {
        super(message);
    }

    /**
     * Constructor for a wrapped TopiaVetoException over a {@code cause} with a {@code message}.
     *
     * @param message exception message
     * @param cause   exception cause
     */
    public TopiaVetoException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor for a wrapped TopiaVetoException over a {@code cause}.
     *
     * @param cause exception cause
     */
    public TopiaVetoException(Throwable cause) {
        super(cause);
    }

}
