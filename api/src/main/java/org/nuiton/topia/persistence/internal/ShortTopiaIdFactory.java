package org.nuiton.topia.persistence.internal;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.io.BaseEncoding;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaIdFactory;

import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * <p>
 * Does the same than {@link FullyQualifiedNamePlusUuidTopiaIdFactory} but produces shorter topiaId. The class-name part uses only simple
 * class names, and the random part is base64 encoded.
 * </p>
 * Note: This TopiaIdFactory requires that you do not have several entities with the same simple class name.
 *
 * @author Arnaud Thimel (Code Lutin)
 */
public class ShortTopiaIdFactory implements TopiaIdFactory {

    private static final long serialVersionUID = 1;

    private static final Logger log = LogManager.getLogger(ShortTopiaIdFactory.class);

    protected static final Map<String, Class<? extends TopiaEntity>> ENTITY_CLASSES = Maps.newConcurrentMap();

    @Override
    public <E extends TopiaEntity> String newTopiaId(Class<E> entityClass, TopiaEntity topiaEntity) {
        String uuid = UUID.randomUUID().toString();
        String escapedUuid = uuid.replace("-", "").toUpperCase();
        byte[] bytes = BaseEncoding.base16().decode(escapedUuid);
        String randomPart = BaseEncoding.base64Url().encode(bytes);
        // Base64 may produce tailing '=' as padding, remove it
        randomPart = randomPart.replace("=", "");
        return newTopiaId(entityClass, randomPart);
    }

    @Override
    public <E extends TopiaEntity> String newTopiaId(Class<E> entityClass, String randomPart) {
        if (!entityClass.isInterface()) {
            throw new IllegalArgumentException(
                    "Only interface is permit to create id: " + entityClass);
        }
        String simpleName = entityClass.getSimpleName();
        Preconditions.checkArgument(!simpleName.contains(getSeparator()), "Your entity class name must not contains the separator");
        return simpleName + getSeparator() + randomPart;
    }

    @Override
    public <E extends TopiaEntity> Class<E> getClassName(String topiaId) {
        String simpleClassName = StringUtils.substringBefore(topiaId, getSeparator());
        try {
            return findClassName(simpleClassName);
        } catch (ClassNotFoundException cnfe) {
            throw new TopiaException("Can't find class for topiaId = '" + topiaId + "'", cnfe);
        }
    }

    protected <E extends TopiaEntity> Class<E> findClassName(String simpleClassName) throws ClassNotFoundException {
        Class<E> result;
        if (ENTITY_CLASSES.containsKey(simpleClassName)) {
            result = (Class<E>) ENTITY_CLASSES.get(simpleClassName);
        } else {
            Set<Class<?>> candidates = Sets.newHashSet();
            for (Package aPackage : Package.getPackages()) {
                String fqn = aPackage.getName() + "." + simpleClassName;
                try {
                    Class<?> aClass = Class.forName(fqn);
                    candidates.add(aClass);
                } catch (Exception eee) {
                    // Nothing to do
                }
            }
            Iterables.removeIf(candidates, new Predicate<>() {
                @Override
                public boolean apply(Class<?> aClass) {
                    return !TopiaEntity.class.isAssignableFrom(aClass);
                }
            });
            if (candidates.size() == 1) {
                result = (Class<E>) candidates.iterator().next();
                ENTITY_CLASSES.put(simpleClassName, result);
            } else {
                String message = String.format("Unable to find class with simple name=%s. " +
                                                       "Candidates are: %s", simpleClassName, candidates);
                throw new ClassNotFoundException(message);
            }
        }
        return result;
    }

    @Override
    public String getSeparator() {
        return "_";
    }

    @Override
    public String getRandomPart(String topiaId) {
        return StringUtils.substringAfter(topiaId, getSeparator());
    }

    @Override
    public boolean isTopiaId(String str) {
        boolean isTopiaId = false;
        if (str != null) {
            String simpleClassName = StringUtils.substringBefore(str, getSeparator());
            try {
                findClassName(simpleClassName);
                isTopiaId = StringUtils.isNotBlank(StringUtils.substringAfter(str, getSeparator()));
            } catch (ClassNotFoundException eee) {
                // nothing to do, just return false
                if (log.isDebugEnabled()) {
                    log.debug(eee);
                }
            }
        }
        return isTopiaId;
    }

}
