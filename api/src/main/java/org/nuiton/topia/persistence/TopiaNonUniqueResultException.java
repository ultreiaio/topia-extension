package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Map;

/**
 * Exception raised when a query returns more than one result while it was expected to return only one (or no result).
 *
 * @since 3.0
 */
public class TopiaNonUniqueResultException extends TopiaQueryException {

    private static final long serialVersionUID = 8057839164405947600L;

    public TopiaNonUniqueResultException(String hql, Map<String, Object> hqlParameters) {
        super("Given query returns more than one result", hql, hqlParameters);
    }

}
