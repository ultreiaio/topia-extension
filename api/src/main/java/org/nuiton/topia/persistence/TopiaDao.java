package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.internal.AbstractTopiaPersistenceContext;
import org.nuiton.topia.persistence.pager.FilterRuleGroupOperator;
import org.nuiton.topia.persistence.pager.PaginationResult;
import org.nuiton.topia.persistence.support.QuerySupport;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * This contract represents the common operations any Dao should be able to provide as API.
 *
 * @author bleny
 * @since 3.0
 */
public interface TopiaDao<E extends TopiaEntity> extends QuerySupport {

    /**
     * Return the class of the entity managed by this DAO.
     *
     * @return this DAO's managed entity's class
     */
    Class<E> getEntityClass();

    /**
     * Obtains the batch size used to load data.
     * <p>
     * Default value if 1000.
     *
     * @return the batch size.
     * @since 2.6.14
     */
    int getBatchSize();

    /**
     * Set a new default batch size.
     *
     * @param batchSize new batch size to use when iterating.
     * @since 2.6.14
     */
    void setBatchSize(int batchSize);

    /**
     * @return instantiate of managed entity <strong>not persisted</strong>.
     * @since 2.3.1
     */
    E newInstance();

    /**
     * @return instantiate of managed entity <strong>not persisted</strong>.
     * @since 2.6
     */
    E newInstance(Map<String, Object> properties);

    /**
     * @return instantiate of managed entity <strong>not persisted</strong>.
     * @since 2.6
     */
    E newInstance(String propertyName, Object propertyValue, Object... otherPropertyNamesAndValues);

    /**
     * Creates a new pager initialized for the first page of all data of the dao.
     *
     * <strong>Note:</strong> This method will execute a count query to init
     * the pager.
     *
     * @param pageSize size of a page
     * @return the initialized pager.
     * @since 3.0
     */
    PaginationResult<E> initPagination(int pageSize);

    /**
     * Creates a new pager initialized for the first page of data of the given
     * query.
     *
     * <strong>Note:</strong> This method will execute a count query to init
     * the pager.
     *
     * @param hql      query
     * @param params   params of the query
     * @param pageSize size of a page
     * @return the initialized pager.
     * @since 3.0
     */
    PaginationResult<E> initPagination(String hql, Map<String, Object> params, int pageSize);

    /**
     * Set id on not persisted entity.
     * @param entity the instance to check
     * @return entity with id
     * @since 4.0
     */
    E initId(E entity);

    /**
     * Creates an entity not created without the DAO using any of the others
     * create methods. The instance may have been created elsewhere.
     *
     * @param entity the instance to persist
     * @return the persisted entity (with its topiaId valued)
     * @since 2.3.1
     */
    E create(E entity);

    /**
     * @param propertyName                FIXME
     * @param propertyValue               FIXME
     * @param otherPropertyNamesAndValues FIXME
     * @return FIXME
     * @since 3.0
     */
    E create(String propertyName, Object propertyValue, Object... otherPropertyNamesAndValues);

    /**
     * Creates a new instance of the entity managed by the DAO
     *
     * @param properties the key-value list of properties that the created entity will have.
     * @return the newly created entity
     * @throws IllegalArgumentException if some property type is not the expected one
     */
    E create(Map<String, Object> properties);

    /**
     * @return FIXME
     * @since 3.0
     */
    E create();

    /**
     * Update an entity. May be used for an entity coming from another context.
     *
     * @param entity the entity to create or update
     * @return the given entity
     */
    E update(E entity);

    /**
     * Deletes the given entity from the storage
     *
     * @param entity the entity to remove
     */
    void delete(E entity);

    /**
     * Finds all the entities managed by this DAO.
     *
     * @return the full list of entities in no particular (non-deterministic) order
     */
    List<E> findAll();

    /**
     * Stream all the entities managed by this DAO.
     * <p>
     * Actual behavior rely on implementation: caller should {@link Stream#close()} the stream (may depend on implementation).
     *
     * @return the full list of entities in no particular (non-deterministic) order
     */
    Stream<E> streamAll();

    /**
     * Find all the ids for the entities managed by this DAO.
     *
     * @return the ids of all the entities
     */
    // TODO AThimel 20/07/13 This method should return a Set ?
    List<String> findAllIds();

    /**
     * Count the number of existing entities.
     *
     * @return number of total entities
     * @since 2.3.4
     */
    long count();

    /**
     * Creates a QueryBuilder without restriction
     *
     * @return FIXME
     * @since 3.0
     */
    TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> forAll();

    /**
     * @param properties FIXME
     * @return FIXME
     * @since 3.0
     */
    TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> forProperties(Map<String, Object> properties);

    /**
     * @param propertyName                FIXME
     * @param propertyValue               FIXME
     * @param otherPropertyNamesAndValues FIXME
     * @return FIXME
     * @since 3.0
     */
    TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> forProperties(String propertyName,
                                                                Object propertyValue,
                                                                Object... otherPropertyNamesAndValues);

    /**
     * @return FIXME
     * @since 3.0
     */
    TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> newQueryBuilder(FilterRuleGroupOperator filterRuleGroupOperator);

    /**
     * @param propertyName  FIXME
     * @param propertyValue FIXME
     * @return FIXME
     * @since 3.0
     */
    TopiaQueryBuilderRunQueryStep<E> forContains(String propertyName, Object propertyValue);

    /**
     * @param propertyName  FIXME
     * @param propertyValue FIXME
     * @return FIXME
     * @since 3.0
     */
    TopiaQueryBuilderRunQueryStep<E> forEquals(String propertyName, Object propertyValue);

    /**
     * @param propertyName   FIXME
     * @param propertyValues FIXME
     * @return FIXME
     * @since 3.0
     */
    TopiaQueryBuilderRunQueryStep<E> forIn(String propertyName, Collection<?> propertyValues);

    /**
     * @param topiaId FIXME
     * @return FIXME
     * @since 3.0
     */
    TopiaQueryBuilderRunQueryWithUniqueResultStep<E> forTopiaIdEquals(String topiaId);

    /**
     * @param topiaIds FIXME
     * @return FIXME
     * @since 3.0
     */
    TopiaQueryBuilderRunQueryStep<E> forTopiaIdIn(Collection<String> topiaIds);


    /**
     * <b>Note:</b> this method is generated, so we don't need any more to do new {@code Class.newInstance()}
     *
     * @return a new instance
     */
    E newInstance0();

    TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> newQueryBuilder();

    void init(AbstractTopiaPersistenceContext persistenceContext);

    long findSingleResult(TopiaSqlQuery<Long> query);

    <O> List<O> findMultipleResult(TopiaSqlQuery<O> query);

    TopiaDaoSupplier topiaDaoSupplier();

    TopiaSqlSupport getTopiaSqlSupport();

    TopiaEntityEnum getTopiaEntityEnum();
}
