package org.nuiton.topia.persistence;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.ServiceLoaders;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * Created on 17/06/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.5
 */
public abstract class TopiaApplicationContextFactory<P extends TopiaPersistenceContext, A extends TopiaApplicationContext<P>> {

    private static final Logger log = LogManager.getLogger(TopiaApplicationContextFactory.class);
    public static TopiaApplicationContextFactory<?, ?> INSTANCE;
    final Map<String, A> cache = new TreeMap<>();

    public static TopiaApplicationContextFactory<?, ?> get() {
        if (INSTANCE == null) {
            INSTANCE = ServiceLoaders.loadUniqueService(TopiaApplicationContextFactory.class);
        }
        return INSTANCE;
    }

    public static <P extends TopiaPersistenceContext, A extends TopiaApplicationContext<P>> A createTopiaApplicationContext(TopiaConfigurationExtension topiaConfiguration) {
        @SuppressWarnings("unchecked") TopiaApplicationContextFactory<P, A> topiaApplicationContextFactory = (TopiaApplicationContextFactory<P, A>) get();
        return topiaApplicationContextFactory.createApplicationContext(topiaConfiguration, topiaApplicationContextFactory.cache);
    }

    public static <P extends TopiaPersistenceContext, A extends TopiaApplicationContext<P>> A getTopiaApplicationContext(String authenticationToken) {
        @SuppressWarnings("unchecked") TopiaApplicationContextFactory<P, A> topiaApplicationContextFactory = (TopiaApplicationContextFactory<P, A>) get();
        return topiaApplicationContextFactory.getTopiaApplicationContext0(authenticationToken);
    }

    public static void clearContexts(String authenticationToken) {
        get().clearContexts0(authenticationToken);
    }

    public static void close() {
        get().close0();
    }

    protected static void close(TopiaApplicationContext<?> topiaApplicationContext) {
        if (!topiaApplicationContext.isClosed()) {
            try {
                topiaApplicationContext.close();
            } catch (Exception e) {
                log.error(String.format("Could not close topiaApplicationContext: %s", topiaApplicationContext), e);
            }
        }
    }

    protected abstract A createApplicationContext(TopiaConfigurationExtension topiaConfiguration, Map<String, A> cache);

    public A getTopiaApplicationContext0(String authenticationToken) {
        A result = cache.get(authenticationToken);
        Objects.requireNonNull(result, "Did not find how to create A from: " + authenticationToken);
        return result;
    }

    public void clearContexts0(String authenticationToken) {
        A toRemove = cache.remove(authenticationToken);
        if (toRemove == null) {
            // nothing to clean
            return;
        }
        log.info(String.format("Close remaining topia application context for: %s", authenticationToken));
        close(toRemove);
        log.info(String.format("Still using %d topia application context(s).", cache.size()));
    }

    public void close0() {
        for (A topiaApplicationContext : new LinkedHashSet<>(cache.values())) {
            close(topiaApplicationContext);
        }
        cache.clear();
    }

}
