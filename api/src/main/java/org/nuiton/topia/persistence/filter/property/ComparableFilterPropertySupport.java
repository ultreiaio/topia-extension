package org.nuiton.topia.persistence.filter.property;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaQueryBuilderAddCriteriaOrRunQueryStep;
import org.nuiton.topia.persistence.filter.FilterOperation;

import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

/**
 * Created on 05/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.35
 */
public abstract class ComparableFilterPropertySupport<O extends Comparable<?>> extends ObjectFilterPropertySupport<O> {

    public ComparableFilterPropertySupport(String propertyName, Class<O> type, Function<String, O> factory, O defaultValue) {
        this(propertyName, FLAVORS_COMPARABLE, type, factory, defaultValue);
    }

    public ComparableFilterPropertySupport(String propertyName, Set<FilterOperation> flavors, Class<O> type, Function<String, O> factory, O defaultValue) {
        super(propertyName, flavors, "", type, factory, defaultValue);
    }

    protected abstract <E extends TopiaEntity> void addMin(TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> queryBuilder, String persistencePropertyName, O propertyValue);

    protected abstract <E extends TopiaEntity> void addMax(TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> queryBuilder, String persistencePropertyName, O propertyValue);

    @Override
    protected final <E extends TopiaEntity> void consumeObject(TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> queryBuilder, FilterOperation flavor, String labelColumnName, O propertyValue) {
        String persistencePropertyName = getPersistencePropertyKey(labelColumnName);
        switch (Objects.requireNonNull(flavor)) {
            case MIN:
                addMin(queryBuilder, persistencePropertyName, propertyValue);
                return;
            case MAX:
                addMax(queryBuilder, persistencePropertyName, propertyValue);
                return;
        }
        super.consumeObject(queryBuilder, flavor, labelColumnName, propertyValue);
    }
}
