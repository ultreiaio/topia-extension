package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

public class HqlAndParametersBuilderTest {

    protected class MyHqlAndParametersBuilder extends HqlAndParametersBuilder {

        public MyHqlAndParametersBuilder(Class entityClass) {
            super(entityClass);
        }

        @Override
        public String putHqlParameterWithAvailableName(String propertyName, Object value) {
            return super.putHqlParameterWithAvailableName(propertyName, value);
        }

    }

    protected MyHqlAndParametersBuilder hqlAndParametersBuilder =
            new MyHqlAndParametersBuilder(TopiaEntity.class);

    protected static final Collection<String> SOME_VALUES =
            Lists.newArrayList("value1", "value2", "value3");

    protected static final Collection<String> SOME_VALUES_WITH_NULL =
            Lists.newArrayList("value1", "value2", null, "value3");

    @Test
    public void testFindAvailableHqlParameterName() {

        String availableHqlParameterName1 = hqlAndParametersBuilder.putHqlParameterWithAvailableName("survey.topiaId", "topiaId1");
        String availableHqlParameterName2 = hqlAndParametersBuilder.putHqlParameterWithAvailableName("survey.topiaId", "topiaId1");

        Assert.assertNotEquals(availableHqlParameterName1, availableHqlParameterName2);
        Assert.assertFalse(availableHqlParameterName1.contains("."));
        Assert.assertFalse(availableHqlParameterName2.contains("."));

    }

    @Test
    public void testEqualsNull() {

        hqlAndParametersBuilder.addEquals("myProp", null);

        String actualHql = hqlAndParametersBuilder.getHql();

        Assert.assertFalse(actualHql.contains("myProp = null"));
        Assert.assertTrue(actualHql.contains("myProp is null"));

    }

    @Test
    public void testAddNotEqualsToValue() {

        hqlAndParametersBuilder.addNotEquals("myProp", "value");

        String actualHql = hqlAndParametersBuilder.getHql();

        Assert.assertTrue(actualHql.contains("myProp != "));
        Assert.assertFalse(actualHql.contains("myProp = "));

    }

    @Test
    public void testAddNotEqualsToNull() {

        hqlAndParametersBuilder.addNotEquals("myProp", null);

        String actualHql = hqlAndParametersBuilder.getHql();

        Assert.assertTrue(actualHql.contains("myProp is not null"));

    }

    @Test
    public void testAddInWithEmptyValues() {

        hqlAndParametersBuilder.addIn("myProp", Collections.emptyList());

        String actualHql = hqlAndParametersBuilder.getHql();

        Assert.assertFalse(actualHql.contains("myProp in (:)"));
        Assert.assertFalse(actualHql.contains("myProp in ()"));

    }

    @Test
    public void testGetParameterName() {

        String actualParameterName = hqlAndParametersBuilder.getParameterName("yearlyDeclaration.survey.topiaId");

        Assert.assertFalse("an hql argument name must not contains '.'", actualParameterName.contains("."));
        Assert.assertFalse("an hql argument name must not contains ' '", actualParameterName.contains(" "));
        Assert.assertFalse("an hql argument name must not contains ':'", actualParameterName.contains(":"));

        Assert.assertEquals("yearlyDeclarationSurveyTopiaId", actualParameterName);

    }

    @Test
    public void testPreventRemoveParameters() {

        ImmutableMap<String, String> hqlParameters =
                ImmutableMap.of("myParameter", "aRandomValue",
                                "myParameter1", "aRandomValue1",
                                "myParameter2", "aRandomValue2");

        ImmutableMap<String, String> conflictingHqlParameters =
                ImmutableMap.of("myParameter", "anotherRandomValue",
                                "myParameter1", "anotherRandomValue1",
                                "myParameter2", "anotherRandomValue2",
                                "myParameter20", "aSafeValue");

        hqlAndParametersBuilder.addWhereClause("myProperty = :myParameter, myProperty = :myParameter1, myProperty2 = :myParameter2||", hqlParameters);

        hqlAndParametersBuilder.addWhereClause("myOtherProperty = :myParameter, myOtherProperty1 = :myParameter1, myOtherProperty2 = :myParameter2, myOtherProperty20 = :myParameter20", conflictingHqlParameters);

        Map hqlParameters1 = hqlAndParametersBuilder.getHqlParameters();
        String hql = hqlAndParametersBuilder.getHql();
        Assert.assertEquals(7, hqlParameters1.size());
        Assert.assertEquals("aRandomValue", hqlParameters1.get("myParameter"));
        Assert.assertEquals("aRandomValue1", hqlParameters1.get("myParameter1"));
        Assert.assertEquals("aRandomValue2", hqlParameters1.get("myParameter2"));
        Assert.assertEquals("aSafeValue", hqlParameters1.get("myParameter20"));
        Assert.assertEquals("anotherRandomValue", hqlParameters1.get("myParameter0")); // myParameter resolved
        Assert.assertEquals("anotherRandomValue1", hqlParameters1.get("myParameter10")); // myParameter1 resolved
        Assert.assertEquals("anotherRandomValue2", hqlParameters1.get("myParameter21")); // myParameter2 resolved

        // first sql (not modified)
        Assert.assertTrue(hql.contains("myProperty = :myParameter, myProperty = :myParameter1, myProperty2 = :myParameter2||"));
        // second sql (with resolved properties)
        Assert.assertTrue(hql.contains("myOtherProperty = :myParameter0, myOtherProperty1 = :myParameter10, myOtherProperty2 = :myParameter21, myOtherProperty20 = :myParameter20"));
    }

    @Test
    public void testRangesAll() {
        hqlAndParametersBuilder.addIn("myProperty", com.google.common.collect.Range.all());
        Assert.assertFalse(StringUtils.containsIgnoreCase(hqlAndParametersBuilder.getHql(), "where"));
    }

    @Test
    public void testRangesSingleton() {
        hqlAndParametersBuilder.addIn("myProperty", com.google.common.collect.Range.singleton(24));
        Assert.assertTrue(StringUtils.containsIgnoreCase(hqlAndParametersBuilder.getHql(), "where (topiaEntity_.myProperty >= :myProperty0) and (topiaEntity_.myProperty <= :myProperty1)"));
    }

    @Test
    public void testRangesSingleBound() {
        hqlAndParametersBuilder.addIn("myProperty", com.google.common.collect.Range.atLeast(18));
        Assert.assertTrue(StringUtils.containsIgnoreCase(hqlAndParametersBuilder.getHql(), "where topiaEntity_.myProperty >= :myProperty0"));
    }

    @Test
    public void testRanges() {
        hqlAndParametersBuilder.addIn("myProperty", com.google.common.collect.Range.openClosed(18, 25));
        Assert.assertTrue(StringUtils.containsIgnoreCase(hqlAndParametersBuilder.getHql(), "where (topiaEntity_.myProperty > :myProperty0) and (topiaEntity_.myProperty <= :myProperty1)"));
        Assert.assertEquals(18, hqlAndParametersBuilder.getHqlParameters().get("myProperty0"));
        Assert.assertEquals(25, hqlAndParametersBuilder.getHqlParameters().get("myProperty1"));
    }

    @Test
    public void testRangesNotIn() {
        hqlAndParametersBuilder.addNotIn("myProperty", com.google.common.collect.Range.openClosed(18, 25));
        Assert.assertTrue(StringUtils.containsIgnoreCase(hqlAndParametersBuilder.getHql(), "where (topiaEntity_.myProperty <= :myProperty0) and (topiaEntity_.myProperty > :myProperty1)"));
        Assert.assertEquals(18, hqlAndParametersBuilder.getHqlParameters().get("myProperty0"));
        Assert.assertEquals(25, hqlAndParametersBuilder.getHqlParameters().get("myProperty1"));
    }

    @Test
    public void testCommonRangesSingleton() {
        hqlAndParametersBuilder.addIn("myProperty", org.apache.commons.lang3.Range.is(24));
        Assert.assertTrue(StringUtils.containsIgnoreCase(hqlAndParametersBuilder.getHql(), "where (topiaEntity_.myProperty >= :myProperty0) and (topiaEntity_.myProperty <= :myProperty1)"));
    }

    @Test
    public void testCommonRanges() {
        hqlAndParametersBuilder.addIn("myProperty", org.apache.commons.lang3.Range.between(18, 25));
        Assert.assertTrue(StringUtils.containsIgnoreCase(hqlAndParametersBuilder.getHql(), "where (topiaEntity_.myProperty >= :myProperty0) and (topiaEntity_.myProperty <= :myProperty1)"));
        Assert.assertEquals(18, hqlAndParametersBuilder.getHqlParameters().get("myProperty0"));
        Assert.assertEquals(25, hqlAndParametersBuilder.getHqlParameters().get("myProperty1"));
    }

    @Test
    public void testCommonRangesNotIn() {
        hqlAndParametersBuilder.addNotIn("myProperty", org.apache.commons.lang3.Range.between(18, 25));
        Assert.assertTrue(StringUtils.containsIgnoreCase(hqlAndParametersBuilder.getHql(), "where (topiaEntity_.myProperty < :myProperty0) and (topiaEntity_.myProperty > :myProperty1)"));
        Assert.assertEquals(18, hqlAndParametersBuilder.getHqlParameters().get("myProperty0"));
        Assert.assertEquals(25, hqlAndParametersBuilder.getHqlParameters().get("myProperty1"));
    }
}
