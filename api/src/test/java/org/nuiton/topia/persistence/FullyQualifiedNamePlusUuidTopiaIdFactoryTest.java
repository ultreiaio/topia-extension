package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.topia.persistence.internal.AbstractTopiaEntity;
import org.nuiton.topia.persistence.internal.FullyQualifiedNamePlusUuidTopiaIdFactory;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class FullyQualifiedNamePlusUuidTopiaIdFactoryTest {

    interface EntityA extends TopiaEntity {
    }

    class EntityAImpl extends AbstractTopiaEntity implements EntityA {

        @Override
        public void accept(TopiaEntityVisitor visitor) throws TopiaException {

        }
    }

    interface EntityB extends TopiaEntity {
    }

    class EntityBImpl extends AbstractTopiaEntity implements EntityB {

        @Override
        public void accept(TopiaEntityVisitor visitor) throws TopiaException {

        }
    }

    protected FullyQualifiedNamePlusUuidTopiaIdFactory topiaFactory;

    public FullyQualifiedNamePlusUuidTopiaIdFactoryTest() {
        topiaFactory = new FullyQualifiedNamePlusUuidTopiaIdFactory();
    }

    @Test
    public void testNewTopiaId() {

        EntityA entityA1 = new EntityAImpl();

        EntityA entityA2 = new EntityAImpl();

        String topiaId1 = topiaFactory.newTopiaId(EntityA.class, entityA1);

        String topiaId2 = topiaFactory.newTopiaId(EntityA.class, entityA2);

        Assert.assertNotNull(topiaId1);

        Assert.assertTrue(topiaId1.startsWith(EntityA.class.getName() + topiaFactory.getSeparator()));

        Assert.assertNotEquals(topiaId1, topiaId2);

    }

    @Test
    public void testGetClassName() {

        EntityA entityA = new EntityAImpl();

        EntityB entityB = new EntityBImpl();

        String topiaIdA = topiaFactory.newTopiaId(EntityA.class, entityA);

        String topiaIdB = topiaFactory.newTopiaId(EntityB.class, entityB);

        Assert.assertEquals(EntityA.class, topiaFactory.getClassName(topiaIdA));

        Assert.assertEquals(EntityB.class, topiaFactory.getClassName(topiaIdB));

    }

    @Test
    public void testGetRandomPart() {

        EntityA entityA1 = new EntityAImpl();

        EntityA entityA2 = new EntityAImpl();

        String topiaId1 = topiaFactory.newTopiaId(EntityA.class, entityA1);

        String topiaId2 = topiaFactory.newTopiaId(EntityA.class, entityA2);

        String randomPart1 = topiaFactory.getRandomPart(topiaId1);

        String randomPart2 = topiaFactory.getRandomPart(topiaId2);

        Assert.assertNotNull(randomPart1);

        Assert.assertFalse(randomPart1.isEmpty());

        Assert.assertFalse(randomPart1.startsWith(EntityA.class.getName()));

        Assert.assertFalse(randomPart1.startsWith(topiaFactory.getSeparator()));

        Assert.assertNotEquals(randomPart1, randomPart2);

    }

    @Test
    public void testGetSeparator() {

        String separator = topiaFactory.getSeparator();

        Assert.assertNotNull(separator);

        Assert.assertFalse(separator.isEmpty());

    }

}
