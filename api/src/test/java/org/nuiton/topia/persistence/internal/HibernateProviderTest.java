package org.nuiton.topia.persistence.internal;

/*-
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.junit.Assert;
import org.junit.Test;

public class HibernateProviderTest {

    @Test
    public void testHikariCpHibernateConnectionProviderClassName() {
        testProviderClassName(HibernateProvider.HIKARI_CP_HIBERNATE_CONNECTION_PROVIDER_CLASS_NAME);
    }

    @Test
    public void testC3P0HibernateConnectionProviderClassName() {
        testProviderClassName(HibernateProvider.C3P0_HIBERNATE_CONNECTION_PROVIDER_CLASS_NAME);
    }

    private void testProviderClassName(String connectionProviderClassName) {
        try {
            Class<?> connectionProviderClass = Class.forName(connectionProviderClassName);
            Assert.assertTrue(connectionProviderClassName + " must be a subclass of " + ConnectionProvider.class, ConnectionProvider.class.isAssignableFrom(connectionProviderClass));
        } catch (ClassNotFoundException e) {
            Assert.fail(connectionProviderClassName + " should be available in classpath at runtime, maybe the constant is wrong and the library moved the connection provider");
        }
    }
}
