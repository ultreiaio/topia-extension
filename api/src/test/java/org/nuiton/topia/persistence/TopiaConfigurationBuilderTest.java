package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.topia.persistence.internal.LegacyTopiaIdFactory;
import org.nuiton.topia.persistence.internal.TopiaConnectionProvider;

import java.util.Map;
import java.util.Properties;

public class TopiaConfigurationBuilderTest {

    private static final Logger log = LogManager.getLogger(TopiaConfigurationBuilderTest.class);
    protected Properties properties;

    @Before
    public void setUp() {

        properties = new Properties();
        properties.put("hibernate.connection.driver_class", "org.h2.Driver");
        properties.put("hibernate.connection.url", "jdbc:h2:file:/tmp/my-super-directory/h2data");
        properties.put("hibernate.connection.username", "sa");
        properties.put("hibernate.connection.password", "");
        properties.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        properties.put("hibernate.show_sql", "false");
        properties.put("hibernate.format_sql", "true");
        properties.put("hibernate.use_sql_comments", "true");
        properties.put("hibernate.ejb.naming_strategy", org.hibernate.cfg.ImprovedNamingStrategy.class.getName());
        properties.put("hibernate.c3p0.min_size", "5");
        properties.put("hibernate.c3p0.max_size", "20");
        properties.put("hibernate.c3p0.timeout", "1800");
        properties.put("hibernate.c3p0.max_statements", "50");
        properties.put("hibernate.connection.provider_class", TopiaConnectionProvider.class.getName());
        properties.put("topia.persistence.topiaIdFactoryClassName", LegacyTopiaIdFactory.class.getName());
        properties.put("topia.service.migration", HibernateTopiaMigrationService.class.getName());

    }

    @Test
    public void build() {

        TopiaConfiguration topiaConfiguration =
                new TopiaConfigurationBuilder().forTestDatabase(getClass(), "build")
                        .useHibernateUpdate()
                        .validateSchemaOnStartup()
                        .useDefaultConnectionPool()
                        .build();

        Assert.assertTrue("any generated topia configuration must have initSchema to true", topiaConfiguration.isInitSchema());

    }

    @Test
    public void forTest() {

        new TopiaConfigurationBuilder().forTest(getClass(), "forTest");

    }

    @Test
    public void testReadProperties() {

        BeanTopiaConfiguration topiaConfiguration = new TopiaConfigurationBuilder().readProperties(properties);

        Assert.assertTrue(topiaConfiguration.getHibernateExtraConfiguration().containsValue(org.hibernate.cfg.ImprovedNamingStrategy.class.getName()));

    }

    @Test
    public void testToMap() {

        TopiaConfiguration topiaConfiguration = new TopiaConfigurationBuilder().readProperties(properties);

        Map<String, String> map = new TopiaConfigurationBuilder().toMap(topiaConfiguration);

        if (log.isDebugEnabled()) {
            log.debug("generated map is " + map);
        }

        Assert.assertEquals(properties.size(), map.size());

    }
}
