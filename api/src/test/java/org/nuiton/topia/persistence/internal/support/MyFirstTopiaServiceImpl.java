package org.nuiton.topia.persistence.internal.support;

/*
 * #%L
 * ToPIA Extension :: API
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaApplicationContext;

import java.util.Map;

public class MyFirstTopiaServiceImpl implements MyFirstTopiaService {

    private static final Logger log = LogManager.getLogger(MyFirstTopiaServiceImpl.class);
    protected Map<String, String> serviceConfiguration;

    public Map<String, String> getServiceConfiguration() {
        return serviceConfiguration;
    }

    @Override
    public void initTopiaService(TopiaApplicationContext<?> topiaApplicationContext, Map<String, String> serviceConfiguration) {
        this.serviceConfiguration = serviceConfiguration;
        if (log.isDebugEnabled()) {
            log.debug("configuration is " + serviceConfiguration);
        }
        if (log.isDebugEnabled()) {
            log.debug("");
        }
    }

    @Override
    public void close() {
        // nothing to do
    }
}
