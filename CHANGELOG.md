# ToPIA Extension changelog

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2022-07-31 00:22.

## Version [2.1.1](https://gitlab.com/ultreiaio/topia-extension/-/milestones/109)

**Closed at 2022-07-31.**


### Issues
No issue.

## Version [2.1.0](https://gitlab.com/ultreiaio/topia-extension/-/milestones/108)

**Closed at 2022-07-29.**


### Issues
  * [[enhancement 145]](https://gitlab.com/ultreiaio/topia-extension/-/issues/145) **Group all api modules in one Maven module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.7](https://gitlab.com/ultreiaio/topia-extension/-/milestones/107)

**Closed at 2022-07-28.**


### Issues
  * [[enhancement 144]](https://gitlab.com/ultreiaio/topia-extension/-/issues/144) **Be able to clean a context (from the authentication token) even if it is no more in application context cache.** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.6](https://gitlab.com/ultreiaio/topia-extension/-/milestones/106)

**Closed at 2022-06-22.**


### Issues
  * [[enhancement 143]](https://gitlab.com/ultreiaio/topia-extension/-/issues/143) **AddGeneratedSchemaConsumer may not find resource (in maven context)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.5](https://gitlab.com/ultreiaio/topia-extension/-/milestones/105)

**Closed at 2022-06-17.**


### Issues
  * [[enhancement 140]](https://gitlab.com/ultreiaio/topia-extension/-/issues/140) **Remove TopiaApplicationContextCache** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 141]](https://gitlab.com/ultreiaio/topia-extension/-/issues/141) **Add getAuthenticationToken() method on TopiaApplicationContext** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 142]](https://gitlab.com/ultreiaio/topia-extension/-/issues/142) **Introduce TopiaApplicationContextFactory** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.4](https://gitlab.com/ultreiaio/topia-extension/-/milestones/104)

**Closed at 2022-06-16.**


### Issues
  * [[enhancement 139]](https://gitlab.com/ultreiaio/topia-extension/-/issues/139) **Add a new sql request to use a generated schema from class-path** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.3](https://gitlab.com/ultreiaio/topia-extension/-/milestones/103)

**Closed at 2022-06-15.**


### Issues
  * [[bug 137]](https://gitlab.com/ultreiaio/topia-extension/-/issues/137) **generateHibernateOneToOneReverse unique key not used** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 138]](https://gitlab.com/ultreiaio/topia-extension/-/issues/138) **Add drop object on unique key in generated hibernate mapping** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.1](https://gitlab.com/ultreiaio/topia-extension/-/milestones/101)

**Closed at 2022-05-13.**


### Issues
  * [[bug 135]](https://gitlab.com/ultreiaio/topia-extension/-/issues/135) **TopiaHibernateTagValues in not in service loader** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.0](https://gitlab.com/ultreiaio/topia-extension/-/milestones/100)

**Closed at 2022-05-13.**


### Issues
  * [[enhancement 134]](https://gitlab.com/ultreiaio/topia-extension/-/issues/134) **Copy topia and remove all the crap** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.79](https://gitlab.com/ultreiaio/topia-extension/-/milestones/99)

**Closed at 2022-05-08.**


### Issues
  * [[enhancement 133]](https://gitlab.com/ultreiaio/topia-extension/-/issues/133) **Add unicity constrainst on one-to-one reverse foreign key** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.78](https://gitlab.com/ultreiaio/topia-extension/-/milestones/98)

**Closed at 2022-05-05.**


### Issues
No issue.

## Version [1.77](https://gitlab.com/ultreiaio/topia-extension/-/milestones/97)

**Closed at 2022-05-05.**


### Issues
  * [[enhancement 132]](https://gitlab.com/ultreiaio/topia-extension/-/issues/132) **Replace TopiaFireSupport by a simple propertyChangeSupport** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.76](https://gitlab.com/ultreiaio/topia-extension/-/milestones/96)

**Closed at 2022-05-03.**


### Issues
  * [[enhancement 130]](https://gitlab.com/ultreiaio/topia-extension/-/issues/130) **Introduce one-to-one composition** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 131]](https://gitlab.com/ultreiaio/topia-extension/-/issues/131) **open TopiaEntitySqlDescriptorGenerator** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.75](https://gitlab.com/ultreiaio/topia-extension/-/milestones/95)

**Closed at 2022-04-16.**


### Issues
  * [[enhancement 129]](https://gitlab.com/ultreiaio/topia-extension/-/issues/129) **Review Replicate Partial API and introduce call back for simple Replicate operation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.74](https://gitlab.com/ultreiaio/topia-extension/-/milestones/94)

**Closed at 2022-04-11.**


### Issues
  * [[enhancement 123]](https://gitlab.com/ultreiaio/topia-extension/-/issues/123) **Improve Consumer API and introduce ReplicateLayoutRequest** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 124]](https://gitlab.com/ultreiaio/topia-extension/-/issues/124) **Add TimeLog in SqlScriptConsumer** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 125]](https://gitlab.com/ultreiaio/topia-extension/-/issues/125) **Review default batchSize (move from 1000 to 50)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 126]](https://gitlab.com/ultreiaio/topia-extension/-/issues/126) **Migration should never use batch mode** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 127]](https://gitlab.com/ultreiaio/topia-extension/-/issues/127) **Use case sensitive column names in Sql models** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 128]](https://gitlab.com/ultreiaio/topia-extension/-/issues/128) **Add extra mapping in CopyPlan model** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.73](https://gitlab.com/ultreiaio/topia-extension/-/milestones/93)

**Closed at 2022-03-18.**


### Issues
  * [[bug 122]](https://gitlab.com/ultreiaio/topia-extension/-/issues/122) **Fix TopiaEntitySqlReplicatePlanTask when no ids selected** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 120]](https://gitlab.com/ultreiaio/topia-extension/-/issues/120) **Improve association table column names order** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 121]](https://gitlab.com/ultreiaio/topia-extension/-/issues/121) **Improve TopiaMetadataModel design** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.72](https://gitlab.com/ultreiaio/topia-extension/-/milestones/92)

**Closed at 2022-03-15.**


### Issues
  * [[enhancement 119]](https://gitlab.com/ultreiaio/topia-extension/-/issues/119) **Make TopiaSqlScript JsonAware** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.71](https://gitlab.com/ultreiaio/topia-extension/-/milestones/91)

**Closed at 2022-03-13.**


### Issues
No issue.

## Version [1.70](https://gitlab.com/ultreiaio/topia-extension/-/milestones/90)

**Closed at 2022-03-13.**


### Issues
  * [[bug 117]](https://gitlab.com/ultreiaio/topia-extension/-/issues/117) **Bad copy of TopiaMetadataEntity, so any logic on it is broken :(** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 115]](https://gitlab.com/ultreiaio/topia-extension/-/issues/115) **Make any resource lazy loaded by a SingletonSupplier** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 116]](https://gitlab.com/ultreiaio/topia-extension/-/issues/116) **Add TagValues class from observe-toolkit** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 118]](https://gitlab.com/ultreiaio/topia-extension/-/issues/118) **Review TopiaSqlScript and TopiaBlobsContainer json issues** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.69](https://gitlab.com/ultreiaio/topia-extension/-/milestones/89)

**Closed at 2022-03-09.**


### Issues
  * [[enhancement 110]](https://gitlab.com/ultreiaio/topia-extension/-/issues/110) **Replace TopiaEntityScript by TopiaEntitySqlScript** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 111]](https://gitlab.com/ultreiaio/topia-extension/-/issues/111) **Replace paths entity centric by sql model centric approach** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 112]](https://gitlab.com/ultreiaio/topia-extension/-/issues/112) **move metadata model to service sql and clean all API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 113]](https://gitlab.com/ultreiaio/topia-extension/-/issues/113) **Add nice method TopiaSqlScript#content** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 114]](https://gitlab.com/ultreiaio/topia-extension/-/issues/114) **Introduce TopiaIdFactoryForBulkSupport** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.68](https://gitlab.com/ultreiaio/topia-extension/-/milestones/88)

**Closed at 2022-03-05.**


### Issues
  * [[enhancement 107]](https://gitlab.com/ultreiaio/topia-extension/-/issues/107) **Introduce CopyPlan and clean sql service API and improve Usage API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 108]](https://gitlab.com/ultreiaio/topia-extension/-/issues/108) **Improve application context models loading** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 109]](https://gitlab.com/ultreiaio/topia-extension/-/issues/109) **Generate any resources in src/main/resources directory instead of generated directory** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.67](https://gitlab.com/ultreiaio/topia-extension/-/milestones/87)

**Closed at 2022-03-01.**


### Issues
  * [[enhancement 106]](https://gitlab.com/ultreiaio/topia-extension/-/issues/106) **Introduce TopiaSqlService** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.66](https://gitlab.com/ultreiaio/topia-extension/-/milestones/86)

**Closed at 2022-02-15.**


### Issues
  * [[enhancement 105]](https://gitlab.com/ultreiaio/topia-extension/-/issues/105) **Improve DAO API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.65](https://gitlab.com/ultreiaio/topia-extension/-/milestones/85)

**Closed at 2022-02-11.**


### Issues
  * [[enhancement 104]](https://gitlab.com/ultreiaio/topia-extension/-/issues/104) **Improve EntityFilterProperty (add withDefaultValue and isPrimitive methods) add fix flavors order** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.64](https://gitlab.com/ultreiaio/topia-extension/-/milestones/84)

**Closed at 2022-01-30.**


### Issues
  * [[enhancement 103]](https://gitlab.com/ultreiaio/topia-extension/-/issues/103) **Improve Filter API (introduce flavor in FilterProperty) and use only one FilterProperty for one entity property (flavors does all operation possible for the type)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.63](https://gitlab.com/ultreiaio/topia-extension/-/milestones/83)

**Closed at 2022-01-28.**


### Issues
  * [[enhancement 102]](https://gitlab.com/ultreiaio/topia-extension/-/issues/102) **Introduce filter API (from ObServe toolkit)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.62](https://gitlab.com/ultreiaio/topia-extension/-/milestones/82)

**Closed at 2022-01-20.**


### Issues
  * [[enhancement 101]](https://gitlab.com/ultreiaio/topia-extension/-/issues/101) **Remove  executeQueryByParentId and  executeQueryGetParentId in DaoQuerySupport and replace it by a light Relation API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.61](https://gitlab.com/ultreiaio/topia-extension/-/milestones/81)

**Closed at 2022-01-19.**


### Issues
  * [[enhancement 96]](https://gitlab.com/ultreiaio/topia-extension/-/issues/96) **Remove nasty getParentDtoType and getParentDtoType2 on dao** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 97]](https://gitlab.com/ultreiaio/topia-extension/-/issues/97) **Remove deprecated generated methods in dao (findByXXX)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 98]](https://gitlab.com/ultreiaio/topia-extension/-/issues/98) **Remove nasty WithDelegate API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 99]](https://gitlab.com/ultreiaio/topia-extension/-/issues/99) **Introduce TopiaDaoExt to get all the power in one class (instead of having some Topia&lt;E&gt;&amp;DaoQuerySuport everywhere** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 100]](https://gitlab.com/ultreiaio/topia-extension/-/issues/100) **Improve how to create dao (at last never ever to Class.forName, use instead a generated mapping)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.60](https://gitlab.com/ultreiaio/topia-extension/-/milestones/80)

**Closed at 2022-01-08.**


### Issues
  * [[enhancement 95]](https://gitlab.com/ultreiaio/topia-extension/-/issues/95) **Add more logic (from observe toolkit)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.59](https://gitlab.com/ultreiaio/topia-extension/-/milestones/79)

**Closed at 2021-12-30.**


### Issues
  * [[enhancement 94]](https://gitlab.com/ultreiaio/topia-extension/-/issues/94) **Introduce groupBy tagValues** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.58](https://gitlab.com/ultreiaio/topia-extension/-/milestones/78)

**Closed at 2021-11-28.**


### Issues
  * [[enhancement 93]](https://gitlab.com/ultreiaio/topia-extension/-/issues/93) **Improve migration service resources** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.58](https://gitlab.com/ultreiaio/topia-extension/-/milestones/78)

**Closed at 2021-11-28.**


### Issues
  * [[enhancement 93]](https://gitlab.com/ultreiaio/topia-extension/-/issues/93) **Improve migration service resources** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.57](https://gitlab.com/ultreiaio/topia-extension/-/milestones/77)

**Closed at 2021-11-25.**


### Issues
  * [[bug 92]](https://gitlab.com/ultreiaio/topia-extension/-/issues/92) **Order by on many-to-many is generated on table column but should be on collection** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.57](https://gitlab.com/ultreiaio/topia-extension/-/milestones/77)

**Closed at 2021-11-25.**


### Issues
  * [[bug 92]](https://gitlab.com/ultreiaio/topia-extension/-/issues/92) **Order by on many-to-many is generated on table column but should be on collection** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.56](https://gitlab.com/ultreiaio/topia-extension/-/milestones/76)

**Closed at 2021-11-17.**


### Issues
  * [[enhancement 91]](https://gitlab.com/ultreiaio/topia-extension/-/issues/91) **Improve generated mapping for not-null and unique keys** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.56](https://gitlab.com/ultreiaio/topia-extension/-/milestones/76)

**Closed at 2021-11-17.**


### Issues
  * [[enhancement 91]](https://gitlab.com/ultreiaio/topia-extension/-/issues/91) **Improve generated mapping for not-null and unique keys** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.55](https://gitlab.com/ultreiaio/topia-extension/-/milestones/75)

**Closed at 2021-11-13.**


### Issues
No issue.

## Version [1.55](https://gitlab.com/ultreiaio/topia-extension/-/milestones/75)

**Closed at 2021-11-13.**


### Issues
No issue.

## Version [1.54](https://gitlab.com/ultreiaio/topia-extension/-/milestones/74)

**Closed at 2021-11-07.**


### Issues
  * [[enhancement 90]](https://gitlab.com/ultreiaio/topia-extension/-/issues/90) **Introduce TopiaUsageReverseAssociation in TopiaUsage API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.54](https://gitlab.com/ultreiaio/topia-extension/-/milestones/74)

**Closed at 2021-11-07.**


### Issues
  * [[enhancement 90]](https://gitlab.com/ultreiaio/topia-extension/-/issues/90) **Introduce TopiaUsageReverseAssociation in TopiaUsage API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.53](https://gitlab.com/ultreiaio/topia-extension/-/milestones/73)

**Closed at 2021-10-02.**


### Issues
  * [[enhancement 89]](https://gitlab.com/ultreiaio/topia-extension/-/issues/89) **Improve TopiaUsageSupport using a unique list as result with no other any copies** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.53](https://gitlab.com/ultreiaio/topia-extension/-/milestones/73)

**Closed at 2021-10-02.**


### Issues
  * [[enhancement 89]](https://gitlab.com/ultreiaio/topia-extension/-/issues/89) **Improve TopiaUsageSupport using a unique list as result with no other any copies** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.52](https://gitlab.com/ultreiaio/topia-extension/-/milestones/72)

**Closed at 2021-09-27.**


### Issues
No issue.

## Version [1.52](https://gitlab.com/ultreiaio/topia-extension/-/milestones/72)

**Closed at 2021-09-27.**


### Issues
No issue.

## Version [1.51](https://gitlab.com/ultreiaio/topia-extension/-/milestones/71)

**Closed at 2021-07-31.**


### Issues
  * [[enhancement 88]](https://gitlab.com/ultreiaio/topia-extension/-/issues/88) **Generate more stuff on simple association (and fix cascade)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.51](https://gitlab.com/ultreiaio/topia-extension/-/milestones/71)

**Closed at 2021-07-31.**


### Issues
  * [[enhancement 88]](https://gitlab.com/ultreiaio/topia-extension/-/issues/88) **Generate more stuff on simple association (and fix cascade)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.50](https://gitlab.com/ultreiaio/topia-extension/-/milestones/70)

**Closed at 2021-07-31.**


### Issues
  * [[enhancement 86]](https://gitlab.com/ultreiaio/topia-extension/-/issues/86) **Fix simple many associations** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 87]](https://gitlab.com/ultreiaio/topia-extension/-/issues/87) **Extends TopiaMetadataEntity with TopiaMetadataSimpleAssociation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.50](https://gitlab.com/ultreiaio/topia-extension/-/milestones/70)

**Closed at 2021-07-31.**


### Issues
  * [[enhancement 86]](https://gitlab.com/ultreiaio/topia-extension/-/issues/86) **Fix simple many associations** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 87]](https://gitlab.com/ultreiaio/topia-extension/-/issues/87) **Extends TopiaMetadataEntity with TopiaMetadataSimpleAssociation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.49](https://gitlab.com/ultreiaio/topia-extension/-/milestones/69)

**Closed at 2021-07-04.**


### Issues
  * [[enhancement 85]](https://gitlab.com/ultreiaio/topia-extension/-/issues/85) **Improve EntityTransformer (do not generate toDto and fromDto method inside entity)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.49](https://gitlab.com/ultreiaio/topia-extension/-/milestones/69)

**Closed at 2021-07-04.**


### Issues
  * [[enhancement 85]](https://gitlab.com/ultreiaio/topia-extension/-/issues/85) **Improve EntityTransformer (do not generate toDto and fromDto method inside entity)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.48](https://gitlab.com/ultreiaio/topia-extension/-/milestones/68)

**Closed at 2021-06-05.**


### Issues
  * [[bug 84]](https://gitlab.com/ultreiaio/topia-extension/-/issues/84) **Fix generated entity attributes with digits and notNull tag values** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 83]](https://gitlab.com/ultreiaio/topia-extension/-/issues/83) **Move to java 11** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.48](https://gitlab.com/ultreiaio/topia-extension/-/milestones/68)

**Closed at 2021-06-05.**


### Issues
  * [[bug 84]](https://gitlab.com/ultreiaio/topia-extension/-/issues/84) **Fix generated entity attributes with digits and notNull tag values** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 83]](https://gitlab.com/ultreiaio/topia-extension/-/issues/83) **Move to java 11** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.47](https://gitlab.com/ultreiaio/topia-extension/-/milestones/67)

**Closed at 2021-04-29.**


### Issues
  * [[enhancement 81]](https://gitlab.com/ultreiaio/topia-extension/-/issues/81) **Add a way to queries and sql-queries in entity mapping** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 82]](https://gitlab.com/ultreiaio/topia-extension/-/issues/82) **Add a way to add more methods on abstract dao** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.47](https://gitlab.com/ultreiaio/topia-extension/-/milestones/67)

**Closed at 2021-04-29.**


### Issues
  * [[enhancement 81]](https://gitlab.com/ultreiaio/topia-extension/-/issues/81) **Add a way to queries and sql-queries in entity mapping** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 82]](https://gitlab.com/ultreiaio/topia-extension/-/issues/82) **Add a way to add more methods on abstract dao** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.46](https://gitlab.com/ultreiaio/topia-extension/-/milestones/66)

**Closed at 2021-04-25.**


### Issues
  * [[enhancement 80]](https://gitlab.com/ultreiaio/topia-extension/-/issues/80) **Introduce new Topia query API to be able to delegate form query to another dao and then just transform data in good format** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.46](https://gitlab.com/ultreiaio/topia-extension/-/milestones/66)

**Closed at 2021-04-25.**


### Issues
  * [[enhancement 80]](https://gitlab.com/ultreiaio/topia-extension/-/issues/80) **Introduce new Topia query API to be able to delegate form query to another dao and then just transform data in good format** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.45](https://gitlab.com/ultreiaio/topia-extension/-/milestones/65)

**Closed at 2021-04-25.**


### Issues
  * [[enhancement 79]](https://gitlab.com/ultreiaio/topia-extension/-/issues/79) **Use static on class to generate some stuff on entities but not make entity part of pu** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.45](https://gitlab.com/ultreiaio/topia-extension/-/milestones/65)

**Closed at 2021-04-25.**


### Issues
  * [[enhancement 79]](https://gitlab.com/ultreiaio/topia-extension/-/issues/79) **Use static on class to generate some stuff on entities but not make entity part of pu** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.44](https://gitlab.com/ultreiaio/topia-extension/-/milestones/64)

**Closed at 2021-03-13.**


### Issues
  * [[bug 78]](https://gitlab.com/ultreiaio/topia-extension/-/issues/78) **Fix bad generated code   of method EntityAbstract.accept0** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.44](https://gitlab.com/ultreiaio/topia-extension/-/milestones/64)

**Closed at 2021-03-13.**


### Issues
  * [[bug 78]](https://gitlab.com/ultreiaio/topia-extension/-/issues/78) **Fix bad generated code   of method EntityAbstract.accept0** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.43](https://gitlab.com/ultreiaio/topia-extension/-/milestones/63)

**Closed at 2021-02-22.**


### Issues
  * [[bug 76]](https://gitlab.com/ultreiaio/topia-extension/-/issues/76) **Fix some major issues in sql API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 75]](https://gitlab.com/ultreiaio/topia-extension/-/issues/75) **Introduce AddVersionTableRequest request** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 77]](https://gitlab.com/ultreiaio/topia-extension/-/issues/77) **Add H2 API to store blob** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.43](https://gitlab.com/ultreiaio/topia-extension/-/milestones/63)

**Closed at 2021-02-22.**


### Issues
  * [[bug 76]](https://gitlab.com/ultreiaio/topia-extension/-/issues/76) **Fix some major issues in sql API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 75]](https://gitlab.com/ultreiaio/topia-extension/-/issues/75) **Introduce AddVersionTableRequest request** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 77]](https://gitlab.com/ultreiaio/topia-extension/-/issues/77) **Add H2 API to store blob** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.42](https://gitlab.com/ultreiaio/topia-extension/-/milestones/62)

**Closed at 2021-02-15.**


### Issues
  * [[bug 73]](https://gitlab.com/ultreiaio/topia-extension/-/issues/73) **Make reverseSelector any select with in a ReverseAssociation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.42](https://gitlab.com/ultreiaio/topia-extension/-/milestones/62)

**Closed at 2021-02-15.**


### Issues
  * [[bug 73]](https://gitlab.com/ultreiaio/topia-extension/-/issues/73) **Make reverseSelector any select with in a ReverseAssociation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.41](https://gitlab.com/ultreiaio/topia-extension/-/milestones/61)

**Closed at 2021-02-05.**


### Issues
  * [[enhancement 72]](https://gitlab.com/ultreiaio/topia-extension/-/issues/72) **Add delete-orphan on composite many-to-one properties** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.41](https://gitlab.com/ultreiaio/topia-extension/-/milestones/61)

**Closed at 2021-02-05.**


### Issues
  * [[enhancement 72]](https://gitlab.com/ultreiaio/topia-extension/-/issues/72) **Add delete-orphan on composite many-to-one properties** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.40](https://gitlab.com/ultreiaio/topia-extension/-/milestones/60)

**Closed at 2021-01-29.**


### Issues
  * [[enhancement 70]](https://gitlab.com/ultreiaio/topia-extension/-/issues/70) **Simplify generated code** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 71]](https://gitlab.com/ultreiaio/topia-extension/-/issues/71) **Always add not-null to false if required on natural-id** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.40](https://gitlab.com/ultreiaio/topia-extension/-/milestones/60)

**Closed at 2021-01-29.**


### Issues
  * [[enhancement 70]](https://gitlab.com/ultreiaio/topia-extension/-/issues/70) **Simplify generated code** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 71]](https://gitlab.com/ultreiaio/topia-extension/-/issues/71) **Always add not-null to false if required on natural-id** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.39](https://gitlab.com/ultreiaio/topia-extension/-/milestones/59)

**Closed at 2021-01-27.**


### Issues
  * [[bug 69]](https://gitlab.com/ultreiaio/topia-extension/-/issues/69) **Make TopiaExtensionTagValues available in ModelExtensionReader** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.39](https://gitlab.com/ultreiaio/topia-extension/-/milestones/59)

**Closed at 2021-01-27.**


### Issues
  * [[bug 69]](https://gitlab.com/ultreiaio/topia-extension/-/issues/69) **Make TopiaExtensionTagValues available in ModelExtensionReader** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.38](https://gitlab.com/ultreiaio/topia-extension/-/milestones/58)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.38](https://gitlab.com/ultreiaio/topia-extension/-/milestones/58)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.37](https://gitlab.com/ultreiaio/topia-extension/-/milestones/57)

**Closed at 2021-01-22.**


### Issues
  * [[bug 68]](https://gitlab.com/ultreiaio/topia-extension/-/issues/68) **Fix generated index on many to many reverse attribute** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.37](https://gitlab.com/ultreiaio/topia-extension/-/milestones/57)

**Closed at 2021-01-22.**


### Issues
  * [[bug 68]](https://gitlab.com/ultreiaio/topia-extension/-/issues/68) **Fix generated index on many to many reverse attribute** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.36](https://gitlab.com/ultreiaio/topia-extension/-/milestones/56)

**Closed at 2021-01-03.**


### Issues
  * [[enhancement 66]](https://gitlab.com/ultreiaio/topia-extension/-/issues/66) **Add a way to not set property back in entity or dto** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 67]](https://gitlab.com/ultreiaio/topia-extension/-/issues/67) **Manage more precisely notNull value on composite relation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.36](https://gitlab.com/ultreiaio/topia-extension/-/milestones/56)

**Closed at 2021-01-03.**


### Issues
  * [[enhancement 66]](https://gitlab.com/ultreiaio/topia-extension/-/issues/66) **Add a way to not set property back in entity or dto** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 67]](https://gitlab.com/ultreiaio/topia-extension/-/issues/67) **Manage more precisely notNull value on composite relation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.35](https://gitlab.com/ultreiaio/topia-extension/-/milestones/55)

**Closed at 2020-12-21.**


### Issues
  * [[bug 65]](https://gitlab.com/ultreiaio/topia-extension/-/issues/65) **Fix length usage on string type** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 64]](https://gitlab.com/ultreiaio/topia-extension/-/issues/64) **Introduce digits tag value and use it on generated model** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.35](https://gitlab.com/ultreiaio/topia-extension/-/milestones/55)

**Closed at 2020-12-21.**


### Issues
  * [[bug 65]](https://gitlab.com/ultreiaio/topia-extension/-/issues/65) **Fix length usage on string type** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 64]](https://gitlab.com/ultreiaio/topia-extension/-/issues/64) **Introduce digits tag value and use it on generated model** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.34](https://gitlab.com/ultreiaio/topia-extension/-/milestones/54)

**Closed at 2020-12-21.**


### Issues
  * [[enhancement 62]](https://gitlab.com/ultreiaio/topia-extension/-/issues/62) **Let&#39;s add not-null on any composition in generated hibernate mapping** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 63]](https://gitlab.com/ultreiaio/topia-extension/-/issues/63) **Let&#39;s add not-null on any many-to-many in generated hibernate mapping** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.34](https://gitlab.com/ultreiaio/topia-extension/-/milestones/54)

**Closed at 2020-12-21.**


### Issues
  * [[enhancement 62]](https://gitlab.com/ultreiaio/topia-extension/-/issues/62) **Let&#39;s add not-null on any composition in generated hibernate mapping** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 63]](https://gitlab.com/ultreiaio/topia-extension/-/issues/63) **Let&#39;s add not-null on any many-to-many in generated hibernate mapping** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.33](https://gitlab.com/ultreiaio/topia-extension/-/milestones/53)

**Closed at 2020-12-20.**


### Issues
  * [[bug 59]](https://gitlab.com/ultreiaio/topia-extension/-/issues/59) **Fix generated indexes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 60]](https://gitlab.com/ultreiaio/topia-extension/-/issues/60) **Add schema name in generated fk** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 61]](https://gitlab.com/ultreiaio/topia-extension/-/issues/61) **Review and simplify code in hibernate generator + generate extra script (indexes, foereign keys, ... schema definition)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.33](https://gitlab.com/ultreiaio/topia-extension/-/milestones/53)

**Closed at 2020-12-20.**


### Issues
  * [[bug 59]](https://gitlab.com/ultreiaio/topia-extension/-/issues/59) **Fix generated indexes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 60]](https://gitlab.com/ultreiaio/topia-extension/-/issues/60) **Add schema name in generated fk** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 61]](https://gitlab.com/ultreiaio/topia-extension/-/issues/61) **Review and simplify code in hibernate generator + generate extra script (indexes, foereign keys, ... schema definition)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.32](https://gitlab.com/ultreiaio/topia-extension/-/milestones/52)

**Closed at 2020-12-18.**


### Issues
  * [[bug 58]](https://gitlab.com/ultreiaio/topia-extension/-/issues/58) **Generate some missing indexes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.32](https://gitlab.com/ultreiaio/topia-extension/-/milestones/52)

**Closed at 2020-12-18.**


### Issues
  * [[bug 58]](https://gitlab.com/ultreiaio/topia-extension/-/issues/58) **Generate some missing indexes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.31](https://gitlab.com/ultreiaio/topia-extension/-/milestones/51)

**Closed at 2020-12-13.**


### Issues
  * [[bug 57]](https://gitlab.com/ultreiaio/topia-extension/-/issues/57) **Inverse is not well generated on one-to-many** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.31](https://gitlab.com/ultreiaio/topia-extension/-/milestones/51)

**Closed at 2020-12-13.**


### Issues
  * [[bug 57]](https://gitlab.com/ultreiaio/topia-extension/-/issues/57) **Inverse is not well generated on one-to-many** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.30](https://gitlab.com/ultreiaio/topia-extension/-/milestones/50)

**Closed at 2020-11-30.**


### Issues
  * [[bug 56]](https://gitlab.com/ultreiaio/topia-extension/-/issues/56) **Inverse relation many-to-one are not well generated in hibernate mapping** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.30](https://gitlab.com/ultreiaio/topia-extension/-/milestones/50)

**Closed at 2020-11-30.**


### Issues
  * [[bug 56]](https://gitlab.com/ultreiaio/topia-extension/-/issues/56) **Inverse relation many-to-one are not well generated in hibernate mapping** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.29](https://gitlab.com/ultreiaio/topia-extension/-/milestones/49)

**Closed at 2020-09-29.**


### Issues
  * [[bug 49]](https://gitlab.com/ultreiaio/topia-extension/-/issues/49) **Ad yet last bug (association flag is not set back to false if used twice...)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[bug 50]](https://gitlab.com/ultreiaio/topia-extension/-/issues/50) **Be able to consume replicate more than one id with blobs container** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[bug 51]](https://gitlab.com/ultreiaio/topia-extension/-/issues/51) **Delete operation should treat first all association tables, then entity tables** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 52]](https://gitlab.com/ultreiaio/topia-extension/-/issues/52) **Remove service-script** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 53]](https://gitlab.com/ultreiaio/topia-extension/-/issues/53) **Optimize delete operation sql code** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 54]](https://gitlab.com/ultreiaio/topia-extension/-/issues/54) **Introduce standalone tag value to make all entities that should not be taken account in an entry point path** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 55]](https://gitlab.com/ultreiaio/topia-extension/-/issues/55) **Simplify usage of TopiaMetadataModelPaths and replication orders** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.29](https://gitlab.com/ultreiaio/topia-extension/-/milestones/49)

**Closed at 2020-09-29.**


### Issues
  * [[bug 49]](https://gitlab.com/ultreiaio/topia-extension/-/issues/49) **Ad yet last bug (association flag is not set back to false if used twice...)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[bug 50]](https://gitlab.com/ultreiaio/topia-extension/-/issues/50) **Be able to consume replicate more than one id with blobs container** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[bug 51]](https://gitlab.com/ultreiaio/topia-extension/-/issues/51) **Delete operation should treat first all association tables, then entity tables** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 52]](https://gitlab.com/ultreiaio/topia-extension/-/issues/52) **Remove service-script** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 53]](https://gitlab.com/ultreiaio/topia-extension/-/issues/53) **Optimize delete operation sql code** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 54]](https://gitlab.com/ultreiaio/topia-extension/-/issues/54) **Introduce standalone tag value to make all entities that should not be taken account in an entry point path** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 55]](https://gitlab.com/ultreiaio/topia-extension/-/issues/55) **Simplify usage of TopiaMetadataModelPaths and replication orders** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.28](https://gitlab.com/ultreiaio/topia-extension/-/milestones/48)

**Closed at 2020-09-26.**


### Issues
  * [[bug 48]](https://gitlab.com/ultreiaio/topia-extension/-/issues/48) **Some dependencies are missing while computing order** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.28](https://gitlab.com/ultreiaio/topia-extension/-/milestones/48)

**Closed at 2020-09-26.**


### Issues
  * [[bug 48]](https://gitlab.com/ultreiaio/topia-extension/-/issues/48) **Some dependencies are missing while computing order** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.27](https://gitlab.com/ultreiaio/topia-extension/-/milestones/47)

**Closed at 2020-09-26.**


### Issues
  * [[enhancement 45]](https://gitlab.com/ultreiaio/topia-extension/-/issues/45) **Introduce skipNavigation tagValue and use it in some model builder to avoid any need to specialized it.** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 46]](https://gitlab.com/ultreiaio/topia-extension/-/issues/46) **Introduce TopiaMetadataModelPaths** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 47]](https://gitlab.com/ultreiaio/topia-extension/-/issues/47) **Introduce a new brand sql model (using metadata model) with everything generated, goodbye service-script** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.27](https://gitlab.com/ultreiaio/topia-extension/-/milestones/47)

**Closed at 2020-09-26.**


### Issues
  * [[enhancement 45]](https://gitlab.com/ultreiaio/topia-extension/-/issues/45) **Introduce skipNavigation tagValue and use it in some model builder to avoid any need to specialized it.** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 46]](https://gitlab.com/ultreiaio/topia-extension/-/issues/46) **Introduce TopiaMetadataModelPaths** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 47]](https://gitlab.com/ultreiaio/topia-extension/-/issues/47) **Introduce a new brand sql model (using metadata model) with everything generated, goodbye service-script** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.26](https://gitlab.com/ultreiaio/topia-extension/-/milestones/46)

**Closed at 2020-08-31.**


### Issues
  * [[enhancement 42]](https://gitlab.com/ultreiaio/topia-extension/-/issues/42) **Add in migration resource a way to finalize it (by generating yet another version script)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 43]](https://gitlab.com/ultreiaio/topia-extension/-/issues/43) **Add a way to templatize migration version scripts** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 44]](https://gitlab.com/ultreiaio/topia-extension/-/issues/44) **Improve SqlScriptReader API (using nice String stream )** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.26](https://gitlab.com/ultreiaio/topia-extension/-/milestones/46)

**Closed at 2020-08-31.**


### Issues
  * [[enhancement 42]](https://gitlab.com/ultreiaio/topia-extension/-/issues/42) **Add in migration resource a way to finalize it (by generating yet another version script)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 43]](https://gitlab.com/ultreiaio/topia-extension/-/issues/43) **Add a way to templatize migration version scripts** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 44]](https://gitlab.com/ultreiaio/topia-extension/-/issues/44) **Improve SqlScriptReader API (using nice String stream )** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.25](https://gitlab.com/ultreiaio/topia-extension/-/milestones/45)

**Closed at 2020-08-22.**


### Issues
  * [[enhancement 40]](https://gitlab.com/ultreiaio/topia-extension/-/issues/40) **Improve multiple multiplicity generated entity methods** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 41]](https://gitlab.com/ultreiaio/topia-extension/-/issues/41) **Le&#39;ts copy also to dto setXXXSize method from entity collection when possible** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.25](https://gitlab.com/ultreiaio/topia-extension/-/milestones/45)

**Closed at 2020-08-22.**


### Issues
  * [[enhancement 40]](https://gitlab.com/ultreiaio/topia-extension/-/issues/40) **Improve multiple multiplicity generated entity methods** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 41]](https://gitlab.com/ultreiaio/topia-extension/-/issues/41) **Le&#39;ts copy also to dto setXXXSize method from entity collection when possible** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.24](https://gitlab.com/ultreiaio/topia-extension/-/milestones/44)

**Closed at 2020-08-17.**


### Issues
  * [[enhancement 38]](https://gitlab.com/ultreiaio/topia-extension/-/issues/38) **Introduce TopiaEntityScript** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 39]](https://gitlab.com/ultreiaio/topia-extension/-/issues/39) **Improve a little MetadataModel API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.24](https://gitlab.com/ultreiaio/topia-extension/-/milestones/44)

**Closed at 2020-08-17.**


### Issues
  * [[enhancement 38]](https://gitlab.com/ultreiaio/topia-extension/-/issues/38) **Introduce TopiaEntityScript** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 39]](https://gitlab.com/ultreiaio/topia-extension/-/issues/39) **Improve a little MetadataModel API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.23](https://gitlab.com/ultreiaio/topia-extension/-/milestones/43)

**Closed at 2020-08-16.**


### Issues
  * [[enhancement 33]](https://gitlab.com/ultreiaio/topia-extension/-/issues/33) **Introduce TopiaMetadataReverseAssociation and get in from TopiaMetadataModel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 34]](https://gitlab.com/ultreiaio/topia-extension/-/issues/34) **Add nice toString on TopiaMetadataLink classes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 35]](https://gitlab.com/ultreiaio/topia-extension/-/issues/35) **Introduce TopiaMetadataEntityPath and his builder** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 36]](https://gitlab.com/ultreiaio/topia-extension/-/issues/36) **Introduce entryPoint notion on metadata entity** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 37]](https://gitlab.com/ultreiaio/topia-extension/-/issues/37) **Add entityType on TopiaMetadataEntity (to make easier usage from java world)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.23](https://gitlab.com/ultreiaio/topia-extension/-/milestones/43)

**Closed at 2020-08-16.**


### Issues
  * [[enhancement 33]](https://gitlab.com/ultreiaio/topia-extension/-/issues/33) **Introduce TopiaMetadataReverseAssociation and get in from TopiaMetadataModel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 34]](https://gitlab.com/ultreiaio/topia-extension/-/issues/34) **Add nice toString on TopiaMetadataLink classes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 35]](https://gitlab.com/ultreiaio/topia-extension/-/issues/35) **Introduce TopiaMetadataEntityPath and his builder** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 36]](https://gitlab.com/ultreiaio/topia-extension/-/issues/36) **Introduce entryPoint notion on metadata entity** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 37]](https://gitlab.com/ultreiaio/topia-extension/-/issues/37) **Add entityType on TopiaMetadataEntity (to make easier usage from java world)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.22](https://gitlab.com/ultreiaio/topia-extension/-/milestones/42)

**Closed at 2020-05-17.**


### Issues
No issue.

## Version [1.22](https://gitlab.com/ultreiaio/topia-extension/-/milestones/42)

**Closed at 2020-05-17.**


### Issues
No issue.

## Version [1.21](https://gitlab.com/ultreiaio/topia-extension/-/milestones/41)

**Closed at 2020-05-17.**


### Issues
  * [[bug 31]](https://gitlab.com/ultreiaio/topia-extension/-/issues/31) **Missing a predicate test in TopiaUsage** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 32]](https://gitlab.com/ultreiaio/topia-extension/-/issues/32) **Rethink Usage support (generate definition on each entity)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.21](https://gitlab.com/ultreiaio/topia-extension/-/milestones/41)

**Closed at 2020-05-17.**


### Issues
  * [[bug 31]](https://gitlab.com/ultreiaio/topia-extension/-/issues/31) **Missing a predicate test in TopiaUsage** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 32]](https://gitlab.com/ultreiaio/topia-extension/-/issues/32) **Rethink Usage support (generate definition on each entity)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.20](https://gitlab.com/ultreiaio/topia-extension/-/milestones/39)

**Closed at 2020-05-12.**


### Issues
  * [[enhancement 30]](https://gitlab.com/ultreiaio/topia-extension/-/issues/30) **Detect if a postgres user is a super user** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.20](https://gitlab.com/ultreiaio/topia-extension/-/milestones/39)

**Closed at 2020-05-12.**


### Issues
  * [[enhancement 30]](https://gitlab.com/ultreiaio/topia-extension/-/issues/30) **Detect if a postgres user is a super user** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.19](https://gitlab.com/ultreiaio/topia-extension/-/milestones/38)

**Closed at 2020-04-13.**


### Issues
  * [[enhancement 29]](https://gitlab.com/ultreiaio/topia-extension/-/issues/29) **Be able to fix temporary directory to use to create new files** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.19](https://gitlab.com/ultreiaio/topia-extension/-/milestones/38)

**Closed at 2020-04-13.**


### Issues
  * [[enhancement 29]](https://gitlab.com/ultreiaio/topia-extension/-/issues/29) **Be able to fix temporary directory to use to create new files** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.18](https://gitlab.com/ultreiaio/topia-extension/-/milestones/37)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.18](https://gitlab.com/ultreiaio/topia-extension/-/milestones/37)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.17](https://gitlab.com/ultreiaio/topia-extension/-/milestones/36)

**Closed at 2020-02-06.**


### Issues
  * [[bug 27]](https://gitlab.com/ultreiaio/topia-extension/-/issues/27) **Fix TopiaEntityTransformer (always generates abstract classes)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 28]](https://gitlab.com/ultreiaio/topia-extension/-/issues/28) **Improve EntityDtoMapping** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.17](https://gitlab.com/ultreiaio/topia-extension/-/milestones/36)

**Closed at 2020-02-06.**


### Issues
  * [[bug 27]](https://gitlab.com/ultreiaio/topia-extension/-/issues/27) **Fix TopiaEntityTransformer (always generates abstract classes)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 28]](https://gitlab.com/ultreiaio/topia-extension/-/issues/28) **Improve EntityDtoMapping** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.16](https://gitlab.com/ultreiaio/topia-extension/-/milestones/35)

**Closed at 2020-01-25.**


### Issues
  * [[enhancement 26]](https://gitlab.com/ultreiaio/topia-extension/-/issues/26) **Improve EntityToDtoMapping api** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.16](https://gitlab.com/ultreiaio/topia-extension/-/milestones/35)

**Closed at 2020-01-25.**


### Issues
  * [[enhancement 26]](https://gitlab.com/ultreiaio/topia-extension/-/issues/26) **Improve EntityToDtoMapping api** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.15](https://gitlab.com/ultreiaio/topia-extension/-/milestones/34)

**Closed at 2020-01-23.**


### Issues
  * [[enhancement 25]](https://gitlab.com/ultreiaio/topia-extension/-/issues/25) **Add Entity toDto and fromDto methods (and generate it)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.15](https://gitlab.com/ultreiaio/topia-extension/-/milestones/34)

**Closed at 2020-01-23.**


### Issues
  * [[enhancement 25]](https://gitlab.com/ultreiaio/topia-extension/-/issues/25) **Add Entity toDto and fromDto methods (and generate it)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.14](https://gitlab.com/ultreiaio/topia-extension/-/milestones/33)

**Closed at 2019-08-02.**


### Issues
  * [[bug 24]](https://gitlab.com/ultreiaio/topia-extension/-/issues/24) **Fix yet another synonyms problem (with Set entity name)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.14](https://gitlab.com/ultreiaio/topia-extension/-/milestones/33)

**Closed at 2019-08-02.**


### Issues
  * [[bug 24]](https://gitlab.com/ultreiaio/topia-extension/-/issues/24) **Fix yet another synonyms problem (with Set entity name)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.13](https://gitlab.com/ultreiaio/topia-extension/-/milestones/32)

**Closed at 2019-07-25.**


### Issues
  * [[enhancement 23]](https://gitlab.com/ultreiaio/topia-extension/-/issues/23) **Fix more synonyms problems :)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.13](https://gitlab.com/ultreiaio/topia-extension/-/milestones/32)

**Closed at 2019-07-25.**


### Issues
  * [[enhancement 23]](https://gitlab.com/ultreiaio/topia-extension/-/issues/23) **Fix more synonyms problems :)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.12](https://gitlab.com/ultreiaio/topia-extension/-/milestones/29)

**Closed at 2019-07-16.**


### Issues
  * [[enhancement 19]](https://gitlab.com/ultreiaio/topia-extension/-/issues/19) **Permits to use synonyms inside same entity** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 20]](https://gitlab.com/ultreiaio/topia-extension/-/issues/20) **Remove hardcode code in org.nuiton.topia.service.script.executor.SqlTablesRequestExecutorSupport.ReadSqlWork#ReadSqlWork** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 21]](https://gitlab.com/ultreiaio/topia-extension/-/issues/21) **Add a generic way to replicate entity with recursion** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.12](https://gitlab.com/ultreiaio/topia-extension/-/milestones/29)

**Closed at 2019-07-16.**


### Issues
  * [[enhancement 19]](https://gitlab.com/ultreiaio/topia-extension/-/issues/19) **Permits to use synonyms inside same entity** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 20]](https://gitlab.com/ultreiaio/topia-extension/-/issues/20) **Remove hardcode code in org.nuiton.topia.service.script.executor.SqlTablesRequestExecutorSupport.ReadSqlWork#ReadSqlWork** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 21]](https://gitlab.com/ultreiaio/topia-extension/-/issues/21) **Add a generic way to replicate entity with recursion** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.11](https://gitlab.com/ultreiaio/topia-extension/-/milestones/28)

**Closed at 2019-07-01.**


### Issues
  * [[enhancement 18]](https://gitlab.com/ultreiaio/topia-extension/-/issues/18) **Add isOwner method in JdbcPostgresHelper** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.11](https://gitlab.com/ultreiaio/topia-extension/-/milestones/28)

**Closed at 2019-07-01.**


### Issues
  * [[enhancement 18]](https://gitlab.com/ultreiaio/topia-extension/-/issues/18) **Add isOwner method in JdbcPostgresHelper** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.10](https://gitlab.com/ultreiaio/topia-extension/-/milestones/25)

**Closed at 2019-06-26.**


### Issues
  * [[enhancement 16]](https://gitlab.com/ultreiaio/topia-extension/-/issues/16) **Add entity-name in generated hibernate mapping to support synonym entities** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 17]](https://gitlab.com/ultreiaio/topia-extension/-/issues/17) **Only generate some stuff on concrete entity** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.10](https://gitlab.com/ultreiaio/topia-extension/-/milestones/25)

**Closed at 2019-06-26.**


### Issues
  * [[enhancement 16]](https://gitlab.com/ultreiaio/topia-extension/-/issues/16) **Add entity-name in generated hibernate mapping to support synonym entities** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 17]](https://gitlab.com/ultreiaio/topia-extension/-/issues/17) **Only generate some stuff on concrete entity** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.9](https://gitlab.com/ultreiaio/topia-extension/-/milestones/24)

**Closed at 2019-06-02.**


### Issues
  * [[bug 15]](https://gitlab.com/ultreiaio/topia-extension/-/issues/15) **Topia delete method not correct (again and again)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.9](https://gitlab.com/ultreiaio/topia-extension/-/milestones/24)

**Closed at 2019-06-02.**


### Issues
  * [[bug 15]](https://gitlab.com/ultreiaio/topia-extension/-/issues/15) **Topia delete method not correct (again and again)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.8](https://gitlab.com/ultreiaio/topia-extension/-/milestones/23)

**Closed at 2019-05-13.**


### Issues
  * [[enhancement 14]](https://gitlab.com/ultreiaio/topia-extension/-/issues/14) **Add a new delete sql query (Report from 1.1.x)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.8](https://gitlab.com/ultreiaio/topia-extension/-/milestones/23)

**Closed at 2019-05-13.**


### Issues
  * [[enhancement 14]](https://gitlab.com/ultreiaio/topia-extension/-/issues/14) **Add a new delete sql query (Report from 1.1.x)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.7](https://gitlab.com/ultreiaio/topia-extension/-/milestones/21)

**Closed at 2019-02-13.**


### Issues
  * [[enhancement 12]](https://gitlab.com/ultreiaio/topia-extension/-/issues/12) **Improve TopiaUsageSupport** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.7](https://gitlab.com/ultreiaio/topia-extension/-/milestones/21)

**Closed at 2019-02-13.**


### Issues
  * [[enhancement 12]](https://gitlab.com/ultreiaio/topia-extension/-/issues/12) **Improve TopiaUsageSupport** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.6](https://gitlab.com/ultreiaio/topia-extension/-/milestones/18)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.6](https://gitlab.com/ultreiaio/topia-extension/-/milestones/18)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.5](https://gitlab.com/ultreiaio/topia-extension/-/milestones/16)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.5](https://gitlab.com/ultreiaio/topia-extension/-/milestones/16)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.4](https://gitlab.com/ultreiaio/topia-extension/-/milestones/15)

**Closed at 2018-12-17.**


### Issues
  * [[bug 9]](https://gitlab.com/ultreiaio/topia-extension/-/issues/9) **Arch got a regression while not using relative names...** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.4](https://gitlab.com/ultreiaio/topia-extension/-/milestones/15)

**Closed at 2018-12-17.**


### Issues
  * [[bug 9]](https://gitlab.com/ultreiaio/topia-extension/-/issues/9) **Arch got a regression while not using relative names...** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.3](https://gitlab.com/ultreiaio/topia-extension/-/milestones/13)

**Closed at *In progress*.**


### Issues
  * [[enhancement 8]](https://gitlab.com/ultreiaio/topia-extension/-/issues/8) **Be able to manage Homonym Classes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.3](https://gitlab.com/ultreiaio/topia-extension/-/milestones/13)

**Closed at *In progress*.**


### Issues
  * [[enhancement 8]](https://gitlab.com/ultreiaio/topia-extension/-/issues/8) **Be able to manage Homonym Classes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.2](https://gitlab.com/ultreiaio/topia-extension/-/milestones/3)

**Closed at 2018-12-12.**


### Issues
  * [[bug 7]](https://gitlab.com/ultreiaio/topia-extension/-/issues/7) **Fix hibernate mapping file generation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 5]](https://gitlab.com/ultreiaio/topia-extension/-/issues/5) **Add schema in generated topia entity enum** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 6]](https://gitlab.com/ultreiaio/topia-extension/-/issues/6) **Sort generated entity enum literals** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.2](https://gitlab.com/ultreiaio/topia-extension/-/milestones/3)

**Closed at 2018-12-12.**


### Issues
  * [[bug 7]](https://gitlab.com/ultreiaio/topia-extension/-/issues/7) **Fix hibernate mapping file generation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 5]](https://gitlab.com/ultreiaio/topia-extension/-/issues/5) **Add schema in generated topia entity enum** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 6]](https://gitlab.com/ultreiaio/topia-extension/-/issues/6) **Sort generated entity enum literals** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.17](https://gitlab.com/ultreiaio/topia-extension/-/milestones/31)

**Closed at 2020-04-14.**


### Issues
No issue.

## Version [1.1.17](https://gitlab.com/ultreiaio/topia-extension/-/milestones/31)

**Closed at 2020-04-14.**


### Issues
No issue.

## Version [1.1.16](https://gitlab.com/ultreiaio/topia-extension/-/milestones/30)

**Closed at 2019-07-16.**


### Issues
No issue.

## Version [1.1.16](https://gitlab.com/ultreiaio/topia-extension/-/milestones/30)

**Closed at 2019-07-16.**


### Issues
No issue.

## Version [1.1.15](https://gitlab.com/ultreiaio/topia-extension/-/milestones/27)

**Closed at 2019-07-03.**


### Issues
  * [[enhancement 22]](https://gitlab.com/ultreiaio/topia-extension/-/issues/22) **Retro report from version 1.12** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.15](https://gitlab.com/ultreiaio/topia-extension/-/milestones/27)

**Closed at 2019-07-03.**


### Issues
  * [[enhancement 22]](https://gitlab.com/ultreiaio/topia-extension/-/issues/22) **Retro report from version 1.12** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.14](https://gitlab.com/ultreiaio/topia-extension/-/milestones/26)

**Closed at 2019-06-05.**


### Issues
No issue.

## Version [1.1.14](https://gitlab.com/ultreiaio/topia-extension/-/milestones/26)

**Closed at 2019-06-05.**


### Issues
No issue.

## Version [1.1.13](https://gitlab.com/ultreiaio/topia-extension/-/milestones/22)

**Closed at 2019-05-13.**


### Issues
  * [[enhancement 13]](https://gitlab.com/ultreiaio/topia-extension/-/issues/13) **Add a new delete sql query** (Thanks to ) (Reported by Tony CHEMIT)

## Version [1.1.13](https://gitlab.com/ultreiaio/topia-extension/-/milestones/22)

**Closed at 2019-05-13.**


### Issues
  * [[enhancement 13]](https://gitlab.com/ultreiaio/topia-extension/-/issues/13) **Add a new delete sql query** (Thanks to ) (Reported by Tony CHEMIT)

## Version [1.1.12](https://gitlab.com/ultreiaio/topia-extension/-/milestones/20)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.1.12](https://gitlab.com/ultreiaio/topia-extension/-/milestones/20)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.1.11](https://gitlab.com/ultreiaio/topia-extension/-/milestones/19)

**Closed at 2019-02-01.**


### Issues
  * [[enhancement 11]](https://gitlab.com/ultreiaio/topia-extension/-/issues/11) **Add a way to change script layout in migration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.11](https://gitlab.com/ultreiaio/topia-extension/-/milestones/19)

**Closed at 2019-02-01.**


### Issues
  * [[enhancement 11]](https://gitlab.com/ultreiaio/topia-extension/-/issues/11) **Add a way to change script layout in migration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.10](https://gitlab.com/ultreiaio/topia-extension/-/milestones/17)

**Closed at 2019-01-10.**


### Issues
  * [[bug 10]](https://gitlab.com/ultreiaio/topia-extension/-/issues/10) **Bug in usages code** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.10](https://gitlab.com/ultreiaio/topia-extension/-/milestones/17)

**Closed at 2019-01-10.**


### Issues
  * [[bug 10]](https://gitlab.com/ultreiaio/topia-extension/-/issues/10) **Bug in usages code** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.9](https://gitlab.com/ultreiaio/topia-extension/-/milestones/12)

**Closed at 2018-11-28.**


### Issues
  * [[enhancement 4]](https://gitlab.com/ultreiaio/topia-extension/-/issues/4) **Add schemas in TopiaMetadataModel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.9](https://gitlab.com/ultreiaio/topia-extension/-/milestones/12)

**Closed at 2018-11-28.**


### Issues
  * [[enhancement 4]](https://gitlab.com/ultreiaio/topia-extension/-/issues/4) **Add schemas in TopiaMetadataModel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.8](https://gitlab.com/ultreiaio/topia-extension/-/milestones/11)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.1.8](https://gitlab.com/ultreiaio/topia-extension/-/milestones/11)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.1.7](https://gitlab.com/ultreiaio/topia-extension/-/milestones/10)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.1.7](https://gitlab.com/ultreiaio/topia-extension/-/milestones/10)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.1.6](https://gitlab.com/ultreiaio/topia-extension/-/milestones/9)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.1.6](https://gitlab.com/ultreiaio/topia-extension/-/milestones/9)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.1.5](https://gitlab.com/ultreiaio/topia-extension/-/milestones/8)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.1.5](https://gitlab.com/ultreiaio/topia-extension/-/milestones/8)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.1.4](https://gitlab.com/ultreiaio/topia-extension/-/milestones/7)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.1.4](https://gitlab.com/ultreiaio/topia-extension/-/milestones/7)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.1.3](https://gitlab.com/ultreiaio/topia-extension/-/milestones/6)

**Closed at 2018-08-02.**


### Issues
  * [[enhancement 3]](https://gitlab.com/ultreiaio/topia-extension/-/issues/3) **Migrates to Log4J2** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.3](https://gitlab.com/ultreiaio/topia-extension/-/milestones/6)

**Closed at 2018-08-02.**


### Issues
  * [[enhancement 3]](https://gitlab.com/ultreiaio/topia-extension/-/issues/3) **Migrates to Log4J2** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.2](https://gitlab.com/ultreiaio/topia-extension/-/milestones/5)

**Closed at 2018-07-13.**


### Issues
  * [[bug 2]](https://gitlab.com/ultreiaio/topia-extension/-/issues/2) **Use sql batch to improve performance** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.2](https://gitlab.com/ultreiaio/topia-extension/-/milestones/5)

**Closed at 2018-07-13.**


### Issues
  * [[bug 2]](https://gitlab.com/ultreiaio/topia-extension/-/issues/2) **Use sql batch to improve performance** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.1](https://gitlab.com/ultreiaio/topia-extension/-/milestones/4)

**Closed at 2018-05-25.**


### Issues
  * [[bug 1]](https://gitlab.com/ultreiaio/topia-extension/-/issues/1) **Fix a bug in computation of TopiaTable for association table on a top level entity** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.1](https://gitlab.com/ultreiaio/topia-extension/-/milestones/4)

**Closed at 2018-05-25.**


### Issues
  * [[bug 1]](https://gitlab.com/ultreiaio/topia-extension/-/issues/1) **Fix a bug in computation of TopiaTable for association table on a top level entity** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1](https://gitlab.com/ultreiaio/topia-extension/-/milestones/2)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.1](https://gitlab.com/ultreiaio/topia-extension/-/milestones/2)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0](https://gitlab.com/ultreiaio/topia-extension/-/milestones/1)

**Closed at 2018-05-22.**


### Issues
No issue.

## Version [1.0](https://gitlab.com/ultreiaio/topia-extension/-/milestones/1)

**Closed at 2018-05-22.**


### Issues
No issue.

