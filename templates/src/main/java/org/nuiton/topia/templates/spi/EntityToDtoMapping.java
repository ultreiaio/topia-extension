package org.nuiton.topia.templates.spi;

/*-
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.topia.templates.TopiaEntityTransformer;

import java.io.Closeable;
import java.util.Objects;
import java.util.Set;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public abstract class EntityToDtoMapping implements Closeable {

    private TopiaEntityTransformer transformer;

    public TopiaEntityTransformer getTransformer() {
        return transformer;
    }

    public void init(TopiaEntityTransformer topiaEntityTransformer) {
        this.transformer = Objects.requireNonNull(topiaEntityTransformer);
    }

    @Override
    public abstract void close();

    public abstract Set<Class<?>> getDtoTypes(ObjectModelClass entity);

    public abstract boolean accept(ObjectModelClass entity);

    public abstract boolean acceptReference(ObjectModelClass entity);

    public abstract boolean acceptToDtoProperty(Class<?> dtoType, String propertyName);

    public abstract boolean acceptFromDtoProperty(Class<?> dtoType, String propertyName, boolean multiple, boolean isEntity);

    public abstract String getToDtoSimpleMethod(Class<?> dtoType, ObjectModelAttribute attribute, String operationPrefix, boolean isEntity);

    public abstract String getToDtoMultipleMethod(Class<?> dtoType, ObjectModelAttribute attribute, boolean isEntity);

    public abstract String getFromDtoSimpleMethod(Class<?> dtoType, ObjectModelAttribute attribute, String operationPrefix, boolean isEntity);

    public abstract String getFromDtoMultipleMethod(Class<?> dtoType, ObjectModelAttribute attribute, Class<?> collectionInterface, boolean isEntity);

    public abstract Class<?> getDtoContract(ObjectModelClass input);

    public abstract Class<?> getDtoType();
}
