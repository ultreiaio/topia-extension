package org.nuiton.topia.templates;

/*
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/*{generator option: parentheses = false}*/

/*{generator option: writeString = +}*/

import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnumProvider;
import org.nuiton.topia.persistence.internal.AbstractTopiaApplicationContext;
import org.nuiton.topia.persistence.internal.AbstractTopiaPersistenceContextConstructorParameter;

import java.util.Properties;
import java.util.Set;

/**
 * To generate PersistenceHelper
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
@Component(role = Template.class, hint = "org.nuiton.topia.templates.ApplicationContextTransformer")
public class ApplicationContextTransformer extends ObjectModelTransformerToJava {

    protected TopiaTemplateHelper templateHelper;

    protected final TopiaCoreTagValues topiaCoreTagValues;

    public ApplicationContextTransformer() {
        this.topiaCoreTagValues = new TopiaCoreTagValues();
    }

    @Override
    public void transformFromModel(ObjectModel input) {

        if (templateHelper == null) {
            templateHelper = new TopiaTemplateHelper(model);
        }

        String packageName = templateHelper.getApplicationContextPackage(this, model);

        String applicationContextAbstractName = templateHelper.getApplicationContextAbstractName(model);

        String applicationContextConcreteName = templateHelper.getApplicationContextConcreteName(model);

        boolean generateAbstract = !getResourcesHelper().isJavaFileInClassPath(packageName + "." + applicationContextAbstractName);

        boolean generateConcrete = !getResourcesHelper().isJavaFileInClassPath(packageName + "." + applicationContextConcreteName);

        if (generateAbstract) {

            generateAbstract(packageName,
                             applicationContextAbstractName);
        }

        if (generateConcrete) {

            generateImpl(packageName,
                         applicationContextAbstractName,
                         applicationContextConcreteName);
        }

    }

    protected void generateAbstract(String packageName,
                                    String className) {

        // try to find a super class by tag-value
        String superClass = topiaCoreTagValues.getApplicationContextSuperClassTagValue(model);

        if (superClass == null) {

            // no super-class, use default one
            superClass = AbstractTopiaApplicationContext.class.getName();
        } else {

            //TODO check that super class instance of ApplicationPersistenceContext

        }

        ObjectModelClass output = createAbstractClass(className, packageName);

        String persistenceContextConcreteName = templateHelper.getPersistenceContextConcreteName(model);
        setSuperClass(output, superClass + "<" + persistenceContextConcreteName + ">");

        // detect if there is a contract to set on abstract
        String contractName = templateHelper.getApplicationContextInterfaceName(model);

        boolean addPersistenceContextContract = getResourcesHelper().isJavaFileInClassPath(packageName + "." + contractName);

        if (addPersistenceContextContract) {
            addInterface(output, packageName + "." + contractName);
        }

        String modelName = model.getName();

        String entityEnumName = modelName + "EntityEnum";

        addImport(output, TopiaEntityEnumProvider.class);

        addInterface(output, TopiaEntityEnumProvider.class.getName() + "<" + entityEnumName + ">");

        addImport(output, TopiaEntity.class);

        addConstructors(output, false);

        ObjectModelOperation op;

        // newPersistenceContext method
        addImport(output, AbstractTopiaPersistenceContextConstructorParameter.class);
        op = addOperation(output, "newPersistenceContext", persistenceContextConcreteName, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, op, Override.class);
        setOperationBody(op, ""
/*{

        <%=persistenceContextConcreteName%> newContext = new <%=persistenceContextConcreteName%>(
                new AbstractTopiaPersistenceContextConstructorParameter(
                        getHibernateProvider(),
                        getTopiaFiresSupport(),
                        getTopiaIdFactory(),
                        getSessionRegistry(),
                        getConfiguration().getSlowQueriesThreshold()
                )
        );
        registerPersistenceContext(newContext);
        return newContext;
    }*/
        );

        // getModelVersion method
        String modelVersion = model.getVersion();
        op = addOperation(output, "getModelVersion", "String", ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, op, Override.class);
        setOperationBody(op, ""
/*{
        return "<%=modelVersion%>";
    }*/
        );

        // getModelName method
        op = addOperation(output, "getModelName", "String", ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, op, Override.class);
        setOperationBody(op, ""
/*{
        return "<%=modelName%>";
    }*/
        );

        // getContractClass method
        op = addOperation(output, "getContractClass", "<T extends TopiaEntity> Class<T>", ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, op, Override.class);
        addParameter(op, "Class<T>", "klass");
        setOperationBody(op, ""
/*{
        return <%=entityEnumName%>.getContractClass(klass);
    }*/
        );

        // getContractClasses method
        op = addOperation(output, "getContractClasses", "Class<? extends TopiaEntity>[]", ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, op, Override.class);
        setOperationBody(op, ""
/*{
        return <%=entityEnumName%>.getContractClasses();
    }*/
        );

        // getImplementationClass method
        op = addOperation(output, "getImplementationClass", "<T extends TopiaEntity> Class<T>", ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, op, Override.class);
        addParameter(op, "Class<T>", "klass");
        setOperationBody(op, ""
/*{
        return <%=entityEnumName%>.getImplementationClass(klass);
    }*/
        );

        // getImplementationClasses method
        addImport(output, Set.class);
        op = addOperation(output, "getImplementationClasses", "Set<Class<? extends TopiaEntity>>", ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, op, Override.class);
        setOperationBody(op, ""
/*{
        return <%=entityEnumName%>.getImplementationClasses();
    }*/
        );

        // getContracts method
        op = addOperation(output, "getContracts", entityEnumName + "[]", ObjectModelJavaModifier.PUBLIC);
        setOperationBody(op, ""
/*{
        return <%=entityEnumName%>.getContracts();
    }*/
        );

        op = addOperation(output, "getEntityEnum", "<E extends TopiaEntity> " + entityEnumName,
                          ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, op, Override.class);
        addParameter(op, "Class<E>", "type");
        setOperationBody(op, ""
/*{
        return <%=entityEnumName%>.valueOf(type);
    }*/
        );

        op = addOperation(output, "getEntityEnum", entityEnumName,
                          ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, op, Override.class);
        addParameter(op, String.class, "name");
        setOperationBody(op, ""
/*{
        return <%=entityEnumName%>.valueOf(name);
    }*/
        );

    }

    protected ObjectModelClass generateImpl(String packageName,
                                            String applicationContextAbstractName,
                                            String applicationContextConcreteName) {

        ObjectModelClass output = createClass(applicationContextConcreteName, packageName);

        setSuperClass(output, applicationContextAbstractName);

        addConstructors(output, true);
        return output;
    }

    protected void addConstructors(ObjectModelClass output, boolean isPublic) {

        ObjectModelJavaModifier visibility = isPublic ?
                                             ObjectModelJavaModifier.PUBLIC :
                                             ObjectModelJavaModifier.PROTECTED;

        ObjectModelOperation constructor = addConstructor(output, visibility);
        addAnnotation(output, constructor, Deprecated.class);
        addParameter(constructor, Properties.class, "properties");
        setOperationBody(constructor, ""
/*{
        super(properties);
    }*/
        );

        constructor = addConstructor(output, visibility);
        addAnnotation(output, constructor, Deprecated.class);
        addParameter(constructor, "java.util.Map<String, String>", "configuration");
        setOperationBody(constructor, ""
/*{
        super(configuration);
    }*/
        );

        constructor = addConstructor(output, visibility);
        addParameter(constructor, TopiaConfiguration.class, "topiaConfiguration");
        setOperationBody(constructor, ""
/*{
        super(topiaConfiguration);
    }*/
        );
    }

}
