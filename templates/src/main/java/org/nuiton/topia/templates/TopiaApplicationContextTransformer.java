package org.nuiton.topia.templates;

/*-
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

import com.google.common.collect.ImmutableMap;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.topia.persistence.TopiaConfigurationExtension;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.templates.spi.TopiaTemplateHelperExtension;

import java.util.Map;
import java.util.function.Supplier;

/**
 * Created on 19/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.61
 */
@Component(role = Template.class, hint = "org.nuiton.topia.templates.TopiaPersistenceContextTransformer")
public class TopiaApplicationContextTransformer extends ApplicationContextTransformer {

    private ObjectModelClass output;

    @Override
    protected void generateAbstract(String packageName, String className) {
        super.generateAbstract(packageName, className);
        TopiaEntityDaoTransformer.removeOperation(output, "newPersistenceContext");

        addImport(output, TopiaDao.class);
        addImport(output, ImmutableMap.class);
        addImport(output, Supplier.class);
        addImport(output, Map.class);
        ObjectModelOperation createDaoMapping = addOperation(output, "createDaoMapping", "Map<String, Supplier<TopiaDao<?>>>", ObjectModelJavaModifier.PROTECTED);
        addAnnotation(output, createDaoMapping, Override.class);
        StringBuilder body = new StringBuilder(""/*{
        return ImmutableMap.<String, Supplier<TopiaDao<?>>>builder()}*/);
        TopiaTemplateHelperExtension templateHelper = new TopiaTemplateHelperExtension(model);
        for (ObjectModelClass entityClass : templateHelper.getEntityClasses(model, true)) {
            if (entityClass.isAbstract()) {
                continue;
            }
            String source = entityClass.getQualifiedName();
            String daoName = source + "TopiaDao";
            body.append(""/*{
                .put(<%=source%>.class.getName(), <%=daoName%>::new)}*/);
        }
        body.append(""/*{
                .build();
    }*/);
        setOperationBody(createDaoMapping, body.toString());
    }

    @Override
    protected void addConstructors(ObjectModelClass output, boolean isPublic) {
        this.output = output;
        ObjectModelOperation constructor = addConstructor(output, isPublic ? ObjectModelJavaModifier.PUBLIC : ObjectModelJavaModifier.PROTECTED);
        addParameter(constructor, TopiaConfigurationExtension.class, "topiaConfiguration");
        setOperationBody(constructor, ""/*{
        super(topiaConfiguration);
    }*/
        );
    }
}
