/**
 * TODO-FD20100507 : Need update this javadoc for ToPIA 2.4
 *
 * <h1>Les Générateurs</h1>
 *
 * <h2>TopiaMetaGenerator</h2>
 *
 * <p>TopiaMetaGenerator permet d'enchainer les différents
 * générateurs.</p>
 *
 * <h2>Les DAO</h2>
 *
 * <p>Il il y a trois générateurs pour les DAO</p>
 *
 * <ul>
 *   <li>DAOHelperGenerator</li>
 *
 *   <li>DAOAbstractGenerator</li>
 *
 *   <li>DAOGenerator</li>
 * </ul>
 *
 * <p><b><i>DAOHelperGenerator</i></b> permet de récupérer les DAOs
 * générés spécifiquement pour l'application sans avoir besoin de passer
 * le type de l'entité en paramètre. Cette classe contient donc une
 * methode get par type d'entity qui permet de récupérer le DAO associé.
 * Ces méthodes sont des méthodes statiques et prennent en paramètre un
 * TopiaContext.</p>
 *
 * <p><b><i>DAOAbstractGenerator</i></b> est une classe abstraite même si
 * elle peut implanter toutes les méthodes de l'interface TopiaDAO. De
 * cette façon on oblige l'existance d'une classe concrète qui en hérite
 * soit développé par le développeur soit généré par DAOGenerator.
 * DAOAbstractGenerator contient toutes les méthodes findBy, findAllBy,
 * ... associées aux attributs existants. La classe généré hérite
 * directement ou indirectement de TopiaDAODelegator.</p>
 *
 * <p><b><i>DAOGenerator</i></b> génère une classe vide qui permet au
 * programme de compiler, si le développeur à besoin de méthode find
 * supplémentaire sur son DAO, il lui suffit de d'implanter cette classe
 * dans ses sources, le processus de génération écrasera alors la classe
 * généré par la classe développée spécifiquement.</p>
 *
 * <h2>Les entités</h2>
 *
 * <p>Les entités sont de pure POJO et ne contiennent pas de référence en
 * interne sur le TopiaContext ou le DAO qui les à créée, elle peuvent
 * donc facilement migrer, être utilisé dans différents context, ... le
 * but etant qu'elle reste des classes complètement déconnecté à
 * l'exécution du framework.</p>
 *
 * <p>La seul contrainte est qu'elles implante TopiaEntity</p>
 *
 * <p>Il il y a trois générateurs pour les entités, plus un pour le
 * mapping hibernate</p>
 *
 * <ul>
 *   <li>EntityInterfaceGenerator</li>
 *
 *   <li>EntityAbstractGenerator</li>
 *
 *   <li>EntityImplGenerator</li>
 *
 *   <li>EntityHibernateMappingGenerator</li>
 * </ul>
 *
 * <p><b><i>EntityInterfaceGenerator</i></b> génère l'interface de
 * l'entité avec les méthodes d'accès aux attributs et les opérations
 * définis par l'utilisateur dans son diagrammme de classe. Elle implante
 * TopiaEntity</p>
 *
 * <p><b><i>EntityAbstractGenerator</i></b> génère une classe qui
 * implante l'interface de l'entité et étend AbstractTopiaEntity qui
 * implante les méthodes du framework, méthode d'accès aux attributs
 * topiaId, topiaVersion et topiaCreateDate.</p>
 *
 * <p><b><i>EntityImplGenerator</i></b> génère une classe vide qui permet
 * au programme de compiler si l'entité n'a pas d'opération spécifique.
 * Si elle a des opérations spécifiques le développeur doit implanter
 * cette classe dans ses sources et y mettre le code pour les opérations
 * spécifiques, le processus de génération écrasera alors la classe
 * généré par la classe développée spécifiquement.</p>
 *
 * <p><b><i>EntityHibernateMappingGenerator</i></b> génère le fichier de
 * mapping pour entité. L'interface est déclaré dans le mapping et est
 * mappé sur une table préfixé par I. Ensuite le Impl est déclaré en
 * union-subclass de cette interface. On a besoin de l'interface car les
 * méthodes l'utilise dans les signatures de méthode lorsqu'il y a un
 * lien entre deux entités.</p>
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 */
package org.nuiton.topia.templates;

/*
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
