package org.nuiton.topia.templates.sql.plan;

/*-
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.service.sql.model.TopiaEntitySqlDescriptor;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlDescriptors;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSelector;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlTable;
import org.nuiton.topia.service.sql.plan.delete.TopiaEntitySqlDeletePlan;
import org.nuiton.topia.service.sql.plan.delete.TopiaEntitySqlDeletePlanTask;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Created on 28/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.67
 */
public abstract class TopiaEntitySqlDeletePlanBuilder {

    protected final Set<TopiaEntitySqlDeletePlanTask> tasks;
    protected final Set<TopiaEntitySqlDeletePlanTask> associationTasks;
    protected final TopiaEntitySqlDescriptors descriptors;
    protected final List<String> entityNames;

    public TopiaEntitySqlDeletePlanBuilder(TopiaEntitySqlDescriptors descriptors) {
        this.descriptors = Objects.requireNonNull(descriptors);
        this.tasks = new LinkedHashSet<>();
        this.associationTasks = new LinkedHashSet<>();
        this.entityNames = descriptors.stream().map(d -> d.getTable().getEntityName()).collect(Collectors.toList());
    }

    protected abstract void startConsumeTable(TopiaEntitySqlDescriptor descriptor);

    public final TopiaEntitySqlDeletePlan build() {
        onTables(this::startConsumeTable);
        Set<TopiaEntitySqlDeletePlanTask> result = new LinkedHashSet<>(associationTasks);
        result.addAll(tasks);
        return new TopiaEntitySqlDeletePlan(result);
    }

    public List<String> getEntityNames() {
        return entityNames;
    }

    public void onTables(Consumer<TopiaEntitySqlDescriptor> consumer) {
        descriptors.forEach(consumer);
    }

    protected void addSimpleTask(String gav, String deleteSql) {
        tasks.add(new TopiaEntitySqlDeletePlanTask(gav, deleteSql));
    }

    protected void addAssociationTask(String gav, String deleteSql) {
        associationTasks.add(new TopiaEntitySqlDeletePlanTask(gav, deleteSql));
    }

    protected void addTableDeleteSql(TopiaEntitySqlTable table, TopiaEntitySqlSelector selector, boolean noPath) {
        addSimpleTask(table.getSchemaAndTableName(), SqlHelper.newDeleteStatementSql(table, noPath, selector));
    }
}
