package org.nuiton.topia.templates;

/*
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelEnumeration;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.ObjectModelPackage;
import org.nuiton.eugene.models.object.xml.ObjectModelAttributeImpl;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.util.TopiaEntityHelper;

import java.lang.reflect.Array;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/*{generator option: parentheses = false}*/

/*{generator option: writeString = +}*/

/**
 * Will generate XyzEntityEnum (where Xyz = Model name)
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
@Component(role = Template.class, hint = "org.nuiton.topia.templates.EntityEnumTransformer")
public class EntityEnumTransformer extends ObjectModelTransformerToJava {
    private static final Logger log = LogManager.getLogger(EntityEnumTransformer.class);
    protected TopiaTemplateHelper templateHelper;

    protected final TopiaCoreTagValues topiaCoreTagValues;
    protected final TopiaHibernateTagValues topiaHibernateTagValues;

    public EntityEnumTransformer() {
        this.topiaCoreTagValues = new TopiaCoreTagValues();
        this.topiaHibernateTagValues = new TopiaHibernateTagValues();
    }

    @Override
    public void transformFromModel(ObjectModel input) {

        if (templateHelper == null) {
            templateHelper = new TopiaTemplateHelper(model);
        }

        String packageName = templateHelper.getApplicationContextPackage(this, model);

        String entityEnumName = templateHelper.getEntityEnumName(model);

        generateEntityEnum(packageName, entityEnumName);

    }

    protected void generateEntityEnum(String packageName, String entityEnumName) {
        List<ObjectModelClass> classes =
                templateHelper.getEntityClasses(model, true);

        ObjectModelEnumeration entityEnum;

        if (log.isDebugEnabled()) {
            log.debug("Will generate standalone " + entityEnumName +
                    " in package " + packageName);
        }
        entityEnum = createEnumeration(entityEnumName, packageName);
        addImport(entityEnum, TopiaEntity.class);
        addImport(entityEnum, Arrays.class);
        addImport(entityEnum, ArrayUtils.class);
        addImport(entityEnum, Modifier.class);

        // generate TopiaEntityEnum
        createEntityEnum(entityEnum,
                entityEnumName,
                         classes
        );
    }

    protected void createEntityEnum(ObjectModelEnumeration entityEnum,
                                    String entityEnumName,
                                    List<ObjectModelClass> classes) {

        ObjectModelAttributeImpl attr;
        ObjectModelOperation op;

        addInterface(entityEnum, TopiaEntityEnum.class);

        addImport(entityEnum, Array.class);

        for (ObjectModelClass clazz : classes) {
            String clazzName = clazz.getName();

            boolean withNatural = false;
            boolean withNotNull = false;
            StringBuilder naturalIdsParams = new StringBuilder();
            StringBuilder notNullParams = new StringBuilder();

            Set<ObjectModelAttribute> naturalIdsAttributes = templateHelper.getNaturalIdAttributes(clazz);
            for (ObjectModelAttribute attribute: naturalIdsAttributes) {
                withNatural = true;
                // attribut metier
                naturalIdsParams.append(", \"").append(attribute.getName()).append("\"");
            }
            Set<ObjectModelAttribute> notNullIdsAttributes = templateHelper.getNotNullAttributes(clazz);
            for (ObjectModelAttribute attribute : notNullIdsAttributes) {
                withNotNull = true;
                // attribut not-null
                notNullParams.append(", \"").append(attribute.getName()).append("\"");
            }

            StringBuilder params = new StringBuilder(clazzName + ".class");

            ObjectModelPackage aPackage = model.getPackage(clazz);
            String dbSchema = topiaHibernateTagValues.getDbSchemaNameTagValue(clazz, aPackage, model);
            if (dbSchema == null) {
                params.append(", null");
            } else {
                params.append(", \"").append(dbSchema.toLowerCase()).append("\"");
            }

            String dbTable  = templateHelper.getDbName(clazz);
            params.append(", \"").append(dbTable.toLowerCase()).append("\"");

            if (withNotNull) {
                params.append(", new String[]{" + notNullParams.substring(2) + "}");
            } else {
                params.append(", ArrayUtils.EMPTY_STRING_ARRAY");
            }
            if (withNatural) {
                params.append(", ").append(naturalIdsParams.substring(2));
            }
            addLiteral(entityEnum, clazzName + '(' + params.toString() + ')');

            addImport(entityEnum, clazz);
        }

        attr = (ObjectModelAttributeImpl) addAttribute(entityEnum, "contract", "Class<? extends TopiaEntity>", null, ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.FINAL);
        attr.setDocumentation("The contract of the entity.");

        attr = (ObjectModelAttributeImpl) addAttribute(entityEnum, "dbSchemaName", "String", null, ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.FINAL);
        attr.setDocumentation("The optional name of database schema of the entity (if none was filled, will be {@code null}).");

        attr = (ObjectModelAttributeImpl) addAttribute(entityEnum, "dbTableName", "String", null, ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.FINAL);
        attr.setDocumentation("The name of the database table for the entity.");

        attr = (ObjectModelAttributeImpl) addAttribute(entityEnum, "implementationFQN", "String", null, ObjectModelJavaModifier.PRIVATE);
        attr.setDocumentation("The fully qualified name of the implementation of the entity.");

        attr = (ObjectModelAttributeImpl) addAttribute(entityEnum, "implementation", "Class<? extends TopiaEntity>",null, ObjectModelJavaModifier.PRIVATE);
        attr.setDocumentation("The implementation class of the entity (will be lazy computed at runtime).");

        attr = (ObjectModelAttributeImpl) addAttribute(entityEnum, "naturalIds", "String[]", null, ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.FINAL);
        attr.setDocumentation("The array of property involved in the natural key of the entity.");

        attr = (ObjectModelAttributeImpl) addAttribute(entityEnum, "notNulls", "String[]", null, ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.FINAL);
        attr.setDocumentation("The array of not null properties of the entity.");

        // constructor
        op = addConstructor(entityEnum, ObjectModelJavaModifier.PACKAGE);
        addParameter(op,"Class<? extends TopiaEntity>","contract");
        addParameter(op,"String","dbSchemaName");
        addParameter(op,"String","dbTableName");
        addParameter(op,"String[]","notNulls");
        addParameter(op,"String ...","naturalIds");
        setOperationBody(op, ""
/*{
        this.contract = contract;
        this.dbSchemaName = dbSchemaName;
        this.dbTableName = dbTableName;
        this.notNulls = Arrays.copyOf(notNulls, notNulls.length);
        this.naturalIds = naturalIds;
        implementationFQN = contract.getName() + "Impl";
    }*/
        );

        // getContract method
        op = addOperation(entityEnum, "getContract", "Class<? extends TopiaEntity>", ObjectModelJavaModifier.PUBLIC);
        addAnnotation(entityEnum,op,Override.class);
        setOperationBody(op, ""
/*{
        return contract;
    }*/
        );

        // dbSchemaName method
        op = addOperation(entityEnum, "dbSchemaName", String.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(entityEnum,op,Override.class.getSimpleName());
        setOperationBody(op, ""
/*{
        return dbSchemaName;
    }*/
        );

        // dbTableName method
        op = addOperation(entityEnum, "dbTableName", String.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(entityEnum,op,Override.class.getSimpleName());
        setOperationBody(op, ""
/*{
        return dbTableName;
    }*/
        );

        // getNaturalIds method
        op = addOperation(entityEnum, "getNaturalIds", "String[]", ObjectModelJavaModifier.PUBLIC);
        addAnnotation(entityEnum,op,Override.class);
        setOperationBody(op, ""
/*{
        return naturalIds;
    }*/
        );

        // isUseNaturalIds method
        op = addOperation(entityEnum, "isUseNaturalIds", "boolean", ObjectModelJavaModifier.PUBLIC);
        addAnnotation(entityEnum,op,Override.class);
        setOperationBody(op, ""
/*{
        return naturalIds.length > 0;
    }*/
        );

        // getNotNulls method
        op = addOperation(entityEnum, "getNotNulls", "String[]", ObjectModelJavaModifier.PUBLIC);
        addAnnotation(entityEnum,op,Override.class);
        setOperationBody(op, ""
/*{
        return notNulls;
    }*/
        );

        // isUseNotNulls method
        op = addOperation(entityEnum, "isUseNotNulls", "boolean", ObjectModelJavaModifier.PUBLIC);
        addAnnotation(entityEnum,op,Override.class);
        setOperationBody(op, ""
/*{
        return notNulls.length > 0;
    }*/
        );

        // getImplementationFQN method
        op = addOperation(entityEnum, "getImplementationFQN","String",ObjectModelJavaModifier.PUBLIC);
        addAnnotation(entityEnum, op, Override.class);
        setOperationBody(op, ""
/*{
        return implementationFQN;
    }*/
        );

        // setImplementationFQN method
        op = addOperation(entityEnum, "setImplementationFQN","void",ObjectModelJavaModifier.PUBLIC);
        addAnnotation(entityEnum,op,Override.class);
        addParameter(op,"String","implementationFQN");
        if (false) {
            setOperationBody(op, ""
/*{
        this.implementationFQN = implementationFQN;
        implementation = null;
        // reinit the operators store
        EntityOperatorStore.clear();
    }*/
            );
        } else {
            setOperationBody(op, ""
/*{
        this.implementationFQN = implementationFQN;
        this.implementation = null;
    }*/
            );
        }

        // accept method
        op = addOperation(entityEnum, "accept","boolean",ObjectModelJavaModifier.PUBLIC);
        addAnnotation(entityEnum,op,Override.class);
        addParameter(op,"Class<? extends TopiaEntity>","klass");
        setOperationBody(op, ""
/*{
        <%=entityEnumName%> constant = valueOf(klass);
        boolean result = constant.getContract() == contract;
        return result;
    }*/
        );

        // getImplementation method
        op = addOperation(entityEnum, "getImplementation","Class<? extends TopiaEntity>",
                ObjectModelJavaModifier.PUBLIC);
        addAnnotation(entityEnum,op,Override.class);
        addImport(entityEnum, TopiaException.class);
        setOperationBody(op, ""
/*{
        if (implementation == null) {
            try {
                implementation = (Class<? extends TopiaEntity>) Class.forName(implementationFQN);
            } catch (ClassNotFoundException e) {
                throw new TopiaException("could not find class " + implementationFQN, e);
            }
        }
        return implementation;
    }*/
        );

        // valueOf method
        op = addOperation(entityEnum, "valueOf", entityEnumName,
                ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        addParameter(op,"TopiaEntity", "entity");
        setOperationBody(op, ""
/*{
        return valueOf(entity.getClass());
    }*/
        );

        // valueOf method
        op = addOperation(entityEnum, "valueOf", entityEnumName,
                ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        addParameter(op, "final Class<?>", "klass");
        addImport(entityEnum, Sets.class);
        addImport(entityEnum, Set.class);
        addImport(entityEnum, List.class);
        addImport(entityEnum, ArrayList.class);
        addImport(entityEnum, Predicate.class);
        addImport(entityEnum, Iterables.class);
        addImport(entityEnum, Iterable.class);
        addImport(entityEnum, TopiaEntityHelper.class);
        setOperationBody(op, ""
/*{
        if (klass.isInterface()) {
            return valueOf(klass.getSimpleName());
        }

        Class<?> contractClass = TopiaEntityHelper.getContractClass(<%=entityEnumName%>.values(), (Class) klass);

        if (contractClass != null) {

            return valueOf(contractClass.getSimpleName());
        }

        throw new IllegalArgumentException("no entity defined for the class " + klass + " in : " + Arrays.toString(<%=entityEnumName%>.values()));
    }*/
        );

        // getContracts method
        op = addOperation(entityEnum, "getContracts", entityEnumName + "[]",
                ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        setOperationBody(op, ""
/*{
        <%=entityEnumName%>[] result = values();
        return result;
    }*/
        );

        // getContractClass method
        op = addOperation(entityEnum, "getContractClass", "<T extends TopiaEntity> Class<T>",
                ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        addParameter(op,"Class<T>", "klass");
        setOperationBody(op, ""
/*{
        <%=entityEnumName%> constant = valueOf(klass);
        Class<T> result = (Class<T>) constant.getContract();
        return result;
    }*/
        );

        // getContractClasses method
        op = addOperation(entityEnum, "getContractClasses", "Class<? extends TopiaEntity>[]",
                ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        setOperationBody(op, ""
/*{
        <%=entityEnumName%>[] values = values();
        Class<? extends TopiaEntity>[] result = (Class<? extends TopiaEntity>[]) Array.newInstance(Class.class, values.length);
        for (int i = 0; i < values.length; i++) {
            result[i] = values[i].getContract();
        }
        return result;
    }*/
        );

        // getContractClasses method
        op = addOperation(entityEnum, "getImplementationClass", "<T extends TopiaEntity> Class<T>",
                ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        addParameter(op,"Class<T>", "klass");
        setOperationBody(op, ""
/*{
        <%=entityEnumName%> constant = valueOf(klass);
        Class<T> result = (Class<T>) constant.getImplementation();
        return result;
    }*/
        );

        addImport(entityEnum, LinkedHashSet.class);
        // getImplementationClasses method
        op = addOperation(entityEnum, "getImplementationClasses", "Set<Class<? extends TopiaEntity>>",
                ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        setOperationBody(op, ""
/*{
        <%=entityEnumName%>[] values = values();
        Set<Class<? extends TopiaEntity>> result = new LinkedHashSet<Class<? extends TopiaEntity>>();
        for (int i = 0; i < values.length; i++) {
            result.add(values[i].getImplementation());
        }
        return result;
    }*/
        );

        // getImplementationClassesAsString method
        op = addOperation(entityEnum, "getImplementationClassesAsString", "String",
                ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        setOperationBody(op, ""
/*{
        StringBuilder buffer = new StringBuilder();
        for (Class<? extends TopiaEntity> aClass : getImplementationClasses()) {
            buffer.append(',').append(aClass.getName());
        }
        String result = buffer.substring(1);
        return result;
    }*/
        );

        if (false) {
            // getOperator method
            op = addOperation(entityEnum, "getOperator", "<T extends TopiaEntity> EntityOperator<T>",
                    ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
            addParameter(op,"Class<T>", "klass");
            setOperationBody(op, ""
/*{
        <%=entityEnumName%> constant = valueOf(klass);
        EntityOperator<T> result = EntityOperatorStore.getOperator(constant);
        return result;
    }*/
            );
        }

    }

}
