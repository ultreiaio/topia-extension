package org.nuiton.topia.templates.spi;

/*-
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelPackage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created on 29/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.40
 */
public class EntityClassContext {

    private final ObjectModel model;
    private final ObjectModelPackage aPackage;
    private final ObjectModelClass input;
    private final String tableName;
    private final String schema;
    private final List<ObjectModelAttribute> attributes = new ArrayList<>();

    public EntityClassContext(TopiaTemplateHelperExtension templateHelper, ObjectModel model, ObjectModelPackage aPackage, ObjectModelClass input) {
        this.model = model;
        this.input = Objects.requireNonNull(input);
        this.aPackage = aPackage;
        this.tableName = templateHelper.getDbName(input);
        this.schema = templateHelper.getSchema(input);
        List<ObjectModelClass> inputs = new LinkedList<>();
        ObjectModelClass current = input;
        while (current != null && templateHelper.isEntity(current)) {
            inputs.add(current);
            Collection<ObjectModelClass> superclasses = current.getSuperclasses();
            if (superclasses.size() > 0) {
                current = superclasses.iterator().next();
            } else {
                current = null;
            }
        }
        Collections.reverse(inputs);
        inputs.forEach(this::detectAttributes);

    }

    public ObjectModel getModel() {
        return model;
    }

    public String getTableName() {
        return tableName;
    }

    public boolean isUseSchema() {
        return schema != null;
    }

    public String getSchema() {
        return schema;
    }

    public List<ObjectModelAttribute> getAttributes() {
        return attributes;
    }

    public ObjectModelClass getInput() {
        return input;
    }

    public ObjectModelPackage getPackage() {
        return aPackage;
    }

    protected void detectAttributes(ObjectModelClass input) {
        attributes.addAll(input.getAttributes());
    }


    private void generateFromTagValue(Map<String, String> map, String attributeName, String tagValue) {
        generateFromTagValue(map, attributeName, tagValue, null);
    }

    private void generateFromTagValue(Map<String, String> map, String attributeName, Boolean tagValue) {
        generateFromTagValue(map, attributeName, tagValue == null ? null : String.valueOf(tagValue), null);
    }

    private void generateFromTagValue(Map<String, String> map, String attributeName, String tagValue, String defaultValue) {
        String value = null;
        if (StringUtils.isNotEmpty(tagValue)) {
            value = tagValue;

        } else if (defaultValue != null) {
            value = defaultValue;
        }
        if (value != null) {
            map.put(attributeName, "\"" + value + "\"");
        }
    }

    private String attributesToString(Map<String, String> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            sb.append(" ").append(entry.getKey()).append("=").append(entry.getValue());
        }
        return sb.toString();
    }
}
