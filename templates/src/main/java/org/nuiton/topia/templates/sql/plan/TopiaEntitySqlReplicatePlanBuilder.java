package org.nuiton.topia.templates.sql.plan;

/*-
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import org.nuiton.topia.service.sql.metadata.TopiaMetadataComposition;
import org.nuiton.topia.service.sql.metadata.TopiaMetadataEntity;
import org.nuiton.topia.service.sql.metadata.TopiaMetadataModel;
import org.nuiton.topia.service.sql.model.AbstractTopiaEntitySqlTable;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlAssociationTable;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlDescriptor;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlDescriptors;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSelector;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSimpleAssociationTable;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlTable;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePlan;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePlanTask;

import java.beans.Introspector;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.topia.templates.sql.plan.SqlHelper.SQL_WHERE_CLAUSE_ALIAS;

/**
 * Created on 06/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.69
 */
public class TopiaEntitySqlReplicatePlanBuilder {
    protected final Set<TopiaEntitySqlReplicatePlanTask> tasks;
    protected final Set<TopiaEntitySqlReplicatePlanTask> associationTasks;
    protected final TopiaEntitySqlDescriptors entityDescriptor;
    private final TopiaMetadataModel metadataModel;
    private final Set<String> standaloneLiterals;
    private final Map<String, String> fqnToLiteral;
    private final TopiaEntitySqlDescriptor mainDescriptor;
    private final ArrayListMultimap<String, TopiaMetadataComposition> mandatoryReverseCompositions;
    private final Set<String> literals;
    private final Set<String> missingTypes;

    public TopiaEntitySqlReplicatePlanBuilder(TopiaMetadataModel metadataModel,
                                              Set<String> standaloneLiterals,
                                              Map<String, String> fqnToLiteral,
                                              TopiaEntitySqlDescriptors entityDescriptor,
                                              TopiaEntitySqlDescriptor mainDescriptor,
                                              ArrayListMultimap<String, TopiaMetadataComposition> mandatoryReverseCompositions) {
        this.metadataModel = metadataModel;
        this.standaloneLiterals = standaloneLiterals;
        this.fqnToLiteral = fqnToLiteral;
        this.entityDescriptor = entityDescriptor;
        this.mainDescriptor = mainDescriptor;
        this.mandatoryReverseCompositions = ArrayListMultimap.create();
        this.tasks = new LinkedHashSet<>();
        this.associationTasks = new LinkedHashSet<>();
        this.literals = entityDescriptor.stream().map(d -> d.getTable().getEntityName()).collect(Collectors.toSet()).stream().map(fqnToLiteral::get).collect(Collectors.toSet());
        this.missingTypes = new LinkedHashSet<>();
        for (Map.Entry<String, Collection<TopiaMetadataComposition>> entry : mandatoryReverseCompositions.asMap().entrySet()) {
            for (TopiaMetadataComposition composition : entry.getValue()) {
                TopiaMetadataEntity owner = composition.getOwner();
                TopiaMetadataEntity target = composition.getTarget();
                if (!literals.contains(target.getType())) {
                    missingTypes.add(target.getType());
                }
                this.mandatoryReverseCompositions.put(owner.getType(), composition);
            }
        }
    }

    public TopiaEntitySqlReplicatePlan build() {
        entityDescriptor.forEach(this::startConsumeTable);
        Set<TopiaEntitySqlReplicatePlanTask> result = new LinkedHashSet<>(tasks);
        result.addAll(associationTasks);
        return new TopiaEntitySqlReplicatePlan(result);
    }

    protected void startConsumeTable(TopiaEntitySqlDescriptor descriptor) {
        List<TopiaEntitySqlAssociationTable> associations = descriptor.getAssociations();
        List<TopiaEntitySqlSimpleAssociationTable> simpleAssociations = descriptor.getSimpleAssociations();
        TopiaEntitySqlTable table = descriptor.getTable();
        String entityName = table.getEntityName();
        String schemaName = table.getSchemaName();
        String tableName = table.getTableName();
        Set<String> blobColumnNames = table.getBlobProperties();
        String recursiveColumnName = table.getOptionalRecursiveProperty().orElse(null);
        boolean mainTable = mainDescriptor.getTable().equals(table);
        TopiaMetadataEntity metadataEntity = metadataModel.getEntity(fqnToLiteral.get(entityName));
        TopiaEntitySqlSelector selector = mainTable ? SqlHelper.newMainSelector(table) : SqlHelper.newSelector(mainDescriptor, table);
        Set<String> columnsToReplace = new LinkedHashSet<>();

        List<String> parentColumnNames = new LinkedList<>();
        Set<String> oneToManyAssociationInverses = metadataEntity.getOneToManyAssociationInverses();
        Set<String> oneToOneCompositionInverses = metadataEntity.getOneToOneCompositionInverses();
        if (!oneToManyAssociationInverses.isEmpty()) {
            String parentType = oneToManyAssociationInverses.iterator().next();
            String parentColumnName = metadataEntity.getDbColumnName(parentType);
            parentColumnNames.add(parentColumnName);
        }
        if (!oneToOneCompositionInverses.isEmpty()) {
            String parentType = oneToOneCompositionInverses.iterator().next();
            String parentColumnName = metadataEntity.getDbColumnName(parentType);
            parentColumnNames.add(parentColumnName);
        }
        if (!mainTable && parentColumnNames.size()>1) {
            String parent = mainDescriptor.getTable().getTableName();
            if (parentColumnNames.contains(parent)) {
                parentColumnNames.clear();
                parentColumnNames.add(parent);
            } else  {
                throw new IllegalArgumentException(String.format("Can't manage with this case: %s", parentColumnNames));
            }
        }
        Set<String> requiredTypes = new LinkedHashSet<>();
        Set<String> realRequiredTypes = new LinkedHashSet<>();
        String literalName = metadataEntity.getType();
        if (mandatoryReverseCompositions.containsKey(literalName)) {
            for (TopiaMetadataComposition composition : mandatoryReverseCompositions.get(literalName)) {
                String type = composition.getTarget().getType();
                if (missingTypes.contains(type)) {
                    requiredTypes.add(type);
                    realRequiredTypes.add(composition.getTarget().getSchemaAndTableName());
                }
            }
        }
        List<String> columNames = List.copyOf(table.getAuthorizedColumnNames());
        Set<String> columnsToDetach = new LinkedHashSet<>();
        for (Map.Entry<String, String> entry : metadataEntity.getManyToOneAssociations().entrySet()) {
            addSpecialColumns(columnsToReplace, columnsToDetach, requiredTypes, entry);
        }
        for (Map.Entry<String, String> entry : metadataEntity.getReversedAssociations().entrySet()) {
            addSpecialColumns(columnsToReplace, columnsToDetach, requiredTypes, entry);
        }
        // the parent column is always to replace
        parentColumnNames.forEach(columnsToReplace::remove);
        boolean useBlob = !blobColumnNames.isEmpty();
        String from = SqlHelper.newFrom(selector);
        tasks.add(new TopiaEntitySqlReplicatePlanTask(schemaName,
                                                      tableName,
                                                      from,
                                                      parentColumnNames,
                                                      columNames,
                                                      columnsToReplace,
                                                      columnsToDetach,
                                                      realRequiredTypes,
                                                      useBlob,
                                                      mainTable,
                                                      recursiveColumnName));

        String parentTableTableName = table.getTableName();
        if (associations != null && !associations.isEmpty()) {
            Map<String, String> manyToManyAssociations = metadataEntity.getManyToManyAssociations();
            Set<String> columnsToReplaceTmp = new LinkedHashSet<>();
            for (Map.Entry<String, String> entry : manyToManyAssociations.entrySet()) {
                if (!literals.contains(entry.getValue())) {
                    // this one need to be replace
                    columnsToReplaceTmp.add(metadataEntity.getDbColumnName(entry.getKey()));
                }
            }
            for (TopiaEntitySqlAssociationTable association : associations) {
                Set<String> associationColumnsToReplace = new LinkedHashSet<>(association.getAuthorizedColumnNames());
                associationColumnsToReplace.removeAll(columnsToReplaceTmp);
                associationColumnsToReplace.add(parentTableTableName);
                startConsumeAssociationTable(tableName, mainTable, selector, parentTableTableName, association, associationColumnsToReplace);
            }
        }
        if (simpleAssociations != null && !simpleAssociations.isEmpty()) {
            Set<String> associationColumnsToReplace = Set.of(parentTableTableName);
            for (TopiaEntitySqlSimpleAssociationTable association : simpleAssociations) {
                startConsumeAssociationTable(tableName, mainTable, selector, parentTableTableName, association, associationColumnsToReplace);
            }
        }
    }

    protected void startConsumeAssociationTable(String parentTableName, boolean mainTable, TopiaEntitySqlSelector selector, String parentColumnName, AbstractTopiaEntitySqlTable table, Set<String> columnsToReplace) {
        List<String> columNames = List.copyOf(table.getAuthorizedColumnNames());
        String tableName = table.getTableName();
        String schemaName = table.getSchemaName();
        TopiaEntitySqlSelector.Builder builder = TopiaEntitySqlSelector.builder().setFromClause(table.getSchemaAndTableName());
        if (mainTable) {
            builder.setWhereClauseAlias(String.format(SQL_WHERE_CLAUSE_ALIAS, tableName, parentColumnName));
        } else {
            builder.setWhereClauseAlias(selector.getWhereClauseAlias())
                    .addJoinClause(String.format("INNER JOIN %1$s ON %2$s.%3$s = %4$s.topiaId %5$s",
                                                 selector.getFromClause(),
                                                 tableName,
                                                 parentColumnName,
                                                 parentTableName,
                                                 selector.getJoinClauses()));
        }
        TopiaEntitySqlSelector associationSelector = builder.build();
        String from = SqlHelper.newFrom(associationSelector);
        associationTasks.add(new TopiaEntitySqlReplicatePlanTask(schemaName,
                                                                 tableName,
                                                                 from,
                                                                 List.of(parentColumnName),
                                                                 columNames,
                                                                 columnsToReplace,
                                                                 Set.of(),
                                                                 Set.of(),
                                                                 false,
                                                                 false,
                                                                 null));
    }

    private void addSpecialColumns(Set<String> columnsToReplace, Set<String> columnsToDetach, Set<String> requiredTypes, Map.Entry<String, String> entry) {
        String literalName = entry.getValue();
        if (literals.contains(literalName)) {
            // this one need to be replace
            columnsToReplace.add(entry.getKey());
        } else if (!standaloneLiterals.contains(literalName) && !requiredTypes.contains(literalName)) {
            // this one need to be detach
            int endIndex = literalName.lastIndexOf("_");
            String columnDefinition = entry.getKey() + "~" + literalName.substring(0, endIndex) + "." + Introspector.decapitalize(literalName.substring(endIndex + 1));
            columnsToDetach.add(columnDefinition);
        }
    }
}

