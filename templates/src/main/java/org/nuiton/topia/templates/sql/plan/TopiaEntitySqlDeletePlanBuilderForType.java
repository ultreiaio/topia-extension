package org.nuiton.topia.templates.sql.plan;

/*-
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.service.sql.model.AbstractTopiaEntitySqlTable;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlAssociationTable;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlDescriptor;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlDescriptors;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlReverseAssociationTable;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlReverseCompositionTable;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSelector;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSimpleAssociationTable;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlTable;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Created on 28/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.67
 */
public class TopiaEntitySqlDeletePlanBuilderForType extends TopiaEntitySqlDeletePlanBuilder {

    private final TopiaEntitySqlDescriptor mainDescriptor;

    public TopiaEntitySqlDeletePlanBuilderForType(TopiaEntitySqlDescriptors descriptors, TopiaEntitySqlDescriptor mainDescriptor) {
        super(descriptors);
        this.mainDescriptor = mainDescriptor;
    }

    @Override
    protected void startConsumeTable(TopiaEntitySqlDescriptor descriptor) {
        TopiaEntitySqlTable table = descriptor.getTable();
        List<TopiaEntitySqlAssociationTable> associations = descriptor.getAssociations();
        List<TopiaEntitySqlSimpleAssociationTable> simpleAssociations = descriptor.getSimpleAssociations();
        boolean mainTable = mainDescriptor.getTable().equals(table);
        TopiaEntitySqlSelector selector = mainTable ? SqlHelper.newMainSelector(table) : SqlHelper.newSelector(mainDescriptor, table);
        // apply if required reverse association updates
        descriptor.getReverseAssociationTables().ifPresent(p -> p.forEach(t -> startConsumeReverseAssociationTable(table, selector, t, mainTable)));
        // apply if required reverse composition deletes
        descriptor.getReverseCompositionTables().ifPresent(p -> p.forEach(t -> startConsumeReverseCompositionTable(table, selector, t, mainTable)));

        addTableDeleteSql(table, selector, descriptor.isNoPath());
        if (associations != null) {
            for (TopiaEntitySqlAssociationTable association : associations) {
                startConsumeAssociationTable(selector, table, association);
            }
        }
        if (simpleAssociations != null) {
            for (TopiaEntitySqlSimpleAssociationTable association : simpleAssociations) {
                startConsumeAssociationTable(selector, table, association);
            }
        }
    }

    protected void startConsumeReverseAssociationTable(TopiaEntitySqlTable parentTable, TopiaEntitySqlSelector mainSelector, TopiaEntitySqlReverseAssociationTable table, boolean mainTable) {
        Optional<String> selector = startConsumeReverseTable(parentTable, mainSelector, table, mainTable);
        selector.ifPresent(where -> addSimpleTask(table.getSchemaAndTableName(),SqlHelper.newDeleteReverseAssociationStatementSql(table, where)));
    }

    protected void startConsumeReverseCompositionTable(TopiaEntitySqlTable parentTable, TopiaEntitySqlSelector mainSelector, TopiaEntitySqlReverseCompositionTable table, boolean mainTable) {
        Optional<String> selector = startConsumeReverseTable(parentTable, mainSelector, table, mainTable);
        selector.ifPresent(where -> addSimpleTask(table.getSchemaAndTableName(),SqlHelper.newDeleteReverseCompositionStatementSql(table, where)));
    }

    protected void startConsumeAssociationTable(TopiaEntitySqlSelector selector, TopiaEntitySqlTable parentTable, AbstractTopiaEntitySqlTable table) {
        addAssociationTask(table.getSchemaAndTableName(),SqlHelper.newDeleteStatementSql(mainDescriptor.getTable(), parentTable, table, selector));
    }

    protected Optional<String> startConsumeReverseTable(TopiaEntitySqlTable parentTable, TopiaEntitySqlSelector mainSelector, AbstractTopiaEntitySqlTable table, boolean mainTable) {
        if (getEntityNames().contains(table.getEntityName())) {
            // the table is part of the plan, nothing to do on reverse
            return Optional.empty();
        }
        String columnName = table.getJoinColumnName();
        //FIXME Try to apply this optimisation at runtime? not sure this is possible
//        TopiaEntitySqlSelectArgument selectArgument = context.getRequest().getSelectArgument();
//        if (selectArgument == null) {
//            return Optional.of(String.format("%s.%s IS NOT NULL", table.getTableName(), columnName));
//        }
        if (mainSelector.getJoinClauses().isEmpty()) {
            TopiaEntitySqlSelector selector = TopiaEntitySqlSelector.builder().setFromClause(mainSelector.getFromClause()).setWhereClauseAlias(mainSelector.getWhereClauseAlias()).build();
            if (mainTable) {
                return Optional.of(selector.getWhereClauseWithValuesPrototype(table.getTableName() + "." + columnName));
            }
            String whereClauseAlias = mainSelector.getWhereClauseAlias();
            String select = whereClauseAlias.substring(0, whereClauseAlias.indexOf(".") + 1) + TopiaEntity.PROPERTY_TOPIA_ID;
            String generate = selector.generatePrototype(select);
            return Optional.of(String.format("%s.%s IN ( %s )", table.getTableName(), columnName, generate));
        }
        String join = String.format("INNER JOIN %1$s.%2$s %2$s_ ON %2$s_.%3$s = %4$s.topiaId %5$s", table.getSchemaName(), table.getTableName(), columnName, parentTable.getTableName(), mainSelector.getJoinClauses());
        TopiaEntitySqlSelector selector = TopiaEntitySqlSelector.builder().setFromClause(mainSelector.getFromClause()).setWhereClauseAlias(mainSelector.getWhereClauseAlias()).addJoinClause(join).build();
        String generate = selector.generatePrototype(table.getTableName() + "." + columnName);
        return Optional.of(String.format("%s.%s IN ( %s )", table.getTableName(), columnName, generate));
    }

}
