package org.nuiton.topia.templates.sql;

/*-
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelGenerator;
import org.nuiton.topia.service.sql.metadata.TopiaMetadataEntity;
import org.nuiton.topia.service.sql.metadata.TopiaMetadataModel;
import org.nuiton.topia.service.sql.metadata.TopiaMetadataModelPaths;
import org.nuiton.topia.templates.spi.TopiaTemplateHelperExtension;

import java.io.File;
import java.io.IOException;

/**
 * Created on 13/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.27
 */
public abstract class TopiaMetadataModelGeneratorSupport extends ObjectModelGenerator {

    protected TopiaMetadataModel metadataModel;
    protected TopiaMetadataModelPaths allPaths;
    private TopiaTemplateHelperExtension templateHelper;

    public static File getNotGeneratedResourceDirector(File destDir) {
        return destDir.toPath() // java
                .getParent() // generated-sources
                .getParent() // target
                .getParent() // basedir
                .resolve("src")
                .resolve("main")
                .resolve("resources").toFile();
    }

    public TopiaTemplateHelperExtension getTemplateHelper() {
        if (templateHelper == null) {
            templateHelper = new TopiaTemplateHelperExtension(model);
        }
        return templateHelper;
    }

    public TopiaMetadataModelPaths getAllPaths() {
        if (allPaths == null) {
            allPaths = TopiaMetadataEntityPathsBuilder.create(metadataModel);
        }
        return allPaths;
    }

    @Override
    public void applyTemplate(ObjectModel model, File destDir) throws IOException {
        File realTarget = TopiaMetadataModelGeneratorSupport.getNotGeneratedResourceDirector(destDir);
        super.applyTemplate(model, realTarget);
    }

    protected TopiaMetadataEntity getEntityEnumName(ObjectModelClass input) {
        String name = getTemplateHelper().getEntityEnumLiteralName(input);
        return metadataModel.getEntity(name);
    }
}
