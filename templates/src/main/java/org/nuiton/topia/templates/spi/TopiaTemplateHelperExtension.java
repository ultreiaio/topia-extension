package org.nuiton.topia.templates.spi;

/*-
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelPackage;
import org.nuiton.topia.templates.TopiaHibernateTagValues;
import org.nuiton.topia.templates.TopiaTemplateHelper;

import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * Created on 19/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public class TopiaTemplateHelperExtension extends TopiaTemplateHelper {

    private final Map<String, String> indexes;
    private final Map<String, String> uniqueKeys;
    private final Map<String, String> foreignKeys;
    private final TopiaExtensionTagValues extensionTagValues = new TopiaExtensionTagValues();

    public TopiaTemplateHelperExtension(ObjectModel model) {
        super(model);
        indexes = new TreeMap<>();
        foreignKeys = new TreeMap<>();
        uniqueKeys = new TreeMap<>();
    }

    public TopiaExtensionTagValues extensionTagValues() {
        return extensionTagValues;
    }

    /**
     * Obtain the reverse db name of a reverse attribute.
     *
     * <strong>Note that the reverse attribute can't be null here.</strong>
     * <ul>
     * <li>Try first to get the reverse db Name from the ReverseDbname tag-value</li>
     * <li>If not found, try then the ReverseDbname tag-value on the same attribute but from this other side of the relation</li>
     * <li>If not found, try then just get the name of the reverse attribute</li>
     * </ul>
     *
     * @param attr the attribute to seek
     * @return the value of the reverse db name on the reverse attribute
     */
    @Override
    public String getReverseDbNameOnReverseAttribute(ObjectModelAttribute attr) {
        ObjectModelAttribute reverseAttribute = attr.getReverseAttribute();
        if (reverseAttribute == null) {
            throw new IllegalArgumentException("The reverse attribute can't be null, but was on " + attr);
        }
        if (!Objects.equals(reverseAttribute.getReverseAttribute(), attr)) {
            //FIXME Bug in eugene the reverse attribute may not be the good one
            return getDbName(attr);
        }
        return super.getReverseDbNameOnReverseAttribute(attr);
    }

    @Override
    public String getManyToManyTableName(ObjectModelAttribute attr) {
        String result;
        if (attr.hasAssociationClass()) {
            result = getDbName(attr.getAssociationClass());
        } else {
            result = topiaHibernateTagValues.getManytoManyTableNameTagValue(attr);
            if (StringUtils.isEmpty(result)) {
                String name = attr.getName();
                String revers = attr.getReverseAttributeName();

                // FIXME echatellier 20170414 in case of attribute with * multiplicity
                // name is wrong. Should always be "parenttablename_attributename" and name sort compare
                if (name.compareToIgnoreCase(revers) < 0) {
                    result = name + '_' + revers;
                } else {
                    result = revers + '_' + name;
                }
            }
        }
        return result;
    }
    public Pair<String, String> registerIndexKeyName(String schema, String tableName, String attrColumn) {
        String result = "idx_";
        if (schema != null) {
            result += schema + "_";
        }
        result += tableName + "_" + attrColumn;
        result = result.toLowerCase();
        if (!indexes.containsKey(result)) {
            String create = String.format("CREATE INDEX %s ON %s.%s(%s)", result, schema, tableName, attrColumn);
            indexes.put(result, create);
            return Pair.of(result, create);
        }
        return null;
    }

    public Pair<String, String> registerUniqueKeyName(String schema, String tableName, String attrColumn) {
        String result = "uk_";
        if (schema != null) {
            result += schema + "_";
        }
        result += tableName + "_" + attrColumn;
        result = result.toLowerCase();
        if (!uniqueKeys.containsKey(result)) {
            String create = String.format("ALTER TABLE %s.%s ADD CONSTRAINT %s unique (%s)", schema, tableName, result, attrColumn);
            uniqueKeys.put(result, create);
            return Pair.of(result, create);
        }
        return null;
    }

    public void registerForeignKey(String result, String schema, String tableName, ObjectModelAttribute attr) {
        String attrColumn = getDbName(attr);
        if (!foreignKeys.containsKey(result)) {
            ObjectModelClassifier attrClassifier = attr.getClassifier();
            String attrSchema = attrClassifier == null ? schema : getSchema(attrClassifier);
            String attrTableName = attrClassifier == null ? attrColumn : getDbName(attrClassifier);
            String create = String.format("ALTER TABLE %s.%s ADD CONSTRAINT %s FOREIGN KEY(%s) REFERENCES %s.%s", schema, tableName, result, attrColumn, attrSchema, attrTableName);
            foreignKeys.put(result, create);
        }
    }

    public Map<String, String> getIndexes() {
        return indexes;
    }

    public Map<String, String> getForeignKeys() {
        return foreignKeys;
    }

    public Map<String, String> getUniqueKeys() {
        return uniqueKeys;
    }

    public String getSchema(ObjectModelClassifier classifier) {
        return topiaHibernateTagValues.getDbSchemaNameTagValue(classifier, model.getPackage(classifier), model);
    }

    public String getEntityEnumLiteralName(ObjectModelClassifier clazz) {
        String literalName = clazz.getName();
        ObjectModelPackage aPackage = model.getPackage(clazz);
        String dbSchema = topiaHibernateTagValues.getDbSchemaNameTagValue(clazz, aPackage, model);
        if (dbSchema != null) {
            literalName = dbSchema + "_" + literalName;
        }
        return literalName;
    }

    public TopiaHibernateTagValues topiaHibernateTagValues() {
        return topiaHibernateTagValues;
    }
}
