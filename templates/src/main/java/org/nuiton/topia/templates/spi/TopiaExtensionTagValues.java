package org.nuiton.topia.templates.spi;

/*-
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.models.extension.tagvalue.TagValueMetadata;
import org.nuiton.eugene.models.extension.tagvalue.TagValueUtil;
import org.nuiton.eugene.models.extension.tagvalue.matcher.EqualsTagValueNameMatcher;
import org.nuiton.eugene.models.extension.tagvalue.provider.DefaultTagValueMetadatasProvider;
import org.nuiton.eugene.models.extension.tagvalue.provider.TagValueMetadatasProvider;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelPackage;

import java.util.List;
import java.util.Set;

@Component(role = TagValueMetadatasProvider.class, hint = "topia-extension")
@AutoService(TagValueMetadatasProvider.class)
public class TopiaExtensionTagValues extends DefaultTagValueMetadatasProvider {

    public TopiaExtensionTagValues() {
        super(Store.values());
    }

    @Override
    public String getDescription() {
        return "Tag values Topia-extension";
    }

    public boolean isSkipModelNavigation(ObjectModelPackage modelPackage, ObjectModelClassifier classifier, ObjectModelAttribute attribute) {
        return TagValueUtil.findBooleanTagValue(Store.skipModelNavigation, modelPackage, classifier, attribute);
    }

    public boolean isGenerateIndex(ObjectModelPackage modelPackage, ObjectModelClassifier classifier, ObjectModelAttribute attribute) {
        return TagValueUtil.findBooleanTagValue(Store.generateIndex, modelPackage, classifier, attribute);
    }

    public Integer getDigits(ObjectModelPackage modelPackage, ObjectModelClassifier classifier, ObjectModelAttribute attribute) {
        String tagValue = TagValueUtil.findDirectTagValue(Store.digits, modelPackage, classifier, attribute);
        return tagValue == null ? null : Integer.valueOf(tagValue);
    }

    public boolean isEntryPoint(ObjectModelClassifier model) {
        return TagValueUtil.findBooleanTagValue(Store.entryPoint, model);
    }

    public String getTopiaEntitySqlModelResourceSuperClassTagValue(ObjectModel model) {
        return TagValueUtil.findTagValue(Store.topiaEntitySqlModelResourceSuperClass, model);
    }

    public String[] getGroupBy(ObjectModelClassifier model) {
        String result = TagValueUtil.findNotEmptyTagValue(Store.groupBy, model);
        return result == null ? null : result.split("\\s*,\\s*");
    }

    public boolean isStandalone(ObjectModel model, ObjectModelPackage aPackage, ObjectModelClassifier classifier) {
        return TagValueUtil.findBooleanTagValue(Store.standalone, model, aPackage, classifier);
    }

    public enum Store implements TagValueMetadata {
        /**
         * To set a type as an entry point.
         * <p>
         * You can use it only on a specific classifier.
         *
         * @see #isEntryPoint(ObjectModelClassifier)
         * @since 4.0
         */
        entryPoint("To set a type as an entry point", Boolean.class, "false", ObjectModelClassifier.class),
        /**
         * To set groupBy for an entry point (comma separated property names).
         * <p>
         * You can use it only on a specific classifier.
         *
         * @see #getGroupBy(ObjectModelClassifier)
         */
        groupBy("To set groupBy for an entry point (comma separated property names)", String.class, null, ObjectModelClassifier.class),
        /**
         * To set a type as an entry point.
         * <p>
         * You can use it only on a specific classifier.
         *
         * @see #isStandalone(ObjectModel, ObjectModelPackage, ObjectModelClassifier)
         * @since 4.0
         */
        standalone("To set a type as standalone (means not in a entry point)", Boolean.class, "false", ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),
        /**
         * To set a type as an entry point.
         * <p>
         * You can use it only on a specific attribute.
         *
         * @see #isSkipModelNavigation(ObjectModelPackage, ObjectModelClassifier, ObjectModelAttribute)
         * @since 4.0
         */
        skipModelNavigation("To skip navigation of an relation.", Boolean.class, "false", ObjectModel.class, ObjectModelPackage.class, ObjectModelAttribute.class),
        /**
         * To set a type as an entry point.
         * <p>
         * You can use it only on a specific attribute.
         *
         * @see #isGenerateIndex(ObjectModelPackage, ObjectModelClassifier, ObjectModelAttribute)
         * @since 4.0
         */
        generateIndex("To generate index on the attribute.", Boolean.class, "false", ObjectModel.class, ObjectModelPackage.class, ObjectModelAttribute.class),
        /**
         * To set number of digits to use on a numeric attribute.
         * <p>
         * You can use it only on a specific attribute.
         *
         * @see #getDigits(ObjectModelPackage, ObjectModelClassifier, ObjectModelAttribute)
         * @since 4.0
         */
        digits("To add digit precision on number attribute.", Integer.class, null, ObjectModel.class, ObjectModelPackage.class, ObjectModelAttribute.class),

        topiaEntitySqlModelResourceSuperClass("Optional TopiaEntitySqlModelResourceSuperClass", String.class, null, ObjectModel.class),
        ;

        private final Set<Class<?>> targets;
        private final Class<?> type;
        private final String description;
        private final String defaultValue;

        Store(String description, Class<?> type, String defaultValue, Class<?>... targets) {
            this.targets = Set.copyOf(List.of(targets));
            this.type = type;
            this.description = description;
            this.defaultValue = defaultValue;
        }

        @Override
        public String getName() {
            return name();
        }

        @Override
        public Set<Class<?>> getTargets() {
            return targets;
        }

        @Override
        public Class<?> getType() {
            return type;
        }

        @Override
        public Class<EqualsTagValueNameMatcher> getMatcherClass() {
            return EqualsTagValueNameMatcher.class;
        }

        @Override
        public String getDescription() {
            return description;
        }

        @Override
        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public boolean isDeprecated() {
            return false;
        }

    }
}

