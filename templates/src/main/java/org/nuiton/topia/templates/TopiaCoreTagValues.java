package org.nuiton.topia.templates;

/*
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.common.collect.ImmutableSet;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.models.extension.tagvalue.TagValueMetadata;
import org.nuiton.eugene.models.extension.tagvalue.TagValueUtil;
import org.nuiton.eugene.models.extension.tagvalue.matcher.EqualsTagValueNameMatcher;
import org.nuiton.eugene.models.extension.tagvalue.provider.DefaultTagValueMetadatasProvider;
import org.nuiton.eugene.models.extension.tagvalue.provider.TagValueMetadatasProvider;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelPackage;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.Set;


/**
 * All extra tag values usable in topia generators.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5
 */
@AutoService(TagValueMetadatasProvider.class)
@Component(role = TagValueMetadatasProvider.class, hint = "topia-core")
public class TopiaCoreTagValues extends DefaultTagValueMetadatasProvider {

    @Override
    public String getDescription() {
        return "topia.core.tagvalues";
    }

    public enum Store implements TagValueMetadata {

        /**
         * Stéréotype pour les objets devant être générées sous forme d'entités
         *
         * @see TopiaTemplateHelper#isEntity(ObjectModelClassifier)
         * @see #isEntity(ObjectModelClassifier, ObjectModelPackage)
         */
        entity("topia.stereotypes.entity", boolean.class, null, ObjectModelPackage.class, ObjectModelClassifier.class),
        /**
         * Stéréotype pour les attributs étant des clés primaires.
         *
         * @see #isPrimaryKey(ObjectModelAttribute)
         * @deprecated since 3.0, use nowhere in ToPIA
         */
        primaryKey("topia.stereotypes.primaryKey", boolean.class, null, ObjectModelAttribute.class),

        /**
         * Tag pour ajouter une annotation à un champ.
         *
         * @see #getAnnotationTagValue(ObjectModelAttribute)
         * @since 2.5
         */
        annotation("topia.core.tagValue.annotation", String.class, null, ObjectModelAttribute.class),

        /**
         * To specify the abstract dao to use.
         * <p>
         * If none given, will use the {@code org.nuiton.topia.persistence.TopiaDAOImpl}.
         * <p>
         * Other value possible is {@code org.nuiton.topia.persistence.TopiaDAOLegacy}
         *
         * @see #getDaoImplementationTagValue(ObjectModel)
         * @since 2.5
         */
        daoImplementation("topia.core.tagValue.daoImplementation", String.class, null, ObjectModel.class),

        /**
         * Tag to specify a super class to use instead of
         * {@link org.nuiton.topia.persistence.internal.AbstractTopiaPersistenceContext}.
         *
         * <strong>Note:</strong> the class must implements
         * {@link org.nuiton.topia.persistence.internal.AbstractTopiaPersistenceContext}.
         *
         * @see org.nuiton.topia.persistence.internal.AbstractTopiaPersistenceContext
         * @see #getPersistenceContextSuperClassTagValue(ObjectModel)
         * @since 3.0
         */
        persistenceContextSuperClass("topia.core.tagValue.persistenceContextSuperClass", String.class, null, ObjectModel.class),

        /**
         * Tag to specify a super class to use instead of
         * {@link org.nuiton.topia.persistence.internal.AbstractTopiaApplicationContext}.
         *
         * <strong>Note:</strong> the class must implement
         * {@link org.nuiton.topia.persistence.internal.AbstractTopiaApplicationContext}.
         *
         * @see org.nuiton.topia.persistence.internal.AbstractTopiaApplicationContext
         * @see #getApplicationContextSuperClassTagValue(ObjectModel)
         * @since 3.0
         */
        applicationContextSuperClass("topia.core.tagValue.applicationContextSuperClass", String.class, null, ObjectModel.class),

        /**
         * Tag to specify a super class to use instead of {@link org.nuiton.topia.persistence.internal.AbstractTopiaDao}.
         *
         * <strong>Note:</strong> the class must implements {@link org.nuiton.topia.persistence.TopiaDao}.
         *
         * @see org.nuiton.topia.persistence.internal.AbstractTopiaDao
         * @see #getDaoSuperClassTagValue(ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 3.0
         */
        daoSuperClass("topia.core.tagValue.daoSuperClass", String.class, null, ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),

        /**
         * Tag to specify a super class to use instead of {@link org.nuiton.topia.persistence.internal.AbstractTopiaEntity}.
         *
         * <strong>Note:</strong> the class must implements {@link TopiaEntity}.
         *
         * @see TopiaEntity
         * @see #getEntitySuperClassTagValue(ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 3.0
         */
        entitySuperClass("topia.core.tagValue.entitySuperClass", String.class, null, ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),

        /**
         * Tag to specify if PropertyChangeListener support should be generated
         *
         * @see #isGeneratePropertyChangeSupport(ObjectModelClass, ObjectModel)
         * @since 3.0
         */
        generatePropertyChangeSupport("topia.core.tagValue.generatePropertyChangeSupport", boolean.class, "false", ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class);


        private final Set<Class<?>> targets;
        private final Class<?> type;
        private final String i18nDescriptionKey;
        private final String defaultValue;

        Store(String i18nDescriptionKey, Class<?> type, String defaultValue, Class<?>... targets) {
            this.targets = ImmutableSet.copyOf(targets);
            this.type = type;
            this.i18nDescriptionKey = i18nDescriptionKey;
            this.defaultValue = defaultValue;
        }

        @Override
        public String getName() {
            return name();
        }

        @Override
        public Set<Class<?>> getTargets() {
            return targets;
        }

        @Override
        public Class<?> getType() {
            return type;
        }

        @Override
        public Class<EqualsTagValueNameMatcher> getMatcherClass() {
            return EqualsTagValueNameMatcher.class;
        }

        @Override
        public String getDescription() {
            return i18nDescriptionKey;
        }

        @Override
        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public boolean isDeprecated() {
            return false;
        }

    }

    public TopiaCoreTagValues() {
        super(Store.values());
    }

    /**
     * Obtain the value of the {@link Store#annotation} tag value on the given attribute.
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#annotation
     * @since 2.5
     */
    public String getAnnotationTagValue(ObjectModelAttribute attribute) {
        return TagValueUtil.findTagValue(Store.annotation, attribute);
    }

    /**
     * Obtains the value of the {@link Store#daoImplementation} tag value on the given model.
     *
     * @param model model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#daoImplementation
     * @since 2.5
     */
    public String getDaoImplementationTagValue(ObjectModel model) {
        return TagValueUtil.findTagValue(Store.daoImplementation, model);
    }

    /**
     * Obtains the value of the {@link Store#persistenceContextSuperClass} tag value on the model.
     *
     * @param model model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#persistenceContextSuperClass
     * @since 3.0
     */
    public String getPersistenceContextSuperClassTagValue(ObjectModel model) {
        return TagValueUtil.findTagValue(Store.persistenceContextSuperClass, model);
    }

    /**
     * Obtains the value of the {@link Store#applicationContextSuperClass} tag value on the model.
     *
     * @param model model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#applicationContextSuperClass
     * @since 3.0
     */
    public String getApplicationContextSuperClassTagValue(ObjectModel model) {
        return TagValueUtil.findTagValue(Store.applicationContextSuperClass, model);
    }

    /**
     * Obtains the value of the {@link Store#daoSuperClass} tag value on the given classifier or on the model.
     *
     * @param model      model to seek
     * @param aPackage   FIXME
     * @param classifier FIXME
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#daoSuperClass
     * @since 3.0
     */
    public String getDaoSuperClassTagValue(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findTagValue(Store.daoSuperClass, classifier, aPackage, model);
    }

    /**
     * Obtains the value of the {@link Store#entitySuperClass} tag value on the given classifier or on the model.
     *
     * @param model      model to seek
     * @param aPackage   FIXME
     * @param classifier FIXME
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#entitySuperClass
     * @since 3.0
     */
    public String getEntitySuperClassTagValue(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findTagValue(Store.entitySuperClass, classifier, aPackage, model);
    }

    public boolean isGeneratePropertyChangeSupport(ObjectModelClass clazz, ObjectModel model) {
        ObjectModelPackage modelPackage = model.getPackage(clazz);
        return TagValueUtil.findBooleanTagValue(Store.generatePropertyChangeSupport, clazz, modelPackage, model);
    }

    /**
     * Check if the given classifier has the {@link Store#entity} stereotype.
     *
     * @param aPackage   FIXME
     * @param classifier classifier to test
     * @return {@code true} if stereotype was found, {@code false otherwise}
     * @see Store#entity
     * @since 2.5
     */
    public boolean isEntity(ObjectModelClassifier classifier, ObjectModelPackage aPackage) {
        boolean hasStereotype = TagValueUtil.findBooleanTagValue(Store.entity, classifier);
        if (!hasStereotype && aPackage != null) {
            hasStereotype = TagValueUtil.findBooleanTagValue(Store.entity, aPackage);
        }
        return hasStereotype;
    }

    /**
     * Check if the given attribute has the {@link Store#primaryKey} stereotype.
     *
     * @param attribute attribute to test
     * @return {@code true} if stereotype was found, {@code false otherwise}
     * @see Store#primaryKey
     * @since 2.5
     * @deprecated since 3.0, not used in ToPIA
     */
    @Deprecated
    public boolean isPrimaryKey(ObjectModelAttribute attribute) {
        return TagValueUtil.findBooleanTagValue(Store.primaryKey, attribute);
    }

}
