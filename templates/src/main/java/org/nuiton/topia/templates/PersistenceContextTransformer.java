package org.nuiton.topia.templates;

/*
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.topia.persistence.TopiaDaoSupplier;
import org.nuiton.topia.persistence.internal.AbstractTopiaPersistenceContext;
import org.nuiton.topia.persistence.internal.AbstractTopiaPersistenceContextConstructorParameter;

import java.util.List;

/**
 * To generate PersistenceHelper
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
@Component(role = Template.class, hint = "org.nuiton.topia.templates.PersistenceContextTransformer")
public class PersistenceContextTransformer extends ObjectModelTransformerToJava {

    protected TopiaTemplateHelper templateHelper;

    protected final TopiaCoreTagValues topiaCoreTagValues;

    public PersistenceContextTransformer() {
        this.topiaCoreTagValues = new TopiaCoreTagValues();
    }

    @Override
    public void transformFromModel(ObjectModel input) {

        if (templateHelper == null) {
            templateHelper = new TopiaTemplateHelper(model);
        }

        String packageName = templateHelper.getPersistenceContextPackage(this, model);

        String daoSupplierName = templateHelper.getDaoSupplierName(model);

        String persistenceContextAbstractName = templateHelper.getPersistenceContextAbstractName(model);

        String persistenceContextConcreteName = templateHelper.getPersistenceContextConcreteName(model);

        boolean generateDaoSupplier =
                !getResourcesHelper().isJavaFileInClassPath(packageName + "." + daoSupplierName);

        boolean generateAbstract =
                !getResourcesHelper().isJavaFileInClassPath(packageName + "." + persistenceContextAbstractName);

        boolean generateConcrete =
                !getResourcesHelper().isJavaFileInClassPath(packageName + "." + persistenceContextConcreteName);

        if (generateDaoSupplier) {

            generateDaoSupplier(packageName,
                                daoSupplierName);
        }

        if (generateAbstract) {

            generateAbstract(packageName,
                             persistenceContextAbstractName,
                             daoSupplierName);
        }

        if (generateConcrete) {

            generateImpl(packageName,
                         persistenceContextAbstractName,
                         persistenceContextConcreteName);
        }

    }

    protected void generateDaoSupplier(String packageName, String className) {

        ObjectModelInterface output = createInterface(className, packageName);

        // detect if there is a contract to set on abstract
        String contractName = templateHelper.getDaoSupplierInterfaceName(model);

        boolean addPersistenceContextContract = getResourcesHelper().isJavaFileInClassPath(packageName + "." + contractName);

        if (addPersistenceContextContract) {
            addInterface(output, packageName + "." + contractName);
        }

        addInterface(output, TopiaDaoSupplier.class);

        List<ObjectModelClass> entityClasses =
                templateHelper.getEntityClasses(model, true);

        for (ObjectModelClass clazz : entityClasses) {

            String daoContractName = templateHelper.getContractDaoName(clazz);
            String daoClazzName = templateHelper.getConcreteDaoName(clazz);

            // specialized getXXXDao method
            addOperation(
                    output,
                    "get" + daoContractName,
                    clazz.getPackageName() + '.' + daoClazzName);
        }
    }

    protected void generateAbstract(String packageName,
                                    String className,
                                    String daoSupplierName) {

        ObjectModelClass output = createAbstractClass(className, packageName);

        // try to find a super class by tag-value
        String superClass = topiaCoreTagValues.getPersistenceContextSuperClassTagValue(model);

        if (superClass == null) {

            // no super-class, use default one
            superClass = AbstractTopiaPersistenceContext.class.getName();
        } else {

            //TODO check that super class instance of TopiaPersistenceContext
        }

        setSuperClass(output, superClass);

        // detect if there is a contract to set on abstract
        String contractName = templateHelper.getPersistenceContextInterfaceName(model);

        boolean addPersistenceContextContract = getResourcesHelper().isJavaFileInClassPath(packageName + "." + contractName);

        if (addPersistenceContextContract) {
            addInterface(output, packageName + "." + contractName);
        }

        addInterface(output, packageName + "." + daoSupplierName);
        addContructor(output, false);

        List<ObjectModelClass> entityClasses =
                templateHelper.getEntityClasses(model, true);

        for (ObjectModelClass clazz : entityClasses) {
            String clazzName = clazz.getName();

            String daoContractName = templateHelper.getContractDaoName(clazz);
            String daoClazzName = templateHelper.getConcreteDaoName(clazz);

            // specialized getXXXDao method
            ObjectModelOperation op = addOperation(
                    output,
                    "get" + daoContractName,
                    clazz.getPackageName() + '.' + daoClazzName);
            addAnnotation(output, op, Override.class);
            addImport(output, clazz);
            setOperationBody(op, ""
/*{
        <%=daoClazzName%> result = getDao(<%=clazzName%>.class, <%=daoClazzName%>.class);
        return result;
    }*/
            );
        }
    }

    protected ObjectModelClass generateImpl(String packageName,
                                            String entityAbstractName,
                                            String entityConcreteName) {

        ObjectModelClass output = createClass(entityConcreteName, packageName);

        setSuperClass(output, entityAbstractName);
        addContructor(output, true);
        return output;
    }

    protected void addContructor(ObjectModelClass output, boolean isPublic) {

        ObjectModelJavaModifier visibility = isPublic ?
                ObjectModelJavaModifier.PUBLIC :
                ObjectModelJavaModifier.PROTECTED;

        ObjectModelOperation constructor = addConstructor(output, visibility);
        addParameter(constructor, AbstractTopiaPersistenceContextConstructorParameter.class, "parameter");
        setOperationBody(constructor, ""
/*{
        super(parameter);
    }*/
        );
    }

}
