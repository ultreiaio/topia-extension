package org.nuiton.topia.templates;

/*
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

import io.ultreia.java4all.lang.Strings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.EugeneCoreTagValues;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.java.extension.ObjectModelAnnotation;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAssociationClass;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.ObjectModelPackage;
import org.nuiton.topia.persistence.TopiaQueryBuilderAddCriteriaOrRunQueryStep;
import org.nuiton.topia.persistence.TopiaQueryBuilderRunQueryWithUniqueResultStep;
import org.nuiton.topia.persistence.internal.support.HibernateTopiaJpaSupport;
import org.nuiton.topia.persistence.support.TopiaHibernateSupport;
import org.nuiton.topia.templates.spi.EntityClassContext;
import org.nuiton.topia.templates.spi.EntityToDtoMapping;
import org.nuiton.topia.templates.spi.TopiaTemplateHelperExtension;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.ServiceLoader;
import java.util.Set;

/**
 * To generate all {@code DAO} related classes for a given entity.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5.4
 */
@Component(role = Template.class, hint = "org.nuiton.topia.templates.TopiaEntityDaoTransformer")
public class TopiaEntityDaoTransformer extends EntityDaoTransformer {
    private static final Logger log = LogManager.getLogger(TopiaEntityDaoTransformer.class);
    protected String[] relativeNameExcludes;
    protected boolean useRelativeName;

    protected TopiaTemplateHelperExtension templateHelper;
    protected EntityClassContext classContext;
    boolean useDtoMapping;

    public static void removeOperation(ObjectModelClass result, String... names) {
        List<String> methodNames = List.of(names);
        result.getOperations().removeIf(o -> methodNames.contains(o.getName()));
    }

    @Override
    public void transformFromModel(ObjectModel model) {

        templateHelper = new TopiaTemplateHelperExtension(model);
        for (EntityToDtoMapping ignored : ServiceLoader.load(EntityToDtoMapping.class, getClassLoader())) {
            useDtoMapping = true;
            break;
        }

        EugeneCoreTagValues coreTagValues = new EugeneCoreTagValues();
        useRelativeName = coreTagValues.isUseRelativeName(model);
        Set<String> relativeNameExcludes = coreTagValues.getRelativeNameExcludes(model);
        this.relativeNameExcludes = relativeNameExcludes == null ? new String[0] : relativeNameExcludes.toArray(new String[0]);

        super.transformFromModel(model);
        List<ObjectModelClass> allEntities = templateHelper.getEntityClasses(model, true);
        for (ObjectModelClass entity : allEntities) {
            if (templateHelper.isAbstract(entity)) {
                allEntitiesFqn.remove(entity.getQualifiedName());
            }
        }
    }

    @Override
    public void transformFromClass(ObjectModelClass clazz) {
        if (templateHelper.isAbstract(clazz)) {
            return;
        }
        classContext = new EntityClassContext(templateHelper, model, model.getPackage(clazz), clazz);
        super.transformFromClass(clazz);
    }

    @Override
    protected void generateGeneratedDao(ObjectModelClass clazz, String clazzName, String clazzFQN) {
        String outputName = templateHelper.getGeneratedDaoName(clazz) + "<E extends " + clazzName + '>';
        ObjectModelClass daoAbstractClass = createAbstractClass(outputName, clazz.getPackageName());
        addImport(daoAbstractClass, clazz.getQualifiedName());

        // super class

        String superClassName = topiaCoreTagValues.getDaoSuperClassTagValue(clazz, getPackage(clazz), model);
        if (superClassName == null) {
            superClassName = templateHelper.getParentDaoFqn(this, model);
            addImport(daoAbstractClass, superClassName);
        }
        superClassName += "<E>";

        setSuperClass(daoAbstractClass, superClassName);

        String prefix = getConstantPrefix(clazz);
        setConstantPrefix(prefix);

        // imports

        addImport(daoAbstractClass, List.class);
        addImport(daoAbstractClass, TopiaQueryBuilderAddCriteriaOrRunQueryStep.class);

        ObjectModelOperation op;

        // getEntityClass

        op = addOperation(daoAbstractClass,
                          "getEntityClass",
                          "Class<E>",
                          ObjectModelJavaModifier.PUBLIC);
        addAnnotation(daoAbstractClass, op, Override.class);
        ObjectModelAnnotation annotation = addAnnotation(daoAbstractClass, op, SuppressWarnings.class);
        addAnnotationParameter(daoAbstractClass, annotation, "value", "unchecked");
        setOperationBody(op, ""
/*{
        return (Class<E>) <%=clazzName%>.class;
    }*/
        );

        // getTopiaEntityEnum
        String literalName = templateHelper.getEntityEnumLiteralName(clazz);
        addImport(daoAbstractClass, entityEnumPackage);
        op = addOperation(daoAbstractClass,
                          "getTopiaEntityEnum",
                          entityEnumName,
                          ObjectModelJavaModifier.PUBLIC);
        addAnnotation(daoAbstractClass, op, Override.class);
        setOperationBody(op, ""
/*{
        return <%=entityEnumName%>.<%=literalName%>;
    }*/
        );
        op = addOperation(daoAbstractClass,
                          "newInstance0",
                          "E",
                          ObjectModelJavaModifier.PUBLIC);
        addAnnotation(daoAbstractClass, op, Override.class);
        setOperationBody(op, ""
/*{
        return getEntityClass().cast(new <%=clazzName%>Impl());
    }*/
        );

        generateDelete(clazz, daoAbstractClass);
        generateNaturalId(daoAbstractClass, clazz);
        generateNotNull(daoAbstractClass, clazz);

        for (ObjectModelAttribute attr : classContext.getAttributes()) {
            if (!attr.isNavigable()) {
                continue;
            }
            if (!GeneratorUtil.isNMultiplicity(attr)) {
                generateNoNMultiplicity(clazzName, daoAbstractClass, attr, false);
            } else {
                generateNMultiplicity(clazzName, daoAbstractClass, attr);
            }
        }

        if (clazz instanceof ObjectModelAssociationClass) {
            ObjectModelAssociationClass assocClass = (ObjectModelAssociationClass) clazz;
            for (ObjectModelAttribute attr : assocClass.getParticipantsAttributes()) {
                if (attr != null) {
                    if (!GeneratorUtil.isNMultiplicity(attr)) {
                        generateNoNMultiplicity(clazzName, daoAbstractClass, attr, true);
                    } else {
                        generateNMultiplicity(clazzName, daoAbstractClass, attr);
                    }
                }
            }
        }
    }

    @Override
    protected void generateNMultiplicity(String clazzName, ObjectModelClass result, ObjectModelAttribute attr) {
        super.generateNMultiplicity(clazzName, result, attr);
        String attrName = attr.getName();
        removeOperation(result,
                        getJavaBeanMethodName("findContains", attrName),
                        getJavaBeanMethodName("findAllContains", attrName));
    }

    @Override
    protected void generateNoNMultiplicity(String clazzName, ObjectModelClass result, ObjectModelAttribute attr, boolean isAssoc) {
        super.generateNoNMultiplicity(clazzName, result, attr, isAssoc);
        String attrName = attr.getName();
        removeOperation(result,
                        getJavaBeanMethodName("findBy", attrName),
                        getJavaBeanMethodName("findAllBy", attrName));
    }

    @Override
    protected void generateDelete(ObjectModelClass clazz, ObjectModelClass result) {

        StringBuilder body = new StringBuilder();
        String modelName = Strings.capitalize(model.getName());
        String providerFQN = getDefaultPackageName() + '.' + modelName + "DAOHelper.getImplementationClass";
        ObjectModelPackage aPackage = getPackage(clazz);

        body.append(""
/*{
        if ( ! entity.isPersisted()) {
            throw new IllegalArgumentException("entity " + entity  + " is not persisted, you can't delete it");
        }
}*/
        );

        boolean hibernateSupportGenerated = false;

        for (ObjectModelAttribute attr : classContext.getAttributes()) {

            String attrType = useRelativeName ? attr.getType() : GeneratorUtil.getSimpleName(attr.getType());

            String reverseAttrName = attr.getReverseAttributeName();
            ObjectModelAttribute reverse = attr.getReverseAttribute();
            if (attr.hasAssociationClass() ||
                    reverse == null || !reverse.isNavigable()) {

                // never treate a non reverse and navigable attribute
                // never treate an association class attribute
                continue;
            }

            // at this point we are sure to have a attribute which is
            // - reverse
            // - navigable
            // - not from an association class
            if (!allEntitiesFqn.contains(attr.getType())) {

                // this attribute is not from an entity, don't treate it
                if (log.isDebugEnabled()) {
                    log.debug("[" + result.getName() + "] Skip attribute [" +
                                      attr.getName() + "] with type " + attr.getType());
                }
                continue;
            }

            // At this point, the attribute type is a entity
            if (GeneratorUtil.isNMultiplicity(attr) &&
                    GeneratorUtil.isNMultiplicity(reverse)) {
                // On doit absolument supprimer pour les relations many-to-many
                // le this de la collection de l'autre cote

                String attrDBName = attr.isNavigable() ? templateHelper.getDbName(attr) : templateHelper.getReverseDbName(attr.getReverseAttribute());
                String attrClassifierDBNameSchemaName = templateHelper.getSchema(attr.getClassifier());
                String attrClassifierDBName = templateHelper.getDbName(attr.getClassifier());
                String attrJoinTableName = attr.isNavigable() ? templateHelper.getManyToManyTableName(attr) : templateHelper.getManyToManyTableName(attr.getReverseAttribute());
                if (attrClassifierDBNameSchemaName != null) {
                    attrClassifierDBName = attrClassifierDBNameSchemaName + "." + attrClassifierDBName;
                    attrJoinTableName = attrClassifierDBNameSchemaName + "." + attrJoinTableName;
                }
                String attrReverseDBName = templateHelper.getReverseDbName(attr);

                if (!hibernateSupportGenerated) {
                    hibernateSupportGenerated = true;
                    addImport(result, TopiaHibernateSupport.class);
                    addImport(result, HibernateTopiaJpaSupport.class);
                    body.append(""
/*{
        TopiaHibernateSupport hibernateSupport = ((HibernateTopiaJpaSupport) topiaJpaSupport).getHibernateSupport();
}*/
                    );
                }

                String literalName = templateHelper.getEntityEnumLiteralName(attr.getClassifier());
                String removeName = getJavaBeanMethodName("remove", reverseAttrName);
                body.append(""
/*{
        {
            String sql = "SELECT main.* " +
                    " FROM <%=attrClassifierDBName%> main, <%=attrJoinTableName%> secondary " +
                    " WHERE main.topiaId=secondary.<%=attrDBName%> " +
                    " AND secondary.<%=attrReverseDBName%>='" + entity.getTopiaId() + "'";
            List<<%=attrType%>> list = hibernateSupport.getHibernateSession()
                    .createNativeQuery(sql)
                    .addEntity("main", <%=entityEnumName%>.<%=literalName%>.getImplementation())
                    .list();

            for (<%=attrType%> item : list) {
                item.<%=removeName%>(entity);
            }
        }
}*/
                );
            } else if (!GeneratorUtil.isNMultiplicity(reverse)) {
                // On doit mettre a null les attributs qui ont cet objet sur les
                // autres entites en one-to-*
                // TODO peut-etre qu'hibernate est capable de faire ca tout seul ?
                // THIMEL: J'ai remplacé reverse.getName() par reverseAttrName sans certitude

                String attrSimpleType = GeneratorUtil.getClassNameFromQualifiedName(attrType);
                if (useRelativeName) {
                    attrSimpleType = attrType;
                } else {
                    addImport(result, attrType);
                    addImport(result, attr.getType() + "TopiaDao"); // AThimel 30/10/13 Not using attrType because we need FQN // Can use  TopiaTemplateHelper.getConcreteDaoFqn(...) ?

                }
                // XXX brendan 04/10/13 do not hard code concrete dao name
                String attrConcreteDaoClassName = attrSimpleType + "TopiaDao";

                String getName = getJavaBeanMethodName("get", reverseAttrName);
                String setName = getJavaBeanMethodName("set", reverseAttrName);

                body.append(""
                        /*{
        {
            <%=attrConcreteDaoClassName%> dao = topiaDaoSupplier
                    .getDao(<%=attrSimpleType%>.class, <%=attrConcreteDaoClassName%>.class);
            List<<%=attrSimpleType%>> list = dao
                    .forProperties(<%=attrSimpleType%>.<%=getConstantName(reverseAttrName)%>, entity)
                    .findAll();
            for (<%=attrSimpleType%> item : list) {

                // sletellier : Set null only if target is concerned by deletion
                if (entity.equals(item.<%=getName%>())) {
                    item.<%=setName%>(null);
                }
            }*/
                );
                if (attr.isAggregate()) {
                    body.append(""
/*{
            topiaDaoSupplier.getDao(<%=attrSimpleType%>.class).delete(item);
}*/
                    );
                }
                body.append(""
/*{
            }
        }
}*/
                );
            }
        }
        if (body.length() > 0) {
            // something specific was done, need to generate the method
            ObjectModelOperation op;
            op = addOperation(result, "delete", "void", ObjectModelJavaModifier.PUBLIC);
            addAnnotation(result, op, Override.class);
            addParameter(op, "E", "entity");
            body.append(""
/*{
        super.delete(entity);
    }*/
            );
            setOperationBody(op, body.toString());
        }
    }

    @Override
    protected void generateFindUsages(ObjectModelClass clazz, ObjectModelClass result, Set<ObjectModelClass> usagesForClass) {
    }

    @Override
    protected void generateCompositeOperation(ObjectModelClass outputAbstract, ObjectModelClass input) {
    }

    @Override
    protected void generateAggregateOperation(ObjectModelClass outputAbstract, ObjectModelClass input) {
    }

    private void generateNaturalId(ObjectModelClass result, ObjectModelClass clazz) {
        Set<ObjectModelAttribute> props = templateHelper.getNaturalIdAttributes(clazz);

        if (!props.isEmpty()) {

            ObjectModelOperation findByNaturalId = addOperation(result, "findByNaturalId", "E", ObjectModelJavaModifier.PUBLIC);
            addAnnotation(result, findByNaturalId, Deprecated.class);

            ObjectModelOperation existByNaturalId = addOperation(result, "existByNaturalId", "boolean", ObjectModelJavaModifier.PUBLIC);
            addAnnotation(result, existByNaturalId, Deprecated.class);

            ObjectModelOperation createByNaturalId = addOperation(result, "createByNaturalId", "E", ObjectModelJavaModifier.PUBLIC);

            addImport(result, TopiaQueryBuilderRunQueryWithUniqueResultStep.class);

            ObjectModelOperation forNaturalId = addOperation(result, "forNaturalId", "TopiaQueryBuilderRunQueryWithUniqueResultStep<E>", ObjectModelJavaModifier.PUBLIC);

            Set<String> propertiesAndArguments = new LinkedHashSet<>();
            Set<String> argumentsOnly = new LinkedHashSet<>();
            String clazzName = clazz.getName();

            for (ObjectModelAttribute attr : props) {

                String propName = attr.getName();
                String type = attr.getType();

                addParameter(findByNaturalId, type, propName);
                addParameter(existByNaturalId, type, propName);
                addParameter(createByNaturalId, type, propName);
                addParameter(forNaturalId, type, propName);

                String property = clazzName + '.' + getConstantName(propName) + ", " + propName;
                propertiesAndArguments.add(property);
                argumentsOnly.add(propName);
            }
            String arguments = String.join(", ", propertiesAndArguments);
            String naturalIdArguments = String.join(", ", argumentsOnly);
            setOperationBody(findByNaturalId, ""
/*{
        return forNaturalId(<%=naturalIdArguments%>).findUnique();
    }*/
            );
            setOperationBody(existByNaturalId, ""
/*{
        return forNaturalId(<%=naturalIdArguments%>).exists();
    }*/
            );
            setOperationBody(createByNaturalId, ""
/*{
        return create(<%=arguments%>);
    }*/
            );
            setOperationBody(forNaturalId, ""
/*{
        return forProperties(<%=arguments%>);
    }*/
            );
        }
    }

}
