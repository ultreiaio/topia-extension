package org.nuiton.topia.templates;

/*-
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.EugeneCoreTagValues;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.topia.persistence.TopiaDaoSupplier;
import org.nuiton.topia.persistence.internal.AbstractTopiaPersistenceContext;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

/**
 * Created by tchemit on 16/12/2018.
 *
 * To generate PersistenceHelper
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
@Component(role = Template.class, hint = "org.nuiton.topia.templates.TopiaPersistenceContextTransformer")
public class TopiaPersistenceContextTransformer extends PersistenceContextTransformer {

    private String[] relativeNameExcludes;
    private boolean useRelativeName;
    private List<ObjectModelClass> entityClasses;

    @Override
    public void transformFromModel(ObjectModel input) {

        templateHelper = new TopiaTemplateHelper(model);
        EugeneCoreTagValues coreTagValues = new EugeneCoreTagValues();
        useRelativeName = coreTagValues.isUseRelativeName(model);
        Set<String> relativeNameExcludesSet = coreTagValues.getRelativeNameExcludes(model);
        relativeNameExcludes = relativeNameExcludesSet==null?new String[0]: relativeNameExcludesSet.toArray(new String[0]);

        entityClasses = templateHelper.getEntityClasses(model, true).stream().filter(t->!t.isAbstract() ||t.isStatic()).collect(Collectors.toList());

        super.transformFromModel(input);
    }

    @Override
    protected void generateDaoSupplier(String packageName, String className) {

        ObjectModelInterface output = createInterface(className, packageName);

        // detect if there is a contract to set on abstract
        String contractName = templateHelper.getDaoSupplierInterfaceName(model);

        boolean addPersistenceContextContract = getResourcesHelper().isJavaFileInClassPath(packageName + "." + contractName);

        if (addPersistenceContextContract) {
            addInterface(output, packageName + "." + contractName);
        }

        addInterface(output, TopiaDaoSupplier.class);

        for (ObjectModelClass clazz : entityClasses) {
            String aPackageName = model.getPackage(clazz).getName();
            String daoContractName = templateHelper.getContractDaoName(clazz);
            String daoClazzName = templateHelper.getConcreteDaoName(clazz);

            String methodName = getMethodName(useRelativeName, relativeNameExcludes, "get", aPackageName, daoContractName);

            if (useRelativeName) {
                daoClazzName = aPackageName + "." + daoClazzName;
            } else {
                addImport(output, clazz);
                addImport(output, aPackageName  + '.' + daoClazzName);
            }

            // specialized getXXXDao method
            addOperation(
                    output,
                    methodName,
                    daoClazzName);
        }
    }

    protected void generateAbstract(String packageName,
                                    String className,
                                    String daoSupplierName) {

        ObjectModelClass output = createAbstractClass(className, packageName);

        // try to find a super class by tag-value
        String superClass = topiaCoreTagValues.getPersistenceContextSuperClassTagValue(model);

        if (superClass == null) {

            // no super-class, use default one
            superClass = AbstractTopiaPersistenceContext.class.getName();
        } else {

            //TODO check that super class instance of TopiaPersistenceContext
        }

        setSuperClass(output, superClass);

        // detect if there is a contract to set on abstract
        String contractName = templateHelper.getPersistenceContextInterfaceName(model);

        boolean addPersistenceContextContract = getResourcesHelper().isJavaFileInClassPath(packageName + "." + contractName);

        if (addPersistenceContextContract) {
            addInterface(output, packageName + "." + contractName);
        }

        addInterface(output, packageName + "." + daoSupplierName);
        addContructor(output, false);

        for (ObjectModelClass clazz : entityClasses) {
            String aPackageName = getPackage(clazz).getName();
            String clazzName = clazz.getName();

            String daoContractName = templateHelper.getContractDaoName(clazz);
            String daoClazzName = templateHelper.getConcreteDaoName(clazz);

            String methodName = getMethodName(useRelativeName, relativeNameExcludes, "get", aPackageName, daoContractName);

            if (useRelativeName) {
                clazzName = aPackageName + "." + clazzName;
                daoClazzName = aPackageName + "." + daoClazzName;
            } else {
                addImport(output, clazz);
                addImport(output, aPackageName + "." + daoClazzName);
            }

            // specialized getXXXDao method
            ObjectModelOperation op = addOperation(output, methodName, daoClazzName);
            addAnnotation(output, op, Override.class);

            setOperationBody(op, ""/*{
        return getDao(<%=clazzName%>.class, <%=daoClazzName%>.class);
    }*/
            );
        }
    }
}
