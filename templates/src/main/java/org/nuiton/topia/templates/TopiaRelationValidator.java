package org.nuiton.topia.templates;

/*
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.validator.ObjectModelValidator;

/**
 * Validateur pour les relations du modèle.
 * Vérifie que :
 * <ul>
 *   <li>Toutes les relations ont au moins une navigabilité</li>
 *   <li>Une relation 1-n unidirectionnelle pointant sur une classe ayant des
 *   sous-classes dans le modèle est incompatibe avec Hibernate</li>
 *   <li>Toutes les relations ont des reverseAttribute</li>
 * </ul>
 *
 * Created: 31 mars 2006
 *
 * @author Arnaud Thimel (Code Lutin)
 */
public class TopiaRelationValidator extends ObjectModelValidator {

    protected TopiaTemplateHelper templateHelper;
    
    public TopiaRelationValidator(ObjectModel model) {
        super(model);
        templateHelper = new TopiaTemplateHelper(model);
    }

    /* (non-Javadoc)
     * @see org.nuiton.eugene.models.object.validator.ObjectModelValidator#validateAttribute(org.nuiton.eugene.models.object.ObjectModelAttribute)
     */
    @Override
    protected boolean validateAttribute(ObjectModelAttribute attr) {
        boolean isValid = true;
        ObjectModelAttribute reverse = attr.getReverseAttribute();

        /* Relation navigabilité */
        //Pour ne pas avoir de doublons, on ne vérifie que sur le premier
        //attribut par ordre alphabétique
        if (GeneratorUtil.isFirstAttribute(attr)) {
            if (!attr.isNavigable() && !reverse.isNavigable()) {
                addError(attr, "La relation entre " + "\"" + reverse.getType()
                        + "\"[" + attr.getName() + "] et " + "\""
                        + attr.getType() + "\"[" + reverse.getName() + "] "
                        + "n'est navigable dans aucun sens");
                isValid = false;
            }
        }

        /* Relation héritage */
        if (templateHelper.hasUnidirectionalRelationOnAbstractType(attr, model)) {
            isValid = false;
            addError(
                    attr,
                    "La relation entre "
                            + "\""
                            + reverse.getType()
                            + "\"["
                            + attr.getName()
                            + "] et "
                            + "\""
                            + attr.getType()
                            + "\"["
                            + reverse.getName()
                            + "] "
                            + "n'est navigable que dans un sens et "
                            + "la classe \""
                            + attr.getType()
                            + "\" a des sous-classes. "
                            + "Des accesseurs doivent donc etre generes pour Hibernate.");
        }

        /* Pas d'inverse */
        if (reverse == null && model.hasClass(attr.getType())) {
            isValid = false;
            addError(attr, "Cet attribut n'a pas d'inverse.");
        }

        return isValid;
    }

} //TopiaRelationValidator
