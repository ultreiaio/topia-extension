package org.nuiton.topia.templates.hibernate;

/*-
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.models.object.ObjectModelAttribute;

/**
 * Created on 19/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public enum RelationType {
    MANY_TO_MANY,
    MANY_TO_MANY_REVERSE,
    ONE_TO_MANY,
    ONE_TO_ONE,
    ONE_TO_ONE_REVERSE,
    MANY_TO_ONE;

    public static RelationType valueOf(ObjectModelAttribute attribute) {
        boolean nMultiplicity = GeneratorUtil.isNMultiplicity(attribute);
        boolean nMultiplicityReverse = GeneratorUtil.isNMultiplicity(attribute.getReverseMaxMultiplicity());
        if (nMultiplicity && nMultiplicityReverse) {
            return MANY_TO_MANY;
        }
        if (nMultiplicity) {
            return ONE_TO_MANY;
        }
        if (nMultiplicityReverse) {
            return MANY_TO_ONE;
        }
        return ONE_TO_ONE;
    }
}
