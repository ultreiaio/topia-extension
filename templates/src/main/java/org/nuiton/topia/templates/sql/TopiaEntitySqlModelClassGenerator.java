package org.nuiton.topia.templates.sql;

/*-
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.topia.service.sql.TopiaEntitySqlModelResource;
import org.nuiton.topia.service.sql.internal.TopiaEntitySqlModelResourceImpl;
import org.nuiton.topia.templates.spi.TopiaTemplateHelperExtension;

/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

/**
 * Created on 24/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.27
 */
@Component(role = Template.class, hint = "org.nuiton.topia.templates.sql.TopiaEntitySqlModelClassGenerator")
public class TopiaEntitySqlModelClassGenerator extends ObjectModelTransformerToJava {

    @Override
    public void transformFromModel(ObjectModel model) {
        TopiaTemplateHelperExtension templateHelper = new TopiaTemplateHelperExtension(model);
        String packageName = templateHelper.getApplicationContextPackage(this, model);
        String superClass = templateHelper.extensionTagValues().getTopiaEntitySqlModelResourceSuperClassTagValue(model);
        String modelName = model.getName();
        String className = modelName + TopiaEntitySqlModelResource.class.getSimpleName();

        ObjectModelClass output = createClass(className, packageName);
        setSuperClass(output, superClass == null ? TopiaEntitySqlModelResourceImpl.class.getName() : superClass);
        addStaticFactory(output);
        ObjectModelOperation constructor = addConstructor(output, ObjectModelJavaModifier.PRIVATE);
        setOperationBody(constructor, ""/*{
        super("<%=modelName%>", getGsonBuilder().create());
    }*/);
    }

    protected void addStaticFactory(ObjectModelClass aClass) {
        String className = aClass.getQualifiedName();
        addAttribute(aClass, "INSTANCE", className, null, ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.STATIC);
        ObjectModelOperation get = addOperation(aClass, "get", className, ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        setOperationBody(get, ""/*{
        return INSTANCE == null ? INSTANCE = new <%=className%>() : INSTANCE;
    }*/);
    }
}
