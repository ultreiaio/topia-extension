package org.nuiton.topia.templates.sql.plan;

/*-
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.service.sql.model.AbstractTopiaEntitySqlTable;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlAssociationTable;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlDescriptor;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlDescriptors;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSelector;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSimpleAssociationTable;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlTable;

import java.util.List;
import java.util.Set;

/**
 * Created on 28/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.67
 */
public class TopiaEntitySqlDeletePlanBuilderForEntryPoint extends TopiaEntitySqlDeletePlanBuilder {

    public TopiaEntitySqlDeletePlanBuilderForEntryPoint(TopiaEntitySqlDescriptors entityDescriptor) {
        super(entityDescriptor);
    }

    @Override
    protected void startConsumeTable(TopiaEntitySqlDescriptor descriptor) {
        TopiaEntitySqlTable table = descriptor.getTable();
        List<TopiaEntitySqlAssociationTable> associations = descriptor.getAssociations();
        List<TopiaEntitySqlSimpleAssociationTable> simpleAssociations = descriptor.getSimpleAssociations();
        int selectorIndex = 0;
        for (TopiaEntitySqlSelector selector : table.getSelectors()) {
            addTableDeleteSql(table, selector, descriptor.isNoPath());
            if (associations != null) {
                for (TopiaEntitySqlAssociationTable association : associations) {
                    startConsumeAssociationTable(selector, descriptor, association, selectorIndex);
                }
            }
            if (simpleAssociations != null) {
                for (TopiaEntitySqlSimpleAssociationTable association : simpleAssociations) {
                    startConsumeAssociationTable(selector, descriptor, association, selectorIndex);
                }
            }
            selectorIndex++;
        }
    }

    protected void startConsumeAssociationTable(TopiaEntitySqlSelector selector, TopiaEntitySqlDescriptor descriptor, AbstractTopiaEntitySqlTable table, int selectorIndex) {
        addAssociationTask(table.getSchemaAndTableName(), SqlHelper.newDeleteStatementSql(descriptor, table, selector, table.getSelectors().get(selectorIndex)));
    }
}
