package org.nuiton.topia.templates;

/*
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.AbstractMetaTransformer;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.java.BeanTransformer;
import org.nuiton.eugene.java.JavaEnumerationTransformer;
import org.nuiton.eugene.java.JavaInterfaceTransformer;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.validator.AttributeNamesValidator;
import org.nuiton.eugene.models.object.validator.ClassNamesValidator;
import org.nuiton.eugene.models.object.validator.ObjectModelValidator;

import java.util.List;

/**
 * Created: 20 déc. 2009
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.3.0
 */
@Component(role = Template.class, hint = "org.nuiton.topia.templates.TopiaMetaTransformer")
public class TopiaMetaTransformer extends AbstractMetaTransformer<ObjectModel> {
    private static final Logger log = LogManager.getLogger(TopiaMetaTransformer.class);
    protected static final ImmutableSet<String> FORBIDDEN_ATTRIBUTE_NAMES =
            ImmutableSet.of("analyze", "next", "value", "values", "begin", "end", "authorization",
                    "order", "user", "when");

    protected static final ImmutableSet<String> FORBIDDEN_CLASS_NAMES =
            ImmutableSet.of("constraint", "user");

    public TopiaMetaTransformer() {

        setTemplateTypes(
                JavaInterfaceTransformer.class,
                BeanTransformer.class,
                JavaEnumerationTransformer.class,
                EntityTransformer.class,
                EntityHibernateMappingGenerator.class,
                EntityDaoTransformer.class,
                EntityEnumTransformer.class,
                ApplicationContextTransformer.class,
                PersistenceContextTransformer.class
        );

    }

    protected boolean validateModel(ObjectModel model) {

        boolean validationSuccess = true;

        for (ObjectModelValidator validator : getValidators(model)) {
            if ( ! validator.validate()) {
                for (String error : validator.getErrors()) {
                    if (log.isWarnEnabled()) {
                        log.warn("[VALIDATION] " + error);
                    }
                }
                // TODO brendan 29/08/14 uncomment line below to resolve #3487
                // validationSuccess = false;
            }
        }

        TopiaTemplateHelper templateHelper = new TopiaTemplateHelper(model);

        // test before all if there is some entities to generate
        List<ObjectModelClass> classes = templateHelper.getEntityClasses(model, true);

        if (classes.isEmpty()) {
            // no entity to generate, can stop safely
            if (log.isWarnEnabled()) {
                log.warn("No entity to generate, " + getClass().getName() + " is skipped");
            }
            validationSuccess = false;
        }

        return validationSuccess;
    }

    protected ImmutableSet<ObjectModelValidator> getValidators(ObjectModel model) {

        AttributeNamesValidator attributeNamesValidator = new AttributeNamesValidator(model);
        for (String sqlKeyword : FORBIDDEN_ATTRIBUTE_NAMES) {
            attributeNamesValidator.addNameAndReason(
                    sqlKeyword, "Le nom d'attribut \"" + sqlKeyword + "\" est incompatible avec certains SGBD");
        }

        ClassNamesValidator classNamesValidator = new ClassNamesValidator(model);
        for (String sqlKeyword : FORBIDDEN_CLASS_NAMES) {
            classNamesValidator.addNameAndReason(
                    sqlKeyword, "Le nom de classe \"" + sqlKeyword + "\" est incompatible avec certains SGBD");
        }

        TopiaJavaValidator topiaJavaValidator = new TopiaJavaValidator(model);

        TopiaRelationValidator topiaRelationValidator = new TopiaRelationValidator(model);

        return ImmutableSet.of(attributeNamesValidator, classNamesValidator, topiaJavaValidator, topiaRelationValidator);

    }

}
