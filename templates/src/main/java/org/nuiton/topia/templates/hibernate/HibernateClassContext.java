package org.nuiton.topia.templates.hibernate;

/*-
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.eugene.EugeneCoreTagValues;
import org.nuiton.eugene.models.extension.tagvalue.TagValueMetadata;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelPackage;
import org.nuiton.topia.service.sql.metadata.TopiaMetadataEntity;
import org.nuiton.topia.service.sql.metadata.TopiaMetadataModel;
import org.nuiton.topia.service.sql.metadata.TopiaMetadataOneToOneComposition;
import org.nuiton.topia.templates.TopiaHibernateTagValues;
import org.nuiton.topia.templates.spi.TopiaTemplateHelperExtension;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created on 29/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.40
 */
public class HibernateClassContext {

    private final TopiaTemplateHelperExtension templateHelper;
    private final TopiaHibernateTagValues topiaHibernateTagValues;
    private final ObjectModel model;
    private final ObjectModelPackage aPackage;
    private final TopiaMetadataEntity metadataEntity;
    private final TopiaMetadataOneToOneComposition oneToOneComposition;
    private final TopiaMetadataModel metadataModel;
    private final ObjectModelClass input;
    private final boolean generateForeignKeyNames;
    private final String tableName;
    private final String schema;
    private final ObjectModelAttribute reverseOneToOneAttribute;
    // la liste des attributs faisant parti de la clef metier
    private final List<ObjectModelAttribute> naturalAttributes = new ArrayList<>();
    // la liste des autres attributs
    private final List<ObjectModelAttribute> noneNaturalAttributes = new ArrayList<>();
    private final boolean mutable;
    private final Map<String, String> optionalAttributesMap = new HashMap<>();
    private final Map<String, String> uniqueKeys;
    private final Map<String, String> indexes;

    HibernateClassContext(TopiaTemplateHelperExtension templateHelper,
                          TopiaHibernateTagValues topiaHibernateTagValues,
                          ObjectModel model,
                          TopiaMetadataModel metadataModel,
                          ObjectModelPackage aPackage,
                          ObjectModelClass input) {
        this.templateHelper = templateHelper;
        this.topiaHibernateTagValues = topiaHibernateTagValues;
        this.model = model;
        this.metadataModel = metadataModel;
        this.input = Objects.requireNonNull(input);
        this.aPackage = aPackage;
        this.generateForeignKeyNames = topiaHibernateTagValues.isGenerateForeignKeyNames(input, model);
        this.tableName = templateHelper.getDbName(input);
        this.schema = templateHelper.getSchema(input);
        String literalName = schema + "_" + input.getName();
        metadataEntity = metadataModel.getEntity(literalName);
        Set<TopiaMetadataOneToOneComposition> oneToOneCompositions = metadataModel.getReverseOneToOneAssociations(literalName);
        if (oneToOneCompositions != null && !oneToOneCompositions.isEmpty()) {
            oneToOneComposition = oneToOneCompositions.iterator().next();
            String oneToOneAttributeName = oneToOneComposition.getTargetPropertyName();

            ObjectModelAttribute reverseOneToOneAttribute = null;
            for (ObjectModelAttribute attribute : input.getAttributes()) {
                if (!attribute.isNavigable()) {
                    reverseOneToOneAttribute = attribute.getReverseAttribute();
                    if (oneToOneAttributeName == null || reverseOneToOneAttribute == null || !Objects.equals(oneToOneAttributeName, reverseOneToOneAttribute.getName())) {
                        continue;
                    }
                    break;
                }
            }
            this.reverseOneToOneAttribute = Objects.requireNonNull(reverseOneToOneAttribute);
        } else {
            oneToOneComposition = null;
            this.reverseOneToOneAttribute = null;
        }

        List<ObjectModelClass> inputs = new LinkedList<>();
        ObjectModelClass current = input;
        while (current != null && templateHelper.isEntity(current)) {
            inputs.add(current);
            Collection<ObjectModelClass> superclasses = current.getSuperclasses();
            if (superclasses.size() > 0) {
                current = superclasses.iterator().next();
            } else {
                current = null;
            }
        }
        Collections.reverse(inputs);
        inputs.forEach(this::detectAttributes);
        if (!naturalAttributes.isEmpty()) {
            // generation de la clef metier
            mutable = topiaHibernateTagValues.getNaturalIdMutableTagValue(input);
        } else {
            mutable = false;
        }

        String clazzDOType = templateHelper.getDOType(input, model);
        String clazzFQN = input.getQualifiedName();

        if (isUseSchema()) {
            generateFromTagValue(optionalAttributesMap, EntityHibernateMappingTransformer.HIBERNATE_ATTRIBUTE_SCHEMA, getSchema());
        }

        //On précise au proxy de quelle interface hérite l'objet

        String proxyTagValue = topiaHibernateTagValues.getProxyInterfaceTagValue(input, aPackage, model);
        if (StringUtils.isEmpty(proxyTagValue) || !proxyTagValue.equals("none")) {
            generateFromTagValue(optionalAttributesMap, EntityHibernateMappingTransformer.HIBERNATE_ATTRIBUTE_PROXY, clazzFQN);
        }
        if (!input.isAbstract()) {
            generateFromTagValue(optionalAttributesMap, EntityHibernateMappingTransformer.HIBERNATE_ATTRIBUTE_ENTITY_NAME, clazzDOType);
        }
        uniqueKeys = new TreeMap<>();
        indexes = new TreeMap<>();
    }

    public ObjectModelAttribute getReverseOneToOneAttribute() {
        return reverseOneToOneAttribute;
    }

    public TopiaMetadataModel getMetadataModel() {
        return metadataModel;
    }

    public TopiaMetadataEntity getMetadataEntity() {
        return metadataEntity;
    }

    public TopiaMetadataOneToOneComposition getOneToOneComposition() {
        return oneToOneComposition;
    }

    public boolean isMutable() {
        return mutable;
    }

    public ObjectModel getModel() {
        return model;
    }

    public List<ObjectModelAttribute> getNaturalAttributes() {
        return naturalAttributes;
    }

    public List<ObjectModelAttribute> getNoneNaturalAttributes() {
        return noneNaturalAttributes;
    }

    public boolean isUnique(ObjectModelAttribute attribute) {
        boolean tagValue = EugeneCoreTagValues.isUnique(attribute);
        if (tagValue) {
            return true;
        }
        if (EugeneCoreTagValues.isUnique(attribute)) {
            return true;
        }
        Boolean value = getBooleanTagValue(attribute, EugeneCoreTagValues.Store.unique);
        return value != null && value;
    }

    public Boolean getNotNullTagValue(ObjectModelAttribute attribute) {
        Boolean tagValue = topiaHibernateTagValues.getNotNullTagValue(attribute);
        if (tagValue != null) {
            return tagValue;
        }
        return getBooleanTagValue(attribute, TopiaHibernateTagValues.Store.notNull);
    }

    public Boolean getBooleanTagValue(ObjectModelAttribute attribute, TagValueMetadata tagValue) {
        String value = getTagValue(attribute, tagValue);
        return value == null ? null : "true".equalsIgnoreCase(value);
    }

    public String getTagValue(ObjectModelAttribute attribute, TagValueMetadata tagValue) {
        return model.getTagValuesStore().onClassifier(getInput().getQualifiedName()).onAttribute(attribute.getName()).onTagValue(tagValue.getName()).single();
    }

    public Map<String, String> getOptionalAttributesMap() {
        return optionalAttributesMap;
    }

    public boolean isGenerateForeignKeyNames() {
        return generateForeignKeyNames;
    }

    public String getTableName() {
        return tableName;
    }

    public boolean isUseSchema() {
        return schema != null;
    }

    public String getSchema() {
        return schema;
    }

    public Pair<String, String> registerIndexKeyName(String tableName, String attrColumn) {
        return registerIndexKeyName(schema, tableName, attrColumn);
    }

    public Pair<String, String> registerIndexKeyName(String attrColumn) {
        return registerIndexKeyName(schema, tableName, attrColumn);
    }

    public Pair<String, String> registerIndexKeyName(String schema, String tableName, String attrColumn) {
        Pair<String, String> pair = templateHelper.registerIndexKeyName(schema, tableName, attrColumn);
        if (pair != null) {
            indexes.put(pair.getKey(), pair.getValue());
        }
        return pair;
    }

    public Pair<String, String> registerUniqueKeyName(String attrColumn) {
        Pair<String, String> pair = templateHelper.registerUniqueKeyName(schema, tableName, attrColumn);
        if (pair != null) {
            uniqueKeys.put(pair.getKey(), pair.getValue());
        }
        return pair;
    }

    public Map<String, String> getUniqueKeys() {
        return uniqueKeys;
    }

    public Map<String, String> getIndexes() {
        return indexes;
    }

    public String registerForeignKeyName(ObjectModelAttribute attr) {
        return registerForeignKeyName(tableName, attr);
    }

    public String registerForeignKeyName(String tableName, ObjectModelAttribute attr) {
        String attrColumn = templateHelper.getDbName(attr);
        String foreignKeyName = ("fk_" + schema + "_" + tableName + "_" + attrColumn).toLowerCase();
        templateHelper.registerForeignKey(foreignKeyName, schema, tableName, attr);
        return foreignKeyName;
    }

    public String registerSimpleForeignKeyName(String tableName, ObjectModelAttribute attr) {
        String attrColumn = attr.getReverseAttributeName();
        String foreignKeyName = ("fk_" + schema + "_" + tableName + "_" + attrColumn).toLowerCase();
        templateHelper.registerForeignKey(foreignKeyName, schema, tableName, attr);
        return foreignKeyName;
    }

    public ObjectModelClass getInput() {
        return input;
    }

    public ObjectModelPackage getPackage() {
        return aPackage;
    }

    protected void detectAttributes(ObjectModelClass input) {
        for (ObjectModelAttribute attr : input.getAttributes()) {
            if (topiaHibernateTagValues.getNaturalIdTagValue(attr)) {
                // attribut metier
                naturalAttributes.add(attr);
            } else {
                // attribut normal
                noneNaturalAttributes.add(attr);
            }
        }
    }


    private void generateFromTagValue(Map<String, String> map, String attributeName, String tagValue) {
        generateFromTagValue(map, attributeName, tagValue, null);
    }

    private void generateFromTagValue(Map<String, String> map, String attributeName, Boolean tagValue) {
        generateFromTagValue(map, attributeName, tagValue == null ? null : String.valueOf(tagValue), null);
    }

    private void generateFromTagValue(Map<String, String> map, String attributeName, String tagValue, String defaultValue) {
        String value = null;
        if (StringUtils.isNotEmpty(tagValue)) {
            value = tagValue;

        } else if (defaultValue != null) {
            value = defaultValue;
        }
        if (value != null) {
            map.put(attributeName, "\"" + value + "\"");
        }
    }

    private String attributesToString(Map<String, String> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            sb.append(" ").append(entry.getKey()).append("=").append(entry.getValue());
        }
        return sb.toString();
    }
}
