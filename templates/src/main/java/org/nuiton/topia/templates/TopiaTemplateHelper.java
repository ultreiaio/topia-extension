package org.nuiton.topia.templates;

/*
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.eugene.AbstractGenerator;
import org.nuiton.eugene.EugeneCoreTagValues;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.java.JavaGeneratorUtil;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.extension.tagvalue.TagValueUtil;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAssociationClass;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelElement;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.ObjectModelPackage;
import org.nuiton.eugene.models.object.ObjectModelParameter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Classe regroupant divers méthodes utiles pour la génération des entités.
 *
 * Remplace {@code TopiageneratorUtil} car on ne veut plus utiliser de méthodes
 * statiques (par exemple pour avoir les valeurs par défaut des tagValues,...).
 *
 * Created on 7/6/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class TopiaTemplateHelper {


    private static final Logger log = LogManager.getLogger(TopiaTemplateHelper.class);
    /** Type de persistence Hibernate */
    public final String PERSISTENCE_TYPE_HIBERNATE = "hibernate";

    /** Type de persistence par défaut (si aucun précisé) */
    public final String PERSISTENCE_TYPE_DEFAULT = PERSISTENCE_TYPE_HIBERNATE;

    /** Propriété des générateurs indiquant le package par défaut */
    public final String PROPERTY_DEFAULT_PACKAGE = "defaultPackage";

    /** Le package par défaut si aucun n'est spécifié */
    public final String DEFAULT_PACKAGE = "org.codelutin.malo";

    /** Stratégie d'heritage par defaut. */
    public final String DEFAULT_INHERITANCE_STRATEGY = "union-subclass";

    protected final ObjectModel model;

    protected final EugeneCoreTagValues eugeneTagValues;
    protected final TopiaCoreTagValues topiaCoreTagValues;
    protected final TopiaHibernateTagValues topiaHibernateTagValues;

    public TopiaTemplateHelper(ObjectModel model) {
        this.model = model;
        this.eugeneTagValues = new EugeneCoreTagValues();
        this.topiaCoreTagValues = new TopiaCoreTagValues();
        this.topiaHibernateTagValues = new TopiaHibernateTagValues();
    }

    @Deprecated
    public TopiaCoreTagValues getTopiaCoreTagValues() {
        return topiaCoreTagValues;
    }

    @Deprecated
    public TopiaHibernateTagValues getTopiaHibernateTagValues() {
        return topiaHibernateTagValues;
    }

    /**
     * Renvoie le type de persistence pour le classifier donné. Si aucun n'est
     * trouvé, le type par défaut est utilisé
     *
     * @param classifier l'élément à tester
     * @return le type de persitence pour l'élément donné.
     * @since 2.5
     */
    public String getPersistenceType(ObjectModelClassifier classifier) {
        String tag = topiaHibernateTagValues.getPersistenceTypeTagValue(classifier);
        //TODO Set default value in tag metadata
        if (tag == null) {
            tag = PERSISTENCE_TYPE_DEFAULT;
        }
        return tag;
    }


    /**
     * Renvoie le package par défaut pour le générateur donné
     *
     * @param generator le générateur donné
     * @return le package par défaut du générator donné
     */
    public String getDefaultPackage(AbstractGenerator<?> generator) {
        String packageName = generator.getProperty(PROPERTY_DEFAULT_PACKAGE);
        if (StringUtils.isBlank(packageName)) {
            packageName = DEFAULT_PACKAGE;
        }
        return packageName;
    }

    public String getApplicationContextPackage(ObjectModelTransformerToJava transformer,
                                               ObjectModel model) {
        return transformer.getDefaultPackageName();
    }

    public String getPersistenceContextPackage(ObjectModelTransformerToJava transformer,
                                               ObjectModel model) {
        return transformer.getDefaultPackageName();
    }

    public String getDaoPackage(ObjectModelTransformerToJava transformer, ObjectModel model) {
        return transformer.getDefaultPackageName();
    }

    public String getParentDaoName(ObjectModel model) {
        return "Abstract" + model.getName() + "TopiaDao";
    }

    public String getParentDaoFqn(ObjectModelTransformerToJava transformer, ObjectModel model) {
        return getDaoPackage(transformer, model) + "." + getParentDaoName(model);
    }

    public String getApplicationContextInterfaceName(ObjectModel model) {
        return model.getName() + "ApplicationContext";
    }

    public String getApplicationContextAbstractName(ObjectModel model) {
        return "Abstract" + model.getName() + "TopiaApplicationContext";
    }

    public String getApplicationContextConcreteName(ObjectModel model) {
        return model.getName() + "TopiaApplicationContext";
    }

    public String getPersistenceContextAbstractName(ObjectModel model) {
        return "Abstract" + model.getName() + "TopiaPersistenceContext";
    }

    public String getPersistenceContextConcreteName(ObjectModel model) {
        return model.getName() + "TopiaPersistenceContext";
    }

    public String getPersistenceContextInterfaceName(ObjectModel model) {
        return model.getName() + "PersistenceContext";
    }

    public String getDaoSupplierInterfaceName(ObjectModel model) {
        return model.getName() + "DaoSupplier";
    }

    public String getDaoSupplierName(ObjectModel model) {
        return model.getName() + "TopiaDaoSupplier";
    }

    public String getEntityEnumName(ObjectModel model) {
        return model.getName() + "EntityEnum";
    }

    public String getEntityAbstractName(ObjectModelClass input) {
        return input.getName() + "Abstract";
    }

    public String getEntityConcreteName(ObjectModelClass input) {
        return input.getName();
    }

    public String getAbstractDaoName(ObjectModelClass input) {
        return "Abstract" + input.getName() + "TopiaDao";
    }

    public String getGeneratedDaoName(ObjectModelClass input) {
        return "Generated" + input.getName() + "TopiaDao";
    }

    public String getConcreteDaoName(ObjectModelClass input) {
        return input.getName() + "TopiaDao";
    }

    public String getContractDaoName(ObjectModelClass input) {
        return input.getName() + "Dao";
    }

    public String getAbstractDaoFqn(ObjectModelClass input) {
        return input.getPackageName() + "." + getAbstractDaoName(input);
    }

    public String getGeneratedDaoFqn(ObjectModelClass input) {
        return input.getPackageName() + "." + getGeneratedDaoName(input);
    }

    public String getConcreteDaoFqn(ObjectModelClass input) {
        return input.getPackageName() + "." + getConcreteDaoName(input);
    }

    public String getEntityPackage(ObjectModelTransformerToJava transformer,
                                   ObjectModel model,
                                   ObjectModelClassifier input) {
        return input.getPackageName();
    }


    /**
     * Obtain the reverse db name of an attribute.
     *
     * Try first to get the reverse db Name from the reverseDbname tag-value, then
     * if attribute has a specific reverse attribute, use his db name, otherwise
     * suffix the db name of the attribute by {@code _id}.
     *
     * @param attr the attribute to seek
     * @return the value of the reverse name
     * @since 2.5
     */
    public String getReverseDbName(ObjectModelAttribute attr) {
        String result = TagValueUtil.findDirectTagValue(TopiaHibernateTagValues.Store.reverseDbName, attr);
        if (StringUtils.isEmpty(result)) {
            if (attr.getReverseAttribute() != null) {
                result = getDbName(attr.getReverseAttribute());
            } else {
                result = getDbName(attr) + "_id";
            }
        }
        return result;
    }

    /**
     * Renvoie le nom BD de l'élement passé en paramètre. Elle se base sur le
     * tag associé si il existe, sinon sur le nom de l'élément
     *
     * @param element l'élément à tester
     * @return le nom de table
     */
    public String getDbName(ObjectModelElement element) {
        if (element == null) {
            return null;
        }
        String value = topiaHibernateTagValues.getDbNameTagValue(element);
        if (value != null) {
            return value;
        }
        return GeneratorUtil.toLowerCaseFirstLetter(element.getName());
    }

    /**
     * Obtain the reverse db name of a reverse attribute.
     *
     * <strong>Note that the reverse attribute can't be null here.</strong>
     * <ul>
     * <li>Try first to get the reverse db Name from the ReverseDbname tag-value</li>
     * <li>If not found, try then the ReverseDbname tag-value on the same attribute but from this other side of the relation</li>
     * <li>If not found, try then just get the name of the reverse attribute</li>
     * </ul>
     *
     * @param attr the attribute to seek
     * @return the value of the reverse db name on the reverse attribute
     * @since 2.9.5.2
     */
    public String getReverseDbNameOnReverseAttribute(ObjectModelAttribute attr) {

        ObjectModelAttribute reverseAttribute = attr.getReverseAttribute();

        if (reverseAttribute == null) {
            throw new IllegalArgumentException("The reverse attribute can't be null, but was on " + attr);
        }

        String result = getReverseDbName(reverseAttribute);
        if (StringUtils.isEmpty(result)) {

            // Try to get it from the other site of the relation
            ObjectModelAttribute reverseAttribute2 = reverseAttribute.getClassifier().getAttribute(attr.getName());
            result = getReverseDbName(reverseAttribute2);

        }

        if (StringUtils.isEmpty(result)) {

            result = GeneratorUtil.toLowerCaseFirstLetter(reverseAttribute.getName());

        }

        return result;

    }

    /**
     * Cherche et renvoie la liste des attributs constituant la clef metier
     * d'une classe.
     *
     * @param clazz la classe à tester
     * @return la liste des attributs de la clef métier
     */
    public Set<ObjectModelAttribute> getNaturalIdAttributes(
            ObjectModelClass clazz) {

        // use {@link LinkedHashSet} to keep order and prevent duplicate natural ids found
        Set<ObjectModelAttribute> results =
                new LinkedHashSet<>();
        for (ObjectModelAttribute attr : clazz.getAttributes()) {
            if (topiaHibernateTagValues.getNaturalIdTagValue(attr)) {
                results.add(attr);
            }
        }

        // sletellier : #2050 Natural id and not null attributes are not propagated on generalized entities (http://nuiton.org/issues/2050)
        Collection<ObjectModelClass> superclasses = clazz.getSuperclasses();
        for (ObjectModelClass superClass : superclasses) {
            Set<ObjectModelAttribute> naturalIdsOfSuperClass = getNaturalIdAttributes(superClass);
            results.addAll(naturalIdsOfSuperClass);
        }
        return results;
    }

    /**
     * Cherche et renvoie la liste des attributs qui ne doivent pas etre null dans
     * une classe.
     *
     * @param clazz la classe à tester
     * @return la liste des attributs qui ne doivent pas etre null
     */
    public Set<ObjectModelAttribute> getNotNullAttributes(
            ObjectModelClass clazz) {

        // use {@link LinkedHashSet} to keep order and prevent duplicate not null found
        Set<ObjectModelAttribute> results =
                new LinkedHashSet<>();
        for (ObjectModelAttribute attr : clazz.getAttributes()) {
            if (isAttributeNotNull(attr)) {
                results.add(attr);
            }
        }

        Collection<ObjectModelClass> superclasses = clazz.getSuperclasses();
        for (ObjectModelClass superClass : superclasses) {
            Set<ObjectModelAttribute> notNullOfSuperClass = getNotNullAttributes(superClass);
            results.addAll(notNullOfSuperClass);
        }

        // Association class participants are obviously not null
        if (clazz instanceof ObjectModelAssociationClass) {
            List<ObjectModelAttribute> participantsAttributes = ((ObjectModelAssociationClass) clazz).getParticipantsAttributes();
            results.addAll(participantsAttributes);
        }

        return results;
    }

    /**
     * Detecte si un attribut est marqué comme non null.
     * Les naturalId sont not null par défaut
     *
     * @param attribute l'attribut à tester
     * @return {@code true} si l'attribut doit être non null,
     * par défaut pour les naturalId, {@code false} sinon..
     * @since 2.6.9
     */
    public boolean isAttributeNotNull(ObjectModelAttribute attribute) {
        Boolean value = topiaHibernateTagValues.getNotNullTagValue(attribute);
        if (value == null) {
            // valeur null, donc pas positionnee
            return topiaHibernateTagValues.getNaturalIdTagValue(attribute);
        }
        return value;
    }

    public String getDOType(ObjectModelElement elem, ObjectModel model) {
        String type = elem.getName();
        if (elem instanceof ObjectModelAttribute) {
            type = ((ObjectModelAttribute) elem).getType();
        }
        if (elem instanceof ObjectModelClass) {
            type = ((ObjectModelClass) elem).getQualifiedName();
        }
        return getDOType(type, model);
    }

    public String getDOType(String type, ObjectModel model) {
        if (!model.hasClass(type)) {
            return type;
        }
        ObjectModelClass clazz = model.getClass(type);
        if (isEntity(clazz)) {
            //tchemit-2011-09-12 What ever abstract or not, we always use an Impl
            type += "Impl";
        }
        return type;
    }

    private static final Set<String> numberTypes = new HashSet<>();

    private static final Set<String> textTypes = new HashSet<>();

    private static final Set<String> booleanTypes = new HashSet<>();

    private static final Set<String> primitiveTypes = new HashSet<>();

    private static final Map<String, String> primitiveTypeToClass = new HashMap<>();

    private static final String VOID_TYPE = "void";

    static {

        primitiveTypeToClass.put("byte", "java.lang.Byte");
        primitiveTypeToClass.put("short", "java.lang.Short");
        primitiveTypeToClass.put("int", "java.lang.Integer");
        primitiveTypeToClass.put("long", "java.lang.Long");
        primitiveTypeToClass.put("float", "java.lang.Float");
        primitiveTypeToClass.put("double", "java.lang.Double");
        primitiveTypeToClass.put("char", "java.lang.Char");
        primitiveTypeToClass.put("boolean", "java.lang.Boolean");

        numberTypes.add("byte");
        numberTypes.add("java.lang.Byte");
        numberTypes.add("Byte");
        numberTypes.add("short");
        numberTypes.add("java.lang.Short");
        numberTypes.add("Short");
        numberTypes.add("int");
        numberTypes.add("java.lang.Integer");
        numberTypes.add("Integer");
        numberTypes.add("long");
        numberTypes.add("java.lang.Long");
        numberTypes.add("Long");
        numberTypes.add("float");
        numberTypes.add("java.lang.Float");
        numberTypes.add("Float");
        numberTypes.add("double");
        numberTypes.add("java.lang.Double");
        numberTypes.add("Double");

        textTypes.add("char");
        textTypes.add("java.lang.Char");
        textTypes.add("Char");
        textTypes.add("java.lang.String");
        textTypes.add("String");

        booleanTypes.add("boolean");
        booleanTypes.add("java.lang.Boolean");
        booleanTypes.add("Boolean");

        primitiveTypes.addAll(numberTypes);
        primitiveTypes.addAll(textTypes);
        primitiveTypes.addAll(booleanTypes);
    }

    public boolean isNumericType(ObjectModelAttribute attr) {
        return numberTypes.contains(attr.getType());
    }

    public boolean isTextType(ObjectModelAttribute attr) {
        return textTypes.contains(attr.getType());
    }

    public boolean isDateType(ObjectModelAttribute attr) {
        return "java.util.Date".equals(attr.getType());
    }

    public boolean isBooleanType(ObjectModelAttribute attr) {
        return booleanTypes.contains(attr.getType());
    }

    public boolean isPrimitiveType(ObjectModelAttribute attr) {
        return primitiveTypes.contains(attr.getType());
    }

    public String getClassForPrimitiveType(ObjectModelAttribute attr) {
        Preconditions.checkState(isPrimitiveType(attr));
        String className = primitiveTypeToClass.get(attr.getType());
        Preconditions.checkNotNull(className);
        return className;
    }

    /**
     * <p>
     * Cette méthode permet de détecter si
     * - l'attribut représente une relation 1-n
     * - cette relation est unidirectionnelle
     * - le type de l'attribut représente un entité
     * - cette entité a des sous-classes dans le modèle
     *
     * Ce cas correspond à une incompatibilité d'Hibernate qui nous oblige a
     * adopter un comportement particulier.
     * </p>
     *
     * @param attr  l'attribut a tester
     * @param model le model
     * @return true si et seulement si il s'agit bien de ce type de relation
     */
    public boolean hasUnidirectionalRelationOnAbstractType(
            ObjectModelAttribute attr,
            ObjectModel model) {
        ObjectModelAttribute reverse = attr.getReverseAttribute();
        //relation 1-n
        if (reverse != null && GeneratorUtil.isNMultiplicity(attr) &&
            !GeneratorUtil.isNMultiplicity(reverse)) {
            //Pas de navigabilité
            if (!reverse.isNavigable()) {
                //Il s'agit d'une entity
                ObjectModelClass clazz = model.getClass(attr.getType());
                if (clazz != null && isEntity(clazz)) {
                    //Cette classe a des sous-classes dans le modèle
                    for (ObjectModelClass subClass : model.getClasses()) {
                        if (subClass.getSuperclasses().contains(clazz)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * Renvoie le nom unique de table pour une relation ManyToMany en fonction
     * de l'attribut {@code attr}
     *
     * @param attr l'attribut servant de base au calcul du nom
     * @return le nom de la table
     */
    public String getManyToManyTableName(ObjectModelAttribute attr) {
        String result;

        if (attr.hasAssociationClass()) {
            result = getDbName(attr.getAssociationClass());
        } else {
            result = topiaHibernateTagValues.getManytoManyTableNameTagValue(attr);
            if (StringUtils.isEmpty(result)) {
                String name = attr.getName();
                String revers = attr.getReverseAttributeName();

                // FIXME echatellier 20170414 in case of attribute with * multiplicity
                // name is wrong. Should always be "parenttablename_attributename" and name sort compare
                if (name.compareToIgnoreCase(revers) < 0) {
                    result = name + '_' + revers;
                } else {
                    result = revers + '_' + name;
                }
            }
        }
        return result.toLowerCase();
    }

    /**
     * Renvoie le type d'interface à utiliser en fonction de l'attribut
     *
     * @param attr l'attribut a traiter
     * @return String
     */
    public String getNMultiplicityHibernateType(
            ObjectModelAttribute attr) {
        if (JavaGeneratorUtil.isOrdered(attr)) {
            return "list";
        } else if (EugeneCoreTagValues.isUnique(attr)) {
            return "set";
        }
        //attr.isOrdered() - On génère le ordered en bag  
        return "bag";
    }

    /**
     * Obtain the list of entities classes with the possibility to sort the
     * result.
     *
     * @param model the current model to scan
     * @param sort  flag to allow sort the result
     * @return the list of filtred classes by their stereotype
     */
    public List<ObjectModelClass> getEntityClasses(ObjectModel model,
                                                   boolean sort) {
        List<ObjectModelClass> classes = new ArrayList<>();
        for (ObjectModelClass clazz : model.getClasses()) {
            if (isEntity(clazz)) {
                classes.add(clazz);
            }
        }
        if (sort && !classes.isEmpty()) {

            Collections.sort(classes, OBJECT_MODEL_CLASS_COMPARATOR);
        }
        return classes;
    }

    public final Comparator<ObjectModelClass>
            OBJECT_MODEL_CLASS_COMPARATOR =
            new Comparator<ObjectModelClass>() {

                @Override
                public int compare(ObjectModelClass o1,
                                   ObjectModelClass o2) {
                    return o1.getQualifiedName().compareTo(
                            o2.getQualifiedName());
                }
            };

    /**
     * Obtain the list of fqn of object involed in the given class.
     *
     * @param aClass       the clazz to inspect
     * @param incomingFqns incoming fqns
     * @return the list of fqn of attributes
     */
    public List<String> getImports(ObjectModelClass aClass,
                                   String... incomingFqns) {
        Set<String> tmp = new HashSet<>();
        tmp.addAll(Arrays.asList(incomingFqns));
        getImports(aClass, tmp);
        return cleanImports(aClass.getPackageName(), tmp);
    }

    /**
     * Obtain the list of fqn of object involed in the given class.
     *
     * @param aClass the class to inspect
     * @param fqns   where to store found fqns
     */
    public void getImports(ObjectModelClass aClass,
                           Set<String> fqns) {
        // scan attributes
        for (ObjectModelAttribute attr : aClass.getAttributes()) {
            fqns.add(attr.getType());
            if (GeneratorUtil.isNMultiplicity(attr)) {
                String collectionType = getCollectionType(attr).getName();
                fqns.add(collectionType);
                String collectionObject = getCollectionInstanceType(attr).getName();
                fqns.add(collectionObject);
            }
        }
        for (ObjectModelAttribute attribute : aClass.getAllOtherAttributes()) {
            fqns.add(attribute.getType());
        }
        // scan associations
        if (aClass instanceof ObjectModelAssociationClass) {
            ObjectModelAssociationClass assoc =
                    (ObjectModelAssociationClass) aClass;
            for (ObjectModelAttribute attr :
                    assoc.getParticipantsAttributes()) {
                if (attr == null) {
                    continue;
                }
                fqns.add(attr.getType());
                if (GeneratorUtil.isNMultiplicity(attr)) {
                    String collectionType = getCollectionType(attr).getName();
                    fqns.add(collectionType);
                    String collectionObject = getCollectionInstanceType(attr).getName();
                    fqns.add(collectionObject);
                }
            }
        }
        // scan operations
        for (ObjectModelOperation operation : aClass.getOperations()) {
            getImports(operation, fqns);
        }
        // scan super interfaces
        for (ObjectModelInterface modelInterface : aClass.getInterfaces()) {
            fqns.add(modelInterface.getQualifiedName());
            getImports(modelInterface, fqns);
        }
        // scan super classes
        for (ObjectModelClass modelClass : aClass.getSuperclasses()) {
            fqns.add(modelClass.getQualifiedName());
            getImports(modelClass);
        }
    }

    /**
     * Obtain the list of fqn of object involed in the given interface.
     *
     * @param anInterface the interface to inspect
     * @param fqns        where to store found fqns
     */
    public void getImports(ObjectModelInterface anInterface,
                           Set<String> fqns) {
        // scan operations
        for (ObjectModelOperation operation : anInterface.getOperations()) {
            getImports(operation, fqns);
        }
        // scan super interfaces
        for (ObjectModelInterface modelInterface : anInterface.getInterfaces()) {
            fqns.add(modelInterface.getQualifiedName());
            getImports(modelInterface, fqns);
        }
    }

    /**
     * Obtain the fqn's list of all involed type in a givne operation.
     *
     * @param operation operation to inspect
     * @param fqns      where to store found fqns
     */
    public void getImports(ObjectModelOperation operation,
                           Set<String> fqns) {
        String fqn = operation.getReturnType();
        fqns.add(fqn);
        for (ObjectModelParameter parameter : operation.getParameters()) {
            fqns.add(parameter.getType());
        }
    }

    /**
     * Clean a set of fqns, transform it into a {@link List} and sort it.
     *
     * @param packageName the current package name
     * @param fqns        the dirty set of fqns
     * @return the sorted cleaned list of fqns.
     */
    public List<String> cleanImports(String packageName,
                                     Set<String> fqns) {
        fqns.removeAll(primitiveTypes);
        fqns.remove(VOID_TYPE);
        int packageLength = packageName.length();
        List<String> genericType = new ArrayList<>();
        for (Iterator<String> it = fqns.iterator(); it.hasNext(); ) {
            String fqn = it.next();
            int lastIndex = fqn.lastIndexOf(".");
            if (lastIndex == packageLength && fqn.startsWith(packageName)) {
                // same package
                it.remove();
                continue;
            }
            int genericIndex = fqn.indexOf('<');
            if (genericIndex != -1) {
                genericType.add(fqn.substring(0, genericIndex));
                it.remove();
            }
        }
        fqns.addAll(genericType);

        ArrayList<String> result = new ArrayList<>(fqns);
        Collections.sort(result);
        return result;
    }

    public Map<ObjectModelClass, Set<ObjectModelClass>>
    searchDirectUsages(ObjectModel model) {
        List<ObjectModelClass> allEntities;
        Map<String, ObjectModelClass> allEntitiesByFQN;
        Map<ObjectModelClass, Set<ObjectModelClass>> usages;

        allEntities = getEntityClasses(model, true);

        allEntitiesByFQN = new TreeMap<>();
        usages = new LinkedHashMap<>();

        // prepare usages map and fill allEntitiesByFQN map
        for (ObjectModelClass klass : allEntities) {
            usages.put(klass, new HashSet<ObjectModelClass>());
            allEntitiesByFQN.put(klass.getQualifiedName(), klass);
        }

        // first pass to detect direct usages
        for (ObjectModelClass klass : allEntities) {
            searchDirectUsages(klass, allEntitiesByFQN, usages);
        }
        allEntities.clear();
        allEntitiesByFQN.clear();
        return usages;

    }

    public void searchDirectUsages(
            ObjectModelClass klass,
            Map<String, ObjectModelClass> allEntitiesByFQN,
            Map<ObjectModelClass, Set<ObjectModelClass>> usages) {

        if (log.isDebugEnabled()) {
            log.debug("for entity " + klass.getQualifiedName());
        }
        for (ObjectModelAttribute attr : klass.getAttributes()) {
            if (!attr.isNavigable()) {
                // skip this case
                continue;
            }
            String type;
            if (attr.hasAssociationClass()) {
                type = attr.getAssociationClass().getQualifiedName();
            } else {
                type = attr.getType();
            }
            if (!allEntitiesByFQN.containsKey(type)) {
                // not a entity, can skip for this attribute
                continue;
            }
            if (log.isDebugEnabled()) {
                log.debug(" uses " + type);
            }
            // register the klass as using the targetEntity
            ObjectModelClass targetEntity = allEntitiesByFQN.get(type);
            Set<ObjectModelClass> classes = usages.get(targetEntity);
            classes.add(klass);
        }
    }

    public boolean isImportNeeded(Collection<ObjectModelOperation> operations,
                                  String importName) {
        if (CollectionUtils.isNotEmpty(operations)) {
            for (ObjectModelOperation op : operations) {
                if (op.getReturnType().contains(importName)) {
                    return true;
                }
                for (ObjectModelParameter param : op.getParameters()) {
                    if (param.getType().contains(importName)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean isCollectionNeeded(
            Collection<ObjectModelOperation> operations) {
        return isImportNeeded(operations,
                              Collection.class.getSimpleName());
    }

    public boolean isSetNeeded(Collection<ObjectModelOperation> operations) {
        return isImportNeeded(operations,
                              Set.class.getSimpleName());
    }

    /**
     * Check if the given attribute type is an entity.
     *
     * @param attribute attribute to test
     * @param model     model containing the attribute
     * @return {@code true} if type of attribute is an entity,
     * {@code false} otherwise
     * @see TopiaCoreTagValues.Store#entity
     * @since 2.7
     */
    public boolean isEntity(ObjectModelAttribute attribute,
                            ObjectModel model) {
        if (isPrimitiveType(attribute)) {
            return false;
        }
        String attributeType = attribute.getType();
        ObjectModelClassifier typeclassifier =
                model.getClassifier(attributeType);
        return typeclassifier != null && isEntity(typeclassifier);
    }

    /**
     * Check if the given classifier has the
     * {@link TopiaCoreTagValues.Store#entity} and is not an enumeration
     *
     * @param classifier classifier to test
     * @return {@code true} if stereotype was found and classifier is not
     * enumeration, {@code false} otherwise
     * @see TopiaCoreTagValues.Store#entity
     * @since 2.5
     */
    public boolean isEntity(ObjectModelClassifier classifier) {
        ObjectModelPackage aPackage = model.getPackage(classifier);
        return !classifier.isEnum() && topiaCoreTagValues.isEntity(classifier, aPackage);
    }

    /**
     * Check if the given classifier is an abstract class
     *
     * @param classifier classifier to test
     * @return {@code true} if the classifier is abstract, {@code false} otherwise
     */
    public boolean isAbstract(ObjectModelClassifier classifier) {
        ObjectModelClass aClass = model.getClass(classifier.getQualifiedName());
        return aClass.isAbstract();
    }

    /**
     * Obtain the value of the {@link TopiaHibernateTagValues.Store#inheritanceStrategy} tag value on the given classifier.
     *
     * @param classifier classifier to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see TopiaHibernateTagValues.Store#inheritanceStrategy
     * @since 3.0
     */
    public String getInheritanceStrategy(ObjectModelClassifier classifier) {
        ObjectModelPackage aPackage = model.getPackage(classifier);
        String value = topiaHibernateTagValues.getInheritanceStrategyTagValue(classifier,
                                                                         aPackage);
        if (value == null) {
            value = DEFAULT_INHERITANCE_STRATEGY;
        }
        return value;
    }

    public Class<?> getCollectionType(ObjectModelAttribute attribute) {

        return JavaGeneratorUtil.getCollectionType(attribute);

    }

    public Class<?> getCollectionInstanceType(ObjectModelAttribute attribute) {

        boolean unique = EugeneCoreTagValues.isUnique(attribute);
        boolean ordered = EugeneCoreTagValues.isOrdered(attribute);
        boolean orderBy = topiaHibernateTagValues.getOrderByTagValue(attribute) != null;

        Class<?> result;

        if (unique && orderBy && !ordered) {
            // Special case, we want to keep the order coming from database
            // This does not mean we keep order while saving a such relation
            result = LinkedHashSet.class;
        } else {
            result = JavaGeneratorUtil.getCollectionInstanceType(attribute);
        }
        return result;

    }

    //---------------------------------------------------------------------------------------------------------------
    //--  DEPRECATED API TO REMOVE  ---------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------------------


    /**
     * dependency to add extra operations for entity dao.
     *
     * @param input FIXME
     * @return FIXME
     * @since 2.3.4
     * @deprecated only used for warn to help migration to 3.0, should be deleted
     */
    // TODO brendan 03/10/14 delete in 3.1
    @Deprecated
    public static String getLegacyDaoName(ObjectModelClass input) {
        return input.getName() + "DAO";
    }

}
