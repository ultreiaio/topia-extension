package org.nuiton.topia.templates.sql.plan;

/*-
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.service.sql.model.AbstractTopiaEntitySqlTable;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlAssociationTable;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlDescriptor;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlDescriptors;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSelector;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSimpleAssociationTable;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlTable;
import org.nuiton.topia.service.sql.plan.copy.TopiaEntitySqlCopyPlan;
import org.nuiton.topia.service.sql.plan.copy.TopiaEntitySqlCopyPlanTask;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created on 04/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.68
 */
public abstract class TopiaEntitySqlCopyPlanBuilder {

    protected final Set<TopiaEntitySqlCopyPlanTask> tasks;
    protected final Set<TopiaEntitySqlCopyPlanTask> associationTasks;
    protected final TopiaEntitySqlDescriptors entityDescriptor;

    public TopiaEntitySqlCopyPlanBuilder(TopiaEntitySqlDescriptors entityDescriptor) {
        this.entityDescriptor = entityDescriptor;
        this.tasks = new LinkedHashSet<>();
        this.associationTasks = new LinkedHashSet<>();
    }

    protected abstract String newFrom(TopiaEntitySqlSelector selector);

    public TopiaEntitySqlCopyPlan build() {
        entityDescriptor.forEach(this::startConsumeTable);
        Set<TopiaEntitySqlCopyPlanTask> result = new LinkedHashSet<>(tasks);
        result.addAll(associationTasks);
        return new TopiaEntitySqlCopyPlan(result);
    }

    protected void startConsumeTable(TopiaEntitySqlDescriptor descriptor) {
        TopiaEntitySqlTable table = descriptor.getTable();
        List<String> columNames = List.copyOf(table.getAuthorizedColumnNames());
        String schemaName = table.getSchemaName();
        String tableName = table.getTableName();
        Set<String> blobColumnNames = table.getBlobProperties();
        boolean useBlob = !blobColumnNames.isEmpty();
        String recursiveColumnName = table.getOptionalRecursiveProperty().orElse(null);
        List<TopiaEntitySqlAssociationTable> associations = descriptor.getAssociations();
        List<TopiaEntitySqlSimpleAssociationTable> simpleAssociations = descriptor.getSimpleAssociations();
        for (TopiaEntitySqlSelector selector : table.getSelectors()) {
            String from = newFrom(selector);
            tasks.add(new TopiaEntitySqlCopyPlanTask(schemaName,
                                                     tableName,
                                                     from,
                                                     columNames,
                                                     useBlob,
                                                     recursiveColumnName));
        }
        if (associations != null && !associations.isEmpty()) {
            associations.forEach(this::startConsumeAssociationTable);
        }
        if (simpleAssociations != null && !simpleAssociations.isEmpty()) {
            simpleAssociations.forEach(this::startConsumeAssociationTable);
        }
    }

    protected void startConsumeAssociationTable(AbstractTopiaEntitySqlTable table) {
        List<String> columNames = List.copyOf(table.getAuthorizedColumnNames());
        String tableName = table.getTableName();
        String schemaName = table.getSchemaName();
        for (TopiaEntitySqlSelector selector : table.getSelectors()) {
            String selectedSql = newFrom(selector);
            associationTasks.add(new TopiaEntitySqlCopyPlanTask(schemaName,
                                                                tableName,
                                                                selectedSql,
                                                                columNames,
                                                                false,
                                                                null));
        }
    }
}

