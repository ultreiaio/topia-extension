package org.nuiton.topia.templates.sql.order;

/*-
 * #%L
 * ToPIA Extension :: Templates
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.service.sql.metadata.TopiaMetadataEntity;
import org.nuiton.topia.service.sql.metadata.TopiaMetadataModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * To compute replication order for a set of entities without any entry point.
 * <p>
 * Created on 24/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.27
 */
public class ReplicationOrderBuilderWithStandalone extends ReplicationOrderBuilder {

    private static final Logger log = LogManager.getLogger(ReplicationOrderBuilderWithStandalone.class);

    public static List<TopiaMetadataEntity> build(TopiaMetadataModel metadataModel) {
        return new ReplicationOrderBuilderWithStandalone(Objects.requireNonNull(metadataModel)).build();
    }

    public static List<TopiaMetadataEntity> build(TopiaMetadataModel metadataModel, Set<TopiaMetadataEntity> entities) {
        return new ReplicationOrderBuilderWithStandalone(Objects.requireNonNull(metadataModel), entities).build();
    }

    protected ReplicationOrderBuilderWithStandalone(TopiaMetadataModel metadataModel, Set<TopiaMetadataEntity> entities) {
        super(metadataModel, entities);
    }

    protected ReplicationOrderBuilderWithStandalone(TopiaMetadataModel metadataModel) {
        super(metadataModel, metadataModel.streamWithStandalone().collect(Collectors.toSet()));
    }

    @Override
    public List<TopiaMetadataEntity> build() {
        Set<TopiaMetadataEntity> entities = getEntities();
        List<TopiaMetadataEntity> result = new ArrayList<>(entities.size());
        List<TopiaMetadataEntity> remainingEntities = new ArrayList<>();
        ArrayListMultimap<TopiaMetadataEntity, TopiaMetadataEntity> dependenciesMapping = computeDependencies(entities);
        ArrayListMultimap<MutableInt, TopiaMetadataEntity> dependenciesCount = ArrayListMultimap.create();
        int maxDepth = 0;
        for (TopiaMetadataEntity entity : TopiaMetadataEntity.sortByFqn(entities)) {
            List<TopiaMetadataEntity> dependencies = dependenciesMapping.get(entity);
            int size = dependencies == null ? 0 : dependencies.size();
            maxDepth = Math.max(maxDepth, size);
            dependenciesCount.put(new MutableInt(size), entity);
        }
        log.debug(String.format("Found %d max depth to process", maxDepth));
        int i = 0;
        int resultSize = entities.size();
        while (result.size() != resultSize) {
            if (i <= maxDepth) {
                List<TopiaMetadataEntity> entitiesForThisRound = dependenciesCount.get(new MutableInt(i));
                if (entitiesForThisRound != null) {
                    remainingEntities.addAll(entitiesForThisRound);
                }
            }
            addRound(i++, remainingEntities, dependenciesMapping, result);
        }
        return Collections.unmodifiableList(result);
    }

    private void addRound(int roundIndex, List<TopiaMetadataEntity> entitiesForThisRound, ArrayListMultimap<TopiaMetadataEntity, TopiaMetadataEntity> dependenciesMapping, List<TopiaMetadataEntity> result) {
        if (roundIndex == 0) {
            // entities with no dependencies
            result.addAll(entitiesForThisRound);
            entitiesForThisRound.clear();
            return;
        }
        Iterator<TopiaMetadataEntity> iterator = entitiesForThisRound.iterator();
        while (iterator.hasNext()) {
            TopiaMetadataEntity entity = iterator.next();
            int indexOf = result.indexOf(entity);
            boolean alreadyIn = indexOf > -1;
            if (alreadyIn) {
                iterator.remove();
                continue;
            }
            List<TopiaMetadataEntity> dependencies = dependenciesMapping.get(entity);
            if (result.containsAll(dependencies)) {
                result.add(entity);
                iterator.remove();
            }
        }
    }

}

